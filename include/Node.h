#ifndef DEF_NODE
#define DEF_NODE
#include "Classes.h"


#include <string>
#include <time.h>

#include <vector>
#include <iostream>
#include <istream>
#include <ostream>
#include <fstream>
#include <sstream>
#include <math.h>
#include "Utils.h"
#include "Coord.h"
#include "Volume.h"
#include "Correl3DGPU.h"
#include "P.h"

class Node : public P {

private :
	int numberOfStruts;
	//std::list<Strut*>* struts;
	std::string param;
	
public :
	Node();
	Node(P p);
	Node(int scan, int color, Coord coord, int numberOfStrut, std::string param);
	Node(std::string line);
	Node(const std::string Directory, const std::string pName);

	~Node();

	void Update(Node P);
	void Update(std::string line);
	std::string write() const;
	//Node findBestCorrel_DICGPU(Correl3DGPU& correlator);
	//Node findBestCorrel_DICGPU(Correl3DGPU& correlator, Coord target);
	//Node optimizeCorrel_DICGPU(Correl3DGPU& correlator, Coord target);

	
    int getNumberOfStruts() const;
	void setNumberOfStruts(int n);
	void addStrut();
	//void addStrut(Strut* st);
	//std::list<Strut*>* getStruts();
	//Strut* getStrut(int i);

	std::string getParam() const;

	std::string tostring() const;
	std::string fullInfoTostring() const;
	//std::string voidfullInfoTostring() const;
	
};


#endif
