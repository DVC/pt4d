///<	file:	conf_reader.hpp
///<	author:	Alexis Faure
///<	date:	2015-02-19
///<	vers:	1.0
///<	Simple tools for reading configuration files
///
///
///
///< Generalities:
///< A config file is composed of value stored in sections:
///
///<  [section1]
///<    value1 = erb
///<    value2 = dtnd
///<    value3 = 47.2
///<  [section2] ...
///<
///<
///< This little framework build a similar object architecture to load this
///< type of file.
///< From the user point of view, only the class 'File' will be called,
///< declaring the name of the concerned file and then retrieving casted values.
///<
///< Typical use is:
///<  File my_file("the_file.txt")
///<  my_file.read_file()
///<  double the_value_i_want_to_extract = my_file.get_double("my_section","value_name");
///<
///<
///< When read_file() is called, the class File fill its private map with the sections of the
///< readed file, and then, fill sections with their values.
///<

#ifndef STRING_TOOLS
#define STRING_TOOLS

#include <string>
#include <vector>
#include "Utils.h"

std::vector<std::string> split(std::string const& chain, char delimiter)
{
	std::vector<std::string> out;

	std::string current_word = "";
	for (std::string::const_iterator  it=chain.begin() ; it!=chain.end(); ++it)
	{
		if (*it != delimiter)
		{
			current_word.insert(current_word.end(),*it);
		} else
		{
			if (!current_word.empty())
			{
				out.push_back(current_word);
				current_word = "";
			}
		}
	}
	if (!current_word.empty())
	{
		out.push_back(current_word);
	}


	return out;


}


std::string trim(std::string const& chain)
{
	std::string out = chain;
	
	std::string::iterator it = out.begin();
	for (; it != out.end() ; ++it)
	{
		if (*it != ' ' and *it != '\t') break;
	}

	if (it != out.begin())
	{
		out.erase(out.begin(),it);
	}
	
	it = out.end()-1;
	for (; it != out.begin() ; --it)
	{
		if (*it != ' ' and *it != '\t') break;
	}

	if (it != out.end())
	{
		out.erase(it+1,out.end());
	}

	return out;

}


bool begin_with(std::string const& chain, char token)
{
	char tmp = *(chain.begin());
	return tmp==token;
}

bool end_with(std::string const& chain, char token)
{
	char tmp = *(chain.end()-1);
	return tmp==token;
}


#endif

#ifndef CONF_READER
#define CONF_READER

#include <string>
#include <map>
#include <fstream>
#include <stdlib.h>
//#include "string_tools.hpp"

namespace conf_reader
{

	// Value class store a value as a std::string, this value could be retrieve
	// from a Section object, and then casted to the required type.
	class Value
	{
		private:
			std::string  m_value;

		public:
			Value(){}
			Value(Value const& rhs){ m_value = rhs.get_value(); }
			void set_value(std::string const& val)
			{
				m_value = val;
			}
			std::string const& get_value() const
			{
				return m_value;
			}



	};
	
	// Section class store a std::map of Value, by this way, it is possible
	// to retrieve a Value depending on its Key in the map.
	class Section
	{
		private:
			std::map<std::string,conf_reader::Value> m_datas;
			std::string m_empty;

		public:
			Section():m_empty(""){}
			void add_value(std::string const& name, std::string const& value)
			{
				Value tmp;
				tmp.set_value(value);
				m_datas[name] = tmp;
			}
			
			void to_cout() const 
			{
				for (std::map<std::string, Value>::const_iterator it=m_datas.begin() ; it != m_datas.end(); ++it)
				{
					std::cout << "\t->" << it->first << '\t' << it->second.get_value() << std::endl;
				}
			}

            void to_log() const 
			{
				for (std::map<std::string, Value>::const_iterator it=m_datas.begin() ; it != m_datas.end(); ++it)
				{
					LINFO << "\t->" << it->first << '\t' << it->second.get_value();
				}
			}

			std::string const& retrieve_value(std::string const& name) const 
			{
				if (m_datas.find(name) != m_datas.end())
				{
					return m_datas.find(name)->second.get_value();
				} else
				{
					//std::cout << "warning: value do not exist in file !" << std::endl;
					std::cout << "warning: " << name << " does not exist in this file !" << std::endl;//version Robin 23/02/15
					return m_empty;
				}
			}

			bool exists(std::string const& name) const
			{
				return m_datas.find(name) != m_datas.end();
			}

	};


	// File class build a std::map of Section based on a given file
	class File
	{
		private:
			std::string  m_filename;
			std::map<std::string, Section> m_sections;

		public:
			File(std::string const& filename):m_filename(filename){}
			void read_file()
			{
				
                LDEBUG << "Begin read";
                std::ifstream f(m_filename.c_str());
				std::string line;
				std::string current_section = "";
                int i=1;
				while(std::getline(f,line))
				{
                    LDEBUG << "read one more line " << i;
					i++;
                    line = trim(line);
					if(begin_with(line, '[') and end_with(line,']'))
					{
						current_section = trim(std::string(line.begin()+1,line.end()-1));
						m_sections[current_section] = Section();
					}
					std::vector<std::string> splitted_line = split(line,'=');
					if (splitted_line.size() == 2)
					{
						m_sections[current_section].add_value(trim(splitted_line[0]),trim(splitted_line[1]));
					}
				}
			}


			void show_in_cout() const
			{
				for (std::map<std::string, Section>::const_iterator it = m_sections.begin(); it != m_sections.end(); ++it)
				{
					std::cout << it->first<< std::endl;
					it->second.to_cout();
				}
				std::cout << std::endl;
			}

			void show_in_log() const
			{
				for (std::map<std::string, Section>::const_iterator it = m_sections.begin(); it != m_sections.end(); ++it)
				{
					LINFO << it->first;
					it->second.to_log();
				}
				LINFO << "";
			}

			double get_double(std::string const& section, std::string const& variable) const
			{
				std::string tmp = m_sections.find(section)->second.retrieve_value(variable);
				return atof(tmp.c_str());
			}

			int get_int(std::string const& section, std::string const& variable) const
			{
				std::string tmp = m_sections.find(section)->second.retrieve_value(variable);
				return atoi(tmp.c_str());
			}

			std::string get_string(std::string const& section, std::string const& variable) const
			{
				return m_sections.find(section)->second.retrieve_value(variable);
			}

			std::vector<double> get_double_vector(std::string const& section, std::string const& variable) const
			{
				std::string tmp = m_sections.find(section)->second.retrieve_value(variable);
				std::vector<std::string> splitted_data = split(trim(tmp),';');
				std::vector<double> out;
				for (std::vector<std::string>::iterator it=splitted_data.begin() ; it !=splitted_data.end() ; ++it)
				{
					out.push_back(atof(it->c_str()));
				}
				return out;
			}

			std::vector<int> get_int_vector(std::string const& section, std::string const& variable) const
			{
				std::string tmp = m_sections.find(section)->second.retrieve_value(variable);
				std::vector<std::string> splitted_data = split(trim(tmp),' ');
				std::vector<int> out;
				for (std::vector<std::string>::iterator it=splitted_data.begin() ; it !=splitted_data.end() ; ++it)
				{
					out.push_back(atoi(it->c_str()));
				}
				return out;
			}

			bool section_exist(std::string const& name) const
			{
				return m_sections.find(name) != m_sections.end();
			}
			bool variable_exist(std::string const& section, std::string const& name) const
			{
				if (m_sections.find(section) != m_sections.end())
				{
					return m_sections.find(section)->second.exists(name);
				} else
				{
					return false;
				}
			}

	};

}

#endif
