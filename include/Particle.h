#ifndef DEF_PARTICLE
#define DEF_PARTICLE
#include "Classes.h"


#include <string>
#include <time.h>

#include <vector>
#include <iostream>
#include <istream>
#include <ostream>
#include <fstream>
#include <sstream>
#include <math.h>
#include "Utils.h"
#include "Coord.h"
#include "Volume.h"
#include "Correl3DGPU.h"
#include "Image3DGPU.h"
#include "P.h"
#include "PSet.h"

template < class T> class PSet;

class Particle : public P {

private :
	//int scan;
	//int color;
	//Coord coord;
	double volume;
	bool boundary;
	double sphericity;
	double shapeFact;
	double shapeFact2;
	double zAngle;
	int globalcolor;
	//int roisize;
	int father;
	int fatherconnect;
	int grandfather;
	int grandfatherconnect;
	int born;
	int dead;
	std::string param;
	
public :
	Particle();
	Particle(P p);
	Particle(int scan, int color, Coord coord, double volume, bool boundary, double sphericity,
		 double shapeFact, double shapeFact2, double zAngle, std::string param);
	Particle(int scan, int color, Coord coord, double volume, bool boundary, double sphericity,
		 double shapeFact, double shapeFact2, double zAngle);
	Particle(std::string line);
	Particle(const std::string line, int scan, int color);
	Particle(const std::string Directory, const std::string pName);

	~Particle();

	void PUpdate(Particle P);
	void PUpdate(std::string line);
	void PUpdate(const std::string line, int scan, int color);
	std::string write() const;
	Particle findBestCorrel_DICGPU(Correl3DGPU& correlator);
	Particle findBestCorrel_DICGPU(Correl3DGPU& correlator, Coord target);
	Particle optimizeCorrel_DICGPU(Correl3DGPU& correlator, Coord target);
	//double distance(const P& part) const;
	//double distance(const Coord& XYZ) const;


	/*
	void writeToFile(const std::string fileDirectory) const;
	std::string getFileName() const;
	int getScan() const;
	void setScan(int color);
	int getColor() const;
	void setColor(int color);
	Coord getCoord()const;
	void setCoord(Coord coord);
	*/
	double getVolume() const;
	void setVolume(double volume);
	bool isBoundary() const;
	void setBoundary(bool boundary);
	double getSphericity() const;
	void setSphericity(double sphericity);
	double getShapeFact() const;
	double getShapeFact2() const;
	double getZAngle() const;
	int getGlobalColor() const;
	void setGlobalColor(int globalcolor);
	int getFather() const;
	void setFather(int father);
	int getFatherconnect() const;
	void setFatherconnect(int fatherconnect);
	int getGrandfather() const;
	void setGrandfather(int grandfather);
	int getGrandfatherconnect() const;
	void setGrandfatherconnect(int grandfatherconnect);
	int getBorn() const;
	void setBorn(int born);
	int getDead() const;
	void setDead(int dead);
	std::string getParam() const;

	std::string tostring() const;
	std::string writeHeader() const;
	std::string fullInfoTostring() const;
	std::string voidfullInfoTostring() const;
	//bool equalColor(const Particle& part) const;
	//Volume getParticleSubVolume(Volume& fullIm) const;
//	ParticlesSet findBestCorrel(ParticlesSet& potential) const;
//	ParticlesSet findBestCorrel_larger(ParticlesSet& potential) const;
//	ParticlesSet findBestCorrel_smaller(ParticlesSet& potential) const;
//	ParticlesSet findBestCorrel_DIC(ParticlesSet potential, ImagePlus imRef, ImagePlus imFinal);
//	ParticlesSet findBestCorrel_DICGPU(ParticlesSet potential, ImagePlus imRef, ImagePlus imFinal, Correl3DGPU correlator);
//	ParticlesSet findBestCorrel_DICGPU(ParticlesSet potential, Correl3DGPU correlator);

//	ParticlesSet findBestCorrel_DIC_multi(final ParticlesSet potential, final ImagePlus imRef, final ImagePlus imFinal);

	Particle* findClosest(PSet<Particle>& potential);

	//ImageStack recolor(ImageStack stack, int[] val);
	//Stack duplicate(Stack src);
	//Stack duplicateEmpty(Stack src);
	//Volume getParticleSubVolumeSafe(Volume& fullIm, const std::string volumeName) const;
	//ImageStack duplicateRoi(final ImageStack src, int zinit, int depth);
	
	PSet<Particle> findBestCorrel_noDIC(PSet<Particle>& potential, Coord target);
	PSet<Particle> findBestCorrel_noDIC_safe(PSet<Particle>& potential, Coord target);
	PSet<Particle> findBestCorrel_DICGPU(PSet<Particle>& potential, Correl3DGPU& correlator, Coord target);

    bool operator<(Particle p);

};


#endif
