#ifndef DEF_PSET
#define DEF_PSET
#include "Classes.h"
#include "Param.h"

#include <iostream>
#include <fstream>
#include <string>
#include <time.h>
#include <vector>
#include <list>
#include <tiffio.h>

#include "Utils.h"
#include "Coord.h"

#include "P.h"
#include "Particle.h"

template < class T >
class PSet {

  private :

    std::string name;
    int StackdimX;
    int StackdimY;
    int StackdimZ;
    std::list<T*>* ps;

  public :

    //Constructor and destructor
    PSet();
    PSet(std::string name);
    PSet(std::string name, std::list<T*> *points);
    PSet(const PSet<T>& pointsSet);
    //PSet(const std::string fileDirectory, const std::string pointsSetName);
    PSet(const std::string fileDirectory, const std::string pointsSetName,const std::vector<PSet<T> >& raw);
    PSet(const std::string fileDirectory, const std::string pointsSetName, std::vector<T* >& raw);
    PSet(std::string name, const std::vector<T*>& raw);
    ~PSet();
    void cleanAll();

//Operator
    operator PSet<P>() { return PSet<P>();}
    //PSet<T> & operator = (const PSet<T>& assign){ return *this;}
    PSet<T> & operator = (const PSet<T>& assign);

//Getter and setter
    std::string getName() const;
    void setName(std::string name);
    std::list<T*>* getP() const;
    void setP(std::list<T*> *points);
    typename std::list<T*>::iterator getIteratorBegin();
    typename std::list<T*>::iterator getIteratorEnd();
    typename std::list<T*>::iterator getIteratorBegin() const;
    typename std::list<T*>::iterator getIteratorEnd() const;
    void addP(T* point);
    int Size() const;
    void removeP(T* part);
    void removeP(int i);
    int getPos(const T& part);
    bool contains(const T& part);
    void setDimensions();
    void setScan(int scan);
    int getStackdimX() const;
    void setStackdimX(int stackdimX);
    int getStackdimY() const;
    void setStackdimY(int stackdimY);
    int getStackdimZ() const;
    void setStackdimZ(int stackdimZ);
    void initGlobalColor();
    int fillGlobalColor(int currentMax);
    bool hasGlobalColor(int color);
    void initCorrel(int correl);
    void initTotalCorrel(int correl);
    int removeUnmatched();
    void resetGlobalColor();
    int maxGlobalColor() const;

//Extractor
    T* findP(int color) ;
    T* findP(int color) const;
    T* getP(int i);
    T* findGlobalColorP(int color) ;
    T* findGlobalColorP(int color) const;

//Subsets from single parameter or distance
    PSet<T> subSet(const Coord& coord, double distX, double distY, double distZ) const;
    PSet<T> subSet(const Coord& coord, double distXY, double distZ) const;
    PSet<T> subSet(const Coord& coord, double dist) const;
    PSet<T> subSet(const PSet<T>& ps) const;
    PSet<T> subSetCorrel(double thr) const;
    PSet<T> subSetTotalCorrel(double thr) const;
    PSet<T> subSetSpatialCoherency(double thr) const;
    PSet<T> subSetVol(double vMin) const;
    T* findClosest(const Coord& XYZ) ;
    PSet<T> closest(PSet<T> pfinal, double distXY, double distZ);
    PSet<Particle> findLost(PSet<Particle> pool);

//Subsets from correlation or multicriteria
    PSet<T> findBestCorrel_DICGPU(PSet<T> &pfinal, Parameters &param); //The new one 2019/04/26
    PSet<T> optimizeCorrel_DICGPU(PSet<T> &pfinal, Parameters &param); //The new one 2019/04/26

    PSet<T> findBestCorrel_DICGPU(PSet<T>& pfinal, int correlDomain, double distXY, double distZ, double correlThr);
    PSet<T> findBestCorrel_DICGPU_aniso(PSet<T> &pfinal, int correlDomainX, int correlDomainY, int correlDomainZ, double distX, double distY, double distZ, double correlThr);
    PSet<T> findBestCorrel_DICGPU_aniso_guided(PSet<T> &pfinal, int correlDomainX, int correlDomainY, int correlDomainZ, double distX, double distY, double distZ, double correlThr);
    PSet<T> findBestCorrel_DICGPU_aniso_guided(PSet<T> &pfinal, int correlDomainX, int correlDomainY, int correlDomainZ, double distX, double distY, double distZ, double correlThr, int initThrXfactor, int initThrYfactor, int initThrZfactor);
    PSet<T> findBestCorrel_DICGPU_aniso_guided_bak(PSet<T> &pfinal, int correlDomainX, int correlDomainY, int correlDomainZ, double distX, double distY, double distZ, double correlThr);
    PSet<T> findBestCorrel_DICGPU_texture(PSet<T> &pfinal, int correlDomainX, int correlDomainY, int correlDomainZ, double distX, double distY, double distZ, double correlThr);
    PSet<T> findBestCorrel_DICGPU(PSet<T>& pfinal, int correlDomain, double distXY, double distZ, double correlThr, std::vector<std::vector<double> > depl);
    PSet<T> optimizeCorrel_DICGPU(PSet<T>& pfinal, int correlDomain, int distThr, double correlThr, int distLocal);
    PSet<T> optimizeCorrel_DICGPU_aniso(PSet<T> &pfinal, int correlDomainX, int correlDomainY, int correlDomainZ, int distThrX, int distThrY, int distThrZ, double correlThr, int distLocal);
    PSet<T> optimizeCorrel_DICGPU_texture(PSet<T> &pfinal, int correlDomainX, int correlDomainY, int correlDomainZ, int distThrX, int distThrY, int distThrZ, double correlThr, int distLocal);
    void findBackwardPosition_noDIC(const PSet<P>& pinit, PSet<P>& pfinal, double correlThr, double thrdist, double interactdist);
    int findParticle_fromLabelled(PSet<Particle> &pfinal, PSet<P> refPoints, PSet<P> finalPoints, double distLocal, double distLabel, double correlThr, int currentMax);

//Coord estimator
	Coord estimateDepl(const Coord& ca, const PSet<T>& pfinal, double correlThr, double dist);
    Coord estimateLargeDepl(const Coord& ca, const PSet<T>& pfinal, double correlThr, double thrdist, double dist);
    Coord estimateDepl(const Coord& ca, const PSet<T>& pfinal, double correlThr, double thrdist, double dist);
    Coord estimatePosition(const Coord& ca, const PSet<T>& pfinal, double correlThr, double dist);
    Coord estimatePositionEq(const Coord& ca, const PSet<T>& pfinal, double correlThr, double dist);
    Coord estimatePositionGauss(const Coord& ca, const PSet<T>& pfinal, double correlThr, double distThr, double interactdist, bool useTotalCorrel);
    Coord estimatePositionGaussInv(const Coord& ca, const PSet<T>& pfinal, double correlThr, double distThr, double interactdist, bool useTotalCorrel);
    Coord estimatePositionGauss(const Coord& ca, const PSet<T>& pfinal, double correlThr, double distThr, bool useTotalCorrel);
    Coord estimatePositionfromQuadraticFit(const Coord& ca, const PSet<T>& pfinal, double distThr);

//Matching functions
    std::vector<PSet<T> > findCorrel(PSet<T> pfinal, double refSlices[], double refSlicesFinal[]);
    void totalCorrel(std::vector<PSet<T> > VpS);
    std::vector<PSet<Particle> > findBestCorrel_noDIC(PSet<Particle> &pfinal, PSet<P> refPoints, PSet<P> finalPoints, double distXY, double distZ);
    std::vector<PSet<Particle> > findBestCorrel_noDIC_safe(PSet<Particle> &pfinal, PSet<P> refPoints, PSet<P> finalPoints, double distX, double distY, double distZ, double correlThr);
    void findIterativeSingleBestCorrel_noDIC(PSet<Particle> &pfinal, PSet<P> refPoints, PSet<P> finalPoints, double distX, double distY, double distZ, double correlThr);
    void findSingleBestCorrel_noDIC_safe(PSet<Particle> &pfinal, PSet<P> refPoints, PSet<P> finalPoints, double distX, double distY, double distZ, double correlThr);
    std::vector<PSet<Particle> > findBestCorrel_DICGPU(PSet<Particle> &pfinal, PSet<P> refPoints, PSet<P> finalPoints, double distXY, double distZ);
    //std::vector<PSet<T> > findBestCorrel_DICGPU(PSet<T> &pfinal, PSet<Particle> &refPoints, PSet<Particle> &finalPoints, double distXY, double distZ);
    int setGlobalNumbers(int current);
    void matchGlobalNumberIntra(PSet<Particle> partSet);
    //int matchGlobalNumberInter(PSet<T> partSet, int current);
    //int globalNumberFromCheckedRef(std::vector<PSet<T> > VpS, int current);
    int globalNumberFromCheckedRef(std::vector<PSet<Particle> > & VpS, int current, int scan);
    //void globalNumberFromCheckedFinal(std::vector<PSet<T> > VpS, PSet<T> ref);
    int globalNumberFromCheckedFinal(std::vector<PSet<Particle> > & VpS, PSet<T> & ref, int current, int scan);
    int globalNumberFromSafe(std::vector<PSet<Particle> > & VpS, PSet<Particle> & ref, int scan );

    //void findNullFromImage(PSet<T> poolr, std::string ImfName, double thrXY,double thrZ);
    void debugCorrel(std::vector<PSet<Particle> > & potential, PSet<Particle> & pfinal, std::vector<PSet<Particle> > & potentialInv);
    //std::vector<PSet<Particle> > findBestCorrel_noDIC(PSet<Particle> &pfinal, std::vector<Coord> refPoints, std::vector<Coord> refDepl, double distXY, double distZ);
    //std::vector<PSet<Particle> > findBestCorrel_noDIC(PSet<Particle>& pfinal, std::vector<std::vector<double> > depl, double distXY, double distZ);
    std::vector<PSet<Particle> > checkCorrel(std::vector<PSet<Particle> > potential, PSet<Particle> pfinal, std::vector<PSet<Particle> > potentialInv);
    std::vector<PSet<Particle> > matchCorrel(std::vector<PSet<Particle> > & potential, PSet<Particle> & pfinal, std::vector<PSet<Particle> > & potentialInv);
    //std::vector<PSet<Particle> > checkCorrel_DIC(std::vector<PSet<Particle> > potential, PSet<Particle> pfinal, std::vector<PSet<Particle> > potentialInv);
    void addUnused(PSet<Particle> & partSet, Particle & part, Coord& coord);
    void updateUnused(Particle & part, Coord& coord);
    void updateUnused(PSet<Particle> &partSet, Particle &part, Coord &coord);
    void updateUnused(PSet<P> &partSet, Particle &part, Coord &coord);
    void updateUnmatched(Particle & part, Coord& coord);
    void updateUnmatched(PSet<Particle> &partSet, Particle &part, Coord &coord);
    void updateUnmatched(PSet<P> &partSet, Particle &part, Coord &coord);

//Quality estimators
    void estimateSpatialCoherency(const PSet<T>& pinit, double distThr, double SCThr);
    void scaleCorrelFactorwithSpatialCoherency();
    void estimateLocalQuadraticCoherency(const PSet<T>& pinit, double distThr);

//Ordering and renumbering
    void renumber();
    PSet<T> rankByVolume();
    void rankByGlobalNumber();
    std::list<T*>* rankLocality( int Thr ) const;

//Counter
    int countValid(double) const;

//Mesh tools
    PSet<P> computeMesh(PSet<P>& newMesh, PSet<T>& pInit, PSet<T>& pFinal, double correlThr, double distThr, double distLocal);
    void computeSafeMesh(PSet<P>& newMesh, PSet<T>& pInit, PSet<T>& pFinal, double correlThr, double distThr, double distLocal);
    void writeVTKMesh(const std::string fileName, const PSet<T> reference, const PSet<T> lastStep);
    void writeVTKParticle(const std::string fileName);
    void writeVTKParticleSorted(const std::string fileName, int nb);

//Output to string
    std::string colorGlobalTostring() ;
    std::string volumeTostring() ;
    std::string sphericityTostring() ;
    std::string shapeFactTostring() ;
    std::string shapeFact2Tostring() ;
    std::string zAngleTostring() ;
    std::string fullInfoTostring() ;
    std::string colorTostring() ;
    std::string correlTostring() ;
    std::string PosTostring();

//Output to file
    void colorToFile(const std::string fileName) ;
    void writeToFile(const std::string fileName) ;
    void writeToFile(const std::string fileDirectory, const std::string pointsSetName) const;
    void write(const std::string fileName) const;
    void writeToPtsFile(const std::string fileName);
    void writeVTKDisplacement(const std::string FileName, const PSet<T> reference, const PSet<T> lastStep);
    void writeToCorrelManuPtsFile(const std::string fileName);
    void writeCastemINP(const std::string directory, const std::string nameprefix, PSet<T> &ref0);
    void writeParamWithBackwardPosition(const std::string fileName);

};


#endif


