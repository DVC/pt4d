#ifndef DEF_UTILS
#define DEF_UTILS


#include "log4cpp/Category.hh"
#include "log4cpp/Appender.hh"
#include "log4cpp/FileAppender.hh"
#include "log4cpp/OstreamAppender.hh"
#include "log4cpp/Layout.hh"
#include "log4cpp/BasicLayout.hh"
#include "log4cpp/PatternLayout.hh"
#include "log4cpp/Priority.hh"

#define PINFO(y) logger.info(y)
#define LINFO logger.infoStream()

#define PERROR(y) logger.error(y)
#define LERROR logger.errorStream()

#define PDEBUG(y) debuglogger.debug(y)
#define LDEBUG debuglogger.debugStream()


#ifdef OPENMP_ENABLE
#include <omp.h>
#endif


#include <time.h>
#include <string>
#include <cstdarg>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <assert.h>


#ifndef INTEL
    #define INTEL 0
#endif

#ifndef LAPACKE
    #define LAPACKE 0
#endif

#if INTEL
    #include <mkl_lapacke.h>
    #include <mkl.h>
#else 
    #if LAPACKE
        #include <lapacke.h>
        #include <cblas.h>
    #else
        #include <cuda_runtime.h>
        #include "cublas_v2.h"
        #include "cusolverDn.h"
        #include "helper_cuda.h"
        //#include "helper_cusolver.h"
    #endif
#endif

extern log4cpp::Category& debuglogger;
extern log4cpp::Category& logger;

int diffclock(clock_t clock1,clock_t clock2);

inline std::string format(const char* fmt, ...)
{
    int size = 512;
    char* buffer = 0;
    buffer = new char[size];
    va_list vl;
    va_start(vl, fmt);
    int nsize = vsnprintf(buffer, size, fmt, vl);
    if(size<=nsize){ //fail delete buffer and try again
        delete[] buffer;
        buffer = 0;
        buffer = new char[nsize+1]; //+1 for /0
        nsize = vsnprintf(buffer, size, fmt, vl);
    }
    std::string ret(buffer);
    va_end(vl);
    delete[] buffer;
    return ret;
}


void DummyHandler(const char* module, const char* fmt, va_list ap);

double median(const double* u, const int N);
void qsort(double* u, int lo0, int hi0);
void swap(double* u, int i, int j);
void sort(double* u, int N);
double* invert(double* A);


//#if LAPACKE
int solve_syst(double* A, double* b, int n);
//#endif
//int solve_XtWXA_XWU(double* X, double* W, double* U, double *A, int N0, int n);
int solve_MXA_MU(double* M, double* X, double* U, double *A, int N0, int n);
#if !LAPACKE && !INTEL
	int linearSolverLU(cusolverDnHandle_t handle, int n, const double *Acopy, int lda, const double *b, double *x);
#endif

#endif
