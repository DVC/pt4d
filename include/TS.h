#ifndef DEF_TS
#define DEF_TS
#include "Classes.h"

#include <iostream>
#include <fstream>
#include <string>
#include <time.h>
#include "Utils.h"
#include "Param.h"
#include "IO.h"
#include "Coord.h"
#include <list>
#include <vector>
#include "PSet.h"
#include "Particle.h"
#include "P.h"
#include "TimeSerie.h"

template <typename Ptr, typename Pg>
class TS : public TimeSerie<Ptr,Pg> {

	public:

	TS();
    ~TS();
    TS(Parameters* param);

};


template <typename Pg>
class TS<Particle, Pg> : public TimeSerie<Particle,Pg> {

	public:

    TS();
    ~TS();
    TS(Parameters* param);
    int trackParticles();
    int trackIncompleteParticles();
    int trackOnlyBackwardPositionParticles();
    int trackNucleateAndMergeParticles();
    int addMeshInfo();
    void recolor(std::vector<PSet<Particle> >);
    std::vector<PSet<Particle> > rankParticles(int &);
    void rankParticles();

};


#endif

