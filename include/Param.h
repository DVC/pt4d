#ifndef H_PARAM
#define H_PARAM


#ifndef TRANSLATION
    #define TRANSLATION 0
#endif

#ifndef ROTATION
    #define ROTATION 0
#endif

#ifndef FIRSTORDER
    #define FIRSTORDER 0
#endif

#if TRANSLATION
    #define DEFORMATIONSIZE 1
#endif

#if ROTATION
    #define DEFORMATIONSIZE 2
#endif

#if FIRSTORDER
    #define DEFORMATIONSIZE 4
#endif

#include <string>
#include <iostream>
#include <fstream>
#include <string>
//#include "conf_reader.hpp"
#include <vector>
#include <tiffio.h>
#include "Classes.h"



struct Parameters
{
    //inputOutput
    char Directory[80];
    char inputDir[80];
    char outputDir[80];
    char prefix[80];
    char prefiximage[80];
    char suffiximage[80];
    char prefixptrs[80];
    char suffixptrs[80];
    char prefixdepl[80];
    char suffixdepl[80];
    char prefixmask[80];
    char suffixmask[80];
    char prefixmesh[80];
    char suffixmesh[80];
    char prefixpgs[80];
    char suffixpgs[80];
    char prefixptrsdef[80];
    char suffixptrsdef[80];
    //char meshname[80];
    char theoreticalSqueleton[80];

    //volumes
    int firstvol;
    int lastvol;
    int intervol;
    std::vector<int> skippedvol;

    //Options
    bool append;
    bool DIC;
    bool rigidguiding;
    bool backwardposition;
    bool missingparticles;
    bool precorrel;
    bool rmbad;
    bool initfromparam;
    bool postprocess;
    bool aniso;
    bool rota;
    int restart;
    bool reverse;
    bool deffield;
    bool deplfield;
    bool exx;
    bool eyy;
    bool ezz;
    bool mesh;

    //GPU
    bool usetex;

    //Correlation
    double thrXYZ;
    int initThrXfactor;
    int initThrYfactor;
    int initThrZfactor;
    double thrX;
    double thrY;
    double thrZ;
    double correlThr;
    int correlDomain;
    int correlDomainX;
    int correlDomainY;
    int correlDomainZ;
    int rotAxis;
    int nrot;
    double deltaTheta;
    double nodesSpacing;

    //Regularisation
    bool optimize;
    double optiThrXYZ;
    double optiThrX;
    double optiThrY;
    double optiThrZ;
    double regulThrDist;
    double regulDist;
    //Parameters() : append(true), precorrel(false), rmbad(false), postprocess(false), aniso(false), rotZ(false), rotZ(false),deplfield(false), exx(false), eyy(false), ezz(false), usetex(true){};
};


void readConfig (const char *fileName, Parameters *param);

void init(std::string Directory);

void finalize();

void generateInputFile(const char *prog, const char *inputName);

#endif
