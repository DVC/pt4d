#ifndef DEF_SQUELETON
#define DEF_SQUELETON
#include "Classes.h"

#include <iostream>
#include <fstream>
#include <string>
#include <time.h>
#include "Utils.h"
#include "Coord.h"
#include <list>
#include "Node.h"
#include "Strut.h"


class Squeleton {
	
	private :
	
	std::string name;
	std::list<Node*>* nodes;
	std::list<Strut*>* struts;
	
	public :

	Squeleton();
    ~Squeleton();
    std::string  getName() const;
    void setName(std::string name);
    void read(std::string fileName);
    void update();
    void splitStruts(double mindist);
    Node* getNode(int num);
    Node* getNodeByColor(int num);
    Strut* getStrut(int num);
    std::list<Node*>* getNodes();
    std::list<Strut*>* getStruts();
    std::list<Node*>* getConnectedNodes(Node* n);
    std::list<Node*>* getConnectedNodes(Node* n, std::list<Node*>* l);
    std::list<Node*>* rankLocalityNode();
    std::list<Node*>* rankLocalityFull();
    double* getStrain(Squeleton* ref, Node* curNode);
    //std::list<Node*>* rankLocality(int Thr);
    bool writeVTK(std::string name);
    bool writeVTKdef(std::string name, Squeleton* ref, Squeleton* last);
    void correl_DICGPU_texture(Squeleton &pfinal, Volume &imRef, Volume &imFinal, int correlDomainX, int correlDomainY, int correlDomainZ, int distThrX, int distThrY, int distThrZ, double correlThr);
    void correl_DICGPU_rigid_guiding(Squeleton &pfinal, Volume &imRef, Volume &imFinal, int correlDomainX, int correlDomainY, int correlDomainZ, int distX, int distY, int distZ, double correlThr, int initThrXfactor, int initThrYfactor, int initThrZfactor);
    Coord estimatePosition(Node* n, Squeleton &pfinal, double correlThr, bool useTotalCorrel);

};

#endif 
