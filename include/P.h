#ifndef DEF_P
#define DEF_P
#include "Classes.h"

#include "Param.h"
#include <string>
#include <time.h>

#include <vector>
#include <iostream>
#include <istream>
#include <ostream>
#include <fstream>
#include <sstream>
#include <math.h>
#include "Utils.h"
#include "Coord.h"
#include "Volume.h"
#include "Correl3DGPU.h"
#include "Image3DGPU.h"

class P {

protected :
	int scan;
	int color;
	Coord coord;
    //#if (ROTATION)
    //    Coord rot;
    //#endif
    double correl;
    double totalcorrel;
    double spatialCoherency;
    int roisize;
    int roiX;
    int roiY;
    int roiZ;

public :
	P();
	//P(P p);
	P(int scan, int color, Coord coord, double correl);
	P(int scan, int color, Coord coord);
	P(std::string line);
	P(const std::string line, int scan, int color);
	P(int scan, int color, Coord coord, int roiX, int roiY, int roiZ);
	P(const std::string Directory, const std::string pName);

	~P();

	virtual void PUpdate(P point);
	virtual void PUpdate(std::string line);
	virtual void PUpdate(const std::string line, int scan, int color);
	virtual std::string write() const;
	P findBestCorrel_DICGPU(Correl3DGPU& correlator);
	P findBestCorrel_DICGPU(Correl3DGPU& correlator, Coord target);
	P optimizeCorrel_DICGPU(Correl3DGPU& correlator, Coord target);
    virtual double distance(const P& part) const;
	virtual double distance(const Coord& XYZ) const;
    double estimateSpatialCoherency(PSet<P>& pi, PSet<P>& pf);
    double estimateSpatialCoherency(PSet<Particle>& pi, PSet<Particle>& pf);
    double estimateLocalQuadraticCoherency(PSet<P>& pi, PSet<P>& pf);
    double estimateLocalQuadraticCoherency(PSet<Particle>& pi, PSet<Particle>& pf);

	void writeToFile(const std::string fileDirectory) const;
	std::string getFileName() const;
	int getScan() const;
	void setScan(int color);
	int getColor() const;
	int getGlobalColor() const;
	void setColor(int color);
	Coord getCoord()const;
	void setCoord(Coord coord);
	double getCorrel() const;
	void setCorrel(double correl);
	double getTotalCorrel() const;
	void setTotalCorrel(double correl);
	double getSpatialCoherency() const;
	void setSpatialCoherency(double SC);
	int getRoiX() const;
	void setRoiX(int roiX);
	int getRoiY() const;
	void setRoiY(int roiY);
	int getRoiZ() const;
	void setRoiZ(int roiZ);
	std::string tostring() const;
	std::string tostringFull() const;
	bool equalColor(const P& part) const;
	Volume getPSubVolume(Volume& fullIm) const;
	Volume getPSubVolumeSafe(Volume& fullIm, const std::string volumeName) const;

	virtual P* findClosest(PSet<P>& potential);
	//operator Particle() {return Particle();}

    bool operator<(P p);

};
#endif
