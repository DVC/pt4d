#ifndef DEF_POINT
#define DEF_POINT
#include "Classes.h"

#include "Param.h"
#include <string>
#include <time.h>

#include <vector>
#include <iostream>
#include <istream>
#include <ostream>
#include <fstream>
#include <sstream>
#include <math.h>
#include "Utils.h"
#include "Coord.h"
#include "Volume.h"
#include "Correl3DGPU.h"
#include "Image3DGPU.h"
#include "P.h"

class Point : public P {

private :

public :
	Point();
	~Point();
	Point(int scan, int color, Coord coord, double correl);
	Point(int scan, int color, Coord coord);
	Point(std::string line);
	Point(const std::string line, int scan, int color);

	Point* findClosest(PSet<Point>& potential);

	/**
  void PointUpdate(Point point);
	void PointUpdate(std::string line);
	void PointUpdate(const std::string line, int scan, int color);
	std::string write() const;
	void writeToFile(const std::string fileDirectory) const;
	std::string getFileName() const;
	int getScan() const;
	void setScan(int color);
	int getColor() const;
	void setColor(int color);
	Coord getCoord()const;
	void setCoord(Coord coord);
	double getCorrel() const;
	void setTotalCorrel(double correl);
	double getTotalCorrel() const;
	void setCorrel(double correl);
	std::string tostring() const;
	std::string tostringFull() const;
	//std::string writeHeader() const;
	//std::string fullInfoTostring() const;
	//std::string voidfullInfoTostring() const;
	bool equalColor(const Point& part) const;
	Volume getPointSubVolume(Volume& fullIm) const;
	Point findBestCorrel_DICGPU(Correl3DGPU& correlator);
	Point findBestCorrel_DICGPU(Correl3DGPU& correlator, Coord target);
	Point optimizeCorrel_DICGPU(Correl3DGPU& correlator, Coord target);
	Volume getPointSubVolumeSafe(Volume& fullIm, const std::string volumeName) const;
    double  distance(const Point& part) const;
	double  distance(const Coord& XYZ) const;
	 */
};
#endif
