#ifndef DEF_TIMESERIE
#define DEF_TIMESERIE
#include "Classes.h"

/**
#if PTRPARTICLE
	typedef Particle Ptr;
#else
	typedef P Ptr;
#endif

#if PGPARTICLE
	typedef Particle Pg;
#else
	typedef P Pg;
#endif
*/

#include <iostream>
#include <fstream>
#include <string>
#include <time.h>
#include "Utils.h"
#include "Coord.h"
#include <list>
#include <vector>
#include "Mesh.h"
#include "PostProc.h"
#include "PSet.h"
#include "Particle.h"
#include "P.h"
#include "IO.h"

template <typename Ptr, typename Pg>
class TimeSerie {
	
  protected :

    std::string inputDir;
    std::string outputDir;
    std::vector<std::string> scanNumberStr;
    std::vector<int> scanNumber;
    
    std::vector<std::string> names;
    
    int startExpe;
    int numberExpe;

    std::string pgsDir;
    std::vector<std::string> pgsPrefix;
    std::vector<std::string> pgsFilesNames;
    std::vector<std::vector<Pg*> > pgsData;
    std::vector<PSet<Pg> > pgs;
    
    std::string ptrsDir;
    std::vector<std::string> ptrsPrefix;
    std::vector<std::string> ptrsFilesNames;
    std::vector<std::vector<Ptr*> > ptrsData;
    std::vector<PSet< Ptr> > ptrs;
    
    std::string ptrsDefDir;
    std::vector<std::string> ptrsDefPrefix;
    std::vector<std::string> ptrsDefFilesNames;

    std::string resptrscorDir;
    std::vector<std::string> resptrscorPrefix;
    std::vector<std::string> resptrscorFilesNames;

    std::string resptrsDir;
    std::vector<std::string> resptrsPrefix;
    std::vector<std::string> resptrsFilesNames;
    std::vector<PSet< Ptr> > resptrs;

    std::string meshesDir;
    std::vector<std::string> meshesPrefix;
    std::vector<std::string> meshesFilesNames;
    std::vector<Mesh*> mesh;
    
    std::string resmeshesDir;
    std::vector<std::string> resmeshesPrefix;
    std::vector<std::string> resmeshesFilesNames;
    std::vector<Mesh*> resmesh;
	
    std::vector<std::vector<std::vector<double> > > preCorrelDeplZ;

    std::vector<std::string> imagesPrefix;
    std::vector<std::string> imagesFilesNames;
    std::vector<std::string> maskFilesNames;
    std::vector<std::string> outputprefixFilesNames;

    Parameters* param;

  public:
    
    TimeSerie();
    ~TimeSerie();
    TimeSerie(Parameters* param);
    void readPgs();
    void readPtrs();
    void readMesh();
    void computeMesh();
    void computeDeplField();
    void writePgs();
    void writePtrs();
    void removeUntracked();
    void readPreCorrel();
    void correl();
    //int trackParticles();
    //void recolor(std::vector<PSet<Particle> >);
    //std::vector<PSet<Particle> > rankParticles(int &);


  private:

    void readP(std::vector<PSet< Particle> > & particles, std::vector<std::vector<Particle*> > & particlesData, std::vector<std::string> & particlesFilesNames, bool Pgs);
    void readP(std::vector<PSet< P> > & particles, std::vector<std::vector<P*> > & particlesData, std::vector<std::string> & particlesFilesNames, bool Pgs);
    void writeP(std::vector<PSet< Particle> > & points, std::string ptype);
    void writeP(std::vector<PSet< P> > & points, std::string ptype);

};


#endif

