#ifndef DEF_POINTSSET
#define DEF_POINTSSET
#include "Classes.h"

#include <iostream>
#include <fstream>
#include <string>
#include <time.h>
#include "Utils.h"
#include "Coord.h"
#include <vector>
#include <list>
#include <tiffio.h>
#include "Point.h"
#include "ParticlesSet.h"

class PointsSet {
	
	private :
	
	std::string name;
	int StackdimX;
	int StackdimY;
	int StackdimZ;
	std::list<Point*>* points;
	
	public :
	
	PointsSet();
	PointsSet(std::string name);
	PointsSet(std::string name, std::list<Point*> *points);
	//PointsSet(std::string name, int StackdimX, int StackdimY, int StackdimZ);
	//PointsSet(std::string name, int StackdimX, int StackdimY, int StackdimZ, std::list<Point*> *points);
	PointsSet(const PointsSet& pointsSet);
	PointsSet(const std::string fileDirectory, const std::string pointsSetName);
    PointsSet(const std::string fileDirectory, const std::string pointsSetName,const std::vector<PointsSet>& raw);
	//PointsSet(std::string name, int StackdimX, int StackdimY, int StackdimZ, const std::vector<Point*>& raw);
	PointsSet(std::string name, const std::vector<Point*>& raw);
	~PointsSet();

	// conversion from A (constructor):
	PointsSet (const ParticlesSet& ps){};
	// conversion from A (assignment):
	PointsSet& operator= (const ParticlesSet& x){return *this;};
	// conversion to A (type-cast operator)
	operator ParticlesSet() {return ParticlesSet();}


	std::string getName() const;
	void setName(std::string name);
	std::list<Point*>* getPoints() const;
	//std::list<Point*>* getPointsRef() const;
	void setPoints(std::list<Point*> *points);
	Point* getPoint(int i);
	std::list<Point*>::iterator getIteratorBegin();
	std::list<Point*>::iterator getIteratorEnd();
	std::list<Point*>::iterator getIteratorBegin() const;
	std::list<Point*>::iterator getIteratorEnd() const;
	//void setPoint(int i, Point* point);
	void addPoint(Point* point);
	int Size() const;
	void removePoint(Point* part);
	void removePoint(int i);
	PointsSet subSet(const Coord& coord, double distXY, double distZ) const;
	PointsSet subSet(const Coord& coord, double dist) const;
	PointsSet subSet(const PointsSet& ps) const;
	PointsSet subSetCorrel(double thr) const;
	PointsSet subSetTotalCorrel(double thr) const;
	PointsSet findBestCorrel_DICGPU(PointsSet& pfinal, int correlDomain, double distXY, double distZ, double correlThr);
	PointsSet findBestCorrel_DICGPU_aniso(PointsSet &pfinal, int correlDomainX, int correlDomainY, int correlDomainZ, double distX, double distY, double distZ, double correlThr);
	PointsSet findBestCorrel_DICGPU_texture(PointsSet &pfinal, int correlDomainX, int correlDomainY, int correlDomainZ, double distX, double distY, double distZ, double correlThr);
	PointsSet findBestCorrel_DICGPU(PointsSet& pfinal, int correlDomain, double distXY, double distZ, double correlThr, std::vector<std::vector<double> > depl);
	PointsSet optimizeCorrel_DICGPU(PointsSet& pfinal, int correlDomain, int distThr, double correlThr, int distLocal);
	PointsSet optimizeCorrel_DICGPU_aniso(PointsSet &pfinal, int correlDomainX, int correlDomainY, int correlDomainZ, int distThrX, int distThrY, int distThrZ, double correlThr, int distLocal);
	PointsSet optimizeCorrel_DICGPU_texture(PointsSet &pfinal, int correlDomainX, int correlDomainY, int correlDomainZ, int distThrX, int distThrY, int distThrZ, double correlThr, int distLocal);
	int getPos(const Point& part);
	int setGlobalNumbers(int current);
	void matchGlobalNumberIntra(PointsSet partSet);
	int matchGlobalNumberInter(PointsSet partSet, int current);
	int globalNumberFromCheckedRef(std::vector<PointsSet> VpS, int current);
	int globalNumberFromCheckedRef(std::vector<PointsSet> & VpS, int current, int scan);
	void globalNumberFromCheckedFinal(std::vector<PointsSet> VpS, PointsSet ref);
	int globalNumberFromCheckedFinal(std::vector<PointsSet> & VpS, PointsSet & ref, int current, int scan);
	void findNullFromImage(PointsSet poolr, std::string ImfName, double thrXY,double thrZ);
	std::vector<PointsSet> findCorrel(PointsSet pfinal, double refSlices[], double refSlicesFinal[]);
	PointsSet rankByVolume();
	Point* findPoint(int color) ;
	Point* findPoint(int color) const;
	Point* findClosest(const Coord& XYZ) ;
	std::string fullInfoTostring() ;
	std::string colorTostring() ;
	std::string colorGlobalTostring() ;
	void colorToFile(const std::string fileName) ;
	void writeToFile(const std::string fileName) ;
	void writeToFile(const std::string fileDirectory, const std::string pointsSetName) const;
	void write(const std::string fileName) const;
	std::string correlTostring() ;
	std::string PosTostring();
	int getStackdimX() const;
	void setStackdimX(int stackdimX);
	int getStackdimY() const;
	void setStackdimY(int stackdimY);
	int getStackdimZ() const;
	int countValid(double) const;
	void setStackdimZ(int stackdimZ);
	PointsSet & operator = (const PointsSet& assign);
	void setDimensions();
	void writeToPtsFile(const std::string fileName);
    void writeVTKDisplacement(const std::string FileName, const PointsSet reference, const PointsSet lastStep);
	void writeToCorrelManuPtsFile(const std::string fileName);
    Coord estimateDepl(const Coord& ca, const PointsSet& pfinal, double correlThr, double dist);
    Coord estimateLargeDepl(const Coord& ca, const PointsSet& pfinal, double correlThr, double thrdist, double dist);
    Coord estimateDepl(const Coord& ca, const PointsSet& pfinal, double correlThr, double thrdist, double dist);
    Coord estimatePosition(const Coord& ca, const PointsSet& pfinal, double correlThr, double dist);
    Coord estimatePositionEq(const Coord& ca, const PointsSet& pfinal, double correlThr, double dist);
    Coord estimatePositionGauss(const Coord& ca, const PointsSet& pfinal, double correlThr, double distThr, double interactdist, bool useTotalCorrel);
    void renumber();
    void setScan(int scan);
    PointsSet computeMesh(PointsSet& newMesh, PointsSet& pInit, PointsSet& pFinal, double correlThr, double distThr, double distLocal);
    void computeSafeMesh(PointsSet& newMesh, PointsSet& pInit, PointsSet& pFinal, double correlThr, double distThr, double distLocal);
    void writeVTKMesh(const string fileName, const PointsSet reference, const PointsSet lastStep);
	void totalCorrel(std::vector<PointsSet> VpS);
		
};
#endif


