#ifndef DEF_STRUT
#define DEF_STRUT

#include <iostream>
#include <fstream>
#include <string>
#include <time.h>
#include "Utils.h"
#include "Node.h"
#include <list>

class Strut {
	
	private :
	
	int number;
	Node* n1;
	Node* n2;
    bool broken;
    list<Strut*>* substruts;
    list<Node*>* subnodes;

	public :
	
	Strut();
	~Strut();
	Strut(int number, Node* n1, Node* n2, bool broken);
    int getNumber() const;
	Node* getNode(int i);
	Node* getOtherNode(Node* n);
    list<Strut*>*  getSubstruts();
    void generateSubstruts(double mindist, list<Node*> &nodes);
    double getLength();
    double getStrain(Strut &s);
    bool isBroken();
    void setBroken(bool broken);
    bool contains(Node* n);
	std::string tostring() const;
};
#endif
