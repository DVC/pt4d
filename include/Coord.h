#ifndef DEF_COORD
#define DEF_COORD

#include <string>
#include <vector>
#include <math.h>
#include <iostream>
#include <sstream>

class Coord {

	private :
		double x;
		double y;
		double z;
	
	public :
		Coord();
		Coord(double x, double y , double z);
		Coord(double x[]);
		//double* getCoord();
		double distXY(const Coord& other) const;
		double distX(const Coord& other) const;
		double distY(const Coord& other) const;
		double distZ(const Coord& other) const;
		double dist(const Coord& other) const;
		std::string toString() const;
		std::string display() const;
		//int findColorInStack(int dx, int dy, int dz, ImageStack st);
		double getX() const;
		void setX(double x);
		double getY() const;
		void setY(double y);
		double getZ() const;
		void setZ(double z);
        double* getVect() const;
		Coord getInt() const;
		Coord Interpolate(const double zref[], const double zfinal[]) const;
		Coord shift( const std::vector<std::vector< double > > depl) const;
		Coord shift(const double depl[]) const;
		Coord shift(const Coord& value) const;
	    Coord mult(double a);
		Coord diff(const Coord& value) const;
   	    Coord findDepl(const std::vector<Coord> pos,const std::vector<Coord > depl,double thrDist) const;
        void operator+=(const Coord &b);	
        void operator/=(const double b);
        Coord min(const Coord& value) const;
        Coord max(const Coord& value) const;
        bool isZero() const;

};
        Coord operator*(const double b, const Coord &c);	
        Coord operator-(const Coord &a, const Coord &b);	
        Coord operator+(const Coord &a, const Coord &b);        
        std::vector<Coord> operator-(const std::vector<Coord> va, const std::vector<Coord> vb);
        bool operator<(const Coord &a, const Coord &b);
        bool operator<=(const Coord &a, const Coord &b); 
        bool operator>(const Coord &a, const Coord &b); 
        bool operator>=(const Coord &a, const Coord &b);




#endif
