#ifndef DEF_MESH
#define DEF_MESH
#include "Classes.h"

#include <iostream>
#include <fstream>
#include <string>
#include <time.h>
#include "Utils.h"
#include "Coord.h"
#include <list>
#include "Cell.h"
#include "PSet.h"

class Mesh {
	
	private :
	
	std::string name;
	PSet<P>* points;
	std::list<Cell*>* cells;
	
	public :

	Mesh();
    ~Mesh();
    void read(std::string fileName, int scan);
    void read(std::string fileName);
    Cell* getCell(int num);
    PSet<P>* getPoints();
    bool isInCell(Coord pt, int number);
    int inWhichCell(Coord pt);
    void computeSafe(Mesh & meshref, PSet<P> ptsref, PSet<P> ptcur , double correlThr, double regulThrDist, double regulDist);
    void writeVTK(std::string name, Mesh &meshref0, Mesh &meshref);
    void writeCastem(std::string fullnameprefix, Mesh &meshref0);
    void writeCastemINP(std::string directory, string nameprefix, Mesh &meshref0);
};

#endif 
