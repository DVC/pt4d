#ifndef DEF_CORREL3DGPU
#define DEF_CORREL3DGPU


#ifdef __CUDACC__
    typedef float2 fComplex;
#else
    typedef struct{
        float x;
        float y;
    } fComplex;
#endif
#include "Param.h"

#include <iostream>
#include <string>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <cufft.h>
#include <cublas_v2.h>
#include <assert.h>
#include <tiffio.h>
#include "cuda_helper.h"
#include "Utils.h"
#include "Coord.h"
#include "Deformation.h"

#define FIXED 128

typedef float3 deformationType;




/*!
 * \file Correl3DGPU.h
 * \brief Correlation 3D
 * \author pierre.lhuissier@simap.grenoble-inp.fr
 * \version 0.1
 */

using namespace std;
    
/*! \class Correl3DGPU
* \brief class of the correlator
*
*  This class manage all the correlation processes
*/

class Correl3DGPU {

public :

    int dataD;/*!< Reference pattern depth*/
    int dataH; /*!< Reference pattern height*/
    int dataW; /*!< Reference pattern width*/
    int kernelD; /*!< Target pattern depth*/
    int kernelH; /*!< Target pattern height*/
    int kernelW; /*!< Target pattern width*/
    int fftD; /*!< FFT kernel depth*/
    int fftH; /*!< FFT kernel height*/
    int fftW; /*!< FFT kernel width*/
    int fullDataD; /*!< Reference full volume depth*/
    int fullDataH; /*!< Reference full volume height*/
    int fullDataW; /*!< Reference full volume width*/
    int fullKernelD; /*!< Target full volume depth*/ 
    int fullKernelH; /*!< Target full volume depth*/
    int fullKernelW; /*!< Target full volume depth*/
    int dZ; /*!< Maximal displacement threshold on z direction*/
    int dY; /*!< Maximal displacement threshold on y direction*/
    int dX; /*!< Maximal displacement threshold on x direction*/
    int domainSize; /*!< Correlation domain size*/
    int displacement; /*!< Maximal displacement*/
    int displacementX; /*!< Maximal displacementX*/
    int displacementY; /*!< Maximal displacementY*/
    int displacementZ; /*!< Maximal displacementZ*/
    int displX; /*!< Maximal displacementX*/
    int displY; /*!< Maximal displacementY*/
    int displZ; /*!< Maximal displacementZ*/
    int optimizeDisplacement; /*!< Maximal displacement*/
    int optimizeDisplacementX; /*!< Maximal displacementX*/
    int optimizeDisplacementY; /*!< Maximal displacementY*/
    int optimizeDisplacementZ; /*!< Maximal displacementZ*/
    int halfPattern; /*!< Radius of correlation domain*/
    int halfPatternX; /*!< X Radius of correlation domain*/
    int halfPatternY; /*!< Y Radius of correlation domain*/
    int halfPatternZ; /*!< Z Radius of correlation domain*/
    int cropHalfPatternX; /*!< X reduced Radius of correlation domain*/
    int cropHalfPatternY; /*!< Y reduced Radius of correlation domain*/
    int cropHalfPatternZ; /*!< Z reduced Radius of correlation domain*/
    int rotAxis;
    int nrot;
    float deltatheta;
    double correlThr; /*!<Correlation threshold*/
    float3 Xold;
    float3 Xnew;

 
    unsigned int data_size; /*!< Reference pattern number of voxels*/
    unsigned int kernel_size;  /*!< Target pattern number of voxels*/
    unsigned int fullData_size;  /*!< Reference full volume number of voxels*/
    unsigned int fullKernel_size;  /*!< Target full volume number of voxels*/
    unsigned int padded_size;  /*!< Padded kernel number of voxels*/
	
    unsigned int mem_fullData_byte; /*!< Reference full volume memory size*/
    unsigned int mem_fullKernel_byte; /*!< Target full volume memory size*/
    unsigned int mem_data_byte; /*!< Reference pattern memory size*/
    unsigned int mem_kernel_byte; /*!< Target pattern memory size*/
    unsigned int mem_padded_byte; /*!< Padded kernel memory size*/
    unsigned int mem_param_byte; /*!< Param memory size*/
    unsigned int mem_shared_byte; /*!< Shared memory size*/
    unsigned int mem_deformation_byte; /*!< Shared memory size*/
    unsigned int deviceTotalMem; /*!< Total allocated memory size*/	        
	
    unsigned char* h_FullData; /*!< Reference full volume (on host)*/
    unsigned char* h_FullKernel; /*!< Target full volume (on host)*/
    float* h_Data; /*!< Reference pattern (on host)*/
    float* h_Kernel; /*!< Target pattern (on host)*/
    float* h_max; /*!< Correlation index (on host)*/
    Parameters *h_Param; 
    deformationType *h_olddef; 
    deformationType *h_newdef; 
    deformationType *h_bestdef; 

    //cudaChannelFormatDesc channelDesc;
    //texture<float, 3, cudaReadModeNormalizedFloat> tex;
    //cudaArray* d_array;

    cudaArray* carrayoldtex;
    cudaChannelFormatDesc channeloldtex; 
    cudaExtent volumesizeoldtex;
    //cudaMemcpy3DParms copyparms={0};
    cudaArray* carraynewtex;
    cudaChannelFormatDesc channelnewtex; 
    cudaExtent volumesizenewtex;


    unsigned char* d_FullData; /*!< Reference full volume (on device)*/
    unsigned char* d_FullKernel; /*!< Reference full volume (on device)*/
    float* d_res; /*!< Correlation result (on device)*/
    float* d_Data; /*!< Reference pattern (on device)*/
    float* d_PaddedData; /*!< Reference padded kernel (on device)*/
    float* d_Kernel; /*!< Target pattern (on device)*/
    float* d_PaddedKernel; /*!< Target padded kernel (on device)*/
    Parameters* d_Param; /*!< Param (on device)*/
    deformationType *d_olddef;
    deformationType *d_newdef;


    fComplex* d_DataSpectrum; /*!< Reference FFT pattern (on device)*/
    fComplex* d_KernelSpectrum; /*!< Target FFT pattern (on device)*/

    cufftHandle fftVolumeFwd; /*!< Forward FFT handle*/
    cufftHandle fftVolumeInv; /*!< Invert FFT handle*/

    cudaDeviceProp deviceProp; /*!< Device properties*/
    int dev; /*!< Device number*/

    cublasHandle_t cbhandle;

private :

    bool normalize; /*!< Normalize*/
    bool fromFull; /*!< Put full volume on device (when multiple correlations)*/
    bool reducedWindow; /*!< Reduce the correlation windows with window*/
    std::string window; /*!< Window (Hanning, exponential ...)*/
    bool filters; /*!< Apply filter*/
    bool intersect; /*!< Account for reduced intersection*/
    bool displThr; /*!< Threshold on displacement*/
    bool deviceFree; /*!< Device is free*/
    bool deviceFullImFree; /*!< Device full volume is free*/
    bool hostFree; /*!< Host is free*/
    bool paramFree; /*!< param is free*/
    bool FFT; /*! Use FFT*/    
    bool aniso; /*! anisotropic pattern and window*/    
    bool rota;
    bool usetex;
    bool bindedtex;
    bool optimize;

public :

    float sumImRef; /*!< Sum of reference pattern*/
    float sumImTarget; /*!< Sum of target pattern*/
    
    	//Correl3DGPU();
    Correl3DGPU(const char * imRef, const char* imTarget);
    void finalize();
	
    void setParam(Parameters& param);
    void setOptimize(bool optimize);
    void setFilter(bool filt);
    void setIntersect(bool inter);
    void setWindow(std::string win);
    void setReducedWindow(bool red);
    void setNormalize(bool norm);
    void setusetex(bool usetex);
    bool getusetex();
    void setfromFull(bool useFull);
    void setdisplThr(bool displThr, int thrZ, int thrY, int thrX);
    void setDomain(int dom);
    void setFFT(bool fft);
    void setaniso(bool aniso);
    void setDisplacement(int depl);
    void setDisplacementX(int depl);
    void setDisplacementY(int depl);
    void setDisplacementZ(int depl);
    void setHalfPattern(int );
    void setHalfPatternX(int );
    void setHalfPatternY(int );
    void setHalfPatternZ(int );
    void setCropHalfPatternX(int );
    void setCropHalfPatternY(int );
    void setCropHalfPatternZ(int );
    void setCorrelThr(double);
    double getCorrelThr();
    void init();
    void freeFullImg();
    void free();
    std::string optionsToString();
    double* correl();
    double* correlRot();
    void setRef(float z, float y, float x);
    void setTarget(float z, float y, float x);
    void setFastImRef(int z, int y, int x);
    void setFastImTarget(int z, int y, int x);
    void setImRef(int z, int y, int x);
    void setImTarget(int z, int y, int x);
    void setImRef(char* img1);
    void setImTarget(char* img2);
    void setFullRef(const char* img1);
    void setFullTarget(const char* img2);
    void setFullRef(char* img1, int dimD, int dimH, int dimW);
    void setFullTarget(char* img2, int dimD, int dimH, int dimW);
    void setFullRef(const char* img1, int dimD, int dimH, int dimW);
    void setFullTarget(const char* img2, int dimD, int dimH, int dimW);
    void padKernel(float *d_Dst,float *d_Src,int fftD,int fftH,	int fftW,int kernelD,int kernelH,int kernelW,int kernelZ,int kernelY,int kernelX);
    void padDataClampToBorder(float *d_Dst,	float *d_Src,int fftD,int fftH,int fftW,int dataD,int dataH,int dataW,int kernelD,int kernelW,int kernelH,int kernelZ,int kernelY,int kernelX);
    void windowData(float *d_Dst,float *d_Src,float a,int fftD,	int fftH,int fftW);
    void windowKernel(float *d_Dst,	float *d_Src,float a,int fftD,int fftH,	int fftW);
    void rescaleData(float *d_Dst,float *d_Src,int fftD,int fftH,int fftW);
    void displacementThr(float *d_Dst,int fftD,int fftH,int fftW,int dZ,int dY,int dX);
    void filter(fComplex *d_Src,fComplex *d_Dst,int fftD,int fftH,int fftW);
    void readfromTIFF(const char* imagePath, unsigned char* pix);
    void writetoRAW(const char * image_path, float* pix, unsigned int imageWidth, unsigned int imageLength, unsigned int imageDepth );
    void writetoTIFF(const char * image_path, unsigned char* pix, unsigned int imageWidth, unsigned int imageLength, unsigned int imageDepth );
    void writetoTIFF(const char * image_path, float* pix, unsigned int imageWidth, unsigned int imageLength, unsigned int imageDepth );
    void readfromRAW(const char *name, unsigned char *m, const unsigned int dim_1, const unsigned int dim_2, const unsigned int dim_3);
    void setDimensions(const char* img1, int& fullDataW, int& fullDataH, int& fullDataD);
    void setRefDimensions(const char* img1);
    void setTargetDimensions(const char* img1);
    //void readfromImagePlus(char* imagePath, float* pix);
    void readfromImagePlus(char* imagePath, unsigned char* pix, int dimZ, int dimY, int dimX);
    void readfromImagePlus(const char* image_path, unsigned char* pix, int dimZ, int dimY, int dimX);
    void readfromFullIm_noFFT(float* d_Dst, unsigned char* d_Src, int halfpattern, int displacement, int dimZ,int dimY,int dimX,int z,int y,int x);
    void readfromFullIm_noFFT_aniso(float* d_Dst, unsigned char* d_Src, int halfpatternX, int halfpatternY, int halfpatternZ, int displacementX, int displacementY, int displacementZ, int dimZ,int dimY,int dimX,int z,int y,int x);
    void readfromFullIm(float* d_Dst,unsigned char* d_Src,int dst_dimZ,	int dst_dimY,int dst_dimX,int dimZ,	int dimY,int dimX,int z,int y,int x	);
    void readfromFullIm_tex(float* d_Dst, unsigned char* d_Src, int halfpatternX, int halfpatternY, int halfpatternZ, int displacementX, int displacementY, int displacementZ, int dimZ,int dimY,int dimX,int z,int y,int x);
    void readfromFullIm(float* d_Dst,unsigned char* d_Src,int dimZ,int dimY,int dimX,int z,int y,int x);
    void readfromFullImData(float* d_Dst,unsigned char* d_Src,int dimZ,	int dimY,int dimX,int z,int y,int x);
    void readfromFullImKernel(float* d_Dst,	unsigned char* d_Src,int dimZ,int dimY,int dimX,int z,int y,int x);
    float getValue(float* d_Src, int dimZ, int dimY, int dimX, int z, int y, int x);
    unsigned char getValue(unsigned char* d_Src, int dimZ, int dimY, int dimX, int z, int y, int x);
    void pixel_correl(float *d_res, float *d_Data, float *d_Kernel, int displacement, int halfPattern);
    void pixel_correl_aniso(float *d_res, float *d_Data, float *d_Kernel, int displacementX, int displacementY, int displacementZ, int halfPatternX, int halfpatternY, int halfpatternZ);
    void pixel_correl_aniso_crop(float *d_res, float *d_Data, float *d_Kernel, int displacementX, int displacementY, int displacementZ, int halfPatternX, int halfpatternY, int halfpatternZ, int cropHalfPatternX, int cropHalfPatternY, int cropHalfPatternZ);
    void pixel_correl_tex(float *d_res, float *d_Data, float *d_Kernel, int displacementX, int displacementY, int displacementZ, int halfPatternX, int halfpatternY, int halfpatternZ);
    void pixel_correl_aniso_rotZ(float *d_res, float *d_Data, float *d_Kernel, int displacementX, int displacementY, int displacementZ, int halfPatternX, int halfpatternY, int halfpatternZ, int nrot, float deltatheta);
    bool validTarget(Coord target);
    void singlepoint_texture_correl(float *d_res, float3 Xold, float3 Xnew, deformationType *olddef, deformationType *newdef, int halfPatternX, int halfpatternY, int halfpatternZ);
    double* correltexscreen();
    void texture_check_value(float *d_res, float3 Xold, deformationType *olddef, deformationType *newdef);
    void pixel_correl_texture_nodef(float *d_res, float3 Xold, float3 Xnew, int displacementX, int displacementY, int displacementZ, int halfPatternX, int halfpatternY, int halfpatternZ);
    double* correltex();
    void pixel_correl_texture_rotZ(float *d_res, float3 Xold, float3 Xnew, int displacementX, int displacementY, int displacementZ, int nrot, float deltatheta, int halfPatternX, int halfpatternY, int halfpatternZ);

};	
#endif

