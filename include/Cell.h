#ifndef DEF_CELL
#define DEF_CELL

#include <iostream>
#include <fstream>
#include <string>
#include <time.h>
#include "Utils.h"
#include "Coord.h"
#include <list>
#include "P.h"
#include "PSet.h"

class Cell {
	
	private :
	
	int number;
	int numberOfPoints;
	std::list<P*>* points;
    PSet<P>* particles;

	public :
	
	Cell();
	Cell(int number, std::list<P*> *points);
    Cell(int number, P* p1, P* p2, P* p3, P* p4, P* p5, P* p6, P* p7, P* p8);
    Cell(int number, P* p1, P* p2, P* p3, P* p4);
    ~Cell();
    P* getPoint(int i);
    void addPoint( P* p);
    Coord getMin();
    Coord getMax();
    std::list<Cell*> split();
    bool isInCell(Coord pt);
    string toINPString();

};
#endif
