macro(GET_SOURCES DIRS)
# ARGV : liste de tous les arguments de la macro
foreach(DIR ${ARGV})
file(GLOB FILES_LIST ${DIR}/*.cu ${DIR}/*.cpp )
list(APPEND SOURCES_FILES ${FILES_LIST})
file(GLOB FILES_LIST ${DIR}/*.h ${DIR}/*.cuh )
list(APPEND HEADER_FILES ${FILES_LIST})
endforeach()
endmacro()
