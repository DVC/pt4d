#ifndef DEF_ROI
#define DEF_ROI

#include <string>

class ROI {

	public :
		ROI();
		ROI(int x, int y, int z, int dimX, int dimY, int dimZ);

	private :
		int x;
		int y;
		int z;
		int dimX;
		int dimY;
		int dimZ;

	public :
		int getX();
		void setX(int x);
		int getY();
		void setY(int y);
		int getZ();
		void setZ(int z);
		int getdimX();
		void setdimX(int dimX);
		int getdimY();
		void setdimY(int dimY);
		int getdimZ();
		void setdimZ(int dimZ);
		ROI intersection(ROI roi);
		bool contains(ROI roi);
		ROI subROI(ROI roi);
};
#endif
