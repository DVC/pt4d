#ifndef DEF_PARTICLESSET
#define DEF_PARTICLESSET

#include <iostream>
#include <fstream>
#include <string>
#include <time.h>
#include "Utils.h"
#include "Coord.h"
#include <vector>
#include <list>
#include <tiffio.h>
#include "PointsSet.h"
#include "Particle.h"

class ParticlesSet{
	
	private :
	
	std::string name;
	int StackdimX;
	int StackdimY;
	int StackdimZ;
	std::list<Particle*>* particles;
	
	public :
	
	ParticlesSet();
	ParticlesSet(std::string name);
	ParticlesSet(std::string name, std::list<Particle*> *particles);
	//ParticlesSet(std::string name, int StackdimX, int StackdimY, int StackdimZ);
	//ParticlesSet(std::string name, int StackdimX, int StackdimY, int StackdimZ, std::list<Particle*> *particles);
	ParticlesSet(const ParticlesSet& particlesSet);
	ParticlesSet(const std::string fileDirectory, const std::string particlesSetName);
    ParticlesSet(const std::string fileDirectory, const std::string particlesSetName,const std::vector<ParticlesSet>& raw);
	//ParticlesSet(std::string name, int StackdimX, int StackdimY, int StackdimZ, const std::vector<Particle*>& raw);
	ParticlesSet(std::string name, const std::vector<Particle*>& raw);
	~ParticlesSet();

	std::string getName() const;
	void setName(std::string name);
	std::list<Particle*>* getParticles() const;
	//std::list<Particle*>* getParticlesRef() const;
	void setParticles(std::list<Particle*> *particles);
	Particle* getParticle(int i);
	std::list<Particle*>::iterator getIteratorBegin();
	std::list<Particle*>::iterator getIteratorEnd();
	//void setParticle(int i, Particle* particle);
	void addParticle(Particle* particle);
	int Size() const;
	void removeParticle(Particle* part);
	void removeParticle(int i);
	ParticlesSet subSet(const Coord& coord, double distXY, double distZ);
	ParticlesSet subSetVol(double vMin);
//	std::vector<ParticlesSet> findBestCorrel(ParticlesSet pfinal, double refSlices[], double refSlicesFinal[], double distXY, double distZ);
//	std::vector<ParticlesSet> findBestCorrel_noDIC(ParticlesSet pfinal, double* depl, double distXY, double distZ);
//	std::vector<ParticlesSet> findBestCorrel_larger_noDIC(ParticlesSet pfinal, double* depl, double distXY, double distZ);
//	std::vector<ParticlesSet> findBestCorrel_smaller_noDIC(ParticlesSet pfinal, double* depl, double distXY, double distZ);
	std::vector<ParticlesSet> findBestCorrel_DIC(ParticlesSet pfinal, double* depl, double distXY, double distZ);
	std::vector<ParticlesSet> findBestCorrel_DICGPU_old(ParticlesSet pfinal, double* depl, double distXY, double distZ);
	std::vector<ParticlesSet> findBestCorrel_DICGPU(ParticlesSet &pfinal, std::vector<Coord> refPoints, std::vector<Coord> refDepl, double distXY, double distZ);
	std::vector<ParticlesSet> findBestCorrel_DICGPU(ParticlesSet &pfinal, PointsSet refPoints, PointsSet finalPoints, double distXY, double distZ);
		//std::vector<ParticlesSet> findBestCorrel_DICGPU(ParticlesSet& pfinal, std::vector<std::vector<double> > depl, double distXY, double distZ);
	std::vector<ParticlesSet> findBestCorrel_noDIC(ParticlesSet &pfinal, PointsSet refPoints, PointsSet finalPoints, double distXY, double distZ);
	std::vector<ParticlesSet> findBestCorrel_noDIC(ParticlesSet &pfinal, std::vector<Coord> refPoints, std::vector<Coord> refDepl, double distXY, double distZ);
	std::vector<ParticlesSet> findBestCorrel_noDIC(ParticlesSet& pfinal, std::vector<std::vector<double> > depl, double distXY, double distZ);
	std::vector<ParticlesSet> checkCorrel(std::vector<ParticlesSet> potential, ParticlesSet pfinal, std::vector<ParticlesSet> potentialInv);
	void debugCorrel(std::vector<ParticlesSet> & potential, ParticlesSet & pfinal, std::vector<ParticlesSet> & potentialInv);
	std::vector<ParticlesSet> matchCorrel(std::vector<ParticlesSet> & potential, ParticlesSet & pfinal, std::vector<ParticlesSet> & potentialInv);
	std::vector<ParticlesSet> checkCorrel_DIC(std::vector<ParticlesSet> potential, ParticlesSet pfinal, std::vector<ParticlesSet> potentialInv);
	//vector<ParticlesSet> checkCorrel_DIC_multi(final vector<ParticlesSet> potential, final ParticlesSet pfinal, final vector<ParticlesSet> potentialInv);
	ParticlesSet closest(ParticlesSet pfinal, double distXY, double distZ);
	ParticlesSet findLost(ParticlesSet pool);
	bool contains(const Particle& part);
	int getPos(const Particle& part);
	int setGlobalNumbers(int current);
	void matchGlobalNumberIntra(ParticlesSet partSet);
	int matchGlobalNumberInter(ParticlesSet partSet, int current);
	int globalNumberFromCheckedRef(std::vector<ParticlesSet> VpS, int current);
	int globalNumberFromCheckedRef(std::vector<ParticlesSet> & VpS, int current, int scan);
	void globalNumberFromCheckedFinal(std::vector<ParticlesSet> VpS, ParticlesSet ref);
	int globalNumberFromCheckedFinal(std::vector<ParticlesSet> & VpS, ParticlesSet & ref, int current, int scan);
	void findNullFromImage(ParticlesSet poolr, std::string ImfName, double thrXY,double thrZ);
	std::vector<ParticlesSet> findCorrel(ParticlesSet pfinal, double refSlices[], double refSlicesFinal[]);
	ParticlesSet rankByVolume();
	Particle* findClosest(const Coord& XYZ) ;
	Particle* findParticle(int color) ;
	Particle* findParticle(int color) const;
	Particle* findGlobalColorParticle(int color) ;
	std::string fullInfoTostring() ;
	std::string colorTostring() ;
	std::string colorGlobalTostring() ;
	void colorToFile(const std::string fileName) ;
	void writeToFile(const std::string fileName) ;
	void writeToFile(const std::string fileDirectory, const std::string particlesSetName) const;
	void write(const std::string fileName) const;
	std::string volumeTostring() ;
	std::string sphericityTostring() ;
	std::string shapeFactTostring() ;
	std::string shapeFact2Tostring() ;
	std::string zAngleTostring() ;
	std::string PosTostring();
	int getStackdimX() const;
	void setStackdimX(int stackdimX);
	int getStackdimY() const;
	void setStackdimY(int stackdimY);
	int getStackdimZ() const;
	void setStackdimZ(int stackdimZ);
	ParticlesSet & operator = (const ParticlesSet &assign);
	void setDimensions();
		
};
#endif


