#ifndef DEF_POSTPROC
#define DEF_POSTPROC
#include "Classes.h"

#include <iostream>
#include <string>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>

#include <cufft.h>
#include <cublas_v2.h>

#include <assert.h>

#include <tiffio.h>
#include "Utils.h"
#include "Coord.h"
#include "Param.h"

#include "PSet.h"
#include "P.h"
#include "Particle.h"

#include "helper_cuda.h"



/*!
 * \file PostProc.h
 * \brief Post Processing
 * \author pierre.lhuissier@simap.grenoble-inp.fr
 * \version 0.1
 */


    
/*! \class PostProc
* \brief class of the post processor
*
*  This class manage all the post processing
*/

class PostProc {

public :

    unsigned long fullDataD; /*!< Reference full volume depth*/
    unsigned long fullDataH; /*!< Reference full volume height*/
    unsigned long fullDataW; /*!< Reference full volume width*/
	unsigned long fullDatasubD;
    float correlThr;
    float regulDist;
    float regulDistThr;

    unsigned long mem_fullData_byte; /*!< Reference full volume memory size*/
	unsigned long fullData_size; /*!< Reference full volume memory size*/

	float* h_Depl; /*!< Displacement Field volume */
    float* h_ezz;
    float* h_Points;
	unsigned char* h_Mask; /*!< Mask for matter*/
	std::string imMask; /*!< Mask for matter*/
    PSet<P> ptsInit;
    PSet<P> ptsFinal;

    Parameters *h_Param; 

    float* d_Depl;
    float* d_Coeff;
    float* d_Points;
    unsigned long nbPoints;


    unsigned long deviceTotalMem;
    bool hostFree;
    bool deviceFree;
    int dev;
    cudaDeviceProp deviceProp; /*!< Device properties*/

public :

    PostProc(Parameters* paramglob, const std::string mask, PSet<P> &ptsI, PSet<P> &ptsF);

    void setPointsInit(PSet<P> &pts);
    void setPointsFinal(PSet<P> &pts);
    void setPoints(PSet<P> &ptsInit, PSet<P> &ptsFinal);
    void setPoints();
    void finalize();
    void setDimensions(const char* img1, unsigned long& fullDataW, unsigned long& fullDataH, unsigned long& fullDataD);
    void setDimensions(const char* img1);
    void init();
    //void init(const char* imMask);
    void free();
    void calcDeplField();
    void checkDeplField();
    void applyMask();
    void checkValue(float *d_Data, unsigned long index);
    void writeDeplField(const std::string image_path);
    void writeDeplField(const std::string prefix, const std::string suffix);
    void writeDeplField(const std::string prefix, const std::string suffix, int dim);
    void writeEpsField(const std::string image_path);

    void readfromRAW(const char *name, unsigned char *m, const unsigned long dim_1, const unsigned long dim_2, const unsigned long dim_3);
    void readfromTIFF(const char * image_path, unsigned char* pix);
    void writetoRAW(const std::string prefix, const std::string suffix, float* pix, unsigned long imageWidth, unsigned long imageLength, unsigned long imageDepth, unsigned long channel );
    void writetoRAW(const char * image_path, float* pix, unsigned long imageWidth, unsigned long imageLength, unsigned long imageDepth );
    void writetoRAW(const char * image_path, float* pix, unsigned long imageWidth, unsigned long imageLength, unsigned long imageDepth , unsigned long channel);
    void writetoRAW(const string prefix, const std::string suffix, float* pix, unsigned long imageWidth, unsigned long imageLength, unsigned long imageDepth, unsigned long channel, int dim );
    void writetoTIFF(const char * image_path, unsigned char* pix, unsigned long imageWidth, unsigned long imageLength, unsigned long imageDepth );
    void writetoTIFF(const char * image_path, float* pix, unsigned long imageWidth, unsigned long imageLength, unsigned long imageDepth, unsigned long channel );
    void writetoTIFF(const char * image_path, float* pix, unsigned long imageWidth, unsigned long imageLength, unsigned long imageDepth );
    void readfromImagePlus(char* image_path, unsigned char* pix, unsigned long dimZ, unsigned long dimY, unsigned long dimX);
    void readfromImagePlus(const char* image_path, unsigned char* pix, unsigned long dimZ, unsigned long dimY, unsigned long dimX);
    void calcDeplField(float *d_Depl, float *d_Coeff, float *d_Points, float regulDistThr, float regulDist, unsigned long fullDataD, unsigned long fullDataH, unsigned long fullDataW, unsigned long fullDatasubD, unsigned long nbPoints);
};	

#endif

