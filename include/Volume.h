#ifndef DEF_VOLUME
#define DEF_VOLUME

#include <vector>
#include "ROI.h"
#include "Coord.h"
#include <iostream>
#include <fstream>
#include <tiffio.h>

class Volume {

	public :
		Volume();
		Volume(std::string name);
		Volume(std::string name, Coord fileDim);
		Volume(std::string name, Coord fileDim, ROI fileROI);

	private :
		std::string name;
		Coord fileDim;
		ROI fileROI;
		double* data;
		ROI roi;

	public :
		Coord getFileDim() const;
		void setFileDim(Coord fileDim);
		ROI getFileRoi() const;
		void setFileRoi(ROI fileRoi);
		std::string getName() const;
		void setName(std::string name);
		ROI getRoi() const;
		void setRoi(ROI roi);
		const double* getData() const;
		void setData(double* data);
		void setData();

		int getdimX();
		int getdimY();
		int getdimZ();
		void setDimensions();
		ROI getROI();
		void setROI(ROI roi);
		Volume duplicateRoi();
		void recolor(int new_color[]);
		int vol();


};
#endif
