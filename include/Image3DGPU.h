#ifndef DEF_IMAGE3DGPU
#define DEF_IMAGE3DGPU


#include "Param.h"

#include <iostream>
#include <string>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <cufft.h>
#include <cublas_v2.h>
#include <assert.h>
#include <tiffio.h>
#include "cuda_helper.h"
#include "Utils.h"
#include "Coord.h"
#include "Deformation.h"

#define FIXED 128
typedef int imtype;


/*!
 * \file Image3DGPU.h
 * \brief Image 3D
 * \author pierre.lhuissier@simap.grenoble-inp.fr
 * \version 0.1
 */

using namespace std;
    
/*! \class Image3DGPU
* \brief class of the Image 3D
*
*  This class manage Image3D
*/

class Image3DGPU {

public :

    int fullDataD; /*!< Reference full volume depth*/
    int fullDataH; /*!< Reference full volume height*/
    int fullDataW; /*!< Reference full volume width*/
    int displacement; /*!< Maximal displacement*/
    int displacementX; /*!< Maximal displacementX*/
    int displacementY; /*!< Maximal displacementY*/
    int displacementZ; /*!< Maximal displacementZ*/
     
    unsigned int data_size; /*!< Reference pattern number of voxels*/
    unsigned int fullData_size;  /*!< Reference full volume number of voxels*/
	
	unsigned int mem_fullData_byte; /*!< Reference full volume memory size*/
	unsigned int mem_data_byte; /*!< Reference pattern memory size*/
	unsigned int mem_param_byte; /*!< Param memory size*/
	unsigned int mem_shared_byte; /*!< Shared memory size*/
    unsigned int deviceTotalMem; /*!< Total allocated memory size*/	        
	
	imtype* h_FullData; /*!< Reference full volume (on host)*/
	imtype* h_Data; /*!< Reference pattern (on host)*/
	int* h_max; /*!< Correlation index (on host)*/
	float* v_max; /*!< Correlation index (on host)*/
    Parameters *h_Param; 

    imtype* d_FullData; /*!< Reference full volume (on device)*/
	float* d_res; /*!< Correlation result (on device)*/
	imtype* d_Data; /*!< Reference pattern (on device)*/
	Parameters* d_Param; /*!< Param (on device)*/

    cudaDeviceProp deviceProp; /*!< Device properties*/
    int dev; /*!< Device number*/

    cublasHandle_t cbhandle;

private :

    bool deviceFree; /*!< Device is free*/
   	bool deviceFullImFree; /*!< Device full volume is free*/
    bool hostFree; /*!< Host is free*/
    bool paramFree; /*!< param is free*/

public :

    Image3DGPU(const char * im);
    void finalize();
	
	void setParam(Parameters& param);
	void setDisplacement(int depl);
	void setDisplacementX(int depl);
	void setDisplacementY(int depl);
	void setDisplacementZ(int depl);
	void init();
	void freeFullImg();
	void free();
	std::string optionsToString();
	void setIm(char* img1);
	void setFullIm(const char* img1);
	void setFullIm(char* img1, int dimD, int dimH, int dimW);
	void setFullIm(const char* img1, int dimD, int dimH, int dimW);
	void readfromTIFF(const char* imagePath, imtype* pix);
    void writetoRAW(const char * image_path, float* pix, unsigned int imageWidth, unsigned int imageLength, unsigned int imageDepth );
	void writetoTIFF(const char * image_path, imtype* pix, unsigned int imageWidth, unsigned int imageLength, unsigned int imageDepth );
	void writetoTIFF(const char * image_path, float* pix, unsigned int imageWidth, unsigned int imageLength, unsigned int imageDepth );
	void readfromRAW(const char *name, imtype *m, const unsigned int dim_1, const unsigned int dim_2, const unsigned int dim_3);
	void setDimensions(const char* img1, int& fullDataW, int& fullDataH, int& fullDataD);
	void setDimensions(const char* img1);
	void readfromImagePlus(char* imagePath, imtype* pix, int dimZ, int dimY, int dimX);
	void readfromImagePlus(const char* image_path, imtype* pix, int dimZ, int dimY, int dimX);
    float getValue(float* d_Src, int dimZ, int dimY, int dimX, int z, int y, int x);
    imtype getValue(imtype* d_Src, int dimZ, int dimY, int dimX, int z, int y, int x);

    int label_dist(Coord X);
    
   };	
#endif

