#ifndef CLASSES_H
#define CLASSES_H

class Cell;
class Coord;
class Correl3DGPU;
class Mesh;
class P;
struct Parameters;
class Particle;
//class Point;
class PostProc;
template < typename T> class PSet;
class ROI;
template < typename Ptr, typename Pg> class TimeSerie;
template < typename Ptr, typename Pg> class TS;
class Volume;

#endif
