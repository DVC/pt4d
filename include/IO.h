
#ifndef IO_H
#define IO_H
#include "Classes.h"

#include <iostream>
#include <fstream>
#include <string>
//#include <filesystem>
#include <time.h>
#include "Utils.h"
#include "Coord.h"
#include <list>
#include <vector>
#include "Mesh.h"
#include "PSet.h"
#include "Particle.h"
#include "P.h"
	

//void checkAndCreateDir(std::string Directory);


std::vector<P*> readCorrelManuPtsFile(std::string fileName, int scan);
std::vector<P*> readPtsFile(std::string fileName, int scan);
std::vector<Particle*> readParamFile(std::string fileName, int scan);
std::vector<Particle*> readParamFileConstrained(std::string fileName, int scan, int nbpartcons);
std::vector<std::vector<double> > readFile(std::string filePath);
std::vector<P*> readPFromParamFile(std::string fileName, int scan);

template < typename T > void writeVectorPSet(std::string dir, std::string VectorPSetName, std::vector<PSet<T> > & vector);
//void writeVectorPSet(std::string dir, std::string VectorPSetName, std::vector<PSet<Point> > & vector);
void writePSetGlobal(std::string FileName, std::vector<PSet<Particle> > vector);
void writePSetGlobal(std::string FileName, std::vector<PSet<P> > vector);
template < typename T > void writePointsFiles(std::string FileNamePrefix, std::string FileNameSuffix, std::vector<PSet<T> > vector, bool append);
template < typename T > void readVectorPSet(std::string dir, std::string VectorPSetName, std::vector<PSet<T> > & particles, std::vector<std::vector<T*> > & particlesData);

void writePSetGlobalTranspose(std::string FileName, std::vector<PSet<P> > vector);
void writePSetGlobalTranspose(std::string FileName, std::vector<PSet<Particle> > vector);
int writePSetGlobalTransposeNucleateAndMerge(std::string FileName, std::vector<PSet<Particle> > vector);
void writePSetForMeshDef(std::vector<std::string> FileName, std::vector<PSet<Particle> > vector);


/**
void writeVectorPointsSet(std::string Directory, std::string VectorPointSetName, std::vector<PSet < Point > > & vector);
std::vector<PSet<Point> > readVectorPointsSet(std::string Directory, std::string VectorPointSetName, std::vector<PSet<Point> > & raw);
std::vector<PSet<Point> > readDataPointsSet(std::string Directory, std::string VectorPointSetName);

std::vector<Point*> readMeshPtsFile(std::string fileName, int scan, int& dimx, int& dimy, int& dimz);
*/
void writeFile(std::string filePath, int* values, int dimi, int dimj);
//void writeFile(std::string filePath, int values[], int dim);
/*
 void writevectorPointsSet(std::string FileName, std::vector<PSet<Point> > vector);
void writevectorPointsSetGlobal(std::string FileName, std::vector<PSet<Point> > vector);
void writePointsFiles(std::string FileNamePrefix, std::string FileNameSuffix, std::vector<PSet<Point> > vector, bool append);


void writeVectorParticlesSet(std::string Directory, std::string VectorParticleSetName, std::vector<PSet<Particle> > & vector);
void readVectorParticlesSet(std::string Directory, std::string VectorParticleSetName, std::vector<PSet<Particle> > & raw, std::vector<PSet<Particle> > & v);
void readDataParticlesSet(std::string Directory, std::string VectorParticleSetName, std::vector<PSet<Particle> > & v);

void writevectorParticlesSet(std::string FileName, std::vector<PSet<Particle> > vector);
void writevectorParticlesSetGlobal(std::string FileName, std::vector<PSet<Particle> > vector);
std::vector<Coord> readPtsFile(std::string filePath);
*/



#endif

