import numpy as np
import os
from scipy.interpolate import UnivariateSpline
from matplotlib import pyplot as plt

def readptsfromHRseries(prefix, HR, states):
    """
    Read pts files from HR series

    Parameters
    ----------

        prefix : string
                Name of file prefix
        HR : list of int
                Numbers of the HR scans
        states : list of int
                Numbers of the states

    Returns
    -------

    """
    #------------------------------------------------------------
    deltazHR = 900
    
    rstateu = []
    rstatep = []
    
    for istate in states:
        for iHR in HR:
            pts1 = np.loadtxt( "output_HR%d_state%d/%s_HR%d_%03d.pts" %(iHR, istate, prefix, iHR, istate), skiprows=1)
            pts2 = np.loadtxt( "output_HR%d_state%d/%s_HR%d_%03d.pts" %(iHR, istate, prefix, iHR, istate + 1), skiprows=1)
            u = pts2[:,2:5] - pts1[:,2:5]
            u[:,1:] = pts1[:,3:5] - pts2[:,3:5]
            pts = pts1[:,2:5]
            pts[:,1:] *=-1
            pts[:,2] += (iHR-1)*deltazHR
            
            rstatep.append(pts)
            rstateu.append(u)
    return (rstatep, rstateu)

def generateVirtualStates(rstatep, rstateu, HR, states):
    """
    Generate virtual states from pts files of HR series

    Parameters
    ----------

        rstatep : list of array of pts 
                Real states points lists
        rstateu : list of array of displacements 
                Real states displacements lists
        HR : list of int
                Numbers of the HR scans
        states : list of int
                Numbers of the states

    Returns
    -------

    """
    #------------------------------------------------------------
    vstatev = []
    vstatep = []
    
    for istate in states:
        for iHR in HR:
            pts = rstatep[(istate-1)*len(HR)+iHR-1]
            u = rstateu[(istate-1)*len(HR)+iHR-1]
            npt, nv = interpolateState(pts,u,0)
            vstatep.append(npt)
            vstatev.append(nv)

    for istate in states:
        start = 0
        if(istate == 1):
            start=-len(HR)
        for iHR in HR:
            pts = rstatep[(istate-1)*len(HR)+iHR-1]
            u = rstateu[(istate-1)*len(HR)+iHR-1]
            for dHR in np.arange(start, len(HR)+1):
                if (dHR != 0) and ((istate-1)*len(HR)+iHR-1+dHR >= 0) and ((istate-1)*len(HR)+iHR-1+dHR < len(states)*len(HR)):
                    npt, nv = interpolateState(pts,u,dHR)
                    vstatep[(istate-1)*len(HR)+iHR-1+dHR] = np.append(vstatep[(istate-1)*len(HR)+iHR-1+dHR], npt, axis=0)
                    vstatev[(istate-1)*len(HR)+iHR-1+dHR] = np.append(vstatev[(istate-1)*len(HR)+iHR-1+dHR], nv, axis=0)
    return (vstatep, vstatev)

def interpolateState(pts, u, dHR, deltat=1):
    """
    Interpolate (or even extrapolate) pts and u for a virtual state (other HR time)

    Parameters
    ----------

        pts : array of pts for dHR = 0 
                Real state points
        u : array of displacements from dHR=0 to dHR=4 (same HR acquisition, next state)
                Real state displacements
        dHR : int
                HR spacing (can be negative or positive, 4 is same HR next state)

    Returns
    -------
        newpts :
            list of virtual pts
        v:
            list of virtual 
    """
    newpts = pts + dHR*0.25*u
    newv = u/deltat
    return (newpts, newv)

def pts2vtk(pts, v, name):
    """
    Generate vtk file format from pts and v

    Parameters
    ----------

        pts : array
            Array of points

        v : array
            Array of velocities

        name : string
                Name of output file
        
    Returns
    -------

    """
    #------------------------------------------------------------
    f=open(name,'w')
    # vtk DataFile Version 3.1
    f.write('# vtk DataFile Version 3.1\n')
    f.write('Virtual points\n')
    f.write('ASCII\n')
    f.write('DATASET UNSTRUCTURED_GRID\n')
    f.write('POINTS %d FLOAT\n' %(pts.shape[0]))
    for i in np.arange(0,pts.shape[0]):
        f.write('%f %f %f\n' %(pts[i,0],pts[i,1],pts[i,2]))
    
    f.write('\n')
    f.write('POINT_DATA %d\n' %(pts.shape[0]))
    f.write('SCALARS useless FLOAT 1\n')
    f.write('LOOKUP_TABLE default\n')
    for i in np.arange(0,pts.shape[0]):
        f.write('1\n')
    f.write('VECTORS Velocity FLOAT\n')
    for i in np.arange(0,pts.shape[0]):
        f.write('%f %f %f\n' %(v[i,0],v[i,1],v[i,2]))
    f.write('\n')
    f.close()

def writeVirtualStates(vp, vv, prefix):
    for i in np.arange(0,len(vp)):
        pts2vtk(vp[i], vv[i], "%s_%03d.vtu" %(prefix,i))

def fitAndPlotVirtualStates(vp, vv, showv=False, showd=False):
    x=[]
    xs = np.linspace(0,3500,3501)
    x.append(xs)
    n=0
    for i in np.arange(0,len(vp),4):
        p=vp[i][:,2]
        v=vv[i][:,2]
        d=np.zeros((p.shape[0],2))
        d[:,0] = p + 0.01 * np.random.randn(p.shape[0])
        d[:,1] = v
        r=d[d[:,0].argsort()]
        spl = UnivariateSpline(r[:,0], r[:,1], s=50*p.shape[0])
        if showv:
            plt.figure()
            plt.plot(xs, spl(xs), 'g', lw=3)
            plt.plot(p,v, 'ro', ms=5)
            plt.show()
        x.append(x[n]+spl(x[n]))
        n=n+1
        if showd:
            spld = spl.derivative(n=1)
            plt.figure()
            plt.plot(xs, spld(xs), 'g', lw=3)
            plt.show()
    plt.figure()
    for i in np.arange(0,len(x)):
        spl = UnivariateSpline(x[0], x[i]-x[0], s=x[0].shape[0])
        plt.plot(x[0],x[i]-x[0])
        plt.plot(x[0],spl(x[0]))
    plt.show()

    plt.figure()
    for i in np.arange(0,len(x)):
        spl = UnivariateSpline(x[0], x[i]-x[0], s=x[0].shape[0])
        spld = spl.derivative(n=1)
        plt.plot(x[0], spld(x[0]))
    plt.show()
    return x

def writeDispAndStrain(x):
    disp=np.zeros((x[0].shape[0],len(x)+1))
    strain=np.zeros((x[0].shape[0],len(x)+1))
    print(x[0].shape)
    print(disp.shape)
    disp[:,0]=x[0]
    strain[:,0]=x[0]
    for i in np.arange(0,len(x)):
        spl = UnivariateSpline(x[0], x[i]-x[0], s=x[0].shape[0])
        disp[:,i+1]=spl(x[0])
        spld = spl.derivative(n=1)
        strain[:,i+1]=spld(x[0])
    np.savetxt('virtual/displacements.txt',disp)
    np.savetxt('virtual/strain.txt',strain)

def run_AZ31_X():
    prefix="AZ31_X"
    resdir="virtual/"
    HR = [1, 2, 3, 4]
    states = [1, 2, 3, 4, 5, 6, 7]
    rp,ru = readptsfromHRseries(prefix, HR, states)
    vp, vv = generateVirtualStates(rp, ru, HR, states)
    writeVirtualStates(vp, vv, "%s%s_" %(resdir, prefix))
    r=fitAndPlotVirtualStates(vp, vv, showv=True, showd=False)
    writeDispAndStrain(r) 


