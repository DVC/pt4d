import numpy as np
import os

def cylinder_sample_double(Nlines, SlicesFile, outSlicesFile, pointsFile, outputprefix):
    """
    Crop points file and generate mesh file for Tracking3D

    Parameters
    ----------

        Nlines : int
            Number of points per line (odd value)

        SlicesFile : string
                Name of file containing parameters
                Table: Zslice Xcenter Ycenter Radius

        PointsFile : string
                Name of file containing points (correlManu3D format)
                Table : header nb_points nb_points 1 1
                        point_index 1 X -Y -Z

    Returns
    -------

    """
    #------------------------------------------------------------
    #Open files
    top_bot_slices = np.loadtxt( SlicesFile, skiprows=1)
    outslices = np.loadtxt( outSlicesFile, skiprows=1)

    slices = np.zeros((Nlines,4))
    for i in range(Nlines):
        slices[i,0]=top_bot_slices[0,0]+i*((top_bot_slices[1,0]-top_bot_slices[0,0])/(Nlines-1))
        slices[i,1]=top_bot_slices[0,1]+i*((top_bot_slices[1,1]-top_bot_slices[0,1])/(Nlines-1))
        slices[i,2]=top_bot_slices[0,2]+i*((top_bot_slices[1,2]-top_bot_slices[0,2])/(Nlines-1))
        slices[i,3]=top_bot_slices[0,3]+i*((top_bot_slices[1,3]-top_bot_slices[0,3])/(Nlines-1))

    Nrad = (Nlines-1)/2
    point=np.zeros((Nlines*Nlines*slices.shape[0],5))
    ptvtk=np.zeros((Nlines*Nlines*slices.shape[0],3))
    nb=0
    for k in np.arange(0,slices.shape[0]):
        z=slices[k,0]
        xc=slices[k,1]
        yc=slices[k,2]
        R=slices[k,3]
        for j in np.arange(-Nrad,Nrad+1,1):
            for i in np.arange(-Nrad,Nrad+1,1):
                if(i*j!=0):
                    theta=np.arctan(abs(1.*j/i))
                    alpha=1.*R/(Nrad*min(1./np.cos(theta),1./np.sin(theta)))
                else:
                    alpha=1.*R/Nrad
                x=int(xc+i*alpha)
                y=int(yc+j*alpha)
                point[nb,0]=nb+1
                point[nb,1]=0
                point[nb,2]=x
                point[nb,3]=-y
                point[nb,4]=-z
                ptvtk[nb,0]=x
                ptvtk[nb,1]=y
                ptvtk[nb,2]=z
                nb=nb+1

    hd="%d %d %d %d" %(nb,Nlines,Nlines,slices.shape[0])
    np.savetxt("%s_mesh_001.pts" %(outputprefix),point,fmt='%d',header=hd,comments="")
    
    hdvtk="vtk DataFile Version 3.4\nvtk output\nASCII\nDATASET STRUCTURED GRID\nDIMENSIONS %d %d %d\nPOINTS %d float" %(Nlines,Nlines,slices.shape[0],nb)
    np.savetxt("%s_mesh_001.vtk" %(outputprefix),ptvtk,fmt='%d',header=hdvtk,comments="")

    points_in = np.loadtxt( pointsFile, skiprows=1)
    #sliceinfo = np.zeros((slice[-1,0]-slice[0,0]+1,4))
    sliceinfo = {}
    j=1
    for k in np.arange(0,outslices[-1,0]-outslices[0,0]):
        sli = np.zeros((3,))
        key = outslices[0,0]+k;
        while k>outslices[j,0]:
            j=j+1  
        for i in np.arange(1,4):
            #sli[i-1]=((k-outslices[j-1,0])*outslices[j-1,i] + (outslices[j,0]-k)*outslices[j,i])/(outslices[j,0]-outslices[j-1,0]);
            sli[i-1]=((key-outslices[j-1,0])*outslices[j,i] + (outslices[j,0]-key)*outslices[j-1,i])/(outslices[j,0]-outslices[j-1,0]);
        sliceinfo[key]=sli
    points_out = []
    nb=1
    for k in np.arange(0,points_in.shape[0]):
        if (-points_in[k,4]>slices[0,0]) and (-points_in[k,4]<slices[-1,0]):
            if((points_in[k,2]-sliceinfo[-points_in[k,4]][0])**2+(points_in[k,3]+sliceinfo[-points_in[k,4]][1])**2<= sliceinfo[-points_in[k,4]][2]**2):   
                points_in[k,0]=nb
                nb = nb + 1
                points_out.append(points_in[k])
    hd="%d %d %d %d" %(len(points_out),len(points_out),1,1)
    np.savetxt("%s_001.pts" %(outputprefix),points_out,fmt='%d',header=hd)



def cylinder_sample_double_v2(Nlines, SlicesFile, outSlicesFile, pointsFile, outputprefix):
    """
    Crop points file and generate mesh file for Tracking3D

    Parameters
    ----------

        Nlines : int
            Number of points per line (odd value)

        SlicesFile : string
                Name of file containing parameters
                Table: Zslice Xcenter Ycenter Radius

        PointsFile : string
                Name of file containing points (correlManu3D format)
                Table : header nb_points nb_points 1 1
                        point_index 1 X -Y -Z

    Returns
    -------

    """
    #------------------------------------------------------------
    #Open files
    top_bot_slices = np.loadtxt( SlicesFile, skiprows=1)
    outslices = np.loadtxt( outSlicesFile, skiprows=1)

    slices = np.zeros((Nlines,4))
    for i in range(Nlines):
        slices[i,0]=top_bot_slices[0,0]+i*((top_bot_slices[1,0]-top_bot_slices[0,0])/(Nlines-1))
        slices[i,1]=top_bot_slices[0,1]+i*((top_bot_slices[1,1]-top_bot_slices[0,1])/(Nlines-1))
        slices[i,2]=top_bot_slices[0,2]+i*((top_bot_slices[1,2]-top_bot_slices[0,2])/(Nlines-1))
        slices[i,3]=top_bot_slices[0,3]+i*((top_bot_slices[1,3]-top_bot_slices[0,3])/(Nlines-1))

    Nrad = (Nlines-1)/2
    point=np.zeros((Nlines*Nlines*slices.shape[0],5))
    ptvtk=np.zeros((Nlines*Nlines*slices.shape[0],3))
    nb=0
    for k in np.arange(0,slices.shape[0]):
        z=slices[k,0]
        xc=slices[k,1]
        yc=slices[k,2]
        R=slices[k,3]
        for j in np.arange(-Nrad,Nrad+1,1):
            for i in np.arange(-Nrad,Nrad+1,1):
                if(i*j!=0):
                    theta=np.arctan(abs(1.*j/i))
                    alpha=1.*R/(Nrad*min(1./np.cos(theta),1./np.sin(theta)))
                else:
                    alpha=1.*R/Nrad
                x=int(xc+i*alpha)
                y=int(yc+j*alpha)
                point[nb,0]=nb+1
                point[nb,1]=0
                point[nb,2]=x
                point[nb,3]=-y
                point[nb,4]=-z
                ptvtk[nb,0]=x
                ptvtk[nb,1]=y
                ptvtk[nb,2]=z
                nb=nb+1

    hd="%d %d %d %d" %(nb,Nlines,Nlines,slices.shape[0])
    np.savetxt("%s_mesh_001.pts" %(outputprefix),point,fmt='%d',header=hd,comments="")
    
    hdvtk="vtk DataFile Version 3.4\nvtk output\nASCII\nDATASET STRUCTURED GRID\nDIMENSIONS %d %d %d\nPOINTS %d float" %(Nlines,Nlines,slices.shape[0],nb)
    np.savetxt("%s_mesh_001.vtk" %(outputprefix),ptvtk,fmt='%d',header=hdvtk,comments="")

    points_in = np.loadtxt( pointsFile, skiprows=1)
    #sliceinfo = np.zeros((slice[-1,0]-slice[0,0]+1,4))
    sliceinfo = {}
    j=1
    #balayage k : 0 -> hauteur_max pix par pix
    for k in np.arange(0,outslices[-1,0]-outslices[0,0]):
        sli = np.zeros((3,))
        key = outslices[0,0]+k; #Z absolu de la slice
        while k>outslices[j,0]:
            j=j+1  #dans notre cas un seul j, j=2 quand k sort de l'echantillon en Z
        for i in np.arange(1,4):
            #i in [1,2,3]
            #sli[i-1]=((k-outslices[j-1,0])*outslices[j-1,i] + (outslices[j,0]-k)*outslices[j,i])/(outslices[j,0]-outslices[j-1,0]);
            sli[i-1]=outslices[j-1,i]+(key-outslices[j-1,0])*(outslices[j,i]-outslices[j-1,i])/(outslices[j,0]-outslices[j-1,0]);
            # petit probleme : k-outslices[j-1,0]+outslices[0,0]?
            #sli[0] =  Z%slice maillage precedente * xc de la slice de maillage precedente
        sliceinfo[key]=sli
    points_out = [] #points qui sont à l interieur du maillage
    nb=1
    for k in np.arange(0,points_in.shape[0]):
        #verification en Z
        if (-points_in[k,4]>slices[0,0]) and (-points_in[k,4]<slices[-1,0]):
            #verification en rayon 
            if((points_in[k,2]-sliceinfo[-points_in[k,4]][0])**2+(points_in[k,3]+sliceinfo[-points_in[k,4]][1])**2<= sliceinfo[-points_in[k,4]][2]**2):   
                #renumerotation des points conserves
                points_in[k,0]=nb
                nb = nb + 1
                points_out.append(points_in[k])
    hd="%d %d %d %d" %(len(points_out),len(points_out),1,1)
    np.savetxt("%s_001.pts" %(outputprefix),points_out,fmt='%d',header=hd)


def cylinder_sample_double_brazilian(Nlines, SlicesFile, outSlicesFile, pointsFile, outputprefix):
    """
    Crop points file and generate mesh file for Tracking3D

    Parameters
    ----------

        Nlines : int
            Number of points per line (odd value)

        SlicesFile : string
                Name of file containing parameters
                Table: Zslice Xcenter Ycenter Radius

        PointsFile : string
                Name of file containing points (correlManu3D format)
                Table : header nb_points nb_points 1 1
                        point_index 1 X -Y -Z

    Returns
    -------

    """
    #------------------------------------------------------------
    #Open files
    top_bot_slices = np.loadtxt( SlicesFile, skiprows=1)
    outslices = np.loadtxt( outSlicesFile, skiprows=1)

    slices = np.zeros((Nlines,4))
    for i in range(Nlines):
        slices[i,0]=top_bot_slices[0,0]+i*((top_bot_slices[1,0]-top_bot_slices[0,0])/(Nlines-1))
        slices[i,1]=top_bot_slices[0,1]+i*((top_bot_slices[1,1]-top_bot_slices[0,1])/(Nlines-1))
        slices[i,2]=top_bot_slices[0,2]+i*((top_bot_slices[1,2]-top_bot_slices[0,2])/(Nlines-1))
        slices[i,3]=top_bot_slices[0,3]+i*((top_bot_slices[1,3]-top_bot_slices[0,3])/(Nlines-1))

    Nrad = (Nlines-1)/2
    point=np.zeros((Nlines*Nlines*slices.shape[0],5))
    ptvtk=np.zeros((Nlines*Nlines*slices.shape[0],3))
    nb=0
    for k in np.arange(0,slices.shape[0]):
        zc=slices[k,0]
        xc=slices[k,1]
        y=slices[k,2]
        R=slices[k,3]
        for j in np.arange(-Nrad,Nrad+1,1):
            for i in np.arange(-Nrad,Nrad+1,1):
                if(i*j!=0):
                    theta=np.arctan(abs(1.*j/i))
                    alpha=1.*R/(Nrad*min(1./np.cos(theta),1./np.sin(theta)))
                else:
                    alpha=1.*R/Nrad
                x=int(xc+i*alpha)
                z=int(zc+j*alpha)
                point[nb,0]=nb+1
                point[nb,1]=0
                point[nb,2]=x
                point[nb,3]=-y
                point[nb,4]=-z
                ptvtk[nb,0]=x
                ptvtk[nb,1]=y
                ptvtk[nb,2]=z
                nb=nb+1

    hd="%d %d %d %d" %(nb,Nlines,Nlines,slices.shape[0])
    np.savetxt("%s_mesh_001.pts" %(outputprefix),point,fmt='%d',header=hd,comments="")
    
    hdvtk="vtk DataFile Version 3.4\nvtk output\nASCII\nDATASET STRUCTURED GRID\nDIMENSIONS %d %d %d\nPOINTS %d float" %(Nlines,Nlines,slices.shape[0],nb)
    np.savetxt("%s_mesh_001.vtk" %(outputprefix),ptvtk,fmt='%d',header=hdvtk,comments="")

    points_in = np.loadtxt( pointsFile, skiprows=1)
    #sliceinfo = np.zeros((slice[-1,0]-slice[0,0]+1,4))
    sliceinfo = {}
    j=1
    for k in np.arange(0,outslices[-1,2]-outslices[0,2]):
        sli = np.zeros((3,))
        key = outslices[0,2]+k;
        while k>outslices[j,2]:
            j=j+1  
        for i,ind in enumerate([0,1,3]):
            sli[i]=((key-outslices[j-1,2])*outslices[j,ind] + (outslices[j,2]-key)*outslices[j-1,ind])/(outslices[j,2]-outslices[j-1,2]);
        sliceinfo[key]=sli
    points_out = []
    nb=1
    for k in np.arange(0,points_in.shape[0]):
        if (-points_in[k,3]>slices[0,2]) and (-points_in[k,3]<slices[-1,2]):
            if((points_in[k,2]-sliceinfo[-points_in[k,3]][1])**2+(points_in[k,4]+sliceinfo[-points_in[k,3]][0])**2<= sliceinfo[-points_in[k,3]][2]**2):   
                points_in[k,0]=nb
                nb = nb + 1
                points_out.append(points_in[k])
    hd="%d %d %d %d" %(len(points_out),len(points_out),1,1)
    np.savetxt("%s_001.pts" %(outputprefix),points_out,fmt='%d',header=hd)



def cylinder_sample(Nlines, SlicesFile, pointsFile, outputprefix):
    """
    Crop points file and generate mesh file for Tracking3D

    Parameters
    ----------

        Nlines : int
            Number of points per line (odd value)

        SlicesFile : string
                Name of file containing parameters
                Table: Zslice Xcenter Ycenter Radius

        PointsFile : string
                Name of file containing points (correlManu3D format)
                Table : header nb_points nb_points 1 1
                        point_index 1 X -Y -Z

    Returns
    -------

    """
    #------------------------------------------------------------
    #Open files
    slices = np.loadtxt( SlicesFile, skiprows=1)

    Nrad = (Nlines-1)/2
    point=np.zeros((Nlines*Nlines*slices.shape[0],5))
    ptvtk=np.zeros((Nlines*Nlines*slices.shape[0],3))
    nb=0
    for k in np.arange(0,slices.shape[0]):
        z=slices[k,0]
        xc=slices[k,1]
        yc=slices[k,2]
        R=slices[k,3]
        for j in np.arange(-Nrad,Nrad+1,1):
            for i in np.arange(-Nrad,Nrad+1,1):
                if(i*j!=0):
                    theta=np.arctan(abs(1.*j/i))
                    alpha=1.*R/(Nrad*min(1./np.cos(theta),1./np.sin(theta)))
                else:
                    alpha=1.*R/Nrad
                x=int(xc+i*alpha)
                y=int(yc+j*alpha)
                point[nb,0]=nb+1
                point[nb,1]=0
                point[nb,2]=x
                point[nb,3]=-y
                point[nb,4]=-z
                ptvtk[nb,0]=x
                ptvtk[nb,1]=y
                ptvtk[nb,2]=z
                nb=nb+1

    hd="%d %d %d %d" %(nb,Nlines,Nlines,slices.shape[0])
    np.savetxt("%s_mesh.pts" %(outputprefix),point,fmt='%d',header=hd)
    
    hdvtk="vtk DataFile Version 3.4\nvtk output\nASCII\nDATASET STRUCTURED GRID\nDIMENSIONS %d %d %d\nPOINTS %d float" %(Nlines,Nlines,slices.shape[0],nb)
    np.savetxt("%s_mesh.vtk" %(outputprefix),ptvtk,fmt='%d',header=hdvtk)

    points_in = np.loadtxt( pointsFile, skiprows=1)
    #sliceinfo = np.zeros((slice[-1,0]-slice[0,0]+1,4))
    sliceinfo = {}
    j=1
    for k in np.arange(0,slices[-1,0]-slices[0,0]):
        sli = np.zeros((3,))
        key = slices[0,0]+k;
        while k>slices[j,0]:
            j=j+1  
        for i in np.arange(1,4):
            sli[i-1]=((k-slices[j-1,0])*slices[j-1,i] + (slices[j,0]-k)*slices[j,i])/(slices[j,0]-slices[j-1,0]);
        sliceinfo[key]=sli
    points_out = []
    nb=1
    for k in np.arange(0,points_in.shape[0]):
        if (-points_in[k,4]>slices[0,0]) and (-points_in[k,4]<slices[-1,0]):
            if((points_in[k,2]-sliceinfo[-points_in[k,4]][0])**2+(points_in[k,3]+sliceinfo[-points_in[k,4]][1])**2<= sliceinfo[-points_in[k,4]][2]**2):   
                points_in[k,0]=nb
                nb = nb + 1
                points_out.append(points_in[k])
    hd="%d %d %d %d" %(len(points_out),len(points_out),1,1)
    np.savetxt("%s.pts" %(outputprefix),points_out,fmt='%d',header=hd)


def cylinder_frame(Nlines, SlicesFile):
    """
    Generate points file for correlManu3D

    Parameters
    ----------

        Nlines : int
            Number of points per line (odd value)

        SlicesFile : string
                Name of file containing parameters
                Table: Zslice Xcenter Ycenter Radius

    Returns
    -------

    """
    #------------------------------------------------------------
    #Open files
    slices = np.loadtxt( SlicesFile, skiprows=1)

    Nrad = (Nlines-1)/2
    point=np.zeros((Nlines*Nlines*slices.shape[0],5))
    ptvtk=np.zeros((Nlines*Nlines*slices.shape[0],3))
    nb=0
    for k in np.arange(0,slices.shape[0]):
        z=slices[k,0]
        xc=slices[k,1]
        yc=slices[k,2]
        R=slices[k,3]
        for j in np.arange(-Nrad,Nrad+1,1):
            for i in np.arange(-Nrad,Nrad+1,1):
                if(i*j!=0):
                    theta=np.arctan(abs(1.*j/i))
                    alpha=1.*R/(Nrad*min(1./np.cos(theta),1./np.sin(theta)))
                else:
                    alpha=1.*R/Nrad
                x=int(xc+i*alpha)
                y=int(yc+j*alpha)
                point[nb,0]=nb+1
                point[nb,1]=0
                point[nb,2]=x
                point[nb,3]=-y
                point[nb,4]=-z
                ptvtk[nb,0]=x
                ptvtk[nb,1]=y
                ptvtk[nb,2]=z
                nb=nb+1

    hd="%d %d %d %d" %(nb,Nlines,Nlines,slices.shape[0])
    np.savetxt('Cylinder_mesh.pts',point,fmt='%d',header=hd,comments="")
    
    hdvtk="vtk DataFile Version 3.4\nvtk output\nASCII\nDATASET STRUCTURED GRID\nDIMENSIONS %d %d %d\nPOINTS %d float" %(Nlines,Nlines,slices.shape[0],nb)
    np.savetxt('Cylinder_mesh.vtk',ptvtk,fmt='%d',header=hdvtk,comments="")

def square_frame(Nlines, SlicesFile):
    """
    Generate points file for correlManu3D

    Parameters
    ----------

        Nlines : int
            Number of points per line (odd value)

        SlicesFile : string
                Name of file containing parameters
                Table: Zslice Xcenter Ycenter Radius

    Returns
    -------

    """
    #------------------------------------------------------------
    #Open files
    slices = np.loadtxt( SlicesFile, skiprows=1)

    Nrad = (Nlines-1)/2
    point=np.zeros((Nlines*Nlines*slices.shape[0],5))
    ptvtk=np.zeros((Nlines*Nlines*slices.shape[0],3))
    nb=0
    for k in np.arange(0,slices.shape[0]):
        z=slices[k,0]
        xc=slices[k,1]
        yc=slices[k,2]
        R=slices[k,3]
        for j in np.arange(-Nrad,Nrad+1,1):
            for i in np.arange(-Nrad,Nrad+1,1):
                alpha=1.*R/Nrad
                x=int(xc+i*alpha)
                y=int(yc+j*alpha)
                point[nb,0]=nb+1
                point[nb,1]=0
                point[nb,2]=x
                point[nb,3]=-y
                point[nb,4]=-z
                ptvtk[nb,0]=x
                ptvtk[nb,1]=y
                ptvtk[nb,2]=z
                nb=nb+1

    hd="%d %d %d %d" %(nb,Nlines,Nlines,slices.shape[0])
    np.savetxt('Square_mesh.pts',point,fmt='%d',header=hd,comments="")
    
    hdvtk="vtk DataFile Version 3.4\nvtk output\nASCII\nDATASET STRUCTURED GRID\nDIMENSIONS %d %d %d\nPOINTS %d float" %(Nlines,Nlines,slices.shape[0],nb)
    np.savetxt('Square_mesh.vtk',ptvtk,fmt='%d',header=hdvtk,comments="")

def bin_param2param(ParamFile):
    """
    Bin coordinates in param file for correlManu3D

    Parameters
    ----------

        ParamFile : string
                Name of file containing parameters

    Returns
    -------

    """
    #------------------------------------------------------------
    #Open files
    slices = np.loadtxt( ParamFile, skiprows=1)
    point=np.zeros((slices.shape[0],5))
    nb=0
    for k in np.arange(0,slices.shape[0]):
        slices[k,1]=slices[k,1]/2
        slices[k,2]=slices[k,2]/2
        slices[k,3]=slices[k,3]/2
        nb=nb+1
    hd="header"
    np.savetxt('Binned_param.dat',slices,fmt='%d',header=hd)


def param2pts(ParamFile):
    """
    Bin coordinates in param file for correlManu3D

    Parameters
    ----------

        ParamFile : string
                Name of file containing parameters

    Returns
    -------

    """
    #------------------------------------------------------------
    #Open files
    slices = np.loadtxt( ParamFile, skiprows=1)
    point=np.zeros((slices.shape[0],5))
    nb=0
    for k in np.arange(0,slices.shape[0]):
        x=slices[k,1]
        y=slices[k,2]
        z=slices[k,3]
        point[nb,0]=nb+1
        point[nb,1]=1
        point[nb,2]=x
        point[nb,3]=-y
        point[nb,4]=-z
        nb=nb+1
    hd="%d %d %d %d" %(nb,1,1,slices.shape[0])
    np.savetxt('param.pts',point,fmt='%d',header=hd)

def param2ptsfiltered(ParamFile,PtsFile):
    """
    Bin coordinates in param file for correlManu3D

    Parameters
    ----------

        ParamFile : string
                Name of file containing parameters

    Returns
    -------

    """
    #------------------------------------------------------------
    #Open files

    os.system("sed 's/false/0/g' %s | sed 's/true/1/g' > tmp.txt" %(ParamFile))
    slices = np.loadtxt( 'tmp.txt', skiprows=0)
    point=np.zeros((slices.shape[0],5))
    nb=0
    for k in np.arange(0,slices.shape[0]):
        x=slices[k,1]
        y=slices[k,2]
        z=slices[k,3]
        point[nb,0]=nb+1
        point[nb,1]=1
        point[nb,2]=x
        point[nb,3]=-y
        point[nb,4]=-z
        nb=nb+1
    hd="%d %d %d %d" %(nb,1,1,slices.shape[0])
    np.savetxt(PtsFile,point,fmt='%d',header=hd)

def param2mesh(ParamFile):
    """
    Bin coordinates in param file for correlManu3D

    Parameters
    ----------

        ParamFile : string
                Name of file containing parameters

    Returns
    -------

    """
    #------------------------------------------------------------
    #Open files
    slices = np.loadtxt( ParamFile, skiprows=1)
    point=np.zeros((slices.shape[0],5))
    ptvtk=np.zeros((slices.shape[0],3))
    nb=0
    for k in np.arange(0,slices.shape[0]):
        x=slices[k,1]
        y=slices[k,2]
        z=slices[k,3]
        point[nb,0]=nb+1
        point[nb,1]=1
        point[nb,2]=x
        point[nb,3]=-y
        point[nb,4]=-z
        ptvtk[nb,0]=x
        ptvtk[nb,1]=y
        ptvtk[nb,2]=z
        nb=nb+1

    hd="%d %d %d %d" %(nb,1,1,slices.shape[0])
    np.savetxt('param_mesh.pts',point,fmt='%d',header=hd,comments="")
    
    hdvtk="vtk DataFile Version 3.4\nvtk output\nASCII\nDATASET STRUCTURED GRID\nDIMENSIONS %d %d %d\nPOINTS %d float" %(1,1,slices.shape[0],nb)
    np.savetxt('param_mesh.vtk',ptvtk,fmt='%d',header=hdvtk,comments="")

def diabolo_frame(Nlines, SlicesFile, Sections):
    """
    Generate points file for correlManu3D

    Parameters
    ----------

        Nlines : int
            Number of points per line (odd value)

        SlicesFile : string
                Name of file containing parameters
                Table: Zslice Xcenter Ycenter Radius
        
        Sections : ndarray
            Position of sections 

    Returns
    -------

    """
    #------------------------------------------------------------
    #Open files
    slices = np.loadtxt( SlicesFile, skiprows=0)

    Nrad = (Nlines-1)/2
    point=np.zeros((Nlines*Nlines*Sections.shape[0],5))
    nb=0
    for k in np.arange(0,Sections.shape[0]):
        print("k=%d\n" %(k))
        z=Sections[k]
        xc=slices[z,1]
        yc=slices[z,2]
        R=np.sqrt(slices[z,3]*slices[z,0]*0.01/3.14)
        for j in np.arange(-Nrad,Nrad+1,1):
            for i in np.arange(-Nrad,Nrad+1,1):
                if(i*j!=0):
                    theta=np.arctan(abs(1.*j/i))
                    alpha=1.*R/(Nrad*min(1./np.cos(theta),1./np.sin(theta)))
                else:
                    alpha=1.*R/Nrad
                x=int(xc+i*alpha)
                y=int(yc+j*alpha)
                point[nb,0]=nb+1
                point[nb,1]=0
                point[nb,2]=x
                point[nb,3]=-y
                point[nb,4]=-z
                nb=nb+1

    hd="%d %d %d %d" %(nb,Nlines,Nlines,Sections.shape[0])
    np.savetxt('Diabolo_mesh.pts',point,fmt='%d',header=hd)

def diabolo_frame_vtk(Nlines, SlicesFile, Sections):
    """
    Generate mesh in vtk format

    Parameters
    ----------

        Nlines : int
            Number of points per line (odd value)

        SlicesFile : string
                Name of file containing parameters
                Table: Image_surface Xcenter Ycenter Sections_surface_ratio
                #Table: Zslice Xcenter Ycenter Radius
        
        Sections : ndarray
            Position of sections 

    Returns
    -------

    """
    #------------------------------------------------------------
    #Open files
    slices = np.loadtxt( SlicesFile, skiprows=0)

    Nrad = (Nlines-1)/2
    point=np.zeros((Nlines*Nlines*Sections.shape[0],5))
    nb=0
    for k in np.arange(0,Sections.shape[0]):
        print("k=%d\n" %(k))
        z=Sections[k]
        xc=slices[z,1]
        yc=slices[z,2]
        R=np.sqrt(slices[z,3]*slices[z,0]*0.01/3.14)
        for j in np.arange(-Nrad,Nrad+1,1):
            for i in np.arange(-Nrad,Nrad+1,1):
                if(i*j!=0):
                    theta=np.arctan(abs(1.*j/i))
                    alpha=1.*R/(Nrad*min(1./np.cos(theta),1./np.sin(theta)))
                else:
                    alpha=1.*R/Nrad
                x=int(xc+i*alpha)
                y=int(yc+j*alpha)
                point[nb,0]=nb+1
                point[nb,1]=0
                point[nb,2]=x
                point[nb,3]=y
                point[nb,4]=z
                nb=nb+1
    f=open('meshfile.vtu','w')
    f.write('# vtk DataFile Version 3.1\n')
    f.write('Mesh file\n')
    f.write('ASCII\n')
    f.write('DATASET STRUCTURED_GRID\n')
    f.write('DIMENSIONS %d %d %d\n' %(Nlines, Nlines, Sections.shape[0]))
    f.write('POINTS %d FLOAT\n' %(point.shape[0]))
    for i in np.arange(0,point.shape[0]):
        f.write('%f %f %f\n' %(point[i,2],point[i,3],point[i,4]))
    f.write('\n')
    f.close()



def readptsfromHRseries(prefix, HR, states):
    """
    Read pts files from HR series

    Parameters
    ----------

        prefix : string
                Name of file prefix
        HR : list of int
                Numbers of the HR scans
        states : list of int
                Numbers of the states

    Returns
    -------

    """
    #------------------------------------------------------------
    deltazHR = 900
    
    rstateu = []
    rstatep = []
    
    for istate in states:
        for iHR in HR:
            pts1 = np.loadtxt( "output_HR%d_state%d/%s_HR%d_%03d.pts" %(iHR, istate, prefix, iHR, istate), skiprows=1)
            pts2 = np.loadtxt( "output_HR%d_state%d/%s_HR%d_%03d.pts" %(iHR, istate, prefix, iHR, istate + 1), skiprows=1)
            u = pts2[:,2:5] - pts1[:,2:5]
            u[:,3:5] = pts1[:,2:5] - pts2[:,2:5]
            pts = pts1[:,2:5]
            pts[:,1:] *=-1
            pts[:,2] += (iHR-1)*deltazHR
            
            rstatep.append(pts)
            rstateu.append(u)
    return (rstatep, rstateu)

def generateVirtualStates(rstatep, rstateu, HR, states):
    """
    Generate virtual states from pts files of HR series

    Parameters
    ----------

        rstatep : list of array of pts 
                Real states points lists
        rstateu : list of array of displacements 
                Real states displacements lists
        HR : list of int
                Numbers of the HR scans
        states : list of int
                Numbers of the states

    Returns
    -------

    """
    #------------------------------------------------------------
    vstatev = []
    vstatep = []
    
    for istate in states:
        for iHR in HR:
            pts = rstatep[istate*len(HR)+iHR-1]
            u = rstateu[istate*len(HR)+iHR-1]
            np, nv = interpolateState(pts,u,0)
            vstatep.append(np)
            vstatev.append(nv)

    for istate in states:
        for iHR in HR:
            pts = rstatep[istate*len(HR)+iHR-1]
            u = rstateu[istate*len(HR)+iHR-1]
            for dHR in np.arange(-len(HR), len(HR)+1):
                if (dHR != 0) and (istate*len(HR)+iHR-1+dHR >= 0) and (istate*len(HR)+iHR-1+dHR < len(states)*len(HR)):
                    np, nv = interpolateState(pts,u,dHR)
                    vstatep[istate*len(HR)+iHR-1+dHR].append(np)
                    vstatev[istate*len(HR)+iHR-1+dHR].append(nv)
    return (vstatep, vstatev)

def interpolateState(pts, u, dHR, deltat=1):
    """
    Interpolate (or even extrapolate) pts and u for a virtual state (other HR time)

    Parameters
    ----------

        pts : array of pts for dHR = 0 
                Real state points
        u : array of displacements from dHR=0 to dHR=4 (same HR acquisition, next state)
                Real state displacements
        dHR : int
                HR spacing (can be negative or positive, 4 is same HR next state)

    Returns
    -------
        newpts :
            list of virtual pts
        v:
            list of virtual 
    """
    newpts = pts + dHR*0.25*u
    newv = u*0.25/deltat
    return (newpts, newv)

def pts2vtk(pts, v, name):
    """
    Generate vtk file format from pts and v

    Parameters
    ----------

        pts : array
            Array of points

        v : array
            Array of velocities

        name : string
                Name of output file
        
    Returns
    -------

    """
    #------------------------------------------------------------
    f=open(name,'w')
    # vtk DataFile Version 3.1
    f.write('# vtk DataFile Version 3.1\n')
    f.write('Virtual points\n')
    f.write('ASCII\n')
    f.write('DATASET UNSTRUCTURED_GRID\n')
    f.write('POINTS %d FLOAT\n' %(pts.shape[0]))
    for i in np.arange(0,pts.shape[0]):
        f.write('%f %f %f\n' %(pts[i,0],pts[i,1],pts[i,2]))
    
    f.write('\n')
    f.write('POINT_DATA %d\n' %(pts.shape[0]))
    f.write('VECTOR Velocity FLOAT\n')
    for i in np.arange(0,pts.shape[0]):
        f.write('%f %f %f\n' %(v[i,0],v[i,1],v[i,2]))
    f.write('\n')
    f.close()

def writeVirtualStates(vp, vv, prefix):
    for i in np.arange(0,len(vp)):
        pts2vtk(vp[i], vv[i], "%s_%03d.vtu" %(prefix,i))

def run_AZ31_X():
    prefix=
    HR = [1 2 3 4]
    states = [1 2 3 4 5 6 7]
    rp,ru = readptsfromHRseries(prefix, HR, states)
    vp, vv = generateVirtualStates(rp, ru, HR, states)
    writeVirtualStates(vp, vv, 
    
