#include "Tracking_Particles_with_DIC.h"


using namespace std;


int main(int nbarg, char* argv[]) {

    init("./");

    printf("\n*******************************************\n\nAuthor : Pierre Lhuissier\n\nContact : pierre.lhuissier@simap.grenoble-inp.fr\n\nProgramm to be used after IJ plugins labeling and param3D\n\n\n*******************************************\n\n");

    Parameters *param;
	param = (Parameters *)malloc(sizeof(Parameters));
    if(nbarg==2)
    {
        LINFO << "Read parameters from " << argv[1];
	    //Configuration file
	    readConfig(argv[1],param);
        LINFO << "Parameters readden";
    }
    else
	{
		LINFO << "Usage :  Tracking_Particles input.file";
		return 0;
	}
    
    TS<Particle,P>* ts = new TS<Particle,P>(param);

    ts->readPtrs();
    LINFO << "Read particles files OK";

    if(param->precorrel)
    	ts->readPgs();
    		

    if(param->backwardposition)
        ts->trackOnlyBackwardPositionParticles();
    else
    {
        int totalParticlesNumber = 0;
        if(param->missingparticles)
            totalParticlesNumber = ts->trackIncompleteParticles();
        else
            totalParticlesNumber = ts->trackNucleateAndMergeParticles();
            //totalParticlesNumber = ts->trackParticles();
    }

    if(param->mesh)
        ts->addMeshInfo();

    //LINFO << "Ranking particles by global number ...";
    //ts->rankParticles();
    //vector<PSet<Particle> > track = ts->rankParticles(totalParticlesNumber);

    //write Volume
    LINFO << "Writing some results ...";
    ts->writePtrs();

    //if(param->recolor)
    //ts->recolor(track);

    LINFO << "Finished!!!";
	finalize();

}


//********************************************************************************************
/**
int main_old(int nbarg, char* argv[]) {

    init("./");

    printf("\n*******************************************\n\nAuthor : Pierre Lhuissier\n\nContact : pierre.lhuissier@simap.grenoble-inp.fr\n\nProgramm to be used after IJ plugins labeling and param3D\n\n\n*******************************************\n\n");

    Parameters *param;
	param = (Parameters *)malloc(sizeof(Parameters));
    if(nbarg==2)
    {
        LINFO << "Read parameters from " << argv[1];
	    //Configuration file
	    readConfig(argv[1],param);
        LINFO << "Parameters readden";
    }
    else
	{
		LINFO << "Usage :  Tracking_Particles input.file";
		return 0;
	}


    bool recolor= true;
    //Parameters
	double thrXY=50;
    double thrZ=20;
    string Directory = param->Directory;
    string prefiximage = param->prefiximage;
    string suffiximage  = param->suffiximage;
    string prefixparam  = param->prefixparticles;
    string suffixparam = param -> suffixparticles;
    string prefixdepl= "";
    string suffixdepl="";
    int firstvol=param->firstvol;
    int lastvol= param->lastvol;
    int intervol=param->intervol;
    int posttreat=param->postprocess;


    bool DIC = false;
    if(DIC)
        cout << "Will use DIC to refine matching" << endl;
    else
        cout << "No DIC" << endl;



	int nombreExpe = (int)((lastvol-firstvol)/intervol + 1);

	vector<string> Num = vector<string>();
	for(int i=firstvol;i<=lastvol;i+=intervol)
	{
		char numscan[4];
		sprintf(numscan,"%03d",i);
		Num.push_back(string(numscan));
	}



	vector<ParticlesSet> *parset = new vector<ParticlesSet>();
	if(posttreat)
	{
		cout << "Used memorised particles" << endl;
		*parset = readDataParticlesSet(Directory+"TrackingFiles/", prefiximage+"_param");
	}
	else
	{
		vector<vector<Particle*> > *rawdata = new vector<vector<Particle*> >();
		cout << "Reading parameters and displacement files ..." << endl;
		for(int i=0; i<nombreExpe; ++i)
		{
			cout << "Looking for scan " << Num[i] << " parameters in file " <<  Directory+prefixparam+Num[i]+suffixparam << endl;
			vector<Particle*> raw = readParamFile(Directory+prefixparam+Num[i]+suffixparam,i);
			rawdata->push_back(raw);
			cout << raw[0]->tostring() << endl;
			//cout << "Record objects parameters in a ParticlesSet" << endl;
			ParticlesSet* rawps = new ParticlesSet(Directory+prefiximage+Num.at(i)+suffiximage, raw);
			//cout << "Sort objects by volume" << endl;
			parset->push_back((*rawps).rankByVolume());
		}
		writeVectorParticlesSet(Directory+"TrackingFiles/",prefiximage+"_param",*parset);
	}

    vector<vector<Coord > > refPoints;
    for(int i=0; i<nombreExpe; ++i)
    {
    	refPoints.push_back(readPtsFile(Directory+prefixdepl+Num[i]+suffixdepl));
    	cout << "Read " << prefixdepl << Num[i] << suffixdepl<< endl;
    }


    vector<vector<Coord > > refDeplp;
    for(int i=0; i<nombreExpe-1; ++i)
    {
    	refDeplp.push_back(refPoints[i+1]-refPoints[i]);
    }

    vector<vector<Coord > > refDeplm;
    for(int i=0; i<nombreExpe-1; ++i)
    {
    	refDeplm.push_back(refPoints[i]-refPoints[i+1]);
    }

     cout << "Read parameters files and displacements files OK" << endl;


    vector<vector<ParticlesSet> > global;

    for(int i=nombreExpe-1; i>0; --i)
    {
    	vector<ParticlesSet> checkedCorrel;
    	if(posttreat)
    	{
    		checkedCorrel = readVectorParticlesSet(Directory+"TrackingFiles/",prefiximage+Num[i]+"_checked", *parset);
    	}
    	else
    	{
    		cout << "Matching scans " << Num[i] << " and " << Num[i-1] << " ..." << endl;

			//Reference Particles pool (from reference scan)
			ParticlesSet poolref = ((*parset)[i]).subSetVol(4);
			cout << "Scan " << Num[i] << " contains " << poolref.Size() << " particles" << endl;

			//Second Particles pool (from next scan)
			ParticlesSet poolfinal = ((*parset)[i-1]).subSetVol(4);
			cout << "Scan " << Num[i-1] << " contains " << poolfinal.Size() << " particles" << endl;

            vector<ParticlesSet> potentialCorrel;
            vector<ParticlesSet> potentialCorrelInv;
            if(DIC){
			//For each particle of reference pool look for the best correlated particles of second pool (forward correlation)
			potentialCorrel = poolref.findBestCorrel_DICGPU(poolfinal, refPoints[i], refDeplm[i-1], thrXY, thrZ);
			//vector<ParticlesSet> potentialCorrel = poolref.findBestCorrel_noDIC(poolfinal, deplm[i-1], thrXY, thrZ);

			//For each particle of second pool look for the best correlated particles of reference pool (backward correlation)
			potentialCorrelInv = poolfinal.findBestCorrel_DICGPU(poolref, refPoints[i-1],refDeplp[i-1], thrXY, thrZ);
			//vector<ParticlesSet> potentialCorrelInv = poolfinal.findBestCorrel_noDIC(poolref, deplp[i-1], thrXY, thrZ);
            }
            else
            {
    	    //For each particle of reference pool look for the best correlated particles of second pool (forward correlation)
			potentialCorrel = poolref.findBestCorrel_noDIC(poolfinal, refPoints[i], refDeplm[i-1], thrXY, thrZ);
			//vector<ParticlesSet> potentialCorrel = poolref.findBestCorrel_noDIC(poolfinal, deplm[i-1], thrXY, thrZ);

			//For each particle of second pool look for the best correlated particles of reference pool (backward correlation)
			potentialCorrelInv = poolfinal.findBestCorrel_noDIC(poolref, refPoints[i-1],refDeplp[i-1], thrXY, thrZ);

            }

			cout << "Potential pairs found." << endl;


			//debug :
			//poolref.debugCorrel(potentialCorrel,poolfinal,potentialCorrelInv);


			//Match forward and backward correlation
			checkedCorrel = poolref.matchCorrel(potentialCorrel,poolfinal,potentialCorrelInv);

			//Write the cross correlation result
			writevectorParticlesSet(Directory+"TrackingFiles/"+prefiximage+Num[i]+"_checked.dat",checkedCorrel);
			writeVectorParticlesSet(Directory+"TrackingFiles/",prefiximage+Num[i]+"_checked",checkedCorrel);
    	}
	    //Keep memory of the correlation for final renumbering
	    global.push_back(checkedCorrel);
    }

    //Concatenate pairs of correlation in a global tracking
    cout << "Concatenating pairs..." << endl;
    int current=0;
    for(int i=nombreExpe-1; i>0; --i)
    {
    	current = (*parset)[i].globalNumberFromCheckedRef(global[nombreExpe-1-i], current, atoi(Num[i].c_str()));
    	(*parset)[i-1].globalNumberFromCheckedFinal(global[nombreExpe-1-i],(*parset)[i], current, atoi(Num[i].c_str()));
    }

    cout << "Ranking particles by global number ..." << endl;

    vector<ParticlesSet> track = vector<ParticlesSet>();
    for(int i=1; i<=current;i++)
    {
    	ParticlesSet* partS = new ParticlesSet();
    	cout << "Looking for particle " << i << endl;
    	for(int j=0;j<nombreExpe;j++)
    	{
    		cout << "Scan " << j << " ";
    		(*partS).addParticle((*parset)[j].findGlobalColorParticle(i));
    	}
    	track.push_back(*partS);
    }

    //write Volume
    cout << "Writing some results ..." << endl;

    string globalName = Directory+prefiximage + "_track_global_info.dat";
    writevectorParticlesSetGlobal(globalName,track);


    recolor=true;
    if(recolor){
    	cout << "Recoloring  volumes ..." << endl;
    	for(int i=0; i<nombreExpe; ++i)
        {
    			//ImagePlus Entree = IJ.openImage(NomMat+ScanName.get(i) + suffiximage);
    			//Entree.show();
    			//ImageStack stackEntree = Entree.getImageStack();
    			int* new_color = new int[65256];
    			for(int j = 0; j<65256; j++)
    			{
    				new_color[j] = 65255;
    			}
    			for(int j=0; j<track.size(); ++j)
    			{
    				int intensFinal = j;

                    if((track[j].getParticle(i)) && (track[j].getParticle(i)->getColor()>=0))
    				    new_color[track[j].getParticle(i)->getColor()] = intensFinal;
    			}
    			new_color[0]=0;
    			ostringstream a;
    			a << Directory << prefiximage << Num[i] << "_label_color_corresp.dat";

    			writeFile(a.str(),new_color,1, 65256);
    			//stackEntree = recolor(stackEntree, new_color);
    			//Entree.setStack(NomMat+ScanName.get(i) + "_label_color.tif", stackEntree);
    			//new FileSaver(Entree).saveAsTiffStack(NomMat+ScanName.get(i) + "_label_color.tif");
    			//Entree.close();
    	}
    }
*/

	    	
    	/**case 1 :
    		//Pair correlation
    		vector<vector<ParticlesSet>> global = new vector<vector<ParticlesSet>>();
     		for(int i=0; i<nombreExpe-1; ++i)
    		{
     			ParticlesSet poolr = (ParticlesSet)(param.get(i));
    			ParticlesSet poolf = (ParticlesSet)(param.get(i+1));
    			ParticlesSet potentialC = poolr.closest(poolf, thrXY, thrZ);
    			String ImfName = NomMat + ScanName.get(i+1)+suffiximage;
    			potentialC.findNullFromImage(poolr,ImfName,thrXY,thrZ);
    			vector<ParticlesSet> scan = new vector<ParticlesSet>();
    			scan.add(poolr);
    			scan.add(potentialC);
    			global.add(scan);
    		}
     		vector<ParticlesSet> scan = new vector<ParticlesSet>();
			scan.add(param.get(nombreExpe-1));
			scan.add(param.get(nombreExpe-1));
			global.add(scan);
    		
			
    		//Concat pairs
     		int currentGlobal=0;
    		for(int i=nombreExpe-2; i>0; --i)
    		{
    			ParticlesSet lost = global.get(i+1).get(0).findLost(global.get(i).get(1));
    			global.get(i+1).add(lost);
    			currentGlobal=global.get(i).get(1).globalNumber(currentGlobal);
    			global.get(i).get(0).matchGlobalNumberIntra(global.get(i).get(1));
    			currentGlobal=global.get(i-1).get(1).matchGlobalNumberInter(global.get(i).get(0),currentGlobal);
    		}
    		global.get(0).get(0).matchGlobalNumberIntra(global.get(0).get(1));
    		
    		vector<ParticlesSet> track = new vector<ParticlesSet>();
    		for(int i=1; i<=currentGlobal;i++)
    		{
    			ParticlesSet partS = new ParticlesSet("particle" + i);
    			for(int j=0;j<nombreExpe;j++)
    			{
    				partS.addParticle(global.get(j).get(1).findGlobalColorParticle(i));
    			}
    			track.add(partS);
    		}
    		//write Volume
    		String colorName = NomMat + "_track_color.dat";
    		writevectorParticlesSetColor(colorName,track);
    		String volName = NomMat + "_track_volume.dat";
    		writevectorParticlesSetVolume(volName,track);
    		String posName = NomMat + "_track_pos_.dat";
    		writevectorParticlesSetPos(posName,track);
    		String spheName = NomMat + "_track_sphericity_.dat";
    		writevectorParticlesSetSphericity(spheName,track);
    		
    		
    		for(int i=0; i<nombreExpe; ++i)
    	    {
    			ImagePlus Entree = IJ.openImage(NomMat+ScanName.get(i) + suffiximage);
    			Entree.show();
    			ImageStack stackEntree = Entree.getImageStack();
    			int[] new_color = new int[65256];
    			for(int j = 0; j<65256; j++)
    			{
    				new_color[j] = 65000;
    			}
    			for(int j=0; j<track.size(); ++j)
    			{
    				int intensFinal = j;
    				if(track.get(j).getParticle(i)!=null)
    				new_color[track.get(j).getParticle(i).getColor()] = intensFinal;
    			}
    			new_color[0]=0;
    			stackEntree = recolor(stackEntree, new_color);
    			Entree.setStack(NomMat+ScanName.get(i) + "_label_color.tif", stackEntree);
    			new FileSaver(Entree).saveAsTiffStack(NomMat+ScanName.get(i) + "_label_color.tif");
    			
    			Entree.close();
    		}
    		break;
    		*/
	
    		
    	
    	/**while((poolref.Size()>0)||(poolfinal.Size()>0))
    	{
    		ParticlesSet subPartref = poolref.subSet(poolref.getParticle(0).getCoord(),thrXY,thrZ);
    		ParticlesSet subPartfinal = poolfinal.subSet(poolref.getParticle(0).getCoord().Interpolate(refSlices[i],refSlices[i-1]),thrXY,thrZ);
    		vector<ParticlesSet> validPart = subPartref.findCorrel(subPartfinal,refSlices[i],refSlices[i-1]);
    		
    		
    	}
    	
    	ParticlesSet potential = param.get(i).FindPotential(param.get(i-1),thrXY,thrZ,thrdvp,thrdvm,thrsph);
    	*/

/**
cout << "Finished!!!" << endl;
	
}
*/

//////////////////////////////////////////////////////////////////////

/////////////////// READ and WRITE functions /////////////////////////


/*

void writeVectorParticlesSet(string Directory, string VectorParticleSetName, vector<ParticlesSet> & vector)
{
	 ostringstream fileName;
	 fileName << Directory << "/VectorParticleSet_" << VectorParticleSetName;
     ofstream fichier(fileName.str().c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier
     if(fichier)  // si l'ouverture a réussi
	 {
	 		 for(int j=0; j<vector.size(); j++)
	 	 	 {
	 			 	 ostringstream psName;
	 			 	 char buff[4];
	 			 	 sprintf(buff,"%04d",j);
	 			     psName << "ParticleSet_" << VectorParticleSetName << buff;
	 				fichier <<  psName.str() <<endl;
	 				vector[j].writeToFile(Directory,psName.str());
	 	 	 }
	 	 	 fichier.close();
	 	 }
	 	 else  // sinon
	 	 cerr << "Erreur à l'ouverture !" << endl;
}

vector<ParticlesSet> readVectorParticlesSet(string Directory, string VectorParticleSetName, vector<ParticlesSet> & raw)
{
	 vector<ParticlesSet>* v = new vector<ParticlesSet>();
	 ostringstream fileName;
	 fileName << Directory << "/VectorParticleSet_" << VectorParticleSetName;
     ifstream fichier(fileName.str().c_str(), ios::in);  //déclaration du flux et ouverture du fichier
     if(fichier)  // si l'ouverture a réussi
	 {
    	 while (fichier.good())
    	 {
    	 	string psName = "";
    	 	char schar[10000];
    	 	fichier.getline(schar,10000);
    	    psName+=schar;
    	    ostringstream fullpsName;
    	    fullpsName << Directory << "/" << psName;
    	    if(ifstream(fullpsName.str().c_str()) && psName!=""){
    	    	ParticlesSet* toto = new ParticlesSet(Directory,psName,raw);
    	    	v->push_back(*toto);
    	    }
    	 }
	 	 fichier.close();
	 }
     else  // sinon
     {
         	 cerr << "Erreur à l'ouverture !" << endl;
          	 cerr << "File " << fileName.str() << " not found!" << endl;
      }
     return *v;
}

vector<ParticlesSet> readDataParticlesSet(string Directory, string VectorParticleSetName)
{
	 vector<ParticlesSet>* v = new vector<ParticlesSet>();
	 ostringstream fileName;
	 fileName << Directory << "/VectorParticleSet_" << VectorParticleSetName;
	 ifstream fichier(fileName.str().c_str(), ios::in);  //déclaration du flux et ouverture du fichier
     if(fichier)  // si l'ouverture a réussi
	 {
    	 while (fichier.good())
    	 {
    	 	string psName = "";
    	 	char schar[10000];
    	 	fichier.getline(schar,10000);
    	    psName+=schar;
    	    ParticlesSet* toto = new ParticlesSet(Directory,psName);
    	    v->push_back(*toto);

    	 }
	 	 fichier.close();
	 }
     else  // sinon
     {
    	 cerr << "Erreur à l'ouverture !" << endl;
     	 cerr << "File " << fileName.str() << " not found!" << endl;
     }
     return *v;
}



 vector<Particle*> readParamFile(string fileName, int scan)
 {
    vector<Particle*> *particles = new vector<Particle*>();
    ifstream File (fileName.c_str(),ios::in);
    if (!File)
    {
    	cerr << "Error : impossible to open file " << fileName << " in read only mode..." << endl;
    }
    else
    {
	cout << "File opened " << endl;
    }
    vector<double> line;
    double a;
    string paramline;
    int nbColonne;
    nbColonne=0;
    stringstream sstr;
    char schar[10000];
    File.getline(schar,10000);
    File.getline(schar,10000);
    paramline+=schar;
    sstr.str(paramline);
    while(sstr >> a)
    {
        nbColonne++;
	//cout << "Column " << nbColonne << " : " << a << endl;
    }

    File.seekg(0, ios::beg);
    File.getline(schar,10000);
    cout << nbColonne << " columns" << endl;
    int pn=0;
    int wp=0;
    while (File.good())
    {
	paramline = "";
	File.getline(schar,10000);
    	paramline+=schar;
	sstr.clear();
    	sstr.str(paramline);
    	if(sstr.str().size()>1)
        {
        //cout << paramline << endl;
    		for(int i=0; i<nbColonne; i++)
    		{
			sstr >> a;
			line.push_back(1.*a);
		}
		if(!isnan(line[0]))
		{
			pn++;
			//cout << "Add one more particle (total " << pn << ")" <<  endl;

			particles->push_back(new Particle(scan,
						(int)line[0],
						Coord((double)line[1],(double)line[2],(double)line[3]),
						(double)line[4],
						(bool)(line[35]==1),
						(double)line[7],
						((double)line[34])/((double)line[32]),
						((double)line[34])/((double)line[33]),
						(double)line[19],
						paramline
						));
			}
		else
		{
			wp++;
			//cout << "Skipped particle ( total " << wp << ")" << endl;
		}
		line.clear();
        }
    }
    File.close();
    cout << fileName << " successfully loaded" << endl;
    cout << "Found " << pn << " valid objects" <<  endl;
    cout << "Skipped " << wp << " objects" << endl;
    return *particles;
}



vector<vector<double> > readFile(string filePath)
{

    ifstream file(filePath.c_str(), ios::in | ios::out);

    if(!file.is_open()) {
        cerr << "Could not open " << filePath << endl;
        return vector<vector<double> >();
    }

    vector<vector<double> > data;
    string line;
    while(!std::getline(file, line, '\n').eof()) {
        istringstream reader(line);
        vector<double> lineData;

        string::const_iterator i = line.begin();
        while(!reader.eof()) {
            double val;
            reader >> val;

            if(reader.fail())
                break;

            lineData.push_back(val);
        }

        data.push_back(lineData);
    }
    return data;
}

vector<Coord> readPtsFile(string filePath)
{

    ifstream file(filePath.c_str(), ios::in | ios::out);
    cout << "Readding " << filePath << endl;
    
    if(!file.is_open()) {
        cerr << "Could not open " << filePath << endl;
        return vector<Coord >();
    }

    vector<Coord > data;
    string line;
    std::getline(file, line, '\n');
    while(!std::getline(file, line, '\n').eof()) {
        istringstream reader(line);
        vector<double> lineData;

        string::const_iterator i = line.begin();
        while(!reader.eof()) {
            double val;
            reader >> val;

            if(reader.fail())
                break;

            lineData.push_back(val);
        }

        data.push_back(Coord(lineData[2],lineData[3],lineData[4]));
    }
    return data;
}
 void writeFile(string filePath, int* values, int dimi, int dimj)
 {
 	 ofstream fichier(filePath.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier
 	 if(fichier)  // si l'ouverture a réussi
 	 {
 		 for(int j=0; j<dimj; j++)
 	 	 {
 			for(int i=0; i<dimi; i++)
 			{
 				fichier << values[j*dimi+i] << endl;
 			}
 	 	 }
 	 	 fichier.close();  // on referme le fichier
 	 }
 	 else  // sinon
 	 cerr << "Erreur à l'ouverture !" << endl;
}

void writeFile(string filePath, int values[], int dim)
{
	 ofstream fichier(filePath.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier
	 if(fichier)  // si l'ouverture a réussi
	 {
		 for(int j=0; j<dim; j++)
	 	 {
			 fichier << values[j] << endl;
	 	 }
	 	 fichier.close();  // on referme le fichier
	 }
	 else  // sinon
	 cerr << "Erreur à l'ouverture !" << endl;
}

void writevectorParticlesSet(string FileName, vector<ParticlesSet> vector)
{
	 ofstream fichier(FileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier
	 	 if(fichier)  // si l'ouverture a réussi
	 	 {
	 		 for(int j=0; j<vector.size(); j++)
	 	 	 {

	 				fichier << vector[j].colorTostring() << endl;
	 	 	 }
	 	 	 fichier.close();  // on referme le fichier
	 	 }
	 	 else  // sinon
	 	 cerr << "Erreur à l'ouverture !" << endl;
}

void writevectorParticlesSetGlobal(string FileName, vector<ParticlesSet> vector)
{

	 ofstream fichier(FileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier
		 	 if(fichier)  // si l'ouverture a réussi
		 	 {
		 		 for(int j=0; j<vector.size(); j++)
		 	 	 {

		 				fichier << vector[j].colorTostring();
		 				fichier << vector[j].colorGlobalTostring();
		 				fichier << vector[j].volumeTostring();
		 				fichier << vector[j].PosTostring() << endl;
		 	 	 }
		 	 	 fichier.close();  // on referme le fichier
		 	 }
		 	 else  // sinon
		 	 cerr << "Erreur à l'ouverture !" << endl;
}

*/
