#include "Tracking.h"

//#define DEBUGMODE

using namespace std;


int main(int nbarg, char* argv[]) {

    init("./");

    LINFO << "\n***************************************************************\n\n               Tracking of points (DVC)          \n\nAuthor : Pierre Lhuissier\n\nContact : pierre.lhuissier@simap.grenoble-inp.fr\n\n***************************************************************\n\n" ;

    //LINFO << "nbarg=" << nbarg;
    //for(int i=0; i<nbarg; i++)
    //    LINFO << "argv[" << i << "]=" << argv[i];
    //if(nbarg>1)
    //{
    //  LINFO << ((string(argv[1]))==(string("--help")));
    //  LINFO << ((string(argv[1]))==(string("-h")));
    //}

    Parameters *param;
    param = (Parameters *)malloc(sizeof(Parameters));
    if((nbarg<2)||((nbarg==2)&&(((string(argv[1]))==(string("--help")))||((string(argv[1]))==(string("-h"))))))
    {
	LINFO << "Usage :  Tracking inputFile (use input file)";
        LINFO << "         Tracking -g inputFile (generate input file)";
        //LINFO << displayMan("Tracking");
        return 0;
    }
    else if(nbarg==2)
    {
        LINFO << "Read parameters from " << argv[1] << "\n";
        //Configuration file
        readConfig(argv[1], param);
        //LINFO << "Parameters readden";
    }
    else
    {
        LINFO << "Generate input file " << argv[2];
        generateInputFile("Tracking",argv[2]);
        return 0;
    }
	
    LINFO << "\n***************************************************************\n" ;
  
    LINFO << "Create Time Serie"; 
    TS<P,P>* ts = new TS<P,P>(param);
	

    LINFO << "Read points to track"; 
    ts->readPtrs();
    LINFO << "\n***************************************************************\n" ;
    //LINFO << "Read points files OK";

    if(param->precorrel)
        ts->readPreCorrel();

    ts->correl();
    
    if(param->rmbad)
    {
    	LINFO << "Remove untracked points";
        ts->removeUntracked();
    }
    
    LINFO << "\n***************************************************************\n" ;

    //write Volume
    LINFO << "Writing some results ...";
    ts->writePtrs();
    //ts->writePgs();
    
    if(param->deplfield)
        ts->computeDeplField();
    
    if(param->mesh)
        ts->computeMesh();
 
    LINFO << "\n***************************************************************\n" ;
    LINFO << "Finished!!!";
    LINFO << "\n***************************************************************\n" ;
    finalize();
   
}



//********************************************************************************************

/**

int main_old(int nbarg, char* argv[]) {

    init("./");

    LINFO << "\n*******************************************\n\n               Tracking of points (DVC)          \n\nAuthor : Pierre Lhuissier\n\nContact : pierre.lhuissier@simap.grenoble-inp.fr\n\n*******************************************\n\n" ;

    Parameters *param;
	param = (Parameters *)malloc(sizeof(Parameters));
    if(nbarg==2)
    {
        LINFO << "Read parameters from " << argv[1];
	    //Configuration file
	    readConfig(argv[1],param);
        LINFO << "Parameters readden";
    }
    else
	{
		LINFO << "Usage :  Tracking input.file";
        //directory images_prefix images_suffix points_prefix points_suffix first_volume last_volume inter_vol correlDomain max_depl_XY max_depl_Z correl_thr";

		return 0;
	}
   
 
    //init(param->Directory);
		
    //cout << "Directory initiated" << endl;
	
	int nombreExpe = (int)((param->lastvol-param->firstvol)/param->intervol + 1);
	int startExpe = ceil((1.*param->restart-param->firstvol)/param->intervol);


	vector<string> Num = vector<string>();
    vector<int> numReal = vector<int>();
	for(int i=param->firstvol;i<=param->lastvol;i+=param->intervol)
	{
        numReal.push_back(i);
		char numscan[3];
		sprintf(numscan,"%03d",i);
		Num.push_back(string(numscan));
	}
	
	vector<string> Num4 = vector<string>();
	for(int i=param->firstvol;i<=param->lastvol;i+=param->intervol)
	{
		char numscan[4];
		sprintf(numscan,"%04d",i);
		Num4.push_back(string(numscan));
	}
	

    vector<PointsSet> global;
    if(param->postprocess)
    {
        LINFO << "Read points with already performed correlation";
        vector<vector<Point*> > *rawdata = new vector<vector<Point*> >();   
        LINFO << "Reading points files ...";
   		vector<Point*> raw1 = readCorrelManuPtsFile(string(param->Directory)+param->prefixpts+Num[0]+param->suffixpts,numReal[0]);
		rawdata->push_back(raw1);
		PointsSet* rawps1 = new PointsSet(string(param->Directory)+param->prefiximage+Num.at(0)+param->suffiximage, raw1);
	    global.push_back(*rawps1);
        for(int i=1; i<nombreExpe; ++i)
        {
			vector<Point*> raw = readPtsFile(string(param->Directory)+param->prefixpts+Num[i]+"_cor"+param->suffixpts,numReal[i]);
			rawdata->push_back(raw);
			PointsSet* rawps = new PointsSet(string(param->Directory)+param->prefiximage+Num.at(i)+param->suffiximage, raw);
	        global.push_back(*rawps);
        }
        global[0].totalCorrel(global); 
    }
    else
    {   
        vector<PointsSet> *dataparam = new vector<PointsSet>();
        vector<vector<Point*> > *rawdata = new vector<vector<Point*> >();   
        LINFO << "Reading points files ...";
        vector<Point*> raw1 = readCorrelManuPtsFile(string(param->Directory)+param->prefixpts+Num[0]+param->suffixpts,numReal[0]);
		rawdata->push_back(raw1);
		PointsSet* rawps1 = new PointsSet(string(param->Directory)+param->prefiximage+Num.at(0)+param->suffiximage, raw1);
        dataparam->push_back(*rawps1);
	    global.push_back(*rawps1);
        for(int i=1; i<=startExpe; ++i)
        {
			vector<Point*> raw = readPtsFile(string(param->Directory)+param->prefixpts+Num[i]+"_cor"+param->suffixpts,numReal[i]);
			rawdata->push_back(raw);
			PointsSet* rawps = new PointsSet(string(param->Directory)+param->prefiximage+Num.at(i)+param->suffiximage, raw);
	        rawps->setScan(numReal[i]);
            dataparam->push_back(*rawps);
	        global.push_back(*rawps);
        }

        for(int i=startExpe+1; i<nombreExpe; ++i)
        {
            //LINFO << "ReadCorrelManuPts " << param->prefixpts << Num[0] << param->suffixpts;
			vector<Point*> raw = readCorrelManuPtsFile(string(param->Directory)+param->prefixpts+Num[0]+param->suffixpts,numReal[i]);
			rawdata->push_back(raw);
            //LINFO << "Create PS" << param->prefiximage << Num[0] << param->suffiximage;
			PointsSet* rawps = new PointsSet(string(param->Directory)+param->prefiximage+Num.at(i)+param->suffiximage, raw);
	        rawps->setScan(numReal[i]);
            dataparam->push_back(*rawps);
        }
        //writeVectorPointsSet(Directory+"TrackingFiles/",prefiximage+"_param",*param);
        LDEBUG << "Read points files OK";
        //LINFO << "Here we are ...";

        vector<vector<vector<double> > > deplp;
        if(param->precorrel)
        {
            LINFO << "precorrel mode is on";
            for(int i=0; i<nombreExpe-1; ++i)
            {
    	        deplp.push_back(readFile(string(param->Directory)+param->prefixdepl+Num4[i]+"_"+Num4[i+1]+param->suffixdepl));
    	        LINFO << "Read " << param->prefixdepl << Num4[i] << "_" << Num4[i+1] << param->suffixdepl;
            }
        }
 
        for(int i=startExpe; i<nombreExpe-1; ++i)
        {
            PointsSet checkedCorrel;
            LINFO << "Matching scans " << Num[i] << " and " << Num[i+1] << " ...";
            
            //Reference Points pool (from reference scan)
            PointsSet poolref = (*dataparam)[i];
            LINFO << "Scan " << Num[i] << " contains " << poolref.Size() << " points";

            //Second Points pool (from next scan)
            PointsSet poolfinal = (*dataparam)[i+1];
            LINFO << "Scan " << Num[i+1] << " contains " << poolfinal.Size() << " points";

            //For each particle of reference pool look for the best correlated particles of second pool (forward correlation)
            if(param->precorrel)
            {
                checkedCorrel = poolref.findBestCorrel_DICGPU(poolfinal, param->correlDomain, param->thrXY, param->thrZ, param->correlThr, deplp[i]);
            }
            else
            {
                if(param->usetex)
                {
                    LDEBUG << "Use texture";
                    checkedCorrel = poolref.findBestCorrel_DICGPU_texture(poolfinal, param->correlDomainX, param->correlDomainY, param->correlDomainZ, param->thrX, param->thrY, param->thrZ, param->correlThr);
                }
                else
                {
                     if(param->aniso)
                        checkedCorrel = poolref.findBestCorrel_DICGPU_aniso(poolfinal, param->correlDomainX, param->correlDomainY, param->correlDomainZ, param->thrX, param->thrY, param->thrZ, param->correlThr);
                    else
                        checkedCorrel = poolref.findBestCorrel_DICGPU(poolfinal, param->correlDomain, param->thrXY, param->thrZ, param->correlThr);
                }
            }
            LINFO << "First iteration : positions found." ;
    
            LINFO << checkedCorrel.countValid(param->correlThr) << "/" << checkedCorrel.Size() << " points ketp";	

            if(param->optimize){	
                LINFO << "Optimizing positions.";
                if(param->usetex)
                {
                    LDEBUG << "Use texture";
                    checkedCorrel = poolref.optimizeCorrel_DICGPU_texture(checkedCorrel, param->correlDomainX, param->correlDomainY, param->correlDomainZ, param->thrX/5, param->thrY/5, param->thrZ/5, param->correlThr, param->regulThrDist);
                }
                else
                {
                    if(param->aniso)
                        checkedCorrel = poolref.optimizeCorrel_DICGPU_aniso(checkedCorrel, param->correlDomainX, param->correlDomainY, param->correlDomainZ, param->thrX/5,param->thrY/5,param->thrZ/5, param->correlThr, param->regulThrDist);
                    else
                        checkedCorrel = poolref.optimizeCorrel_DICGPU(checkedCorrel, param->correlDomain, param->thrXY/5, param->correlThr, param->regulThrDist);
                }
                LINFO << "New positions found.";
            }

	        checkedCorrel.setScan(numReal[i+1]);
 
            if(param->rmbad)
            {   
                LINFO << "Removing wrong points";
                checkedCorrel = checkedCorrel.subSetCorrel(param->correlThr);
                (*dataparam)[i+1] = (*dataparam)[i+1].subSet(checkedCorrel); 
            }
   	        checkedCorrel.writeToCorrelManuPtsFile(string(param->Directory)+param->prefixpts+Num[i+1]+param->suffixpts);
   	        checkedCorrel.writeToPtsFile(string(param->Directory)+param->prefixpts+Num[i+1]+"_cor"+param->suffixpts);
            global.push_back(checkedCorrel);
            checkedCorrel.writeVTKDisplacement(string(param->Directory)+param->prefixpts+Num[i+1]+".vtk",global[0],global[i]);
            writePointsFiles(string(param->Directory)+param->prefixpts+"point_", param->suffixpts, global,param->append);
        }
    }

     

    LINFO << "remove untracked points";
    for(int i=nombreExpe-2; i>=0; --i)
    {
        LDEBUG << "Scan " << Num[i];
        global[i] = global[i].subSet(global[nombreExpe-1]);
        global[i].renumber();
   	    global[i].writeToCorrelManuPtsFile(string(param->Directory)+param->prefixpts+Num[i]+"_post"+param->suffixpts);
    }
    LDEBUG << "Scan " << Num[nombreExpe-1];
    global[nombreExpe-1].renumber();
    global[nombreExpe-1].writeToCorrelManuPtsFile(string(param->Directory)+param->prefixpts+Num[nombreExpe-1]+"_post"+param->suffixpts);


    //write Volume
    LINFO << "Writing some results ...";
    string globalName = string(param->Directory)+param->prefiximage + "_track_global_info.dat";
    writevectorPointsSetGlobal(globalName,global);
    
    if(param->deplfield)
    {
        LINFO << "Computing displacement field on whole image";
        for(int i=startExpe+1; i<nombreExpe; ++i)
        {
            LINFO << "Scan " << Num[i];
            PostProc postproc = PostProc(param, string(param->Directory)+param->prefixmask+Num[i]+param->suffixmask,global[startExpe],global[i]);
            postproc.init();
            postproc.calcDeplField();
            postproc.applyMask();
            //postproc.checkDeplField();
            //postproc.writeDeplField(string(param->Directory)+param->prefiximage+Num[i]+"_depl_" ,".raw",3);
            postproc.writeEpsField(string(param->Directory)+param->prefiximage+Num[i]+"_ezz.raw");
            postproc.finalize();
        }
    }
    
   
    if(param->mesh)
    {
        vector<PointsSet> *meshparam = new vector<PointsSet>();
        vector<vector<Point*> > *meshdata = new vector<vector<Point*> >();   
        LINFO << "Reading mesh file ...";
        int meshDimX=0;
        int meshDimY=0;
        int meshDimZ=0;
        vector<Point*> rawa = readMeshPtsFile(string(param->Directory)+param->meshname,0, meshDimX, meshDimY, meshDimZ);
		meshdata->push_back(rawa);
		PointsSet* rawps1 = new PointsSet(string(param->Directory)+param->prefiximage+Num.at(0)+param->suffiximage, rawa);
        rawps1->setStackdimX(meshDimX);
        rawps1->setStackdimY(meshDimY);
        rawps1->setStackdimZ(meshDimZ);

        meshparam->push_back(*rawps1);

        for(int i=startExpe+1; i<nombreExpe; ++i)
        {
            vector<Point*> raw = readMeshPtsFile(string(param->Directory)+param->meshname,i,meshDimX,meshDimY,meshDimZ);
			meshdata->push_back(raw);
			PointsSet* rawps = new PointsSet(string(param->Directory)+param->prefiximage+Num.at(i)+param->suffiximage, raw);
	        rawps->setScan(numReal[i]);
            rawps->setStackdimX(meshDimX);
            rawps->setStackdimY(meshDimY);
            rawps->setStackdimZ(meshDimZ);
            meshparam->push_back(*rawps);
        }
        //writeVectorPointsSet(Directory+"TrackingFiles/",prefiximage+"_param",*param);
        LDEBUG << "Read points files OK";
            
        LINFO << "Computing mesh";
        int currentmesh=1;
        PointsSet meshref0 = (*meshparam)[0];
        PointsSet meshref = (*meshparam)[0];
        PointsSet meshcur = (*meshparam)[currentmesh];
        meshref.computeSafeMesh(meshcur,global[0],global[startExpe],param->correlThr, param->regulThrDist, param->regulDist);
        meshcur.writeVTKMesh(string(param->Directory)+param->prefixpts+Num[startExpe]+".vtu",meshref0,meshref);
        LINFO << "Mesh state " << startExpe;
        currentmesh++;
        for(int i=startExpe+1; i<=nombreExpe; ++i)
        {
            meshref = meshcur;
            meshcur = (*meshparam)[currentmesh];
            meshref.computeSafeMesh(meshcur,global[i-1],global[i],param->correlThr, param->regulThrDist, param->regulDist);
            meshcur.writeVTKMesh(string(param->Directory)+param->prefixpts+Num[i]+".vtu",meshref0,meshref);
            LINFO << "Mesh state " << i;
        }

    }
 
    LINFO << "Finished!!!";
	finalize();
   
}

*/


