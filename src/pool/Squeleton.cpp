#include "Squeleton.h"

using namespace std;


//Constructor and destructor
Squeleton::Squeleton() {
	this->name = "";
	this->nodes = new list<Node*> ();
	this->struts = new list<Strut*> ();
}

Squeleton::~Squeleton() {
	this->name="";
	this->nodes->clear();
    delete nodes;
    this->struts->clear();
    delete struts;
}

//Getter and setter
string  Squeleton::getName() const {
		return name;
}

void Squeleton::setName(string name) {
		this->name = name;
	}

list<Node*>*  Squeleton::getNodes()
{
		return this->nodes;
}

list<Strut*>*  Squeleton::getStruts()
{
		return this->struts;
}

list<Node*>*  Squeleton::getConnectedNodes(Node* n)
{
    list<Node*> *cn = new list<Node*>();
    for (list<Strut*>::iterator sit=this->struts->begin(); sit!=this->struts->end(); ++sit)
	{
				if((*sit)->contains(n))
                    cn->push_back((*sit)->getOtherNode(n));
	}
	return cn;
}

list<Node*>*  Squeleton::getConnectedNodes(Node* n, list<Node*>* l)
{
    list<Node*> *cn = new list<Node*>();
    for (list<Strut*>::iterator sit=this->struts->begin(); sit!=this->struts->end(); ++sit)
	{
				if((*sit)->contains(n))
                {
	                Node* other = (*sit)->getOtherNode(n);
                    for (list<Node*>::iterator pit=l->begin(); pit!=l->end(); ++pit)
                    {
                        if((*pit)->getColor() == other->getColor())
                            cn->push_back(other);
                    }
                }
    }
	return cn;
}

Node* Squeleton::getNode(int i){
	int j=0;
	for (list<Node*>::iterator pit=this->nodes->begin(); pit!=this->nodes->end(); ++pit, ++j)
	{
		if(i==j)
			return *pit;
	}
	Node* emptyNode(0);
	return emptyNode;
}

Node* Squeleton::getNodeByColor(int i){
	for (list<Node*>::iterator pit=this->nodes->begin(); pit!=this->nodes->end(); ++pit)
	{
		if((*pit)->getColor()==i)
			return *pit;
	}
	Node* emptyNode(0);
	return emptyNode;
}

Strut* Squeleton::getStrut(int i){
	int j=0;
	for (list<Strut*>::iterator pit=this->struts->begin(); pit!=this->struts->end(); ++pit, ++j)
	{
		if(i==j)
			return *pit;
	}
	Strut* emptyStrut(0);
	return emptyStrut;
}


/**
list<Node*>*  Squeleton::rankLocality( int Thr )
{
		list<Node*> *pool = new list<Node*>();
		for (list<Node*>::const_iterator pit=this->nodes->begin(); pit!=this->nodes->end(); ++pit)
		{
				 pool->push_back(&**pit);
		}
		
        list<Node*> *ranked = new list<Node*>();
                    
        ranked->push_back(&**(pool->begin()));
        pool->remove(&**(pool->begin()));
	    	
        for (list<Node*>::iterator pit=ranked->begin(); pit!=ranked->end(); ++pit)
		{ 
            bool found = false;
            double dist = 100000000000;
            list<Node*>::iterator pitpn = pool->begin();
            list<Node*>* connected = this->getConnectedNodes(*pit);
            for (list<Node*>::iterator pitp=connected->begin(); pitp!=connected->end())
            {
                double locdist =  (*pit)->getCoord().dist((*pitp)->getCoord());
                if((locdist < dist)&&(locdist>Thr))
                {
                        dist = locdist;
                        pitpn = pitp;
                }
                if(locdist < Thr)
                {
                    ranked->push_back(&**pitp);
                    pitp = pool->erase(pitp);
                    found = true;
                }
                else
                    ++pitp;
            }
            if((!found)&&!(pool->empty()))
            {
                LDEBUG << "Used closest";
                ranked->push_back(&**pitpn);
                pool->erase(pitpn);
            }
        }
             
		return ranked;
}
*/

list<Node*>*  Squeleton::rankLocalityNode()
{
        LDEBUG << "Rank nodes";
		list<Node*> *pool = new list<Node*>();
		for (list<Node*>::const_iterator pit=this->nodes->begin(); pit!=this->nodes->end(); ++pit)
		{
				 pool->push_back(&**pit);
		}
		
        list<Node*> *ranked = new list<Node*>();
        

        int color = (*(pool->begin()))->getColor();
        ranked->push_back(&**(pool->begin()));
        pool->remove(&**(pool->begin()));
        
        // LDEBUG << "Rank node " << color;
	    	
        for (list<Node*>::iterator pit=ranked->begin(); (!(pool->empty())) && (pit!=ranked->end()); ++pit)
		{ 
            for (list<Strut*>::iterator sit=this->struts->begin(); sit!=this->struts->end(); ++sit)
	        {
				if((*sit)->contains(*(pit)))
                {
	                Node* other = (*sit)->getOtherNode(*(pit));
                    color = other->getColor();
                    bool found = false;
                    list<Node*>::iterator pitn=pool->begin();
                    for (list<Node*>::iterator pitt=pool->begin(); (!found) && (pitt!=pool->end());++pitt)
                    {
                        if((*pitt)->getColor() == color)
                        {
                           found = true;
                           pitn = pitt;
                        }
                    }
                    if(found)
                    {
                        ranked->push_back(&**(pitn));
                        pitn = pool->erase(pitn);
                        //LDEBUG << "Rank node " << color;
                    }
                }
            }
        }
        
        pool = NULL;

		return ranked;
}

list<Node*>*  Squeleton::rankLocalityFull()
{
        LDEBUG << "Rank nodes";
		list<Node*> *pool = new list<Node*>();
		for (list<Node*>::const_iterator pit=this->nodes->begin(); pit!=this->nodes->end(); ++pit)
		{
				 pool->push_back(&**pit);
		}
		
        list<Node*> *ranked = new list<Node*>();
        

        int color = (*(pool->begin()))->getColor();
        ranked->push_back(&**(pool->begin()));
        pool->remove(&**(pool->begin()));
        
       // LDEBUG << "Rank node " << color;
	    	
        for (list<Node*>::iterator pit=ranked->begin(); (!(pool->empty())) && (pit!=ranked->end()); ++pit)
		{ 
            for (list<Strut*>::iterator sit=this->struts->begin(); sit!=this->struts->end(); ++sit)
	        {
                list<Strut*> * substruts = (*sit)->getSubstruts();
                for (list<Strut*>::iterator ssit=substruts->begin(); ssit!=substruts->end(); ++ssit)
				{
                    if((*ssit)->contains(*(pit)))
                    {
	                    Node* other = (*ssit)->getOtherNode(*(pit));
                        color = other->getColor();
                        bool found = false;
                        list<Node*>::iterator pitn=pool->begin();
                        for (list<Node*>::iterator pitt=pool->begin(); (!found) && (pitt!=pool->end());++pitt)
                        {
                            if((*pitt)->getColor() == color)
                            {
                                found = true;
                                pitn = pitt;
                            }
                        }
                        if(found)
                        {
                            ranked->push_back(&**(pitn));
                            pitn = pool->erase(pitn);
                            //LDEBUG << "Rank node " << color;
                        }
                    }
                }
            }
        }
        
        pool = NULL;

		return ranked;
}



bool Squeleton::writeVTK(string fileName)
{
	ofstream fichier(fileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier

	if(fichier)  // si l'ouverture a réussi
	{
         fichier << "# vtk DataFile Version 3.1" << endl;
         fichier << "Squeleton" << endl;
         fichier << "ASCII" << endl;
         fichier << "DATASET POLYDATA" << endl;
         fichier << "POINTS " << this->nodes->size() << " FLOAT" << endl;
	     for (list<Node*>::iterator pit=this->nodes->begin(); pit!=this->nodes->end(); ++pit)
	     {
	    	 fichier << (*pit)->getCoord().getX() << " ";
             fichier << (*pit)->getCoord().getY() << " ";
             fichier << (*pit)->getCoord().getZ() << endl;
	     }
         //fichier << endl;
         fichier << "LINES " << this->struts->size() << " " << 3*this->struts->size() << endl;
	     for (list<Strut*>::iterator sit=this->struts->begin(); sit!=this->struts->end(); ++sit)
         {
                fichier << "2 " << ((*sit)->getNode(1)->getColor()) - 1  << " " << ((*sit)->getNode(2)->getColor()) - 1  << endl;
         }
	     fichier << endl;

                fichier.close();  // on referme le fichier
	 }
	 else  // sinon
	    LERROR << "Erreur à l'ouverture !";

}

bool Squeleton::writeVTKdef(string fileName, Squeleton* reference, Squeleton* previous)
{
    ofstream fichier(fileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier

	if(fichier)  // si l'ouverture a réussi
	{
         fichier << "# vtk DataFile Version 3.1" << endl;
         fichier << "Squeleton" << endl;
         fichier << "ASCII" << endl;
         fichier << "DATASET POLYDATA" << endl;
         fichier << "POINTS " << this->nodes->size() << " FLOAT" << endl;
	     for (list<Node*>::iterator pit=this->nodes->begin(); pit!=this->nodes->end(); ++pit)
	     {
	    	 fichier << (*pit)->getCoord().getX() << " ";
             fichier << (*pit)->getCoord().getY() << " ";
             fichier << (*pit)->getCoord().getZ() << endl;
	     }
         //fichier << endl;
         int nbStruts = 0;
	     for (list<Strut*>::iterator sit=this->struts->begin(); sit!=this->struts->end(); ++sit)
         {
                nbStruts += ((*sit)->getSubstruts())->size();
         }
         fichier << "LINES " << nbStruts << " " << 3*nbStruts << endl;
	     for (list<Strut*>::iterator sit=this->struts->begin(); sit!=this->struts->end(); ++sit)
         {
                list<Strut*> * substruts = (*sit)->getSubstruts();
                for (list<Strut*>::iterator ssit=substruts->begin(); ssit!=substruts->end(); ++ssit)
                {
                    fichier << "2 " << ((*ssit)->getNode(1)->getColor()) - 1  << " " << ((*ssit)->getNode(2)->getColor()) - 1  << endl;
                }
         }
         
         fichier << "POINT_DATA " << this->nodes->size() << endl;
         fichier << "VECTORS GlobalDisplacement FLOAT" << endl;
         list<Node*>::iterator pitRef=reference->getNodes()->begin();
         for (list<Node*>::iterator pit=this->nodes->begin(); pit!=this->nodes->end(); ++pit)
	     {
                    fichier << ((*pit)->getCoord().getX() - (*pitRef)->getCoord().getX()) << " ";
                    fichier << ((*pit)->getCoord().getY() - (*pitRef)->getCoord().getY()) << " ";
                    fichier << ((*pit)->getCoord().getZ() - (*pitRef)->getCoord().getZ()) << endl;
                    ++pitRef;
	     }
                //fichier << endl;
         LDEBUG << "displacement written";
         fichier << "VECTORS IncrementalDisplacement FLOAT" << endl;
         list<Node*>::iterator pitL=previous->getNodes()->begin();
         for (list<Node*>::iterator pit=this->nodes->begin(); pit!=this->nodes->end(); ++pit)
	     {
                    fichier << ((*pit)->getCoord().getX() - (*pitL)->getCoord().getX()) << " ";
                    fichier << ((*pit)->getCoord().getY() - (*pitL)->getCoord().getY()) << " ";
                    fichier << ((*pit)->getCoord().getZ() - (*pitL)->getCoord().getZ()) << endl;
                    ++pitL;
	     }

         fichier << "TENSORS Strain FLOAT" << endl;
         for (list<Node*>::iterator pit=this->nodes->begin(); pit!=this->nodes->end(); ++pit)
	     {
            double* s = this->getStrain(reference, (*pit));

            for(int i=0; i<3; i++)
                fichier << s[i*3+0] << " " << s[i*3+1] << " " << s[i*3+2] << endl;
	        fichier << endl;
         }
                 
         fichier << "CELL_DATA " << this->struts->size() << endl;
         fichier << "SCALARS Strain FLOAT 1" << endl;
         fichier << "LOOKUP_TABLE default" << endl;
         list<Strut*>::iterator sitRef=reference->getStruts()->begin();
         for (list<Strut*>::iterator sit=this->struts->begin(); sit!=this->struts->end(); ++sit)
	     {
                    fichier << ((*sit)->getStrain((**sitRef))) << endl;
                    ++sitRef;
	     }
          fichier.close();  // on referme le fichier
        return true;
     }
	 else  // sinon
	    LERROR << "Erreur à l'ouverture !";
    return false;
}

  double* Squeleton::getStrain(Squeleton* ref, Node* curNode) {
        int nodecolor = curNode->getColor();
        
        //LDEBUG << "Estimate strain for node " << nodecolor;
        
        Node* refNode = ref->getNodeByColor(nodecolor);
        list<Node*>*  refnodes =  ref->getConnectedNodes(refNode);
        list<Node*>*  curnodes =  this->getConnectedNodes(curNode);
        double* X = new double[9];
        double* Y = new double[9];
        double* Id= new double[9];
        double* e = new double[9];
        for(int j=0; j<3; j++){
            for(int i=0; i<3; i++){
                X[j*3+i]=0;
                Y[j*3+i]=0;
                if(i==j)
                    Id[j*3+i]=1;
                else
                    Id[j*3+i]=0;
            }
        }
        
        //LDEBUG << "Initialised lists";

        list<Node*>::iterator nitref=refnodes->begin();
        for (list<Node*>::iterator nitcur=curnodes->begin(); nitcur!=curnodes->end(); ++nitcur, ++nitref)
        {
            Coord refl = (*nitref)->getCoord();
            refl.diff(refNode->getCoord());
            Coord curl = (*nitcur)->getCoord();
            curl.diff(curNode->getCoord());
            //if(nodecolor==1)
            //{
            //    LDEBUG << refl.toString();
            //    LDEBUG << curl.toString();
            //} 
            double* lr = refl.getVect();
            double* l = curl.getVect();
            for(int j=0; j<3; j++){
                for(int i=0; i<3; i++){
                    X[j*3+i] += l[j]*lr[i]; 
                    Y[j*3+i] += lr[j]*lr[i]; 
                }
            }
        }
            //if(nodecolor==1)
            //{
            //    LDEBUG << X[0] << " " << X[1] << " " << X[2] << " " << X[3] << " " << X[4] << " " << X[5] << " " << X[6] << " " << X[7] << " " << X[8];
            //    LDEBUG << Y[0] << " " << Y[1] << " " << Y[2] << " " << Y[3] << " " << Y[4] << " " << Y[5] << " " << Y[6] << " " << Y[7] << " " << Y[8];
            //} 
        double* Z = invert(Y);
        //LDEBUG << Z[0] << " " << Z[1] << " " << Z[2] << " " << Z[3] << " " << Z[4] << " " << Z[5] << " " << Z[6] << " " << Z[7] << " " << Z[8];

       // LDEBUG << "Computed lengths";

        for(int j=0; j<3; j++){
            for(int i=0; i<3; i++){
                e[j*3+i] = 0; 
                for(int k=0; k<3; k++){
                    e[j*3+i] += X[j*3+k]*Z[i*3+k]; 
                }
                
                e[j*3+i] -= Id[j*3+i];
            }
        }
        //if(nodecolor==1)
        //    LDEBUG << e[0] << " " << e[1] << " " << e[2] << " " << e[3] << " " << e[4] << " " << e[5] << " " << e[6] << " " << e[7] << " " << e[8];
       
       LDEBUG << "Got strains";
    
    return e;
}


void Squeleton::read(string fileName) {
	LDEBUG << "Will try to read Squeleton VTK type file " << fileName;
    ifstream File (fileName.c_str(),ios::in);
    if (!File)
    {
    	LERROR << "Error : impossible to open file " << fileName << " in read only mode...";
    }
    else
    {
	        //cout << "File opened " << endl;
    }
    vector<double> line;
    double a;
    string paramline;
        int nbColonne;
        nbColonne=3;
        stringstream sstr;
        char schar[10000];
        File.getline(schar,10000);
        File.getline(schar,10000);
        File.getline(schar,10000);
        File.getline(schar,10000);
        File.getline(schar,10000);
	    paramline = "";
        paramline+=schar;
        int nbpts=0;

        sscanf(schar,"POINTS %d FLOAT\n", &nbpts);

        LINFO << nbpts << "  points in squeleton ";

        //        File.getline(schar,10000);
        int pn=0;
        int wp=0;
        while (File.good()&&(pn<nbpts))
        {
	        paramline = "";
	        File.getline(schar,10000);
    	    paramline+=schar;
	        sstr.clear();
    	    sstr.str(paramline);
    	    if(sstr.str().size()>1)
            {
    		    for(int i=0; i<nbColonne; i++)
    		    {
			        sstr >> a;
			        line.push_back(1.*a);
		        }
		        if(!isnan(line[0]))
		        {
			        pn++;
			        (this->nodes)->push_back(new Node(0,
						(int)pn,
						Coord((double)line[0],(double)line[1],(double)line[2]),0,""));
			    }
		        else
		        {
			        wp++;
		        }
		        line.clear();
            }
        }
        
        //for (list<Node*>::iterator pit=this->nodes->begin(); pit!=this->nodes->end(); ++pit)
		//{
        //    LDEBUG << (*pit)->tostring();
        //}

        File.getline(schar,10000);
   	    paramline = "";
        paramline+=schar;
        int nbsegs=0;
        int nbval=0;

        //LDEBUG << paramline;
        sscanf(schar,"LINES %d %d\n", &nbsegs, &nbval);

        LINFO << nbsegs << "  struts in squeleton ";

        pn=0;
        wp=0;
        while (File.good()&&(pn<=nbsegs))
        {
	        paramline = "";
	        File.getline(schar,10000);
    	    paramline+=schar;
	        sstr.clear();
    	    sstr.str(paramline);
    	    if(sstr.str().size()>1)
            {
    		    for(int i=0; i<nbColonne; i++)
    		    {
			        sstr >> a;
			        line.push_back(1.*a);
		        }
		        if(!isnan(line[0]))
		        {
			        pn++;
			        (this->struts)->push_back(new Strut((int)pn,
			        		this->getNode((int)line[1]),
			        		this->getNode((int)line[2]),
			        		false));
			    }
		        else
		        {
			        wp++;
		        }
		        line.clear();
            }
        }
        //for (list<Strut*>::iterator sit=this->struts->begin(); sit!=this->struts->end(); ++sit)
		//{
        //    LDEBUG << (*sit)->tostring();
        //}
        File.close();
}

void Squeleton::update()
{
    LDEBUG << "Update";
    for (list<Node*>::iterator pit=this->nodes->begin(); pit!=this->nodes->end(); ++pit)
	{
        //LDEBUG << (*pit)->tostring();
		int nb=0;
		for (list<Strut*>::iterator sit=this->struts->begin(); sit!=this->struts->end(); ++sit)
		{
				if((*sit)->contains(*pit))
		        {
                    //LDEBUG << (*sit)->tostring();
                    nb++;
		        }
        }
        LDEBUG << "Node " << (*pit)->getColor() << " is connected to " << nb << " struts";
		(*pit)->setNumberOfStruts(nb);
	}
    LDEBUG << "Update finished";
}

void Squeleton::splitStruts(double mindist)
{

    LDEBUG << "Generate substruts";
	for (list<Strut*>::iterator sit=this->struts->begin(); sit!=this->struts->end(); ++sit)
	{
        (*sit)->generateSubstruts(mindist, *(this->nodes));
    
    }
    LDEBUG << "Split finished";
}



void Squeleton::correl_DICGPU_texture(Squeleton &pfinal, Volume &imRef, Volume &imFinal, int correlDomainX, int correlDomainY, int correlDomainZ, int distX, int distY, int distZ, double correlThr)
{
		LINFO << "Initialize GPU";
		Correl3DGPU corr = Correl3DGPU(imRef.getName().c_str(), imFinal.getName().c_str());
		corr.setfromFull(true);
		corr.setusetex(false);
		corr.setFilter(false);
		corr.setNormalize(true);
		corr.setIntersect(false);
		corr.setReducedWindow(false);
		corr.setdisplThr(false, distZ, distX, distY);
		corr.setDomain(correlDomainX);
        corr.setWindow("none");
        //corr.setWindow("hanning");

		corr.setFFT(false);
        corr.setaniso(true);
        corr.setHalfPatternX(correlDomainX);
        corr.setHalfPatternY(correlDomainY);
        corr.setHalfPatternZ(correlDomainZ);
        corr.setDisplacementX(distX);
        corr.setDisplacementY(distY);
        corr.setDisplacementZ(distZ);
        corr.setCorrelThr(correlThr);

        corr.init();

		LINFO << "Correlation parameters:";
        LINFO << corr.optionsToString();
        LINFO << "Setting Ref";
		corr.setFullRef(imRef.getName().c_str());
		LINFO << "Setting Target";
		corr.setFullTarget(imFinal.getName().c_str());
		clock_t startTime = clock();
		clock_t currentTime = clock();
		int globalTime = diffclock(currentTime,startTime);
		int deltaTime = diffclock(clock(),currentTime);
		int i=0;
		LINFO << "Ready to start correlations";
		list<Node*>::iterator pitF=(pfinal).getNodes()->begin();
        for (list<Node*>::iterator pit=this->nodes->begin(); pit!=this->nodes->end(); ++pit, ++i)
		{
            LDEBUG << (*pit)->tostring();
            if(i%100 == 0)
			{
				deltaTime = diffclock(clock(),currentTime);
				currentTime = clock();
				globalTime = diffclock(currentTime,startTime);
				LINFO << "Proceeded 100 points in " << deltaTime << "ms ; " << i <<"/" << this->nodes->size() <<" in " << globalTime/1000 << "s";
			}
            corr.setRef((*pit)->getCoord().getZ(), (*pit)->getCoord().getY(), (*pit)->getCoord().getX());
            P potential = (*pit)->findBestCorrel_DICGPU(corr);
			(*pitF)->PUpdate(potential);
	        ++pitF;
	    }
		//pfinal.countValid(correlThr);
        corr.freeFullImg();
		corr.free();
		//return pfinal;
	}

void Squeleton::correl_DICGPU_rigid_guiding(Squeleton &pfinal, Volume &imRef, Volume &imFinal, int correlDomainX, int correlDomainY, int correlDomainZ, int distX, int distY, int distZ, double correlThr, int initThrXfactor, int initThrYfactor, int initThrZfactor)
{
	    Correl3DGPU corr2 = Correl3DGPU(imRef.getName().c_str(), imFinal.getName().c_str());
		corr2.setfromFull(true);
		corr2.setFilter(false);
		corr2.setNormalize(true);
		corr2.setIntersect(false);
		corr2.setReducedWindow(false);
		corr2.setdisplThr(false, distZ, distX, distY);
		corr2.setDomain(correlDomainX);
        corr2.setWindow("none");
        //corr.setWindow("hanning");

		corr2.setFFT(false);
        corr2.setaniso(true);
        corr2.setHalfPatternX(correlDomainX);
        corr2.setHalfPatternY(correlDomainY);
        corr2.setHalfPatternZ(correlDomainZ);
        corr2.setDisplacementX(distX);
        corr2.setDisplacementY(distY);
        corr2.setDisplacementZ(distZ);
        corr2.setCorrelThr(correlThr);

        corr2.init();

		LINFO << "Correlation parameters:";
        LINFO << corr2.optionsToString();
        LINFO << "Setting Ref";
		corr2.setFullRef(imRef.getName().c_str());
		LINFO << "Ref contains " << this->nodes->size() <<" nodes and " << this->struts->size() << "struts";

		LINFO << "Setting Target";
		corr2.setFullTarget(imFinal.getName().c_str());
		LINFO << "Ref contains " << pfinal.getNodes()->size() <<" nodes and " << pfinal.getStruts()->size() << "struts";
		clock_t startTime = clock();
		clock_t currentTime = clock();
		int globalTime = diffclock(currentTime,startTime);
		int deltaTime = diffclock(clock(),currentTime);
		int i=0;
		LINFO << "Start precorrelation for rigid estimation";
        list<Node*>::iterator pit0=this->nodes->begin(); 
        corr2.setImRef((int)(*pit0)->getCoord().getZ(), (int)(*pit0)->getCoord().getY(), (int)(*pit0)->getCoord().getX());
        if(false)
        {
        if((*pit0)->getRoiX()>0)
            corr2.setCropHalfPatternX((*pit0)->getRoiX());
        if((*pit0)->getRoiY()>0)
            corr2.setCropHalfPatternY((*pit0)->getRoiY());
        if((*pit0)->getRoiZ()>0)
            corr2.setCropHalfPatternZ((*pit0)->getRoiZ());
        }

        Coord optipos = Coord(0,0,0);
        double offsetcor = -1;
        for(int k=-initThrZfactor+1; k<initThrZfactor; k+=2){
            for(int j=-initThrYfactor+1; j<initThrYfactor; j+=2){
                for(int l=-initThrXfactor+1; l<initThrXfactor; l+=2){
                    Coord offsetactu = Coord(l*distX,j*distY,k*distZ); 
                    LINFO << offsetactu.toString();
                    P guidingP = (*pit0)->findBestCorrel_DICGPU(corr2,(*pit0)->getCoord().shift(offsetactu));
                    if(guidingP.getCorrel()>offsetcor)
                    {
                        offsetcor=guidingP.getCorrel();
                        optipos=guidingP.getCoord();
                    }
                }
            }
        }
       
        Coord offsetf = optipos.diff((*pit0)->getCoord());
        Coord offset = offsetf.getInt();
		
        LINFO << "Applied offset will be (" << offset.getX() << "," << offset.getY() << "," << offset.getZ() << ")";
 
		LINFO << "Ready to start correlations";
		//typename list<T*>::iterator pitF=(pfinal).getP()->begin();
        //list<Node*>* pool = this->rankLocalityNode();
        list<Node*>* pool = this->rankLocalityFull();
        //list<Node*>* pool = this->rankLocality((distX+distY+distZ));
                
        for (list<Node*>::iterator pit= pool->begin(); pit!=pool->end(); ++pit, ++i)
		{
          	if(i%100 == 0)
			{
				deltaTime = diffclock(clock(),currentTime);
				currentTime = clock();
				globalTime = diffclock(currentTime,startTime);
				LINFO << "Proceeded 100 points in " << deltaTime << "ms ; " << i <<"/" << this->nodes->size() <<" in " << globalTime/1000 << "s";
			}
            corr2.setImRef((int)(*pit)->getCoord().getZ(), (int)(*pit)->getCoord().getY(), (int)(*pit)->getCoord().getX());
            if((*pit)->getRoiX()>0)
                corr2.setCropHalfPatternX((*pit)->getRoiX());
            if((*pit)->getRoiY()>0)
                corr2.setCropHalfPatternY((*pit)->getRoiY());
            if((*pit)->getRoiZ()>0)
                corr2.setCropHalfPatternZ((*pit)->getRoiZ());
            P potential;
            if(i==0)
            {
                potential = (*pit)->findBestCorrel_DICGPU(corr2, (*pit)->getCoord().shift(offset));
            }
            else
            {
                LDEBUG << "Point " << (*pit)->getColor() << " start position estimate";
                Coord target = this->estimatePosition(*pit, pfinal, correlThr, true);
                //Coord target = this->estimatePositionGauss((*pit)->getCoord(), pfinal, correlThr, 100, 300 , true);
                Coord targetInt = target.getInt();
                //LINFO << "Offset (" << targetInt.getX()-(int)(*pit)->getCoord().getX() << "," << targetInt.getY()-(int)(*pit)->getCoord().getY() << "," << targetInt.getZ()-(int)(*pit)->getCoord().getZ() << ")";   
                potential = (*pit)->findBestCorrel_DICGPU(corr2, targetInt);
			}
            LDEBUG << potential.write();
            Node* pitF = pfinal.getNodeByColor((*pit)->getColor());
            (pitF)->PUpdate(potential);
	    }
        corr2.freeFullImg();
		corr2.free();
	}


Coord Squeleton::estimatePosition(Node* n, Squeleton &pfinal, double correlThr, bool useTotalCorrel)
{
	list<Node*>* initsubset = this->getConnectedNodes(n);

    Coord ca = n->getCoord();

    Coord target = Coord(0,0,0);
    int nb=0;

    for (list<Node*>::iterator pit=initsubset->begin(); pit!=initsubset->end(); ++pit)
    {
        Node* pot = pfinal.getNodeByColor((*pit)->getColor());
        if((useTotalCorrel && pot->getTotalCorrel()>correlThr)||(!useTotalCorrel && pot->getCorrel()>correlThr))
        {
            Coord cf = pot->getCoord();
            Coord ref = (*pit)->getCoord().mult(-1.);
            Coord depl = cf.shift(ref);
            nb++;
            target = target.shift(depl);
        }
    }
    if(nb>0)
    {
        target = target.mult(1./nb);
        target = ca.shift(target);
        return target;
    }
    else
        return ca;
        
}


