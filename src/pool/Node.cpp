#include "Node.h"

using namespace std;

Node::Node() : P(), numberOfStruts(0), param("")
{
	//struts = new list<Strut*>();
}

Node::Node(P p) : P(), numberOfStruts(0), param("")
{
	this->Update(p);
	//struts = new list<Strut*>();
}

Node::~Node(){

}


Node::Node(int scan, int color, Coord coord, int numberOfStruts, std::string param)
{
	this->scan = scan;
	this->color = color;
	this->coord = coord;
	this->numberOfStruts = numberOfStruts;
	this->param = param;
	//struts = new list<Strut*>();
	
}


Node::Node(string linestr) {
		 double a,b,c;
		 string paramline;
		 stringstream sstr;
		 sstr.str(linestr);
		 if(sstr.str().size()>1)
		 {
		     sstr >> a;
		     if(!isnan(1.*a))
		     {
		    	 	this->scan = (int)(1.*a);
		    	 	sstr >> a;
		     		sstr >> b;
		     		sstr >> c;
		     		this->coord = Coord((double)(1.*a),(double)(1.*b),(double)(1.*c));
		     		sstr >> a;
		     		this->numberOfStruts= (int)(1.*a);
		     		ostringstream paramstr;
		     		while(sstr >> a)
		     		{
		     			paramstr << a << " ";
		     		}
		     		this->param = paramstr.str();
		            //this->cellNumber=-1;
		     }
	  	}
}

Node::Node(const string fileDirectory, const string pName) {
	this->scan = 0;
	this->coord = Coord();
	this->numberOfStruts=0;
	this->param="";

		 ostringstream fileName;
		 fileName << fileDirectory << "/" << pName;

		 ifstream File (fileName.str().c_str(),ios::in);
		    if (!File)
		    {
		    	LERROR << "Error : impossible to open file " << fileName.str() << " in read only mode...";
		    	//cerr << "Particle color is " << color << " from scan " << scan << endl;
		    }

		    double a,b,c;
		   		 string paramline="";
		   		 stringstream sstr;
		   		char schar[10000];
		   		File.getline(schar,10000);
		   		paramline+=schar;
		   		sstr.str(paramline);
		   		 if(sstr.str().size()>1)
		   		 {
		   		     sstr >> a;
		   		     if(!isnan(1.*a))
		   		     {
		   		    	 	this->scan = (int)(1.*a);
		   		    	 	sstr >> a;
		   		     		sstr >> b;
		   		     		sstr >> c;
		   		     		this->coord = Coord((double)(1.*a),(double)(1.*b),(double)(1.*c));
		   		     		sstr >> a;
		   		     		this->numberOfStruts= (int)(1.*a);
		   		     		ostringstream paramstr;
		   		     		while(sstr >> a)
		   		     		{
		   		     			paramstr << a << " ";
		   		     		}
		   		     		this->param = paramstr.str();
		   		            //this->cellNumber=-1;
		   		     }
		   	  }


}

void Node::Update(string linestr) {
	 		 double a,b,c;
	 		 string paramline;
	 		 stringstream sstr;
	 		 sstr.str(linestr);
	 		 if(sstr.str().size()>1)
	 		 {
	 		     sstr >> a;
	 		     if(!isnan(1.*a))
	 		     {
	 		    	this->scan = (int)(1.*a);
	 		    			   		    	 	sstr >> a;
	 		    			   		     		sstr >> b;
	 		    			   		     		sstr >> c;
	 		    			   		     		this->coord = Coord((double)(1.*a),(double)(1.*b),(double)(1.*c));
	 		    			   		     		sstr >> a;
	 		    			   		     		this->numberOfStruts= (int)(1.*a);
	 		    			   		     		ostringstream paramstr;
	 		    			   		     		while(sstr >> a)
	 		    			   		     		{
	 		    			   		     			paramstr << a << " ";
	 		    			   		     		}
	 		    			   		     		this->param = paramstr.str();
	 		    			   		            //this->cellNumber=-1;
	 		     }
	 	  	}
}

	 string Node::write() const{
		 ostringstream sstr;
		 sstr << this->scan << " ";
		 sstr << this->color << " ";
		 sstr << this->coord.toString() << " ";
		 sstr << this->numberOfStruts << " ";
		 sstr << this->param;
		 return sstr.str();
	 }

	 int Node::getNumberOfStruts() const {
		return numberOfStruts;
	}

	 void Node::setNumberOfStruts(int numberOfStruts) {
		this->numberOfStruts = numberOfStruts;
	}

	 void Node::addStrut() {
		 this->numberOfStruts++;;
	}

	 string Node::getParam() const {
			return param;
	}

	 string Node::tostring() const {
		ostringstream sstr;
		sstr << this->color << " ";
		sstr << this->coord.toString() << " ";
		sstr << this->numberOfStruts;
		return sstr.str();
	}
	

	
	 string Node::fullInfoTostring() const {
		ostringstream s;
		s << this->color << " ";
		s << this->param;
		return s.str();
	}
	

/**
P Node::findBestCorrel_DICGPU(Correl3DGPU& correlator)
{
		 return P::findBestCorrel_DICGPU(correlator);
}

P Node::findBestCorrel_DICGPU(Correl3DGPU& correlator, Coord target)
{
		 return P::findBestCorrel_DICGPU(correlator,target);
}

P Node::optimizeCorrel_DICGPU(Correl3DGPU& correlator, Coord target)
{
		 return P::optimizeCorrel_DICGPU(correlator, target);
}

*/

void Node::Update(Node P) {
		this->scan = P.getScan();
		this->color = P.getColor();
		this->coord = P.getCoord();
		//this->numberOfStruts=P.getNumberOfStruts();
		//this->param= P.getParam();

}
