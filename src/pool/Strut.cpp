#include "Strut.h"

using namespace std;

Strut::Strut()
{
	this->number = 0;
	this->n1 = new Node();
	this->n2 = new Node();
	this->broken = false;
    this->substruts = new list<Strut*> ();
    this->subnodes = new list<Node*> ();
}

Strut::Strut(int number, Node* n1, Node* n2, bool broken)
{
	this->number = number;
	this->n1 = n1;
	this->n2 = n2;
	this->broken = broken;
    this->substruts = new list<Strut*> ();
    this->subnodes = new list<Node*> ();
}

Strut::~Strut(){
	delete this->n1;
	delete this->n2;
    this->substruts->clear();
    delete substruts;
    this->subnodes->clear();
    delete subnodes;
}


int Strut::getNumber() const {
	return number;
}

Node* Strut::getNode(int i) {
	if(i==1)
		return this->n1;
	if(i==2)
		return this->n2;
	Node* emptyNode(0);
	return emptyNode;
}

Node* Strut::getOtherNode(Node* n) {
	if(this->n1->getColor()==n->getColor())
		return this->n2;
	if(this->n2->getColor()==n->getColor())
		return this->n1;
	Node* emptyNode(0);
	return emptyNode;
}

list<Strut*>*  Strut::getSubstruts()
{
		return this->substruts;
}

bool Strut::isBroken() {
	return this->broken;
}

void Strut::generateSubstruts(double mindist, list<Node*> &nodes)
{
    double L = this->getLength();
    int nb = ceil(L/mindist);
    LDEBUG << "Strut (" << this->n1->getColor() << "," << this->n2->getColor() << ") has " << nb << " substruts";
    Coord dl = ((this->n2->getCoord()).diff(this->n1->getCoord())).mult(1./nb);
    int pn = nodes.size() + 1;
    Coord c = this->n1->getCoord();
    for(int i=0; i<nb; i++)
    {
        Node* t1;
        Node* t2;
        if(i==0)
        {
            t1 = this->n1;
        }
        else
        {
            t1 = t2;
        }
        if(i<nb-1)
        {
            t2 = new Node(0, (int)pn, c.shift(dl) ,0,"");
            this->subnodes->push_back(t2);
            nodes.push_back(t2);
            pn++;    
        }
        else
        {
            t2 = this->n2;
        }

        this->substruts->push_back(new Strut(i, t1, t2, false));
    }
    LDEBUG << "substrut size is " << this->substruts->size();
}

double Strut::getLength() {
    return (this->n1->getCoord()).dist(this->n2->getCoord());
}

double Strut::getStrain(Strut &s) {
    double L0 = s.getLength();
    double L = this->getLength();
    if((L<0.01)||(L0<0.01))
    {
        LINFO << "Error with strut "<< this->tostring();
        LINFO << "Ref state : L0=" << L0;
        LINFO << s.getNode(1)->write();
        LINFO << s.getNode(2)->write();
        LINFO << "Current state : L=" << L;
        LINFO << this->n1->write();
        LINFO << this->n2->write();
    }
    
    return log(this->getLength()/s.getLength());
}

void Strut::setBroken(bool broken) {
	this->broken=broken;
}

bool Strut::contains(Node* n) {
	if(this->n1->getColor()==n->getColor())
		return true;
	if(this->n2->getColor()==n->getColor())
		return true;
	return false;
}

string Strut::tostring() const {
	ostringstream sstr;
	sstr << this->number << " ";
	sstr << this->n1->getColor() << " ";
	sstr << this->n2->getColor() << " ";
    sstr << this->broken;
	return sstr.str();
}
	
