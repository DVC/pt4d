#include <math.h>
#include <cuda.h>
#include "cuda_helper.h"
#include "cuda_runtime.h"
typedef unsigned int uint;

texture<unsigned char, 3, cudaReadModeNormalizedFloat> oldtex;  // 3D texture
texture<unsigned char, 3, cudaReadModeNormalizedFloat> newtex;  // 3D texture

#define CONSTANT_PI 3.141592654f
#define CONSTANT_PI_PER_DEG 0.017453293f

//int iDivUp(int a, int b);
//int iAlignUp(int a, int b);	
extern "C" void modulateConjugateAndNormalize(
		fComplex *d_Src,
		fComplex *d_Dst,
		int fftD,
		int fftH,
		int fftW,
		int padding
		);


#if TRANSLATION

__device__ inline float3 deform(
    float3* def,
    float3 X,
    float3 dX
){
    return X + def[0] +dX;
}
#endif


#if ROTATION



__device__ inline float3 rotX(
    float3*  def,
    float3 X,
    float3 dX
){
    float3 rX = make_float3(1,0,0);
    float3 rY = make_float3(0, cosf(def[1].x), -sinf(def[1].x));
    float3 rZ = make_float3(0, sinf(def[1].x), cosf(def[1].x));

    float3 dU = make_float3(dot(rX,dX), dot(rY,dX), dot(rZ,dX));
    return X + def[0] + dU;
}

__device__ inline float3 rotY(
    float3*  def,
    float3 X,
    float3 dX
){
    float3 rX = make_float3(cosf(def[1].y), 0, sinf(def[1].y));
    float3 rY = make_float3(0,1,0);
    float3 rZ = make_float3(-sinf(def[1].y), 0, cosf(def[1].y));

    float3 dU = make_float3(dot(rX,dX), dot(rY,dX), dot(rZ,dX));
    return X + def[0] + dU;
}

__device__ inline float3 rotZ(
    float3*  def,
    float3 X,
    float3 dX
){
    float3 rX = make_float3(cosf(def[1].z), -sinf(def[1].z),0);
    float3 rY = make_float3(sinf(def[1].z), cosf(def[1].z),0);
    float3 rZ = make_float3(0,0,1);

    float3 dU = make_float3(dot(rX,dX), dot(rY,dX), dot(rZ,dX));
    return X + def[0] + dU;
}

__device__ inline float3 deform(
    float3* def,
    float3 X,
    float3 dX
){
    return rotY(def, X, dX);
}
#endif

#if FIRSTORDER
__device__ inline float3 deform(
    FirstOrder def,
    float3 X,
    float3 dX
){
    //TODO dUdx, dUdy ... or dudX, dvdX... 

    float3 dU = make_float3(dot(def.dudX,dX), dot(def.dvdX,dX), dot(def.dwdX,dX));
    return X+dU+def.U;
}
#endif



////////////////////////////////////////////////////////////////////////////////
/// Position convolution kernel center at (0, 0, 0) in the image
////////////////////////////////////////////////////////////////////////////////
extern "C" __global__ void padKernel_kernel(
    float *d_Dst,
    float *d_Src,
    int fftD,
    int fftH,
    int fftW,
    int kernelD,
    int kernelH,
    int kernelW,
    int kernelZ,
    int kernelY,
    int kernelX
){
    const int z = threadIdx.x;
    const int y = blockDim.y * blockIdx.y + threadIdx.y;
    const int x = blockDim.z * blockIdx.x + threadIdx.z;

    if(z < kernelD && y < kernelH && x < kernelW){
        int kz = z - kernelZ; if(kz < 0) kz += fftD;
	    int ky = y - kernelY; if(ky < 0) ky += fftH;
        int kx = x - kernelX; if(kx < 0) kx += fftW;
        d_Dst[kz * fftW * fftH + ky * fftW + kx] = d_Src[z * kernelW * kernelH + y * kernelW + x];
    }
}

////////////////////////////////////////////////////////////////////////////////
/// Position convolution kernel center at (0, 0, 0) in the image
////////////////////////////////////////////////////////////////////////////////
extern "C" __global__ void readFromFullIm_kernel(
    float *d_Dst,
    unsigned char *d_Src,
    int fftD,
    int fftH,
    int fftW,
    int fullD,
    int fullH,
    int fullW,
    int Z,
    int Y,
    int X
){
//    const int z = threadIdx.x;
//    const int y = blockDim.y * blockIdx.y + threadIdx.y;
//    const int x = blockDim.z * blockIdx.x + threadIdx.z;
	const int x = threadIdx.x;
    const int y = blockDim.y * blockIdx.x + threadIdx.y;
    const int z = blockDim.z * blockIdx.y + threadIdx.z;

    if(z < fftD && y < fftH && x < fftW){
        int kz = Z - fftD/2 + z + 1;
        int ky = Y - fftH/2 + y + 1;
        int kx = X - fftW/2 + x + 1;
        if( kx < 0 || ky < 0 || kz < 0 || kx >= fullW ||  ky >= fullH ||  kz >= fullD )  
        {
        	d_Dst[z * fftW * fftH + y * fftW + x] = (float)0.;
        }
        else
        {
        	d_Dst[z * fftW * fftH + y * fftW + x] = (float)(d_Src[kz * fullW * fullH + ky * fullW + kx]);
        }
    }
}

extern "C" __global__ void readFromFullIm_noFFT_kernel(
    float *d_Dst,
    unsigned char *d_Src,
    int halfpattern,
    int depl,
    int fullD,
    int fullH,
    int fullW,
    int Z,
    int Y,
    int X
){
	const int x = threadIdx.x;
    const int y = blockDim.y * blockIdx.x + threadIdx.y;
    const int z = blockDim.z * blockIdx.y + threadIdx.z;

    if(z < (2*depl+2*halfpattern+1) && y < (2*depl+2*halfpattern+1) && x < (2*depl+2*halfpattern+1)){
        int kz = Z - depl - halfpattern + z;
        int ky = Y - depl - halfpattern + y;
        int kx = X - depl - halfpattern + x;
        if( kx < 0 || ky < 0 || kz < 0 || kx >= fullW ||  ky >= fullH ||  kz >= fullD )  
        {
        	d_Dst[(z * (2*depl+2*halfpattern+1) + y)*(2*depl+2*halfpattern+1) + x] = (float)(0.);
        }
        else
        {
        	d_Dst[(z * (2*depl+2*halfpattern+1) + y)*(2*depl+2*halfpattern+1) + x] = (float)(d_Src[kz * fullW * fullH + ky * fullW + kx]);
        }
    }
}

extern "C" __global__ void readFromFullIm_noFFT_aniso_kernel(
    float *d_Dst,
    unsigned char *d_Src,
    int halfpatternX,
    int halfpatternY,
    int halfpatternZ,
    int deplX,
    int deplY,
    int deplZ,
    int fullD,
    int fullH,
    int fullW,
    int Z,
    int Y,
    int X
){
	const int x = threadIdx.x;
    const int y = blockDim.y * blockIdx.x + threadIdx.y;
    const int z = blockDim.z * blockIdx.y + threadIdx.z;

    if(z < (2*deplZ+2*halfpatternZ+1) && y < (2*deplY+2*halfpatternY+1) && x < (2*deplX+2*halfpatternX+1)){
        int kz = Z - deplZ - halfpatternZ + z;
        int ky = Y - deplY - halfpatternY + y;
        int kx = X - deplX - halfpatternX + x;
        if( kx < 0 || ky < 0 || kz < 0 || kx >= fullW ||  ky >= fullH ||  kz >= fullD )  
        {
        	d_Dst[(z * (2*deplY+2*halfpatternY+1) + y)*(2*deplX+2*halfpatternX+1) + x] = (float)(0.);
        }
        else
        {
        	d_Dst[(z * (2*deplY+2*halfpatternY+1) + y)*(2*deplX+2*halfpatternX+1) + x] = (float)(d_Src[kz * fullW * fullH + ky * fullW + kx]);
        }
    }
}



extern "C" __global__ void readFromFullIm_window_kernel(
    float *d_Dst,
    unsigned char *d_Src,
    int fftD,
    int fftH,
    int fftW,
    int fullD,
    int fullH,
    int fullW,
    int Z,
    int Y,
    int X
){
//    const int z = threadIdx.x;
//    const int y = blockDim.y * blockIdx.y + threadIdx.y;
//    const int x = blockDim.z * blockIdx.x + threadIdx.z;
	const int x = threadIdx.x;
    const int y = blockDim.y * blockIdx.x + threadIdx.y;
    const int z = blockDim.z * blockIdx.y + threadIdx.z;

    if(z < fftD && y < fftH && x < fftW){
    		double weightsX;
			double weightsY;
			double weightsZ;
			double relPos;

			relPos = (double) x / (double) (fftW - 1);
			if( (relPos <=0.25) || (relPos > 0.75))
			{
				weightsX = 0.;
			}
			else	
			{
				weightsX = 1.0;
			}
			relPos = (double) y / (double) (fftH - 1);
			if( (relPos <=0.25) || (relPos > 0.75))
			{
				weightsY = 0.;
			}
			else	
			{
				weightsY = 1.0;

			}
			relPos = (double) z / (double) (fftD - 1);
			if( (relPos <=0.25) || (relPos > 0.75))
			{
				weightsZ = 0.;
			}
			else	
			{
				weightsZ = 1.0;
			}
    
        int kz = Z - fftD/2 + z + 1;
        int ky = Y - fftH/2 + y + 1;
        int kx = X - fftW/2 + x + 1;
        if( kx < 0 || ky < 0 || kz < 0 || kx >= fullW ||  ky >= fullH ||  kz >= fullD )  
        {
        	d_Dst[z * fftW * fftH + y * fftW + x] = (float)0.;
        }
        else
        {
        	d_Dst[z * fftW * fftH + y * fftW + x] = (float)(d_Src[kz * fullW * fullH + ky * fullW + kx]*weightsX*weightsY*weightsZ);
        }
    }
}


////////////////////////////////////////////////////////////////////////////////
/// Window
////////////////////////////////////////////////////////////////////////////////
extern "C" __global__ void exp_window_kernel(
    float *d_Dst,
    float *d_Src,
    float a,
    int fftD,
    int fftH,
    int fftW
){
    const int z = threadIdx.x;
    const int y = blockDim.y * blockIdx.y + threadIdx.y;
    const int x = blockDim.z * blockIdx.x + threadIdx.z;

    if(z < fftD && y < fftH && x < fftW){
			double weightsX;
			double weightsY;
			double weightsZ;
			double relPos;

			relPos = (double) x / (double) (fftW - 1);
			if ( relPos <= 0.5)
				weightsX = 1.0 - (1.0 / (powf(a, (relPos * 2))));
			else
				weightsX = 1.0 - (1.0 / (powf(a, ((1 - relPos) * 2))));

			relPos = (double) y / (double) (fftH - 1);
			if (relPos <= 0.5)
				weightsY = 1.0 - (1.0 / (powf(a, (relPos * 2))));
			else
				weightsY = 1.0 - (1.0 / (powf(a, ((1 - relPos) * 2))));

			relPos = (double) z / (double) (fftD - 1);
			if (relPos <= 0.5)
				weightsZ = 1.0 - (1.0 / (powf(a, (relPos * 2))));
			else
				weightsZ = 1.0 - (1.0 / (powf(a, ((1 - relPos) * 2))));

        d_Dst[z * fftW * fftH + y * fftW + x] = (float)(d_Src[z * fftW * fftH + y * fftW + x]*weightsX*weightsY*weightsZ);
    }
}

extern "C" __global__ void reduced_exp_window_kernel(
    float *d_Dst,
    float *d_Src,
    float a,
    int fftD,
    int fftH,
    int fftW
){
    const int z = threadIdx.x;
    const int y = blockDim.y * blockIdx.y + threadIdx.y;
    const int x = blockDim.z * blockIdx.x + threadIdx.z;

    if(z < fftD && y < fftH && x < fftW){
			double weightsX;
			double weightsY;
			double weightsZ;
			double relPos;

			relPos = (double) x / (double) (fftW - 1);
			if( (relPos <=0.25) || (relPos > 0.75))
			{
				weightsX = 0.;
			}
			else	
			{
				if ( relPos <= 0.5)
					weightsX = 1.0 - (1.0 / (powf(a, ((relPos -0.25) * 4))));
				else
					weightsX = 1.0 - (1.0 / (powf(a, ((0.75 - relPos) * 4))));
			}
			relPos = (double) y / (double) (fftH - 1);
			if( (relPos <=0.25) || (relPos > 0.75))
			{
				weightsY = 0.;
			}
			else	
			{
				if (relPos <= 0.5)
					weightsY = 1.0 - (1.0 / (powf(a, ((relPos -0.25) * 4))));
				else
					weightsY = 1.0 - (1.0 / (powf(a, ((0.75 - relPos) * 4))));

			}
			relPos = (double) z / (double) (fftD - 1);
			if( (relPos <=0.25) || (relPos > 0.75))
			{
				weightsZ = 0.;
			}
			else	
			{
				if (relPos <= 0.5)
					weightsZ = 1.0 - (1.0 / (powf(a, ((relPos - 0.25) * 4))));
				else
					weightsZ = 1.0 - (1.0 / (powf(a, ((0.75 - relPos) * 4))));
			}
			
        d_Dst[z * fftW * fftH + y * fftW + x] = (float)(d_Src[z * fftW * fftH + y * fftW + x]*weightsX*weightsY*weightsZ);
    }
}

extern "C" __global__ void hanning_window_kernel(
    float *d_Dst,
    float *d_Src,
    float a,
    int fftD,
    int fftH,
    int fftW
){
    const int z = threadIdx.x;
    const int y = blockDim.y * blockIdx.y + threadIdx.y;
    const int x = blockDim.z * blockIdx.x + threadIdx.z;

    if(z < fftD && y < fftH && x < fftW){
			double weightsX;
			double weightsY;
			double weightsZ;
			double relPos;
			double PI = 3.14159;

			relPos = (double) x / (double) (fftW - 1);
			if( (relPos >=0.25) && (relPos < 0.75))
				weightsX = 1.;
			else	
				weightsX = (1.0 - cos(4*PI*relPos))/2.;
			
			relPos = (double) y / (double) (fftH - 1);
			if( (relPos >=0.25) && (relPos < 0.75))
				weightsY = 1.;
			else	
				weightsY = (1.0 - cos(4*PI*relPos))/2.;
				
			relPos = (double) z / (double) (fftD - 1);
			if( (relPos >=0.25) && (relPos < 0.75))
				weightsZ = 1.;
			else	
				weightsZ = (1.0 - cos(4*PI*relPos))/2.;
				
        d_Dst[z * fftW * fftH + y * fftW + x] = (float)(d_Src[z * fftW * fftH + y * fftW + x]*weightsX*weightsY*weightsZ);
    }
}

extern "C" __global__ void copy_kernel(
    float *d_Dst,
    float *d_Src,
    float a,
    int fftD,
    int fftH,
    int fftW
){
    const int x = threadIdx.x;
    const int y = blockDim.y * blockIdx.x + threadIdx.y;
    const int z = blockDim.z * blockIdx.y + threadIdx.z;

    if(z < fftD && y < fftH && x < fftW){
		d_Dst[z * fftW * fftH + y * fftW + x] = d_Src[z * fftW * fftH + y * fftW + x];
    }
}

extern "C" __global__ void reduced_copy_kernel(
    float *d_Dst,
    float *d_Src,
    float a,
    int fftD,
    int fftH,
    int fftW
){
    const int x = threadIdx.x;
    const int y = blockDim.y * blockIdx.x + threadIdx.y;
    const int z = blockDim.z * blockIdx.y + threadIdx.z;
    double minPos = (double)(fftW/2-a) / (double) (fftW - 1);
    double maxPos = (double)(fftW/2+a) / (double) (fftW - 1);
//    const int z = threadIdx.x;
//    const int y = blockDim.y * blockIdx.y + threadIdx.y;
//    const int x = blockDim.z * blockIdx.x + threadIdx.z;

    if(z < fftD && y < fftH && x < fftW){
			double weightsX;
			double weightsY;
			double weightsZ;
			double relPos;

			relPos = (double) x / (double) (fftW - 1);
			if( (relPos <=minPos) || (relPos > maxPos))
			{
				weightsX = 0.;
			}
			else	
			{
				weightsX = 1.0;
			}
			relPos = (double) y / (double) (fftH - 1);
			if( (relPos <=minPos) || (relPos > maxPos))
			{
				weightsY = 0.;
			}
			else	
			{
				weightsY = 1.0;

			}
			relPos = (double) z / (double) (fftD - 1);
			if( (relPos <=minPos) || (relPos > maxPos))
			{
				weightsZ = 0.;
			}
			else	
			{
				weightsZ = 1.0;
			}
			
        d_Dst[z * fftW * fftH + y * fftW + x] = (float)(d_Src[z * fftW * fftH + y * fftW + x]*weightsX*weightsY*weightsZ);
    }
}

extern "C" __global__ void reduced_hanning_window_kernel_bad(
    float *d_Dst,
    float *d_Src,
    float a,
    int fftD,
    int fftH,
    int fftW
){
    const int z = threadIdx.x;
    const int y = blockDim.y * blockIdx.y + threadIdx.y;
    const int x = blockDim.z * blockIdx.x + threadIdx.z;

    int p = 2;
    while(p<fftD)
        p*=2;

    if(z < fftD && y < fftH && x < fftW){
			double weightsX;
			double weightsY;
			double weightsZ;
			double PI = 3.14159;
            double cosfact=4*PI/(p - 1);
            double minPos = p/4;
            double maxPos = p/4*3;
            if ( ( x >= minPos) && ( x < maxPos)) 
                weightsX = 1.;
            else	
                weightsX = (1.0 - cos(cosfact*x))/2.;
            
            if ( ( y >= minPos) && ( y < maxPos)) 
                weightsY = 1.;
            else	
                weightsY = (1.0 - cos(cosfact*y))/2.;

            if ( ( z >= minPos) && ( z < maxPos)) 
                weightsZ = 1.;
            else	
                weightsZ = (1.0 - cos(cosfact*z))/2.;
			
        d_Dst[z * fftW * fftH + y * fftW + x] = (float)(d_Src[z * fftW * fftH + y * fftW + x]*weightsX*weightsY*weightsZ);
    }
}

extern "C" __global__ void reduced_hanning_window_kernel(
    float *d_Dst,
    float *d_Src,
    float a,
    int fftD,
    int fftH,
    int fftW
){
    const int z = threadIdx.x;
    const int y = blockDim.y * blockIdx.y + threadIdx.y;
    const int x = blockDim.z * blockIdx.x + threadIdx.z;
    double minPos1 = (double)(fftW/2-a) / (double) (fftW - 1);
    double maxPos1 = (double)(fftW/2+a) / (double) (fftW - 1);
    double minPos = (double)(fftW/2-2.*a) / (double) (fftW - 1);
    double maxPos = (double)(fftW/2+2.*a) / (double) (fftW - 1);

    if(z < fftD && y < fftH && x < fftW){
			double weightsX;
			double weightsY;
			double weightsZ;
			double relPos;
			double PI = 3.14159;

			relPos = (double) x / (double) (fftW - 1);
			if( (relPos <=minPos) || (relPos > maxPos))
			{
				weightsX = 0.;
			}
			else	
			{
				if ( ( relPos >= minPos1) && ( relPos < maxPos1)) 
					weightsX = 1.;
				else	
					weightsX = (1.0 - cos((relPos-minPos)*PI/a))/2.;
			}

			relPos = (double) y / (double) (fftH - 1);
			if( (relPos <= minPos) || (relPos > maxPos))
			{
				weightsY = 0.;
			}
			else	
			{
				if ( ( relPos >= minPos1) && ( relPos < maxPos1)) 
					weightsY = 1.;
				else	
					weightsY = (1.0 - cos((relPos-minPos)*PI/a))/2.;
			}

			relPos = (double) z / (double) (fftD - 1);
			if( (relPos <=minPos) || (relPos > maxPos))
			{
				weightsZ = 0.;
			}
			else	
			{
				if ( ( relPos >= minPos1) && ( relPos < maxPos1)) 
					weightsZ = 1.;
				else	
					weightsZ = (1.0 - cos((relPos-minPos)*PI/a))/2.;
			}

			
        d_Dst[z * fftW * fftH + y * fftW + x] = (float)(d_Src[z * fftW * fftH + y * fftW + x]*weightsX*weightsY*weightsZ);
    }
}


////////////////////////////////////////////////////////////////////////////////
// Prepare data for "pad to border" addressing mode
////////////////////////////////////////////////////////////////////////////////
extern "C" __global__ void padDataClampToBorder_kernel(
    float *d_Dst,
    float *d_Src,
    int fftD,
    int fftH,
    int fftW,
    int dataD,
    int dataH,
    int dataW,
    int kernelD,
    int kernelH,
    int kernelW,
    int kernelZ,
    int kernelY,
    int kernelX
){
    const int z = threadIdx.x;
    const int y = blockDim.y * blockIdx.y + threadIdx.y;
    const int x = blockDim.z * blockIdx.x + threadIdx.z;
    const int borderD = dataD + kernelZ;
    const int borderH = dataH + kernelY;
    const int borderW = dataW + kernelX;

    if(z < fftD && y < fftH && x < fftW){
        int dz, dy, dx;

        if(z < dataD) dz = z;
        if(y < dataH) dy = y;
        if(x < dataW) dx = x;
        if(z >= dataD && z < borderD) dz = dataD - 1;
        if(y >= dataH && y < borderH) dy = dataH - 1;
        if(x >= dataW && x < borderW) dx = dataW - 1;
        if(z >= borderD) dz = 0;
        if(y >= borderH) dy = 0;
        if(x >= borderW) dx = 0;

        d_Dst[z * fftH * fftW + y * fftW + x] = d_Src[dz * dataH * dataW + dy * dataW + dx];
    }
}


extern "C" __global__ void scale_kernel(
    float *d_Dst,
    float *d_Src,
    int fftD,
    int fftH,
    int fftW
){
    const int z = threadIdx.x;
    const int y = blockDim.y * blockIdx.y + threadIdx.y;
    const int x = blockDim.z * blockIdx.x + threadIdx.z;
	double wZ;
	double wY;
	double wX;

    if(z < fftD && y < fftH && x < fftW){		
		if(x < fftW/2) 
			wX = (2.0*x)/fftW + 1.0;
		else
			wX = (2.0*(fftW-x))/fftW + 1.0;
        if(y < fftH/2) 
			wY = (2.0*y)/fftH + 1.0;
		else
			wY = (2.0*(fftH-y))/fftH + 1.0;
        if(z < fftD/2) 
			wZ = (2.0*z)/fftD + 1.0;
		else
			wZ = (2.0*(fftD-z))/fftD + 1.0;
			
        float a =(float)(d_Src[z * fftW * fftH + y * fftW + x]*(wX*wY*wZ));
        d_Dst[z * fftW * fftH + y * fftW + x] = a;
    }
}

extern "C" __global__ void dispThr_kernel(float *d_Dst, int fftD, int fftH, int fftW, int dZ, int dY, int dX){
    const int z = threadIdx.x;
    const int y = blockDim.y * blockIdx.y + threadIdx.y;
    const int x = blockDim.z * blockIdx.x + threadIdx.z;
	
    if(z < fftD && y < fftH && x < fftW){		
		//if((x < fftW/2-dX)||(x > fftW/2+dX)||(y < fftH/2-dY)||(y > fftH/2+dY)||(z < fftD/2-dZ) || (z > fftD/2+dZ))
		if(((x < fftW-dX)&&(x >= dX))||((y < fftH-dY)&&(y >= dY))||((z < fftD-dZ) && (z >= dZ)))
            d_Dst[z * fftW * fftH + y * fftW + x] = 0;
    }
}

////////////////////////////////////////////////////////////////////////////////
// Modulate Fourier image of padded data by conjugate Fourier image of padded kernel
// and normalize by FFT size
////////////////////////////////////////////////////////////////////////////////
extern "C" inline __device__ void mulConjAndScale(fComplex& a, const fComplex& b, const float& c){
    fComplex t = {c * (a.x * b.x + a.y * b.y), c * (a.y * b.x - a.x * b.y)};
    a = t;
}

 __global__ void modulateConjugateAndNormalize_kernel(
    fComplex *d_Src,
    fComplex *d_Dst,
    int dataSize,
    float c
){
    const int i = blockDim.x * blockIdx.x + threadIdx.x;
//    if(i==1)
    if(i >= dataSize)
        return;

    fComplex a = d_Dst[i];
    fComplex b = d_Src[i];

    mulConjAndScale(a, b, c);
    d_Dst[i] = a;
}

extern "C" inline __device__ void mult(fComplex& a, const double b){
    fComplex t = { b * a.x , b * a.y };
    a = t;
}


extern "C" __global__ void filter_kernel(
    fComplex *d_Src,
    fComplex *d_Dst,
    int fftD,
    int fftH,
    int fftW
){
	const int z = threadIdx.x;
    const int y = blockDim.y * blockIdx.y + threadIdx.y;
    const int x = blockDim.z * blockIdx.x + threadIdx.z;
	double wZ;
	double wY;
	double wX;

    if(z < fftD && y < fftH && x < fftW/2 +1){		
		
		//if(y < fftH/2) 
			wX = (1.0*x*x)/(fftW/2+1);
		//else
		//	wX = (1.0*(fftW-x)*(fftW-x))/fftW;
		
        if(y < fftH/2) 
			wY = (1.0*y*y)/fftH;
		else
			wY = (1.0*(fftH-y)*(fftH-y))/fftH;
        if(z < fftD/2) 
			wZ = (1.0*z*z)/fftD;
		else
			wZ = (1.0*(fftD-z)*(fftD-z))/fftD;
        
        const int i = z * (fftW/2+1) * fftH + y * (fftW/2+1) + x;
        
        fComplex a = d_Src[i];
		mult(a, exp(-wX-wY-wZ));
		d_Dst[i] = a;
    }    
}


extern "C" __global__ void pixel_correl_kernel_simple(
    float *d_res,
    float *d_Data,
    float *d_Kernel,
    int depl,
    int halfpattern
){
    const int index = threadIdx.x + blockDim.x*blockIdx.x;
    const int offsetx=index%(2*depl+1);
    const int offsety=((index-offsetx)/(2*depl+1))%(2*depl+1);
    const int offsetz=(index-offsetx-offsety*(2*depl+1))/((2*depl+1)*(2*depl+1));
    const int res_index = offsetz*(2*depl+1)*(2*depl+1)+offsety*(2*depl+1)+offsetx;
    int kernel_index;
    int data_index;
    double sum_kernel = 0;
    double sum_data = 0;
    double sum_kernel2 = 0;
    double sum_data2 = 0;
    double res = 0;
    if(offsetz<(2*depl+1))
    {
        for(int dz=0; dz<2*halfpattern+1; dz++)
        {
            for(int dy=0; dy<2*halfpattern+1; dy++)
            {
                for(int dx=0; dx<2*halfpattern+1; dx++)
                {
                    kernel_index=dz*(2*halfpattern+1)*(2*halfpattern+1)+dy*(2*halfpattern+1)+dx;
                    data_index=((dz+offsetz)*(2*depl+1+2*halfpattern)+(dy+offsety))*(2*depl+1+2*halfpattern)+dx+offsetx;
                    sum_kernel += d_Kernel[kernel_index];
                    sum_data += d_Data[data_index];
                }
            }
        }

        sum_kernel/=((2*halfpattern+1)*(2*halfpattern+1)*(2*halfpattern+1));
        sum_data/=((2*halfpattern+1)*(2*halfpattern+1)*(2*halfpattern+1));

        for(int dz=0; dz<2*halfpattern+1; dz++)
        {
            for(int dy=0; dy<2*halfpattern+1; dy++)
            {
                for(int dx=0; dx<2*halfpattern+1; dx++)
                {
                    kernel_index=dz*(2*halfpattern+1)*(2*halfpattern+1)+dy*(2*halfpattern+1)+dx;
                    data_index=((dz+offsetz)*(2*depl+1+2*halfpattern)+(dy+offsety))*(2*depl+1+2*halfpattern)+dx+offsetx;
                    res += (d_Kernel[kernel_index]-sum_kernel)*(d_Data[data_index]-sum_data);
                    sum_kernel2 += (d_Kernel[kernel_index]-sum_kernel)*(d_Kernel[kernel_index]-sum_kernel);
                    sum_data2 += (d_Data[data_index]-sum_data)*(d_Data[data_index]-sum_data);
                }
            }
        }
        res *= res;
        res /= (sum_data2*sum_kernel2);
        res = sqrt(res);
        d_res[res_index] = (float)res;
    }
}

extern "C" __global__ void pixel_correl_kernel(
    float *d_res,
    float *d_Data,
    float *d_Kernel,
    int depl,
    int halfpattern
){
    const int index = threadIdx.x + blockDim.x*blockIdx.x;
    const int offsetx=index%(2*depl+1);
    const int offsety=((index-offsetx)/(2*depl+1))%(2*depl+1);
    const int offsetz=(index-offsetx-offsety*(2*depl+1))/((2*depl+1)*(2*depl+1));
    const int res_index = offsetz*(2*depl+1)*(2*depl+1)+offsety*(2*depl+1)+offsetx;
    int kernel_index;
    int data_index;
    double sum_kernel = 0;
    double sum_data = 0;
    double sum_kernel2 = 0;
    double sum_data2 = 0;
    double res = 0;
    int N = ((2*halfpattern+1)*(2*halfpattern+1)*(2*halfpattern+1));
    if(offsetz<(2*depl+1))
    {

        for(int dz=0; dz<2*halfpattern+1; dz++)
        {
            for(int dy=0; dy<2*halfpattern+1; dy++)
            {
                for(int dx=0; dx<2*halfpattern+1; dx++)
                {
                    kernel_index=dz*(2*halfpattern+1)*(2*halfpattern+1)+dy*(2*halfpattern+1)+dx;
                    data_index=((dz+offsetz)*(2*depl+1+2*halfpattern)+(dy+offsety))*(2*depl+1+2*halfpattern)+dx+offsetx;
                    res += d_Kernel[kernel_index]*d_Data[data_index];
                    sum_kernel += d_Kernel[kernel_index];
                    sum_data += d_Data[data_index];
                    sum_kernel2 += d_Kernel[kernel_index]*d_Kernel[kernel_index];
                    sum_data2 += d_Data[data_index]*d_Data[data_index];
                }
            }
        }
        res *= N;
        res -= sum_data*sum_kernel;
        sum_kernel = N*sum_kernel2-(sum_kernel*sum_kernel);
        sum_data = N*sum_data2-(sum_data*sum_data);
        if(sum_data*sum_kernel<0.00001)
            res=0;
        else
            res /= sqrt(sum_data*sum_kernel);
        if(res<0)
            res=0;
        d_res[res_index] = (float)(res+1.);
    }
}

extern "C" __global__ void pixel_correl_aniso_kernel(
    float *d_res,
    float *d_Data,
    float *d_Kernel,
    int deplX,
    int deplY,
    int deplZ,
    int halfpatternX,
    int halfpatternY,
    int halfpatternZ
){
    const int index = threadIdx.x + blockDim.x*blockIdx.x;
    const int offsetx=index%(2*deplX+1);
    const int offsety=((index-offsetx)/(2*deplX+1))%(2*deplY+1);
    const int offsetz=(index-offsetx-offsety*(2*deplX+1))/((2*deplX+1)*(2*deplY+1));
    //const int res_index = offsetz*(2*depl+1)*(2*depl+1)+offsety*(2*depl+1)+offsetx;
    int kernel_index;
    int data_index;
    double sum_kernel = 0;
    double sum_data = 0;
    double sum_kernel2 = 0;
    double sum_data2 = 0;
    double res = 0;
    int N = ((2*halfpatternX+1)*(2*halfpatternY+1)*(2*halfpatternZ+1));
    if(offsetz<(2*deplZ+1))
    {

        for(int dz=0; dz<2*halfpatternZ+1; dz++)
        {
            for(int dy=0; dy<2*halfpatternY+1; dy++)
            {
                for(int dx=0; dx<2*halfpatternX+1; dx++)
                {
                    kernel_index=dz*(2*halfpatternX+1)*(2*halfpatternY+1)+dy*(2*halfpatternX+1)+dx;
                    data_index=((dz+offsetz)*(2*deplY+1+2*halfpatternY)+(dy+offsety))*(2*deplX+1+2*halfpatternX)+dx+offsetx;
                    res += d_Kernel[kernel_index]*d_Data[data_index];
                    sum_kernel += d_Kernel[kernel_index];
                    sum_data += d_Data[data_index];
                    sum_kernel2 += d_Kernel[kernel_index]*d_Kernel[kernel_index];
                    sum_data2 += d_Data[data_index]*d_Data[data_index];
                }
            }
        }
        res *= N;
        res -= sum_data*sum_kernel;
        sum_kernel = N*sum_kernel2-(sum_kernel*sum_kernel);
        sum_data = N*sum_data2-(sum_data*sum_data);
        if(sum_data*sum_kernel<0.00001)
            res=0;
        else
            res /= sqrt(sum_data*sum_kernel);
        if(res<0)
            res=0;
        d_res[index] = (float)(res+1.);
    }
}

extern "C" __global__ void pixel_correl_aniso_crop_kernel(
    float *d_res,
    float *d_Data,
    float *d_Kernel,
    int deplX,
    int deplY,
    int deplZ,
    int halfpatternX,
    int halfpatternY,
    int halfpatternZ,
    int crophalfpatternX,
    int crophalfpatternY,
    int crophalfpatternZ
){
    const int index = threadIdx.x + blockDim.x*blockIdx.x;
    const int offsetx=index%(2*deplX+1);
    const int offsety=((index-offsetx)/(2*deplX+1))%(2*deplY+1);
    const int offsetz=(index-offsetx-offsety*(2*deplX+1))/((2*deplX+1)*(2*deplY+1));
    //const int res_index = offsetz*(2*depl+1)*(2*depl+1)+offsety*(2*depl+1)+offsetx;
    int kernel_index;
    int data_index;
    double sum_kernel = 0;
    double sum_data = 0;
    double sum_kernel2 = 0;
    double sum_data2 = 0;
    double res = 0;
    int N = ((2*crophalfpatternX+1)*(2*crophalfpatternY+1)*(2*crophalfpatternZ+1));
    if(offsetz<(2*deplZ+1))
    {

        for(int dz=halfpatternZ-crophalfpatternZ; dz<halfpatternZ+1+crophalfpatternZ; dz++)
        {
            for(int dy=halfpatternY-crophalfpatternY; dy<halfpatternY+1+crophalfpatternY; dy++)
            {
                for(int dx=halfpatternX-crophalfpatternX; dx<halfpatternX+1+crophalfpatternX; dx++)
                {
                    kernel_index=dz*(2*halfpatternX+1)*(2*halfpatternY+1)+dy*(2*halfpatternX+1)+dx;
                    data_index=((dz+offsetz)*(2*deplY+1+2*halfpatternY)+(dy+offsety))*(2*deplX+1+2*halfpatternX)+dx+offsetx;
                    res += d_Kernel[kernel_index]*d_Data[data_index];
                    sum_kernel += d_Kernel[kernel_index];
                    sum_data += d_Data[data_index];
                    sum_kernel2 += d_Kernel[kernel_index]*d_Kernel[kernel_index];
                    sum_data2 += d_Data[data_index]*d_Data[data_index];
                }
            }
        }
        res *= N;
        res -= sum_data*sum_kernel;
        sum_kernel = N*sum_kernel2-(sum_kernel*sum_kernel);
        sum_data = N*sum_data2-(sum_data*sum_data);
        if(sum_data*sum_kernel<0.00001)
            res=0;
        else
            res /= sqrt(sum_data*sum_kernel);
        if(res<0)
            res=0;
        d_res[index] = (float)(res+1.);
    }
}

extern "C" __global__ void pixel_correl_texture_nodef_kernel(
    float *d_res,
    float3 Xold,
    float3 Xnew,
    int deplX,
    int deplY,
    int deplZ,
    int halfpatternX,
    int halfpatternY,
    int halfpatternZ
){
    const int index = threadIdx.x + blockDim.x*blockIdx.x;
    const int offsetx=index%(2*deplX+1);
    const int offsety=((index-offsetx)/(2*deplX+1))%(2*deplY+1);
    const int offsetz=(index-offsetx-offsety*(2*deplX+1))/((2*deplX+1)*(2*deplY+1));
    //const int res_index = offsetz*(2*depl+1)*(2*depl+1)+offsety*(2*depl+1)+offsetx;
    double sum_kernel = 0;
    double sum_data = 0;
    double sum_kernel2 = 0;
    double sum_data2 = 0;
    double res = 0;
    int N = ((2*halfpatternX+1)*(2*halfpatternY+1)*(2*halfpatternZ+1));
    if(offsetz<(2*deplZ+1))
    {

        for(int dz=-halfpatternZ; dz<=halfpatternZ; dz++)
        {
            for(int dy=-halfpatternY; dy<=halfpatternY; dy++)
            {
                for(int dx=-halfpatternX; dx<=halfpatternX; dx++)
                {
                    float oldVal = tex3D(oldtex,Xold.x+dx, Xold.y+dy, Xold.z+dz);
                    float newVal = tex3D(newtex,Xnew.x+dx+offsetx-deplX, Xnew.y+dy+offsety-deplY, Xnew.z+dz+offsetz-deplZ);
                    res += oldVal*newVal;
                    sum_kernel += oldVal;
                    sum_data += newVal;
                    sum_kernel2 += oldVal*oldVal;
                    sum_data2 += newVal*newVal;
                }
            }
        }
        res *= N;
        res -= sum_data*sum_kernel;
        sum_kernel = N*sum_kernel2-(sum_kernel*sum_kernel);
        sum_data = N*sum_data2-(sum_data*sum_data);
        if(sum_data*sum_kernel<0.00001)
            res=0;
        else
            res /= sqrt(sum_data*sum_kernel);
        if(res<0)
            res=0;
        d_res[index] = (float)(res+1.);
    }
}


extern "C" __global__ void pixel_correl_texture_rotZ_kernel(
    float *d_res,
    float3 Xold,
    float3 Xnew,
    int deplX,
    int deplY,
    int deplZ,
    int nrot,
    float deltatheta,
    int halfpatternX,
    int halfpatternY,
    int halfpatternZ
){
    const int index = threadIdx.x + blockDim.x*blockIdx.x;
    const int offsetx=index%(2*deplX+1);
    const int offsety=((index-offsetx)/(2*deplX+1))%(2*deplY+1);
    const int offsetz=((index-offsetx-offsety*(2*deplX+1))/((2*deplX+1)*(2*deplY+1)))%(2*deplZ+1);
    const int offsetrot=(index-offsetx-offsety*(2*deplX+1)-offsetz*(2*deplX+1)*(2*deplY+1))/((2*deplX+1)*(2*deplY+1)*(2*deplZ+1));
    double sum_kernel = 0;
    double sum_data = 0;
    double sum_kernel2 = 0;
    double sum_data2 = 0;
    double res = 0;
    int N = ((2*halfpatternX+1)*(2*halfpatternY+1)*(2*halfpatternZ+1));
    if(offsetrot<(2*nrot+1))
    {
        float dr = (offsetrot-nrot)*deltatheta*CONSTANT_PI_PER_DEG;
        float3* newdef = new float3[2];
        newdef[0] = make_float3(0, 0, 0);
        newdef[1] = make_float3(0, 0, dr);
        for(int dz=-halfpatternZ; dz<=halfpatternZ; dz++)
        {
            for(int dy=-halfpatternY; dy<=halfpatternY; dy++)
            {
                for(int dx=-halfpatternX; dx<=halfpatternX; dx++)
                {
                    float3 dX = make_float3(dx, dy, dz);
                    float3 oldpos = dX + Xold;
                    float3 newpos = deform(newdef, Xnew, dX);
                    float oldVal = tex3D(oldtex, oldpos.x, oldpos.y, oldpos.z);
                    float newVal = tex3D(newtex, newpos.x, newpos.y, newpos.z);
                    //float oldVal = tex3D(oldtex,Xold.x+dx, Xold.y+dy, Xold.z+dz);
                    //float3 dX = make_float3(dx,dy,dz);
                    //float newposx = Xnew.x + cosf(dr)*dx - sinf(dr)*dy + offsetx - deplX;
                    //float newposy = Xnew.y + sinf(dr)*dx + cosf(dr)*dy + offsety - deplY;
                    //float newposz = Xnew.z + offsetz -deplZ;
                    //float newVal = tex3D(newtex,newposx, newposy, newposz);
                    res += oldVal*newVal;
                    sum_kernel += oldVal;
                    sum_data += newVal;
                    sum_kernel2 += oldVal*oldVal;
                    sum_data2 += newVal*newVal;
                }
            }
        }
        res *= N;
        res -= sum_data*sum_kernel;
        sum_kernel = N*sum_kernel2-(sum_kernel*sum_kernel);
        sum_data = N*sum_data2-(sum_data*sum_data);
        if(sum_data*sum_kernel<0.00001)
            res=0;
        else
            res /= sqrt(sum_data*sum_kernel);
        if(res<0)
            res=0;
        d_res[index] = (float)(res+1.);
    }
}




//HERE TODAY 2015/10/26

extern "C" __global__ void singlepoint_correl_texture_def_kernel(
    float *d_res,
    float3 Xold,
    float3 Xnew,
    float3 *olddef,
    float3 *newdef,
    int halfpatternX,
    int halfpatternY,
    int halfpatternZ
){
    //const int index = threadIdx.x + blockDim.x*blockIdx.x;
    const int index = threadIdx.x;
    const int ix = index%(2*halfpatternX + 1);
    const int iy = (index-ix)/(2*halfpatternX+1);
    const int dx = ix - halfpatternX;    
    const int dy = iy - halfpatternY;    

    int N = (2*halfpatternX+1)*(2*halfpatternY+1);
    int N3D = (2*halfpatternX+1)*(2*halfpatternY+1)*(2*halfpatternZ+1);
    const int bdx = 1024;  
 
    __shared__ double res[bdx*5];
 
    //extern __shared__ double res[];
    double *sumold = &res[bdx];
    double *sumold2 = &res[2*bdx];
    double *sumnew = &res[3*bdx];
    double *sumnew2 = &res[4*bdx];
    
    int i = bdx;
    if(index<i)
    {
        res[index] = 0;
        sumold[index] = 0;
        sumnew[index] = 0;
        sumold2[index] = 0;
        sumnew2[index] = 0;
    }
    __syncthreads();

    if(index<N){
    for(int dz=-halfpatternZ; dz<=halfpatternZ; dz++)
    {   
        float3 dX = make_float3(dx, dy, dz);
        float3 oldpos = deform(olddef, Xold, dX);
        float3 newpos = deform(newdef, Xnew, dX);
        //float3 oldpos = Xold + dX;
        //float3 newpos = Xnew + dX;
        //float3 oldpos = (Xold + (float3)(olddef[0])) + dX;
        //float3 newpos = (Xnew + (float3)(newdef[0])) + dX ;
        //float3 oldpos = Xold + dX + (float3)(olddef[0]);

        float oldval = tex3D(oldtex, oldpos.x, oldpos.y, oldpos.z);
        float newval = tex3D(newtex, newpos.x, newpos.y, newpos.z);
        
        res[index] +=  oldval*newval;
        sumold[index] += oldval;
        sumnew[index] += newval;
        sumold2[index] += oldval*oldval;
        sumnew2[index] += newval*newval;
    }
    }
    __syncthreads();

    i=N-1;
    if(index==0){
        res[index] +=  res[index+i];
        sumold[index] += sumold[index + i];
        sumnew[index] += sumnew[index + i];
        sumold2[index] += sumold2[index +i ];
        sumnew2[index] += sumnew2[index + i];
    }
    
    __syncthreads();
    
    i =(N-1)/2;
    while(i!=0)
    {
        if(index<i)
        {
            res[index] +=  res[index + i];
            sumold[index] += sumold[index + i];
            sumnew[index] += sumnew[index + i];
            sumold2[index] += sumold2[index +i ];
            sumnew2[index] += sumnew2[index + i];
        }
        __syncthreads();
        i/=2;
    }

    if(index==0){
        res[0] *= N3D;
        res[0] -= sumold[0]*sumnew[0];
        sumnew[0] = N3D*sumnew2[0]-(sumnew[0]*sumnew[0]);
        sumold[0] = N3D*sumold2[0]-(sumold[0]*sumold[0]);
        if(sumold[0]*sumnew[0]<0.00001)
            res[0]=0;
        else
            res[0] /= sqrt(sumold[0]*sumnew[0]);
        if(res[0]<0)
            res[0]=0;
        d_res[0] = (float)(res[0]+1.);
        //d_res[0] = (float)((newdef[0]).x);
        //d_res[0] = (float)(10.);

    }
}


extern "C" __global__ void pixel_check_value(
    float *d_res,
    float3 Xold,
    deformationType* olddef,
    deformationType* newdef
){
    const int index = threadIdx.x;
    float newval = (float)tex3D(oldtex, Xold.x, Xold.y, Xold.z);
    //float newval = tex3D(newtex, newdef[0].U.x+Xold.x, newdef[0].U.y+Xold.y, newdef[0].U.z+Xold.z);
    if(index==0){
        d_res[0] = (float)(olddef[0].x);
        d_res[1] = (float)(Xold.x);
        d_res[2] = (float)(Xold.y);
        d_res[3] = (float)(Xold.z);

    }
}


/*
extern "C" __global__ void pixel_correl_tex_kernel(
    float *d_res,
    float *d_Data,
    float *d_Kernel,
    int deplX,
    int deplY,
    int deplZ,
    int halfpatternX,
    int halfpatternY,
    int halfpatternZ
){
    const int index = threadIdx.x + blockDim.x*blockIdx.x;
    const int offsetx=index%(2*deplX+1);
    const int offsety=((index-offsetx)/(2*deplX+1))%(2*deplY+1);
    const int offsetz=(index-offsetx-offsety*(2*deplX+1))/((2*deplX+1)*(2*deplY+1));
    //const int res_index = offsetz*(2*depl+1)*(2*depl+1)+offsety*(2*depl+1)+offsetx;
    int kernel_index;
    int data_index;
    double sum_kernel = 0;
    double sum_data = 0;
    double sum_kernel2 = 0;
    double sum_data2 = 0;
    double res = 0;
    int N = ((2*halfpatternX+1)*(2*halfpatternY+1)*(2*halfpatternZ+1));
    if(offsetz<(2*deplZ+1))
    {

        for(int dz=0; dz<2*halfpatternZ+1; dz++)
        {
            for(int dy=0; dy<2*halfpatternY+1; dy++)
            {
                for(int dx=0; dx<2*halfpatternX+1; dx++)
                {
                    kernel_index=dz*(2*halfpatternX+1)*(2*halfpatternY+1)+dy*(2*halfpatternX+1)+dx;
                    //data_index=((dz+offsetz)*(2*deplY+1+2*halfpatternY)+(dy+offsety))*(2*deplX+1+2*halfpatternX)+dx+offsetx;
                    float data = tex3D(tex,(1.*dx+offsetx)/(2*deplX+1+2*halfpatternX),(1.*dy+offsety)/(2*deplY+1+2*halfpatternY), (1.*dz+offsetz)/(2*deplZ+1+2*halfpatternZ));
                    res += d_Kernel[kernel_index]*data;
                    sum_kernel += d_Kernel[kernel_index];
                    sum_data += data;
                    sum_kernel2 += d_Kernel[kernel_index]*d_Kernel[kernel_index];
                    sum_data2 += data*data;
                }
            }
        }
        res *= N;
        res -= sum_data*sum_kernel;
        sum_kernel = N*sum_kernel2-(sum_kernel*sum_kernel);
        sum_data = N*sum_data2-(sum_data*sum_data);
        if(sum_data*sum_kernel<0.00001)
            res=0;
        else
            res /= sqrt(sum_data*sum_kernel);
        if(res<0)
            res=0;
        d_res[index] = (float)(res+1.);
    }
}
*/

extern "C" __global__ void pixel_correl_aniso_rotZ_kernel(
    float *d_res,
    float *d_Data,
    float *d_Kernel,
    int deplX,
    int deplY,
    int deplZ,
    int halfpatternX,
    int halfpatternY,
    int halfpatternZ,
    int nrot,
    float deltatheta
){
    const int index = threadIdx.x + blockDim.x*blockIdx.x;
    const int offsetx=index%(2*deplX+1);
    const int offsety=((index-offsetx)/(2*deplX+1))%(2*deplY+1);
    const int offsetz=((index-offsetx-offsety*(2*deplX+1))/((2*deplX+1)*(2*deplY+1)))%(2*deplZ+1);
    const int offsetrot=((index-offsetx-offsety*(2*deplX+1)-offsetz*(2*deplX+1)*2*deplY+1)*(2*deplZ+1))/((2*deplX+1)*(2*deplY+1)*(2*deplZ+1));
    //const int res_index = offsetz*(2*depl+1)*(2*depl+1)+offsety*(2*depl+1)+offsetx;
    int kernel_index;
    int data_index;
    double sum_kernel = 0;
    double sum_data = 0;
    double sum_kernel2 = 0;
    double sum_data2 = 0;
    double res = 0;
    int N = ((2*halfpatternX+1)*(2*halfpatternY+1)*(2*halfpatternZ+1));
    if(offsetrot<(2*nrot+1))
    {
        for(int dz=0; dz<2*halfpatternZ+1; dz++)
        {
            for(int dy=0; dy<2*halfpatternY+1; dy++)
            {
                for(int dx=0; dx<2*halfpatternX+1; dx++)
                {
                    kernel_index=dz*(2*halfpatternX+1)*(2*halfpatternY+1)+dy*(2*halfpatternX+1)+dx;
                    data_index=((dz+offsetz)*(2*deplY+1+2*halfpatternY)+(dy+offsety))*(2*deplX+1+2*halfpatternX)+dx+offsetx;
                    res += d_Kernel[kernel_index]*d_Data[data_index];
                    sum_kernel += d_Kernel[kernel_index];
                    sum_data += d_Data[data_index];
                    sum_kernel2 += d_Kernel[kernel_index]*d_Kernel[kernel_index];
                    sum_data2 += d_Data[data_index]*d_Data[data_index];
                }
            }
        }
        res *= N;
        res -= sum_data*sum_kernel;
        sum_kernel = N*sum_kernel2-(sum_kernel*sum_kernel);
        sum_data = N*sum_data2-(sum_data*sum_data);
        if(sum_data*sum_kernel<0.00001)
            res=0;
        else
            res /= sqrt(sum_data*sum_kernel);
        if(res<0)
            res=0;
        d_res[index] = (float)(res+1.);
    }
}





   




