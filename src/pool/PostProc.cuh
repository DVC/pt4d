#include <math.h>
#include <cuda.h>

#include "cuda_runtime.h"
typedef unsigned int uint;

int iDivUp(int a, int b);
int iAlignUp(int a, int b);	
		
extern "C" __global__ void kernel_calcDeplField(
    float *d_Depl,
    float *d_Coeff,
    float *d_Points,
    float regulDistThr,
    float regulDist,
    unsigned long dimD,
    unsigned long dimW,
    unsigned long dimH,
    unsigned long dimsubD,
    unsigned long round,
    unsigned long ipt
){
	const int dx = blockDim.x * blockIdx.x + threadIdx.x - regulDistThr;
    const int dy = blockDim.y * blockIdx.y + threadIdx.y - regulDistThr;
    const int dz = blockDim.z * blockIdx.z + threadIdx.z - regulDistThr;
    
    if(dz < (regulDistThr+1) && dy < (regulDistThr+1) && dx < (regulDistThr+1)){
        long kx = d_Points[ ipt * 6     ] + dx;
        long ky = d_Points[ ipt * 6 + 1 ] + dy;
        long kz = d_Points[ ipt * 6 + 2 ] + dz;
        if( kx >= 0 && ky >= 0 && kz >= round*(dimsubD-2) && kx < dimW &&  ky < dimH &&  kz < (round+1)*(dimsubD-2)+2 && kz < dimD )  
        {
            if((dx*dx+dy*dy+dz*dz)<(regulDistThr*regulDistThr))
            {
                float Coeff = exp(- (dx*dx+dy*dy+dz*dz)/(regulDist*regulDist)); 
        	    d_Coeff[(kz - round * (dimsubD-2))  * dimW * dimH + ky * dimW + kx] += Coeff;
        	    d_Depl[(kz - round * (dimsubD-2))  * dimW * dimH * 3 + ky * dimW * 3 + kx * 3     ] += Coeff*d_Points[ ipt * 6 + 3 ];
            	d_Depl[(kz - round * (dimsubD-2))  * dimW * dimH * 3 + ky * dimW * 3 + kx * 3 + 1 ] += Coeff*d_Points[ ipt * 6 + 4 ];
        	    d_Depl[(kz - round * (dimsubD-2))  * dimW * dimH * 3 + ky * dimW * 3 + kx * 3 + 2 ] += Coeff*d_Points[ ipt * 6 + 5 ];
            }
        }
    }
}

extern "C" __global__ void kernel_calcDefField(
    float *d_Depl,
    float *d_eps,
    unsigned long dimD,
    unsigned long dimW,
    unsigned long dimH,
    unsigned long dimsubD,
    unsigned long round
){
    const unsigned long x = blockDim.x * blockIdx.x + threadIdx.x+1;
    const unsigned long y = blockDim.y * blockIdx.y + threadIdx.y+1;
    const unsigned long z = blockDim.z * blockIdx.z + threadIdx.z+1;
    if( x < dimW &&  y < dimH &&  z < dimsubD )  
    {
        d_eps[x+y*dimW+z*dimW*dimH]=(d_Depl[(x+y*dimW+(z+1)*dimW*dimH)*3+2]-d_Depl[(x+y*dimW+(z-1)*dimW*dimH)*3+2])/2.;
    }
}

extern "C" __global__ void kernel_resetData(
    float *d_Data,
    unsigned long length
){
    const unsigned long x = blockDim.x * blockIdx.x + threadIdx.x;
    const unsigned long xpad = blockDim.x * gridDim.x;
    const unsigned long y = blockDim.y * blockIdx.y + threadIdx.y;
    const unsigned long ypad = blockDim.y * gridDim.y;
    const unsigned long z = blockDim.z * blockIdx.z + threadIdx.z;
    const unsigned long index = z*ypad*xpad + y*xpad + x;
    if(index < length){
		d_Data[index] = (float)(0.);
    }
}

extern "C" __global__ void kernel_div(
    float *d_Num,
    float *d_Denom,
    unsigned long length
){
    const unsigned long x = blockDim.x * blockIdx.x + threadIdx.x;
    const unsigned long xpad = blockDim.x * gridDim.x;
    const unsigned long y = blockDim.y * blockIdx.y + threadIdx.y;
    const unsigned long ypad = blockDim.y * gridDim.y;
    const unsigned long z = blockDim.z * blockIdx.z + threadIdx.z;
    const unsigned long index = z*ypad*xpad + y*xpad + x;
    
    if(index < length){
        if(d_Denom[index]>0)
		    d_Num[index*3] = d_Num[index*3]/d_Denom[index];
    }
}


