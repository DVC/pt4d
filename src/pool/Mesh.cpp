#include "Mesh.h"

using namespace std;

	Mesh::Mesh() {
		this->name = "";
		this->points = new PSet<P>();
        this->cells = new list<Cell*>();
         
 	}
    
    void Mesh::read(string fileName) {
        this->read(fileName,0);
    }
    

    void Mesh::read(string fileName, int scan) {
        LDEBUG << "Will try to read MeshPts type file " << fileName;
        ifstream File (fileName.c_str(),ios::in);
        if (!File)
        {
    	    LERROR << "Error : impossible to open file " << fileName << " in read only mode...";
        }   
        else
        {
	        //cout << "File opened " << endl;
        }
        vector<double> line;
        double a;
        string paramline;
        int nbColonne;
        nbColonne=3;
        stringstream sstr;
        char schar[10000];
        File.getline(schar,10000);
        File.getline(schar,10000);
        File.getline(schar,10000);
        File.getline(schar,10000);
        File.getline(schar,10000);
	    paramline = "";
        paramline+=schar;
        int dimx=0;
        int dimy=0;
        int dimz=0;

        sscanf(schar,"DIMENSIONS %d %d %d\n", &dimx, &dimy, &dimz);
    
        this->points->setStackdimX(dimx);
        this->points->setStackdimY(dimy);
        this->points->setStackdimZ(dimz);

        LINFO << dimx*dimy*dimz << "  points in mesh (" << dimx << "," << dimy << "," << dimz << ")"; 
                

        File.getline(schar,10000);
        int pn=0;
        int wp=0;
        while (File.good()&&(pn<=dimx*dimy*dimz))
        {
	        paramline = "";
	        File.getline(schar,10000);
    	    paramline+=schar;
	        sstr.clear();
    	    sstr.str(paramline);
    	    if(sstr.str().size()>1)
            {
    		    for(int i=0; i<nbColonne; i++)
    		    {
			        sstr >> a;
			        line.push_back(1.*a);
		        }
		        if(!isnan(line[0]))
		        { 
			        pn++;
			        (this->points)->addP(new P(scan,
						(int)pn,
						Coord((double)line[0],(double)line[1],(double)line[2])
					));
			    }
		        else
		        {
			        wp++;
		        }
		        line.clear();
            }
        }
        File.close();
        int cn=1;
        for(int k=0; k<dimz-1; k++){
            for(int j=0; j<dimy-1; j++){
                for(int i=0; i<dimx-1; i++){
                    (this->cells)->push_back(new Cell(cn,
                                                this->points->getP(i+j*dimx+k*dimx*dimy),
                                                this->points->getP(i+1+j*dimx+k*dimx*dimy),
                                                this->points->getP(i+(j+1)*dimx+k*dimx*dimy),
                                                this->points->getP(i+1+(j+1)*dimx+k*dimx*dimy),
                                                this->points->getP(i+j*dimx+(k+1)*dimx*dimy),
                                                this->points->getP(i+1+j*dimx+(k+1)*dimx*dimy),
                                                this->points->getP(i+(j+1)*dimx+(k+1)*dimx*dimy),
                                                this->points->getP(i+1+(j+1)*dimx+(k+1)*dimx*dimy)
                                            ));
                    //LDEBUG << "cell " << cn << " (" << i << "," << j << "," << k << ") ok";   
                    cn++;

                }
            }
        }
        LDEBUG << cn -1 << " cells built";   
    }

    Mesh::~Mesh()
    {
    	this->name = "";
    	this->points->cleanAll();
        delete points;
		LDEBUG << "Deleted points in Mesh";
        for (list<Cell*>::iterator cit=this->cells->end(); cit!=this->cells->begin(); --cit)
		{
				delete *cit;
                *cit=NULL;
		}
    	this->cells->clear();
		LDEBUG << "Deleted cells in Mesh";
    
    	delete cells;

    }

    Cell* Mesh::getCell(int number) {
		int j=0;
	 	for (list<Cell*>::iterator pit=this->cells->begin(); pit!=this->cells->end(); ++pit, ++j)
	 	{
			if(number==j)
				return *pit;
		}
		Cell* emptyCell(0);
		return emptyCell;
	}

    PSet<P>* Mesh::getPoints() {
        return this->points;
    } 

    bool Mesh::isInCell(Coord pt, int number) {
        return (this->getCell(number))->isInCell(pt);
    }

    int Mesh::inWhichCell(Coord pt) {
        int j=0;
	 	for (list<Cell*>::iterator cit=this->cells->begin(); cit!=this->cells->end(); ++cit, ++j)
	 	{
			if((*cit)->isInCell(pt))
				return j;
		}
		return -1;
	}

    void Mesh::computeSafe(Mesh &meshcur, PSet<P> ptsref, PSet<P> ptcur , double correlThr, double regulThrDist, double regulDist) {

//    void Mesh::computeSafe(Mesh &meshref, PSet<P> ptsref, PSet<P> ptcur , double correlThr, double regulThrDist, double regulDist) {
        //meshref.getPoints()->computeSafeMesh(*(this->points), ptsref, ptcur, correlThr, regulThrDist, regulDist);
        this->points->computeSafeMesh(*(meshcur.getPoints()), ptsref, ptcur, correlThr, regulThrDist, regulDist);
    }

    void Mesh::writeVTK(string name, Mesh &meshref0, Mesh &meshref) {
        LDEBUG << "Try to write VTK mesh file " << name;
        (this->points)->writeVTKMesh(name,*(meshref0.getPoints()),*(meshref.getPoints()));
        LDEBUG << "Managed to write VTK mesh file " << name;
    }

    void Mesh::writeCastemINP(string directory, string nameprefix, Mesh &meshref0) {

        ostringstream inpName;
        inpName << directory << nameprefix << ".inp";
        LDEBUG << "Try to write CASTEM INP mesh file " << inpName.str();

        ofstream fichier(inpName.str().c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier

	    if(fichier)  // si l'ouverture a réussi
	    {
            LDEBUG << "file opened";
            fichier << this->points->Size() << " " << this->cells->size() << " 3 0 0" << endl;
	        	//for (list<P*>::const_iterator pit=this->points->getIteratorBegin(); pit!=this->points->getIteratorEnd(); ++pit)
	        	for (list<P*>::const_iterator pit=meshref0.getPoints()->getIteratorBegin(); pit!=meshref0.getPoints()->getIteratorEnd(); ++pit)
	        	{
                    fichier << (*pit)->getColor() << " ";
                    fichier << (*pit)->getCoord().getX() << " ";
                    fichier << (*pit)->getCoord().getY() << " ";
                    fichier << (*pit)->getCoord().getZ() << endl;
	        	}
	        	for (list<Cell*>::const_iterator cit=this->cells->begin(); cit!=this->cells->end(); ++cit)
                {
                    fichier << (*cit)->toINPString() << endl; 
                }
                fichier << "3 1 1 1" << endl;
                fichier << "UX," << endl;
                fichier << "UY," << endl;
                fichier << "UZ," << endl;
                list<P*>::const_iterator pitRef=meshref0.getPoints()->getIteratorBegin();
	        	for (list<P*>::const_iterator pit=this->points->getIteratorBegin(); pit!=this->points->getIteratorEnd(); ++pit)
	        	{
                    fichier << (*pit)->getColor() << " ";
                    fichier << ((*pit)->getCoord().getX() - (*pitRef)->getCoord().getX()) << " ";
                    fichier << ((*pit)->getCoord().getY() - (*pitRef)->getCoord().getY()) << " ";
                    fichier << ((*pit)->getCoord().getZ() - (*pitRef)->getCoord().getZ()) << endl;
                    ++pitRef;
	        	}

                fichier.close();  
    
          }
    }


    void Mesh::writeCastem(string fullnameprefix, Mesh &meshref0) {

        ostringstream inpName;
        inpName << fullnameprefix << ".inp";
        LDEBUG << "Try to write CASTEM INP mesh file " << inpName.str();

        ofstream fichier(inpName.str().c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier

	    if(fichier)  // si l'ouverture a réussi
	    {
            LDEBUG << "file opened";
            fichier << this->points->Size() << " " << this->cells->size() << " 3 0 0" << endl;
	        	//for (list<P*>::const_iterator pit=this->points->getIteratorBegin(); pit!=this->points->getIteratorEnd(); ++pit)
	        	for (list<P*>::const_iterator pit=meshref0.getPoints()->getIteratorBegin(); pit!=meshref0.getPoints()->getIteratorEnd(); ++pit)
	        	{
                    fichier << (*pit)->getColor() << " ";
                    fichier << (*pit)->getCoord().getX() << " ";
                    fichier << (*pit)->getCoord().getY() << " ";
                    fichier << (*pit)->getCoord().getZ() << endl;
	        	}
	        	for (list<Cell*>::const_iterator cit=this->cells->begin(); cit!=this->cells->end(); ++cit)
                {
                    fichier << (*cit)->toINPString() << endl; 
                }
                fichier << "3 1 1 1" << endl;
                fichier << "UX," << endl;
                fichier << "UY," << endl;
                fichier << "UZ," << endl;
                list<P*>::const_iterator pitRef=meshref0.getPoints()->getIteratorBegin();
	        	for (list<P*>::const_iterator pit=this->points->getIteratorBegin(); pit!=this->points->getIteratorEnd(); ++pit)
	        	{
                    fichier << (*pit)->getColor() << " ";
                    fichier << ((*pit)->getCoord().getX() - (*pitRef)->getCoord().getX()) << " ";
                    fichier << ((*pit)->getCoord().getY() - (*pitRef)->getCoord().getY()) << " ";
                    fichier << ((*pit)->getCoord().getZ() - (*pitRef)->getCoord().getZ()) << endl;
                    ++pitRef;
	        	}

                fichier.close();  
    
          }


        ostringstream dgibiName;
        dgibiName << fullnameprefix << ".dgibi";
        LDEBUG << "Try to write CASTEM DGIBI file " << dgibiName.str();

        ofstream fichierd(dgibiName.str().c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier
	    if(fichierd)  // si l'ouverture a réussi
	    {
            fichierd << "OPTI ECHO 0 DIME 3 ELEM CUB8 MODE TRID;" << endl;
            fichierd << "* Lecture des donnees " << endl;
            fichierd << "OPTI LECT " << endl; 
            fichierd << "'" << inpName.str() << "' ;" << endl;
            fichierd << "TBEXT = LIRE 'AVS'; " << endl;
            fichierd << "MAILLAGE = TBEXT.LEMAILLA; " << endl;
            fichierd << "Deplcmt = TBEXT.LECHPOIN; " << endl;
            fichierd << "* Post-traitement " << endl;
            fichierd << "Mail_EF = MODELE Maillage MECANIQUE ELASTIQUE ISOTROPE;" << endl;
            fichierd << "FonctUn = MANU CHML Maillage MASS 1;" << endl;
            fichierd << "VolMail = INTG Mail_EF FonctUn ;" << endl;
            fichierd << "MESSAGE ' Volume=' VolMail;" << endl;
            fichierd << "TRAC Maillage; " << endl;
            fichierd << "TRAC (VECT Deplcmt UX UY UZ VERT) Maillage;" << endl;
            fichierd << "Defo = EPSILON Mail_EF Deplcmt;" << endl;
            fichierd << "MEpXX = (INTG Mail_EF (EXCO EPXX Defo))/VolMail;" << endl;
            fichierd << "MEpYY = (INTG Mail_EF (EXCO EPYY Defo))/VolMail;" << endl;
            fichierd << "MEpZZ = (INTG Mail_EF (EXCO EPZZ Defo))/VolMail;" << endl;
            fichierd << "MEpXY = (INTG Mail_EF (EXCO GAXY Defo))/2./VolMail;" << endl;
            fichierd << "MEpXZ = (INTG Mail_EF (EXCO GAXZ Defo))/2./VolMail;" << endl;
            fichierd << "MEpYZ = (INTG Mail_EF (EXCO GAYZ Defo))/2./VolMail;" << endl;
            fichierd << "MESSAGE '******* Deformation moyenne *******';" << endl;
            fichierd << "MESSAGE ' EPXX=' MEpXX ' EPXY=' MEpXY ' EPXZ=' MEpXZ ;" << endl;
            fichierd << "MESSAGE ' EPXY=' MEpXY ' EPYY=' MEpYY ' EPYZ=' MEpYZ ;" << endl;
            fichierd << "MESSAGE ' EPXZ=' MEpXZ ' EPYZ=' MEpYZ ' EPZZ=' MEpZZ ;" << endl;
            fichierd << "MESSAGE '***********************************';" << endl;
            fichierd << "TRAC Maillage Mail_EF (EXCO EPXX Defo);" << endl;
            fichierd << "TRAC Maillage Mail_EF (EXCO GAXY Defo);" << endl;
            fichierd << "Trce = NOMC Dilat ( ((NOMC Tmp (EXCO EPXX Defo))" << endl;
            fichierd << "       + (NOMC Tmp (EXCO EPYY Defo)) " << endl;
            fichierd << "       + (NOMC Tmp (EXCO EPZZ Defo)) )/3);" << endl;
            fichierd << "DefEq = NOMC DefEq ((2./3.*( " << endl;
            fichierd << "         ((  ((NOMC Tmp (EXCO GAXY Defo))**2)  " << endl;
            fichierd << "           + ((NOMC Tmp (EXCO GAXZ Defo))**2)  " << endl;
            fichierd << "           + ((NOMC Tmp (EXCO GAYZ Defo))**2))/2)  " << endl;
            fichierd << "         + ( ((NOMC Tmp (EXCO EPXX Defo)) - (NOMC Tmp Trce) )**2) " << endl;
            fichierd << "         + ( ((NOMC Tmp (EXCO EPYY Defo)) - (NOMC Tmp Trce) )**2) " << endl;
            fichierd << "         + ( ((NOMC Tmp (EXCO EPZZ Defo)) - (NOMC Tmp Trce) )**2) " << endl;
            fichierd << "                     ))**(0.5));  " << endl;
            fichierd << "TRAC Maillage Mail_EF Trce;" << endl;
            fichierd << "TRAC Maillage Mail_EF DefEq;" << endl;
            fichierd << "TRAC COUPE (0 0 0) (1 0 0) (0 1 1) Maillage Mail_EF DefEq ;" << endl;
            fichierd << "GrTransf = GRADIENT Mail_EF Deplcmt;" << endl;
            fichierd << "MGXX = (INTG Mail_EF (EXCO UX,X GrTransf))/VolMail;" << endl;
            fichierd << "MGXY = (INTG Mail_EF (EXCO UX,Y GrTransf))/VolMail;" << endl;
            fichierd << "MGXZ = (INTG Mail_EF (EXCO UX,Z GrTransf))/VolMail;" << endl;
            fichierd << "MGYX = (INTG Mail_EF (EXCO UY,X GrTransf))/VolMail;" << endl;
            fichierd << "MGYY = (INTG Mail_EF (EXCO UY,Y GrTransf))/VolMail;" << endl;
            fichierd << "MGYZ = (INTG Mail_EF (EXCO UY,Z GrTransf))/VolMail;" << endl;
            fichierd << "MGZX = (INTG Mail_EF (EXCO UZ,X GrTransf))/VolMail;" << endl;
            fichierd << "MGZY = (INTG Mail_EF (EXCO UZ,Y GrTransf))/VolMail;" << endl;
            fichierd << "MGZZ = (INTG Mail_EF (EXCO UZ,Z GrTransf))/VolMail;" << endl;
            fichierd << "MESSAGE '********** Gradient moyen *********';" << endl;
            fichierd << "MESSAGE ' FXX=' MGXX ' FXY=' MGXY ' FXZ=' MGXZ ;" << endl;
            fichierd << "MESSAGE ' FYX=' MGYX ' FYY=' MGYY ' FYZ=' MGYZ ;" << endl;
            fichierd << "MESSAGE ' FZX=' MGZX ' FZY=' MGZY ' FZZ=' MGZZ ;" << endl;
            fichierd << "MESSAGE '***********************************';" << endl;

            fichierd << "OPTI ECHO 1;" << endl;
            
            fichierd.close(); 
       }

}
