#include "Particle.h"

using namespace std;

Particle::Particle() : P(), volume(0), boundary(false), sphericity(0), shapeFact(0), shapeFact2(0), zAngle(0),
		globalcolor(0), father(-1), fatherconnect(-1), grandfather(-1), grandfatherconnect(-1), born(-1), dead(-1), param("")
{

}

Particle::Particle(P p) : P(), volume(0), boundary(false), sphericity(0), shapeFact(0), shapeFact2(0), zAngle(0),
		globalcolor(0), father(-1), fatherconnect(-1), grandfather(-1), grandfatherconnect(-1), born(-1), dead(-1), param("")
{
	this->PUpdate(p);
}

Particle::~Particle(){
	this->color = -1;
}

	 Particle::Particle(int scan, int color, Coord coord, double volume,
			 bool boundary, double sphericity, double shapeFact , double shapeFact2, double zAngle, string param) {
		this->scan = scan;
		this->color = color;
		this->coord = coord;
		this->volume = volume;
		this->boundary = boundary;
		this->sphericity = sphericity;
		this->shapeFact = shapeFact;
		this->shapeFact2 = shapeFact2;
		this->zAngle = zAngle;
		this->globalcolor=0;
		this->father=-1;
		this->fatherconnect=-1;
		this->grandfather=-1;
		this->grandfatherconnect=-1;
		this->born=-1;
		this->dead=-1;
		//this->cellNumber=-1;
		this->param = param;
		this->roisize = 64;
 	}
	
	 Particle::Particle(int scan, int color, Coord coord, double volume,
			bool boundary, double sphericity, double shapeFact , double shapeFact2, double zAngle) {
		this->scan = scan;
		this->color = color;
		this->coord = coord;
		this->volume = volume;
		this->boundary = boundary;
		this->sphericity = sphericity;
		this->shapeFact = shapeFact;
		this->shapeFact2 = shapeFact2;
		this->zAngle = zAngle;
		this->globalcolor=0;
		this->father=-1;
		this->fatherconnect=-1;
		this->grandfather=-1;
		this->grandfatherconnect=-1;
		this->born=-1;
		this->dead=-1;
		//this->cellNumber=-1;
		this->param = "";
		this->roisize = 64;
 	}

	 Particle::Particle(string linestr) {
		 double a,b,c;
		 string paramline;
		 stringstream sstr;
		 sstr.str(linestr);
		 if(sstr.str().size()>1)
		 {
		     sstr >> a;
		     if(!isnan(1.*a))
		     {
		    	 	this->scan = (int)(1.*a);
		    	 	sstr >> a;
		    	    this->color = (int)(1.*a);
		     		sstr >> a;
		     		sstr >> b;
		     		sstr >> c;
		     		this->coord = Coord((double)(1.*a),(double)(1.*b),(double)(1.*c));
		     		sstr >> a;
		     		this->volume = (int)(1.*a);
		     		sstr >> a;
		     		this->boundary = (bool)(1.*a==1);
		     		sstr >> a;
		     		this->sphericity = (double)(1.*a);
		     		sstr >> a;
		     		this->shapeFact = (double)(1.*a);
		     		sstr >> a;
		     		this->shapeFact2 = (double)(1.*a);
		     		sstr >> a;
		     		this->zAngle = (double)(1.*a);
		     		sstr >> a;
		     		this->globalcolor= (int)(1.*a);
		     		sstr >> a;
		     		this->father= (int)(1.*a);
		     		sstr >> a;
		     		this->fatherconnect= (int)(1.*a);
		     		sstr >> a;
		     		this->grandfather= (int)(1.*a);
		     		sstr >> a;
		     		this->grandfatherconnect= (int)(1.*a);
		     		sstr >> a;
		     		this->born= (int)(1.*a);
		     		sstr >> a;
		     		this->dead= (int)(1.*a);
		     		this->roisize = 64;
		     		ostringstream paramstr;
		     		while(sstr >> a)
		     		{
		     			paramstr << a << " ";
		     		}
		     		this->param = paramstr.str();
		            //this->cellNumber=-1;
		     }
	  	}
	 }


	 Particle::Particle(const string fileDirectory, int scan, int color) {
		 this->scan = scan;
		 this->color = color;
		 		this->coord = Coord();
		 		this->volume = 0;
		 		this->boundary = false;;
		 		this->sphericity=0;
		 		this->shapeFact=0;
		 		this->shapeFact2=0;
		 		this->zAngle=0;
		 		this->globalcolor=0;
		 		this->roisize = 64;
		 		this->father=-1;
		 		this->fatherconnect=-1;
		 		this->grandfather=-1;
		 		this->grandfatherconnect=-1;
		 		this->born=-1;
		 		this->dead=-1;
		        //this->cellNumber=-1;
		 		this->param="";

		 ostringstream fileName;
		 char buff [200];
		 sprintf(buff,"/Particle_%04d_%05d",scan,color);
		 fileName << fileDirectory << buff;

		 ifstream File (fileName.str().c_str(),ios::in);
		    if (!File)
		    {
		    	LERROR << "Error : impossible to open file " << fileName.str() << " in read only mode..." ;
		    	LERROR << "Particle color is " << color << " from scan " << scan;
		    }
		    else
		    {
			LINFO << "File of Particle opened : color is " << color << " from scan " << scan;
		    }
		    double a,b,c;
		    		 string dataline="";
		    		 stringstream sstr;
		 		    char schar[10000];
		 		    File.getline(schar,10000);
		 		    dataline+=schar;
		    		 sstr.str(dataline);
		    		 if(sstr.str().size()>1)
		    		 {
		    		     sstr >> a;
		    		     if(!isnan(1.*a))
		    		     {
		    		    	 	this->scan = (int)(1.*a);
		    		    	 	sstr >> a;
		    		    	    this->color = (int)(1.*a);
		    		     		sstr >> a;
		    		     		sstr >> b;
		    		     		sstr >> c;
		    		     		this->coord = Coord((double)(1.*a),(double)(1.*b),(double)(1.*c));
		    		     		sstr >> a;
		    		     		this->volume = (int)(1.*a);
		    		     		sstr >> a;
		    		     		this->boundary = (bool)(1.*a==1);
		    		     		sstr >> a;
		    		     		this->sphericity = (double)(1.*a);
		    		     		sstr >> a;
		    		     		this->shapeFact = (double)(1.*a);
		    		     		sstr >> a;
		    		     		this->shapeFact2 = (double)(1.*a);
		    		     		sstr >> a;
		    		     		this->zAngle = (double)(1.*a);
		    		     		sstr >> a;
		    		     		this->globalcolor= (int)(1.*a);
		    		     		sstr >> a;
		    		     		this->father= (int)(1.*a);
		    		     		sstr >> a;
		    		     		this->fatherconnect= (int)(1.*a);
		    		     		sstr >> a;
		    		     		this->grandfather= (int)(1.*a);
		    		     		sstr >> a;
		    		     		this->grandfatherconnect= (int)(1.*a);
		    		     		sstr >> a;
		    		     		this->born= (int)(1.*a);
		    		     		sstr >> a;
		    		     		this->dead= (int)(1.*a);
		    		     		this->roisize = 64;
		    		     		ostringstream paramstr;
		    		     		while(sstr >> a)
		    		     		{
		    		     			paramstr << a << " ";
		    		     		}
		    		     		this->param = paramstr.str();
		    		     }
		    	  	}
	 	 }



	 Particle::Particle(const string fileDirectory, const string pName) {
		 this->scan = 0;
		 this->color = 0;
		 this->coord = Coord();
		 this->volume = 0;
		 this->boundary = false;;
		 this->sphericity=0;
		 this->shapeFact=0;
		 this->shapeFact2=0;
		 		this->zAngle=0;
		 		this->globalcolor=0;
		 		this->roisize = 64;
		 		this->father=-1;
		 		this->fatherconnect=-1;
		 		this->grandfather=-1;
		 		this->grandfatherconnect=-1;
		 		this->born=-1;
		 		this->dead=-1;
		        //this->cellNumber=-1;
		 		this->param="";

		 ostringstream fileName;
		 fileName << fileDirectory << "/" << pName;

		 ifstream File (fileName.str().c_str(),ios::in);
		    if (!File)
		    {
		    	LERROR << "Error : impossible to open file " << fileName.str() << " in read only mode...";
		    	//cerr << "Particle color is " << color << " from scan " << scan << endl;
		    }

		    double a,b,c;
		    		 string dataline="";
		    		 stringstream sstr;
		 		    char schar[10000];
		 		    File.getline(schar,10000);
		 		    dataline+=schar;
		    		 sstr.str(dataline);
		    		 if(sstr.str().size()>1)
		    		 {
		    		     sstr >> a;
		    		     if(!isnan(1.*a))
		    		     {
		    		    	 	this->scan = (int)(1.*a);
		    		    	 	sstr >> a;
		    		    	    this->color = (int)(1.*a);
		    		     		sstr >> a;
		    		     		sstr >> b;
		    		     		sstr >> c;
		    		     		this->coord = Coord((double)(1.*a),(double)(1.*b),(double)(1.*c));
		    		     		sstr >> a;
		    		     		this->volume = (int)(1.*a);
		    		     		sstr >> a;
		    		     		this->boundary = (bool)(1.*a==1);
		    		     		sstr >> a;
		    		     		this->sphericity = (double)(1.*a);
		    		     		sstr >> a;
		    		     		this->shapeFact = (double)(1.*a);
		    		     		sstr >> a;
		    		     		this->shapeFact2 = (double)(1.*a);
		    		     		sstr >> a;
		    		     		this->zAngle = (double)(1.*a);
		    		     		sstr >> a;
		    		     		this->globalcolor= (int)(1.*a);
		    		     		sstr >> a;
		    		     		this->father= (int)(1.*a);
		    		     		sstr >> a;
		    		     		this->fatherconnect= (int)(1.*a);
		    		     		sstr >> a;
		    		     		this->grandfather= (int)(1.*a);
		    		     		sstr >> a;
		    		     		this->grandfatherconnect= (int)(1.*a);
		    		     		sstr >> a;
		    		     		this->born= (int)(1.*a);
		    		     		sstr >> a;
		    		     		this->dead= (int)(1.*a);
		    		     		this->roisize = 64;
		    		     		ostringstream paramstr;
		    		     		while(sstr >> a)
		    		     		{
		    		     			paramstr << a << " ";
		    		     		}
		    		     		this->param = paramstr.str();
		    		     }
		    	  	}
	 	 }
	 //operator P() { return PSet<P>();}

	 void Particle::PUpdate(const string fileDirectory, int scan, int color) {
		 ostringstream fileName;
		 char buff [200];
		 sprintf(buff,"/Particle_%04d_%05d",scan,color);

		 fileName << fileDirectory << buff;

		 ifstream File (fileName.str().c_str(),ios::in);
		    if (!File)
		    {
		    	cerr << "Error : impossible to open file " << fileName.str() << " in read only mode..." << endl;
		    }
		    else
		    {
		    	cout << "File of Particle opened for update : color is " << color << " from scan " << scan << endl;

		    }

		    string dataline="";
		    stringstream sstr;
		    char schar[10000];
		    File.getline(schar,10000);
		    dataline+=schar;
		    this->PUpdate(dataline);
	 	 }

	 void Particle::PUpdate(string linestr) {
	 		 double a,b,c;
	 		 string paramline;
	 		 stringstream sstr;
	 		 sstr.str(linestr);
	 		 if(sstr.str().size()>1)
	 		 {
	 		     sstr >> a;
	 		     if(!isnan(1.*a))
	 		     {
	 		    	 	this->scan = (int)(1.*a);
	 		    	 	sstr >> a;
	 		    	    this->color = (int)(1.*a);
	 		     		sstr >> a;
	 		     		sstr >> b;
	 		     		sstr >> c;
	 		     		this->coord = Coord((double)(1.*a),(double)(1.*b),(double)(1.*c));
	 		     		sstr >> a;
	 		     		this->volume = (int)(1.*a);
	 		     		sstr >> a;
	 		     		this->boundary = (bool)(1.*a==1);
	 		     		sstr >> a;
	 		     		this->sphericity = (double)(1.*a);
	 		     		sstr >> a;
	 		     		this->shapeFact = (double)(1.*a);
	 		     		sstr >> a;
	 		     		this->shapeFact2 = (double)(1.*a);
	 		     		sstr >> a;
	 		     		this->zAngle = (double)(1.*a);
	 		     		sstr >> a;
	 		     		this->globalcolor= (int)(1.*a);
	 		     		sstr >> a;
	 		     		this->father= (int)(1.*a);
	 		     		sstr >> a;
	 		     		this->fatherconnect= (int)(1.*a);
	 		     		sstr >> a;
	 		     		this->grandfather= (int)(1.*a);
	 		     		sstr >> a;
	 		     		this->grandfatherconnect= (int)(1.*a);
	 		     		sstr >> a;
	 		     		this->born= (int)(1.*a);
	 		     		sstr >> a;
	 		     		this->dead= (int)(1.*a);
    		     		this->roisize = 64;
    		     		ostringstream paramstr;
    		     		while(sstr >> a)
    		     		{
    		     			paramstr << a << " ";
    		     		}
    		     		this->param = paramstr.str();
	 		     }
	 	  	}
	 	 }

	 string Particle::write() const{
		 ostringstream sstr;
		 sstr << this->scan << " ";
		 sstr << this->color << " ";
		 sstr << this->coord.toString() << " ";
		 sstr << this->volume << " ";
		 sstr << this->boundary << " ";
		 sstr << this->sphericity << " ";
		 sstr << this->shapeFact << " ";
		 sstr << this->shapeFact2 << " ";
		 sstr << this->zAngle << " ";
		 sstr << this->globalcolor << " ";
		 sstr << this->father << " ";
		 sstr << this->fatherconnect << " ";
		 sstr << this->grandfather << " ";
		 sstr << this->grandfatherconnect << " ";
		 sstr << this->born << " ";
		 sstr << this->dead << " ";
		 sstr << this->param;
		 return sstr.str();
	 }

	 /*
	 string Particle::getFileName() const
	 {
		 char buff [200];
		 sprintf(buff,"/Particle_%04d_%05d",this->scan,this->color);
	 	 	return (string)buff;
	 }

	 void Particle::writeToFile(const string fileDirectory) const
	 {
		 	 	 ostringstream fileName;
		 	 	 char buff[200];
		 	 	 sprintf(buff,"/Particle_%04d_%05d",this->scan,this->color);

		 	 	 fileName << fileDirectory << buff;

	 			 ofstream fichier(fileName.str().c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier

	 	        if(fichier)  // si l'ouverture a réussi
	 	        {
	 	        	fichier << this->write() << endl;
	 	            fichier.close();  // on referme le fichier
	 	        }
	 	        else  // sinon
	 	                cerr << "Erreur à l'ouverture !" << endl;

	 	}

	 int Particle::getScan() const {
		return scan;
	}

	 void Particle::setScan(int scan) {
		this->scan = scan;
	}

	 int Particle::getColor() const {
		return color;
	}

	 void Particle::setColor(int color) {
		this->color = color;
	}

	 Coord Particle::getCoord() const {
		return coord;
	}

	 void Particle::setCoord(Coord coord) {
		this->coord = coord;
	}
	*/

	 double Particle::getVolume() const {
		return volume;
	}

	 void Particle::setVolume(double volume) {
		this->volume = volume;
	}

	 bool Particle::isBoundary() const {
		return boundary;
	}

	 void Particle::setBoundary(bool boundary) {
		this->boundary = boundary;
	}

	 double Particle::getSphericity() const {
		return sphericity;
	}

	 void Particle::setSphericity(double sphericity) {
		this->sphericity = sphericity;
	}
	
	 double Particle::getShapeFact() const {
		return shapeFact;
	}
	
	 double Particle::getShapeFact2() const {
		return shapeFact2;
	}
	
	 double Particle::getZAngle() const {
		return zAngle;
	}
	
	 int Particle::getGlobalColor() const {
		return globalcolor;
	}

	 void Particle::setGlobalColor(int globalcolor) {
		this->globalcolor = globalcolor;
	}
	
	 int Particle::getFather() const {
		return father;
	}

	 void Particle::setFather(int father) {
		this->father = father;
	}

	 int Particle::getFatherconnect() const {
		return fatherconnect;
	}

	 void Particle::setFatherconnect(int fatherconnect) {
		this->fatherconnect = fatherconnect;
	}

	 int Particle::getGrandfather() const {
		return grandfather;
	}

	 void Particle::setGrandfather(int grandfather) {
		this->grandfather = grandfather;
	}

	 int Particle::getGrandfatherconnect() const {
		return grandfatherconnect;
	}

	 void Particle::setGrandfatherconnect(int grandfatherconnect) {
		this->grandfatherconnect = grandfatherconnect;
	}

	 int Particle::getBorn() const {
		return born;
	}

	 void Particle::setBorn(int born) {
		this->born = born;
	}

	 int Particle::getDead() const {
		return dead;
	}

	 void Particle::setDead(int dead) {
		this->dead = dead;
	}

	 string Particle::getParam() const {
			return param;
	}

	 string Particle::tostring() const {
		ostringstream sstr;
		sstr << this->color << " ";
		sstr << this->coord.toString() << " ";
		sstr << this->volume << " ";
		sstr << this->sphericity << " ";
		sstr << this->globalcolor;
		return sstr.str();
	}
	
	 string Particle::writeHeader() const {//INfo en debut de ligne (ParticleSet, pas pour chaque état !!!
		ostringstream s;
		s << this->globalcolor << " ";
		s << this->grandfather << " ";
		s << this->grandfatherconnect << " ";
		s << this->father << " ";
		s << this->fatherconnect <<" ";
		s << this->born << " ";
		s << this->dead;
		return s.str();
	}
	
	 string Particle::fullInfoTostring() const {
		ostringstream s;
		s << this->color << " ";
		s << this->param;
		return s.str();
	}
	
	 string Particle::voidfullInfoTostring() const {
		ostringstream s;
		s << " 0 ";
		for(int i=0; i<37;i++)
		{ s << " 0 ";}
		return s.str();
	}
	/*
	 bool Particle::equalColor(const Particle& part) const {
		return (this->color == part.getColor());
	}
	*/

	 /*
	 Volume Particle::getParticleSubVolume(Volume& fullIm) const
	{
		fullIm.setRoi(ROI((int)this->getCoord().getX()-roisize/2,(int)this->getCoord().getY()-roisize/2,(int)this->getCoord().getZ()-roisize/2,roisize,roisize,roisize));
		int* new_color = new int[65256];
		for(int j = 0; j<65256; j++)
		{
			new_color[j] = 0;
		}
		new_color[(int)this->getColor()] = 1;

		Volume partRef = fullIm.duplicateRoi();
		partRef.recolor(new_color);
		return partRef;
	}
*/
	 PSet<Particle>  Particle::findBestCorrel_DICGPU(PSet<Particle>& potential, Correl3DGPU& correlator, Coord target){ //The very good one for a single solution
		 PSet<Particle>* potentialCorrel = new PSet<Particle>(potential.getName());
		//potentialCorrel.setName(potential.getName());
		//long startTime = System.currentTimeMillis();
		correlator.setImTarget((int)target.getZ(), (int)target.getY(), (int)target.getX());
		double* value = new double[4];
		value = correlator.correl();
		//cout << "Correl done (" << value[0] << ',' << value[1] << "," << value[2] << ") corr=" << value[3] << endl;
		Coord c = target.shift(value);
		//cout << "Coord " << target.toString() << endl;
		//cout << "Coord shifted " << c.toString() << endl;
		Particle *cible = potential.findClosest(target.shift(value));
		//cout << "Closest found particle " << cible->getColor() << " coord " << cible->getCoord().toString() <<  endl;
		if(cible != 0)
            potentialCorrel->addP(cible);
		//potentialCorrel.addParticle(potential.findClosest(target.shift(value)));

		//cout << "Particle added\n" << endl;
		//long currentTime = (System.currentTimeMillis()-startTime) ;
		//printf("Correl : dx = "+ (int)value[0] + ", dy = "+ (int)value[1] + ", dz = " + (int)value[2] + ", corr = " + value[3] + ", time = "+currentTime + "ms");
		return *potentialCorrel;
	}



    PSet<Particle> Particle::findBestCorrel_noDIC(PSet<Particle>& potential, Coord target){ //For debbuging purpose
    	PSet<Particle>* potentialCorrel = new PSet<Particle>(potential.getName());
		Particle *cible = potential.findClosest(target);
		if(cible != 0)
            potentialCorrel->addP(cible);
		return *potentialCorrel;
	}

    PSet<Particle> Particle::findBestCorrel_noDIC_safe(PSet<Particle>& potential, Coord target){ //For debbuging purpose
    	PSet<Particle>* potentialCorrel = new PSet<Particle>(potential.getName());
		Particle *cible = potential.findClosest(target);
		if(cible != 0)
            potentialCorrel->addP(cible);
        return *potentialCorrel;
	}



	 Particle*  Particle::findClosest(PSet<Particle>& potential)
	{
		if(potential.Size()>0)
		{
			double bestDist=1000000000000;
			list<Particle*>::iterator best = potential.getIteratorBegin();
			for (list<Particle*>::iterator pit=potential.getIteratorBegin(); pit!=potential.getIteratorEnd(); ++pit)
			{
				double dist = this->distance(**pit);
				if(dist<bestDist)
				{
					best=pit;
					bestDist=dist;
				}
			}
			return *best;
		}
		else
		{
			Particle* emptyPart(0);
            // = new Particle();
			return emptyPart;
		}
	}

	 /*
	
	 double  Particle::distance(const Particle& part) const
	{
		return this->coord.dist(part.getCoord());
	}

	 double  Particle::distance(const Coord& XYZ) const
	{
		return this->coord.dist(XYZ);
	}


	    Volume Particle::getParticleSubVolumeSafe(Volume& src, const string volumeName) const
		{
			int* new_color = new int[65256];
			for(int j = 0; j<65256; j++)
			{
				new_color[j] = 0;
			}
			new_color[(int)this->getColor()] = 1;
			//stack.setRoi(new Rectangle((int)this->getCoord().getX()-roisize/2,(int)this->getCoord().getY()-roisize/2,roisize,roisize));
			ROI roi = ROI((int)this->getCoord().getX()-roisize/2,(int)this->getCoord().getY()-roisize/2,(int)this->getCoord().getZ()-roisize/2,roisize,roisize,roisize);
		    ROI image = ROI(0,0,0,src.getdimX(),src.getdimY(),src.getdimZ());
		    ROI intersect = roi.intersection(image);
			Volume dest;
		    if(intersect.contains(roi))
		    {
		    	src.setRoi(roi);
		    	dest = src.duplicateRoi();
		    }
		    else
		    {
		    	printf("border ROI");
		    	src.setRoi(intersect);
		    	dest = src.duplicateRoi();
		    }
		    dest.recolor(new_color);
		    return dest;
		}
*/

/*

	 ParticlesSet Particle::findBestCorrel(ParticlesSet& input) const
	{
		ParticlesSet potential = ParticlesSet(input);
		ParticlesSet potentialCorrel;
		potentialCorrel.setName(potential.getName());
		while(potential.Size()>0)
		{
			double bestCorrel=10000;
			int best=0;
			for(int i=0; i<potential.Size();i++)
			{
				double correl = abs((this->volume-potential.getParticle(i)->getVolume())/this->volume);
				if(correl<bestCorrel)
				{
					best=i;
					bestCorrel=correl;
				}
			}
			potentialCorrel.addParticle(potential.getParticle(best));
			potential.removeParticle(best);
		}
		return potentialCorrel;
	}

	 ParticlesSet Particle::findBestCorrel_larger(const ParticlesSet& input) const
	{
		ParticlesSet potential = ParticlesSet(input);
		ParticlesSet potentialCorrel;
		potentialCorrel.setName(potential.getName());
		
		while(potential.Size()>0)
		{
			double bestCorrel=100000;
			int best=0;
			for(int i=0; i<potential.Size();i++)
			{
				if(potential.getParticle(i)->getVolume()<this->volume*0.8)
				{
					potential.removeParticle(i);
				}
				else if(potential.getParticle(i)->getVolume()>this->volume*0.95)
				{
					double correl = abs((this->volume-potential.getParticle(i)->getVolume())/this->volume);
					if(correl<bestCorrel)
					{
						best=i;
						bestCorrel=correl;
					}
				}
				else
				{
					double correl = 10000*abs((this->volume-potential.getParticle(i)->getVolume())/this->volume);
					if(correl<bestCorrel)
					{
						best=i;
						bestCorrel=correl;
					}
				}
			}
			if(potential.Size()>0)
			{
				potentialCorrel.addParticle(potential.getParticle(best));
				potential.removeParticle(best);
			}
		}
		return potentialCorrel;
	}

	 ParticlesSet Particle::findBestCorrel_smaller(const ParticlesSet& input) const
	{
		 ParticlesSet potential = ParticlesSet(input);
		 ParticlesSet potentialCorrel;
		 potentialCorrel.setName(potential.getName());
		while(potential.Size()>0)
		{
			double bestCorrel=100000;
			int best=0;
			for(int i=0; i<potential.Size();i++)
			{
				if(potential.getParticle(i)->getVolume()>this->volume*1.2)
				{
					potential.removeParticle(i);
				}
				else if(potential.getParticle(i)->getVolume()<this->volume*1.05)
				{
					double correl = abs((this->volume-potential.getParticle(i)->getVolume())/this->volume);
					if(correl<bestCorrel)
					{
						best=i;
						bestCorrel=correl;
					}
				}
				else
				{
					double correl = 10000*abs((this->volume-potential.getParticle(i)->getVolume())/this->volume);
					if(correl<bestCorrel)
					{
						best=i;
						bestCorrel=correl;
					}
				}
			}
			if(potential.Size()>0)
			{
				potentialCorrel.addParticle(potential.getParticle(best));
				potential.removeParticle(best);
			}
		}
		return potentialCorrel;
	}
*/

	/**
	 ParticlesSet Particle::findBestCorrel_DIC(ParticlesSet potential, Stack imRef, Stack imFinal)
	{
		ParticlesSet potentialCorrel = new ParticlesSet(potential.getName());
		double correl[][] = new double[potential.Size()][2];
		clock_t startTime = clock();
		for(int i=0; i<potential.Size();i++)
		{
			Correl3D corr = new Correl3D(true,5);
			ImagePlus part = potential.getParticle(i).getParticleImagePlus(imFinal,"part"+potential.getParticle(i).getColor());
			//printf("volume = "+correl[10000][0]);
			//printf("volume = "+vol(part.getStack()));
			correl[i][0] = i;
			correl[i][1] = corr.correl(imRef,part);
			//printf("with particle "+potential.getParticle(i).getColor()+" x="+potential.getParticle(i).getCoord().getX()+" y="+potential.getParticle(i).getCoord().getY()+" z="+potential.getParticle(i).getCoord().getZ()+" corr="+correl[i][1]+"\n");
			
			part.close();
		}
		Tracking_Particles_with_DIC.quicksortdouble(correl,0,potential.Size()-1,1);
		for(int i=0; i<potential.Size();i++)
		{
			potentialCorrel.addParticle(potential.getParticle((int)correl[i][0]));
		}
		long currentTime = (System.currentTimeMillis()-startTime)/1000 ;
		printf("Time for "+potential.Size()+" particles : "+currentTime + "s");
		return potentialCorrel;
	}
	*/
/*
 *
	 ParticlesSet Particle::findBestCorrel_DICGPU(ParticlesSet potential, Volume imRef, Volume imFinal, Correl3DGPU correlator)
	{
		//Correl3DGPU corr = new Correl3DGPU();
		ParticlesSet potentialCorrel = new ParticlesSet(potential.getName());
		double correl[][] = new double[potential.Size()][2];
		//long startTime = System.currentTimeMillis();
		correlator.setImRef(imRef.getdimX(), imRef.getdimY(), imRef.getdimZ());
		for(int i=0; i<potential.Size();i++)
		{
			
			Volume part = potential.getParticle(i).getParticleSubVolumeSafe(imFinal,"part"+potential.getParticle(i).getColor());
			correlator.setImTarget(part);
			correl[i][0] = i;
			double value[] = correlator.correl();
			correl[i][1] = value[3];
			//printf("with particle "+potential.getParticle(i).getColor()+" x="+potential.getParticle(i).getCoord().getX()+" y="+potential.getParticle(i).getCoord().getY()+" z="+potential.getParticle(i).getCoord().getZ()+" corr="+correl[i][1]+"\n");
		}
		quicksortdouble(correl,0,potential.Size()-1,1);
		for(int i=0; i<potential.Size();i++)
		{
			potentialCorrel.addParticle(potential.getParticle((int)correl[i][0]));
		}
		//long currentTime = (System.currentTimeMillis()-startTime)/1000 ;
		//printf("Time for "+potential.Size()+" particles : "+currentTime + "s");
		return potentialCorrel;
	}


	 ParticlesSet Particle::findBestCorrel_DICGPU(ParticlesSet potential, Correl3DGPU correlator){
		 //The very good one
		//Correl3DGPU corr = new Correl3DGPU();
		ParticlesSet potentialCorrel = new ParticlesSet(potential.getName());
		double correl[][] = new double[potential.Size()][2];
		for(int i=0; i<potential.Size();i++)
		{
			correlator.setImTarget((int)potential.getParticle(i).getCoord().getZ(), (int)potential.getParticle(i).getCoord().getY(), (int)potential.getParticle(i).getCoord().getX());
			correl[i][0] = i;
			double* value = correlator.correl();
			correl[i][1] = value[3];
		}
		quicksortdouble(correl,0,potential.Size()-1,1);
		for(int i=0; i<potential.Size();i++)
		{
			potentialCorrel.addParticle(potential.getParticle((int)correl[i][0]));
		}
		//clock_t currentTime = diffclock(clock(),startTime) ;
		//printf("Time for "+potential.Size()+" particles : "+currentTime + "ms");
		return potentialCorrel;
	}

*/

	


	 /**
	 ParticlesSet  Particle::findBestCorrel_DIC_multi(final ParticlesSet potential, final ImagePlus imRef, final ImagePlus imFinal)
	{
		ParticlesSet potentialCorrel = new ParticlesSet(potential.getName());
		final double[][] correl = new double[potential.Size()][2];
		final AtomicInteger ai = new AtomicInteger(0);
		long startTime = System.currentTimeMillis();
		for (int ithread = 0; ithread < threads.length; ithread++) {

		    // Concurrently run in as many threads as CPUs

			threads[ithread] = new Thread() {
		
			 void run() 
			{
				for (int superi = ai.getAndIncrement(); superi < potential.Size(); superi = ai.getAndIncrement()) {
					int current = superi;
					Correl3D corr = new Correl3D(true,5);
					ImagePlus part = potential.getParticle(current).getParticleImagePlus(imFinal,"part"+potential.getParticle(current).getColor());
					correl[current][0] = current;
					correl[current][1] = corr.correl(imRef,part);
					part.close();
				}
			}
			};
			}
			startAndJoin(threads);
			
		Tracking_Particles_with_DIC.quicksortdouble(correl,0,potential.Size()-1,1);
		for(int i=0; i<potential.Size();i++)
		{
			potentialCorrel.addParticle(potential.getParticle((int)correl[i][0]));
		}
		long currentTime = (System.currentTimeMillis()-startTime)/1000 ;
		printf("Time for "+potential.Size()+" particles : "+currentTime + "s");
		return potentialCorrel;
	}
	*/



/**
 	Stack Particle::recolor(Stack stack, int[] val){
		
		final ImageStack stack1 = duplicate(stack);
		final AtomicInteger ai = new AtomicInteger(1);
		final int[] value  = val;
		final int dimX = stack1.getWidth();
		final int dimY = stack1.getHeight();
		final int dimZ = stack1.getSize();
		
		
		if(stack1.getProcessor(1) instanceof ByteProcessor)
		{
			for (int ithread = 0; ithread < threads.length; ithread++) {

		    // Concurrently run in as many threads as CPUs

			threads[ithread] = new Thread() {
		
			 void run() 
			{	
				for (int superi = ai.getAndIncrement(); superi <= dimZ; superi = ai.getAndIncrement()) {
					int current = superi;
					int pix,m;
					byte[] pixels = (byte[]) stack1.getProcessor(current).getPixels();
					for(int  j=0;j<dimY;j++)
						{
							for(int  i=0;i<dimX;i++)
							{
								m=j*(dimX)+i;
								pix = pixels[m] & 0xff;
								pixels[m] = (byte)value[pix];
							}
						}
				}
			}
			};
			}
			startAndJoin(threads);
		}
		else
		{
			for (int ithread = 0; ithread < threads.length; ithread++) {

		    // Concurrently run in as many threads as CPUs

			threads[ithread] = new Thread() {
		
			 void run() 
			{	
				for (int superi = ai.getAndIncrement(); superi <= dimZ; superi = ai.getAndIncrement()) {
					int current = superi;
					int pix,m;
					short[] pixels = (short[]) stack1.getProcessor(current).getPixels();
					for(int  j=0;j<dimY;j++)
						{
							for(int  i=0;i<dimX;i++)
							{
								m=j*(dimX)+i;
								pix = pixels[m] & 0xffff;
								pixels[m] = (short)value[pix];
							}
						}
				}
			}
			};
			}
			startAndJoin(threads);
		}
		return stack1;
		}

*/

	 /**
 	 Stack Particle::duplicate(Stack src) {
 			    int xSize = src.getdimX();
 			    int ySize = src.getdimY();
 			    int zSize = src.getdimZ();

 			    Stack dest = new Stack(xSize, ySize);
 			    for (int z = 1; z <= zSize; ++z) {
 			      dest.addSlice(Image(src.getSlice(z)));
 			    }
 			    return dest;
 			 }
	   
	  Stack Particle::duplicateEmpty(Stack src) {
		    int xSize = src.getdimX();
		    int ySize = src.getdimY();
		    int zSize = src.getdimZ();

		    Stack dest = new Stack(xSize, ySize);
		    for (int z = 1; z <= zSize; ++z) {
		      dest.addSlice(Image(src.getSlice(z).getdimX(),src.getSlice(z).getdimY()));
		    }
		    return dest;
		 }
*/
	   
	
/**
	    Stack Particle::duplicateRoi(final Stack src, int zinit, int depth) {
		    int xSize = src.getRoi().width;
		    int ySize = src.getRoi().height;
		    int dimZ = src.getSize();
//		    int yOrig = src.getRoi().y;
		    Rectangle roi = new Rectangle(src.getRoi());
		    Rectangle image = new Rectangle(0,0,src.getWidth(),src.getHeight());
		    Rectangle intersect = roi.intersection(image);
			ImageStack dest = new ImageStack(xSize, ySize);
		    if(intersect.contains(roi))
		    {
		    	//printf("full ROI");
		    	for (int z = zinit; z <= zinit+depth; ++z) {
		    		if((z<=0)||(z>dimZ))
		    		{
		    			dest.addSlice("out",src.getProcessor(1).createProcessor(xSize,ySize));
		    		}
		    		else
		    		{
		    			ImageProcessor iptmp =src.getProcessor(z).duplicate();
		    			iptmp.setRoi(roi);
		    			dest.addSlice(src.getSliceLabel(z),iptmp.crop());		    			
		    		}
		    	}
		    }
		    else
		    {
		    	printf("border ROI");
		    	src.setRoi(intersect);
		    	for (int z = zinit; z <= zinit+depth; ++z) {
		    		if((z<=0)||(z>dimZ))
		    		{
		    			dest.addSlice("out",src.getProcessor(1).createProcessor(xSize,ySize));
		    		}
		    		else
		    		{
		    			ImageProcessor iptmp = src.getProcessor(1).createProcessor(xSize,ySize);
		    			iptmp.copyBits(src.getProcessor(z).duplicate().crop(), intersect.x-roi.x, intersect.y-roi.y, 0);
		    			dest.addSlice(src.getSliceLabel(z),iptmp);
		    		}
		    	}
		    }
		    dest.setColorModel(src.getColorModel());
		    return dest;
		  }
	
*/


Particle Particle::findBestCorrel_DICGPU(Correl3DGPU& correlator)
{
		 return (Particle)(P::findBestCorrel_DICGPU(correlator));
}

Particle Particle::findBestCorrel_DICGPU(Correl3DGPU& correlator, Coord target)
{
		 return (Particle)(P::findBestCorrel_DICGPU(correlator,target));
}

Particle Particle::optimizeCorrel_DICGPU(Correl3DGPU& correlator, Coord target)
{
		 return (Particle)(P::optimizeCorrel_DICGPU(correlator, target));
}

//virtual double distance(const P& part) const;
//virtual double distance(const Coord& XYZ) const;



void Particle::PUpdate(Particle P) {
		this->scan = P.getScan();
		this->color = P.getColor();
		this->coord = P.getCoord();
		this->volume = P.getVolume();
		this->boundary = P.isBoundary();;
		this->sphericity=P.getSphericity();
		this->shapeFact=P.getShapeFact();
		this->shapeFact2=P.getShapeFact2();
		this->zAngle=P.getZAngle();
		this->globalcolor=P.getGlobalColor();
		this->roisize = 64;
		this->father=P.getFather();
		this->fatherconnect=P.getFatherconnect();
		this->grandfather=P.getGrandfather();
		this->grandfatherconnect=P.getGrandfatherconnect();
		this->born=P.getBorn();
		this->dead=P.getDead();
		//this->cellNumber=-1;
		this->param= P.getParam();
		this->correl = P.getCorrel();
		this->totalcorrel = P.getTotalCorrel();

}


bool Particle::operator< (Particle p)
{
        return globalcolor<p.getGlobalColor(); 
}

