#include "Correl3DGPU.h"
#include "Correl3DGPU.cuh"
#define CONSTANT_PI_PER_DEG 0.017453293f

/*!
 * \file Correl3DGPU.cu
 * \brief Correlation 3D
 * \author pierre.lhuissier@simap.grenoble-inp.fr
 * \version 0.1
 */

using namespace std;
   
 
/*!
*  \brief Constructor
*
*  Constructor of Correl3DGPU class
*
*  \param imRef : path to reference 3D image
*  \param imTarget : path to target 3D image
*/

Correl3DGPU::Correl3DGPU(const char * imRef, const char* imTarget) {

    setRefDimensions(imRef);
    setTargetDimensions(imTarget);
    fftD = FIXED;
    fftH = FIXED;
    fftW = FIXED;
    dataD = FIXED;
    dataH = FIXED;
    dataW = FIXED;
    kernelD = FIXED;
    kernelH = FIXED;
    kernelW = FIXED;
    fullData_size = fullDataD * fullDataH * fullDataW;
    fullKernel_size = fullKernelD * fullKernelH * fullKernelW;
    data_size = dataD * dataH * dataW;
    kernel_size = kernelD * kernelH * kernelW;
    padded_size = fftD * fftH * fftW;

    mem_fullData_byte = fullData_size*sizeof(unsigned char);
    mem_fullKernel_byte = fullKernel_size*sizeof(unsigned char);
    mem_data_byte = data_size*sizeof(float);
    mem_kernel_byte = kernel_size*sizeof(float);
    mem_padded_byte = padded_size*sizeof(float);
    mem_param_byte = ceil(sizeof(Parameters) / 128.0 )*128;
    mem_shared_byte = kernel_size*sizeof(double);
    mem_deformation_byte = ceil(DEFORMATIONSIZE*sizeof(deformationType) / 128.0 )*128;

    aniso = true;	
    FFT = false;
    normalize = true;
    fromFull = true;
    window = "none";
    reducedWindow = false;
    filters = false;
    intersect = true;
    displThr = false;
    rota = false;
    dX = FIXED/2;
    dY = FIXED/2;
    dZ = FIXED/2;
	   	
    Xold = make_float3();        
    Xnew = make_float3();        

    domainSize = FIXED/2;
    //displacement = FIXED/2;
    displacementX = FIXED/2;
    displacementY = FIXED/2;
    displacementZ = FIXED/2;
    
    displX = FIXED/2;
    displY = FIXED/2;
    displZ = FIXED/2;
    
    optimize = false;
    optimizeDisplacementX = FIXED/8;
    optimizeDisplacementY = FIXED/8;
    optimizeDisplacementZ = FIXED/8;
    //halfPattern = FIXED/4;
    halfPatternX = FIXED/4;
    halfPatternY = FIXED/4;
    halfPatternZ = FIXED/4;
    cropHalfPatternX = 1000;
    cropHalfPatternY = 1000;
    cropHalfPatternZ = 1000;

    rotAxis = 2;
    nrot=10;
    deltatheta=0.1;
    correlThr = 0.7;

    deviceFree = true;
    deviceFullImFree = true;
    hostFree = true;
    paramFree = true;
    bindedtex = false;
    usetex=false;

    deviceTotalMem = 0;	        

    h_max = new float[0];
    hostFree = false;
    dev = 0;
    cudaSetDevice(dev);
    cudaDeviceReset();
    cudaGetDeviceProperties(&deviceProp, dev);
}


void Correl3DGPU::setParam(Parameters &param) {
  if(paramFree)
  {
    cudaMalloc( (void**) &d_Param, mem_param_byte );
    deviceTotalMem += mem_param_byte;
    paramFree=false;
  }
  this->h_Param = &param;
  cudaMemcpy(d_Param, &param, sizeof(Parameters), cudaMemcpyHostToDevice) ;
 
  optimize = (&param)->optimize;

  aniso = (&param)->aniso;
  if(aniso)
  {
    displacementX = (&param)->thrX;
    displacementY = (&param)->thrY;
    displacementZ = (&param)->thrZ;
    if((&param)->optimize)
    {
      optimizeDisplacementX = (&param)->optiThrX;
      optimizeDisplacementY = (&param)->optiThrY;
      optimizeDisplacementZ = (&param)->optiThrZ;
    }
    halfPatternX = (&param)->correlDomainX;
    halfPatternY = (&param)->correlDomainY;
    halfPatternZ = (&param)->correlDomainZ;
  }
  else
  {
    displacementX = (&param)->thrXYZ;
    displacementY = (&param)->thrXYZ;
    displacementZ = (&param)->thrXYZ;
    if((&param)->optimize)
    {
      optimizeDisplacementX = (&param)->optiThrXYZ;
      optimizeDisplacementY = (&param)->optiThrXYZ;
      optimizeDisplacementZ = (&param)->optiThrXYZ;
    }
    halfPatternX = (&param)->correlDomain;
    halfPatternY = (&param)->correlDomain;
    halfPatternZ = (&param)->correlDomain;
  }
  
  usetex = (&param)->usetex;
  
  rota = (&param)->rota;
  if(rota)
  {
    usetex = true;
    rotAxis = (&param)->rotAxis;
    nrot = (&param)->nrot;
    deltatheta = (&param)->deltaTheta;
  }

  correlThr = (&param)->correlThr;
}

/*!
*  \brief Destructor
*
*  Destructor of Correl3DGPU class
*/

void Correl3DGPU::finalize()
{
  if(fromFull)
	freeFullImg();
		
  free();

}

/*!
*  \brief Set optimize conditions
*
*  If true, Displacements are optimizeDisplacements
*/

void Correl3DGPU::setOptimize(bool optimize) {
	this->optimize = optimize;
}
		
/*!
*  \brief Set filter option
*
*  If true, apply filter to correlation result
*/

void Correl3DGPU::setFilter(bool filt) {
		filters = filt;;
}
	
/*!
*  \brief Set intersection option
*
*  If true, normalize the correlation result by accounting for partial intersection of correlation windows
*/

void Correl3DGPU::setIntersect(bool inter) {
		intersect = inter;;
}
	
/*!
*  \brief Set window option
*
*  \param windowType, can be "none", "hanning", "exp"
*/

void Correl3DGPU::setWindow(string windowType) {
		window=windowType;
}
	
/*!
*  \brief Set reducedWindow option
*
*  If true, the target window is reduced to the correlation domain
*/
	
void Correl3DGPU::setReducedWindow(bool red) {
		reducedWindow=red;
}
	
/*!
*  \brief Set normalize option
*
*  If true, normalize the correlation result by accounting for partial intersection of correlation windows
*/

void Correl3DGPU::setNormalize(bool norm) {
        normalize=norm;
}

/*!
*  \brief Set FFT option
*
*  If true, perform correlation with FFT
*/

void Correl3DGPU::setFFT(bool fft) {
        FFT=fft;
}

/*!
*  \brief Set aniso option
*
*  If true, perform correlation with anisotropic window
*/

void Correl3DGPU::setaniso(bool aniso) {
        this->aniso=aniso;
}

void Correl3DGPU::setusetex(bool usetex) {
        this->usetex=usetex;
}			

bool Correl3DGPU::getusetex() {
        return this->usetex;
}/*!
*  \brief Set displacement threshold
*
*  \param thr : If true, use the threshold displacement values to limit allowed displacement
*  \param thrZ : allowed displacement in z direction
*  \param thrY : allowed displacement in y direction
*  \param thrX : allowed displacement in x direction
*/

void Correl3DGPU::setdisplThr(bool thr, int thrZ, int thrY, int thrX) {
	displThr = thr;
    if(displThr)
    {
        this->dZ = thrZ;
        this->dY = thrY;
        this->dX = thrX;
	}
}

/*!
*  \brief Set maximal displacement
*
*  \param displacement
*/

void Correl3DGPU::setDisplacement(int displacement) {
    //this->displacement = displacement;
    //if(!aniso)
    //{
        this->displacementX = displacement;
        this->displacementY = displacement;
        this->displacementZ = displacement;
    //}
}

/*!
*  \brief Set maximal displacementX
*
*  \param displacement
*/

void Correl3DGPU::setDisplacementX(int displacement) {
    this->displacementX = displacement;
}		

/*!
*  \brief Set maximal displacementY
*
*  \param displacement
*/

void Correl3DGPU::setDisplacementY(int displacement) {
    this->displacementY = displacement;
}

/*!
*  \brief Set maximal displacementZ
*
*  \param displacement
*/

void Correl3DGPU::setDisplacementZ(int displacement) {
    this->displacementZ = displacement;
}

/*!
*  \brief Set half pattern size
*
*  \param pattern : X Radius of correlation domain
*/

void Correl3DGPU::setHalfPatternX(int pattern) {
        this->halfPatternX = pattern;
        this->setCropHalfPatternX(this->cropHalfPatternX);
//this->domainSize=2*pattern+1;
}

/*!
*  \brief Set half pattern size
*
*  \param pattern : Y Radius of correlation domain
*/

void Correl3DGPU::setHalfPatternY(int pattern) {
        this->halfPatternY = pattern;
        this->setCropHalfPatternY(this->cropHalfPatternY);
        //this->domainSize=2*pattern+1;
}

/*!
*  \brief Set half pattern size
*
*  \param pattern : Z Radius of correlation domain
*/

void Correl3DGPU::setHalfPatternZ(int pattern) {
        this->halfPatternZ = pattern;
        this->setCropHalfPatternZ(this->cropHalfPatternZ);
        //this->domainSize=2*pattern+1;
}


/*!
*  \brief Set reduced half pattern size
*
*  \param pattern : X Radius of correlation domain
*/

void Correl3DGPU::setCropHalfPatternX(int pattern) {
        this->cropHalfPatternX = min(pattern,this->halfPatternX);
        //this->domainSize=2*pattern+1;
}

/*!
*  \brief Set reduced half pattern size
*
*  \param pattern : Y Radius of correlation domain
*/

void Correl3DGPU::setCropHalfPatternY(int pattern) {
        this->cropHalfPatternY = min(pattern,this->halfPatternY);
        //this->domainSize=2*pattern+1;
}

/*!
*  \brief Set half reduced pattern size
*
*  \param pattern : Z Radius of correlation domain
*/

void Correl3DGPU::setCropHalfPatternZ(int pattern) {
        this->cropHalfPatternZ = min(pattern,this->halfPatternZ);
        //this->domainSize=2*pattern+1;
}

/*!
*  \brief Set half pattern size
*
*  \param pattern : Radius of correlation domain
*/

void Correl3DGPU::setHalfPattern(int pattern) {
        //this->halfPattern = pattern;
        //if(!aniso)
       // {
            this->halfPatternX = pattern;
            this->halfPatternY = pattern;
            this->halfPatternZ = pattern;
            this->setCropHalfPatternX(this->cropHalfPatternX);
            this->setCropHalfPatternY(this->cropHalfPatternY);
            this->setCropHalfPatternZ(this->cropHalfPatternZ);
        //}
        //this->domainSize=2*pattern+1;
}
/*!
*  \brief Set correlation domain
*
*  \param domain : Size of correlation domain
*/

void Correl3DGPU::setDomain(int domain) {
        this->domainSize = domain;
}
	
/*!
*  \brief Set correlation Threshold
*
*  \param thr : Correlation threshold
*/

void Correl3DGPU::setCorrelThr(double thr) {
        this->correlThr = thr;
}

/*!
*  \brief Get correlation Threshold
*
*  \return thr : Correlation threshold
*/

double Correl3DGPU::getCorrelThr() {
        return this->correlThr;
}

/*!
*  \brief Set fromFullImage option
*
*  If true, vopy the full image to the device once
*/
	
void Correl3DGPU::setfromFull(bool useFull) {
    fromFull=useFull;
}

/*!
*  \brief Allocate memory and initialize handle
*/
void Correl3DGPU::init()
{	
    LINFO << "Initialize GPU" ;
    
    displX = displacementX;
    displY = displacementY;
    displZ = displacementZ;
    if(optimize)
    {
      displX = optimizeDisplacementX;
      displY = optimizeDisplacementY;
      displZ = optimizeDisplacementZ;
    }

    if(usetex)
    {
        mem_shared_byte=(2*halfPatternX+1)*(2*halfPatternY+1)*5*sizeof(double);
        cudaMalloc( (void**) &d_newdef, mem_deformation_byte );
        cudaMalloc( (void**) &d_olddef, mem_deformation_byte );
        h_olddef = (deformationType *)malloc(DEFORMATIONSIZE*sizeof(deformationType));
	h_newdef = (deformationType *)malloc(DEFORMATIONSIZE*sizeof(deformationType));
	h_bestdef = (deformationType *)malloc(DEFORMATIONSIZE*sizeof(deformationType));
        deviceTotalMem += mem_deformation_byte*2;
		
        if(ROTATION)
        {
            cudaMalloc((void **)&d_res, ((2*displX+1)*(2*displY+1)*(2*displZ+1)*(2*nrot+1))*sizeof(float));
            deviceTotalMem += ((2*displX+1)*(2*displY+1)*(2*displZ+1)*(2*nrot+1))*sizeof(float);
            LDEBUG << format("Allocated %d floats for d_res\n", (1+2*displX)*(1+2*displY)*(1+2*displZ)*(2*nrot+1));
        }
        else
        {
            cudaMalloc((void **)&d_res, ((2*displX+1)*(2*displY+1)*(2*displZ+1))*sizeof(float));
            deviceTotalMem += ((2*displX+1)*(2*displY+1)*(2*displZ+1))*sizeof(float);
            LDEBUG << format("Allocated %d floats for d_res\n", (1+2*displX)*(1+2*displY)*(1+2*displZ));
        } 
        LINFO << format("Allocated %.4f Mbytes / %.0f Mbytes (%.4f %) on GPU memory for results\n",(float)deviceTotalMem/1048576.0f, (float)deviceProp.totalGlobalMem/1048576.0f, (100.*deviceTotalMem)/deviceProp.totalGlobalMem);
	//cublasInit();
        cublasCreate(&cbhandle);
        deviceFree = false;
    }
    else
    {
        if(FFT)
        {
		cudaMalloc((void **)&d_Data,  mem_data_byte );
        deviceTotalMem += mem_data_byte;

	    cudaMalloc((void **)&d_Kernel, mem_kernel_byte );
        deviceTotalMem += mem_kernel_byte;

	    cudaMalloc((void **)&d_PaddedData,   mem_padded_byte);
        deviceTotalMem += mem_padded_byte;
	    
        cudaMalloc((void **)&d_PaddedKernel, mem_padded_byte);
        deviceTotalMem += mem_padded_byte;

	    cudaMalloc((void **)&d_DataSpectrum,   fftD * fftH * (fftW / 2 + 1) * 2* sizeof(float)) ;
        deviceTotalMem += fftD * fftH * (fftW / 2 + 1) * 2* sizeof(float);

	    cudaMalloc((void **)&d_KernelSpectrum, fftD * fftH * (fftW / 2 + 1) * 2* sizeof(float)) ;
        deviceTotalMem += fftD * fftH * (fftW / 2 + 1) * 2* sizeof(float);
	    
        LDEBUG << format("Allocated %.0f Mbytes / %.0f Mbytes (%.0f %) on GPU memory\n\n",(float)deviceTotalMem/1048576.0f, (float)deviceProp.totalGlobalMem/1048576.0f, (100.*deviceTotalMem)/deviceProp.totalGlobalMem);
	    
        cufftPlan3d(&fftVolumeFwd, fftD, fftH, fftW, CUFFT_R2C);
	    cufftPlan3d(&fftVolumeInv, fftD, fftH, fftW, CUFFT_C2R);
	        
		//cublasInit();
        cublasCreate(&cbhandle);
        deviceFree = false;
        }
	else
        {
            int patternX = 2*halfPatternX+1;
            int patternY = 2*halfPatternY+1;
            int patternZ = 2*halfPatternZ+1;
            int maxpattern = (int)(max(patternX,patternY)*1.5);
	    /*
            if(rota)
            {
              cudaMalloc((void **)&d_Data,  ((maxpattern+2*displX)*(maxpattern+2*displY)*(patternZ+2*displZ))*sizeof(float));
              deviceTotalMem += ((maxpattern+2*displX)*(maxpattern+2*displY)*(patternZ+2*displZ))*sizeof(float);
              LDEBUG << format("Allocated %d floats for d_Data\n", (maxpattern+2*displX)*(maxpattern+2*displY)*(patternZ+2*displZ));
            }
            else
            {
            */
            cudaMalloc((void **)&d_Data,  ((patternX+2*displX)*(patternY+2*displY)*(patternZ+2*displZ))*sizeof(float));
            deviceTotalMem += ((patternX+2*displX)*(patternY+2*displY)*(patternZ+2*displZ))*sizeof(float);
            LDEBUG << format("Allocated %d floats for d_Data\n", (patternX+2*displX)*(patternY+2*displY)*(patternZ+2*displZ));
            //}

        cudaMalloc((void **)&d_Kernel, patternX*patternY*patternZ*sizeof(float));
        deviceTotalMem += patternX*patternY*patternZ*sizeof(float);
        LDEBUG << format("Allocated %d floats for d_Kernel\n", (patternX*patternY*patternZ));
        
	cudaMalloc((void **)&d_res, ((2*displX+1)*(2*displY+1)*(2*displZ+1))*sizeof(float));
        deviceTotalMem += ((2*displX+1)*(2*displY+1)*(2*displZ+1))*sizeof(float);
        LDEBUG << format("Allocated %d floats for d_res\n", (1+2*displX)*(1+2*displY)*(1+2*displZ));
        
        LINFO << format("Allocated %.4f Mbytes / %.0f Mbytes (%.4f %) on GPU memory for resuts\n",(float)deviceTotalMem/1048576.0f, (float)deviceProp.totalGlobalMem/1048576.0f, (100.*deviceTotalMem)/deviceProp.totalGlobalMem);
	        
		//cublasInit();
        cublasCreate(&cbhandle);
        deviceFree = false;
        }
    }
}



void Correl3DGPU::freeFullImg()
{
		if(!deviceFullImFree)
		{
		    cudaFree(carrayoldtex);
		    cudaFree(d_FullData);
            deviceTotalMem -= mem_fullData_byte;
		    cudaFree(carraynewtex);
		    cudaFree(d_FullKernel);
            deviceTotalMem -= mem_fullKernel_byte;
		}
		deviceFullImFree = true;
}
	
/*!
*  \brief Free allocated memory on device, on host and free cufft and cublas handle
*/

void Correl3DGPU::free()
{
	if(!paramFree)
    {
        cudaFree(d_Param);
	    deviceTotalMem -= mem_param_byte;
        paramFree = true;
    }
    
    if(FFT)
    {
		if(!deviceFree)
		{
			cufftDestroy(fftVolumeInv);
			cufftDestroy(fftVolumeFwd);
			//cublasShutdown();
            cublasDestroy(cbhandle);
	
			cudaFree(d_DataSpectrum);
            deviceTotalMem -= fftD * fftH * (fftW / 2 + 1) * 2* sizeof(float);
			cudaFree(d_KernelSpectrum);
            deviceTotalMem -= fftD * fftH * (fftW / 2 + 1) * 2* sizeof(float);
			cudaFree(d_PaddedData);
            deviceTotalMem -= mem_padded_byte;
			cudaFree(d_PaddedKernel);
            deviceTotalMem -= mem_padded_byte;
			cudaFree(d_Data);
            deviceTotalMem -= mem_data_byte;
			cudaFree(d_Kernel);
            deviceTotalMem -= mem_kernel_byte;
		    if(!deviceFullImFree)
			{
                cudaFree(d_FullData);
                deviceTotalMem -= mem_fullData_byte;
			    cudaFree(d_FullKernel);
                deviceTotalMem -= mem_fullKernel_byte;
		    }
		    deviceFullImFree = true;
			deviceFree = true;
		}
		
		if(!hostFree)
	    {
			cudaFreeHost(h_FullData);
			h_FullData = NULL;
			cudaFreeHost(h_FullKernel);
			h_FullKernel = NULL;
		    h_max = NULL;
		    hostFree = true;
		}
    }
    else   
    {
		if(!deviceFree)
		{
			//cublasShutdown();
            cublasDestroy(cbhandle);
			
            cudaFree(d_Data);
            int patternX = 2*halfPatternX+1;
            int patternY = 2*halfPatternY+1;
            int patternZ = 2*halfPatternZ+1;
            if(rota)
            {
                int maxpattern = (int)(max(patternX,patternY)*1.5);
                deviceTotalMem -= ((maxpattern+2*displX)*(maxpattern+2*displY)*(patternZ+2*displZ))*sizeof(float);
            }
            else
            {
                deviceTotalMem -= (patternX+2*displX)*(patternY+2*displY)*(patternZ+2*displZ)*sizeof(float);
            }
            cudaFree(d_Kernel);
            deviceTotalMem -= patternX*patternY*patternZ*sizeof(float);
            
            cudaFree(d_res);
            deviceTotalMem -= (2*displX+1)*(2*displY+1)*(2*displZ+1)*sizeof(float);

            cudaFree(d_olddef);
            cudaFree(d_newdef);


		    if(!deviceFullImFree)
			{
                cudaFree(d_FullData);
                deviceTotalMem -= mem_fullData_byte;
			    cudaFree(d_FullKernel);
                deviceTotalMem -= mem_fullKernel_byte;
		    }
		    deviceFullImFree = true;
			deviceFree = true;
		}
		
		if(!hostFree)
	    {
			cudaFreeHost(h_FullData);
			h_FullData = NULL;
			cudaFreeHost(h_FullKernel);
			h_FullKernel = NULL;
		    h_max = NULL;
            h_olddef=NULL;
            h_newdef=NULL;
            h_bestdef=NULL;
            //h_res=NULL;
		    hostFree = true;
		}
	}    
}
	
/*!
*  \brief Write correlation options to string
*
*  \return string containing correlation options
*/

string Correl3DGPU::optionsToString()
{
    ostringstream s;
    if(fromFull)
        s << "fromFull\n";
		
    if(normalize)
	s << "normalize\n";
		
    if(usetex)
        s << "usetex\n";
    
    if(optimize)
        s << "optimizing mode\n";

    if(FFT)
    {		
        s << "FFT\n";
    
        if(intersect)
    	    s << "intersect\n";
	
        if(filters)
	    s << "filters\n";
		
	s << "Window type=" << window << "\n";
		
        if(reducedWindow)
	    s << "reducedWindow domain=" << domainSize << "\n";
	
        if(displThr)
            s << "displThr dX=" << dX << " dY=" << dY << " dZ=" << dZ << "\n" ;

        s << "FFT size=" << fftD << "\n";
     }
     else
     {
    	  s << "direct correlation (no FFT)\n";
          int displX = displacementX;
          int displY = displacementY;
          int displZ = displacementZ;
          if(optimize)
          {
            displX = optimizeDisplacementX;
            displY = optimizeDisplacementY;
            displZ = optimizeDisplacementZ;
          }

          if(aniso){
                s << "X Radius of correlation domain " << halfPatternX << "\n";
                s << "Y Radius of correlation domain " << halfPatternY << "\n";
                s << "Z Radius of correlation domain " << halfPatternZ << "\n";
                s << "Maximal X displacement  " << displX << "\n";
                s << "Maximal Y displacement  " << displY << "\n";
                s << "Maximal Z displacement  " << displZ << "\n";
          }
          else
          {
                s << "Radius of correlation domain " << halfPatternX << "\n";
                s << "Maximal displacement  " << displX << "\n";
          }
     }
     return s.str();
}

/*!
*  \brief Perform correlation
*
*  \return double[4] containing shifts in (x,y,z) directions and correlation value
*/

double* Correl3DGPU::correl(){
	    
    double* depl = new double[4];
    if(FFT)
    {
        //printf("...multiply conjugate and normalize\n");
        modulateConjugateAndNormalize(d_DataSpectrum, d_KernelSpectrum, fftD, fftH, fftW, 1);
		
	//printf("...invert transform\n");
	cufftExecC2R(fftVolumeInv, (cufftComplex *)d_KernelSpectrum, (cufftReal *)d_PaddedData);
	    
	if(intersect)
	  rescaleData(d_PaddedData, d_PaddedData, fftD, fftH, fftW);

        if(displThr)
          displacementThr(d_PaddedData, fftD, fftH, fftW, dZ, dY, dX);
	
        //printf("...looking for max\n");
	int index=0;
        cublasIsamax(cbhandle, fftD * fftH * fftW, d_PaddedData,1,&index);
        index--;
	cudaMemcpy(h_max, &d_PaddedData[index], sizeof(float), cudaMemcpyDeviceToHost);

        int max_z_GPU = (int)(index/(fftW*fftH));
	int max_y_GPU = (int)((index%(fftW*fftH))/fftW);
	int max_x_GPU = index%fftW;

	depl[0]=(max_x_GPU > kernelW/2) ? (max_x_GPU - kernelW) : max_x_GPU;
	depl[1]=(max_y_GPU > kernelH/2) ? (max_y_GPU - kernelH) : max_y_GPU;
	depl[2]=(max_z_GPU > kernelD/2) ? (max_z_GPU - kernelD) : max_z_GPU;
	//	depl[0] = (max_x_GPU - kernelW/2);
	//	depl[1] = (max_y_GPU - kernelH/2);
	//	depl[2] = (max_z_GPU - kernelD/2);
	if(normalize)
	    depl[3]=(double)h_max[0]/(sumImRef*sumImTarget);
	else
	    depl[3]=(double)h_max[0];

    }
    else
    {
       if(aniso)
        {
            
            //pixel_correl_aniso_crop(d_res, d_Data, d_Kernel, displX, displY, displZ, halfPatternX, halfPatternY, halfPatternZ, cropHalfPatternX, cropHalfPatternY, cropHalfPatternZ);
            pixel_correl_aniso(d_res, d_Data, d_Kernel, displX, displY, displZ, halfPatternX, halfPatternY, halfPatternZ);
	    int index=0;
            cublasIsamax(cbhandle, (2*displX+1)*(2*displY+1)*(2*displZ+1), d_res,1,&index);
            index--;
            //int index = cublasIsamax((2*displacementX+1)*(2*displacementY+1)*(2*displacementZ+1), d_res,1)-1;
	    cudaMemcpy(h_max, &d_res[index], sizeof(float), cudaMemcpyDeviceToHost);
    
    	    int max_z_GPU = (int)(index/((2*displX+1)*(2*displY+1)));
	    int max_y_GPU = (int)((index%((2*displX+1)*(2*displY+1)))/(2*displX+1));
	    int max_x_GPU = index%(2*displX+1);

	    depl[0]=max_x_GPU - displX;
	    depl[1]=max_y_GPU - displY;
	    depl[2]=max_z_GPU - displZ;
	    depl[3]=(double)h_max[0]-1;
       }
       else
       {
            pixel_correl(d_res, d_Data, d_Kernel, displX, halfPatternX);
    /*
        float* h_Corr = new float[(2*displacement+1)*(2*displacement+1)*(2*displacement+1)];
        cudaMallocHost(&h_Corr,((2*displacement+1)*(2*displacement+1)*(2*displacement+1))*sizeof(float));
        cudaMemcpy(h_Corr, d_res, (long)(((2*displacement+1)*(2*displacement+1)*(2*displacement+1))*sizeof(float)), cudaMemcpyDeviceToHost);
        //writetoTIFF("/home/plhuissier/Documents/ID15_mars_2013/AZ31_03/treatment/test/correl.tif", h_Corr, 2*displacement+1, 2*displacement+1, 2*displacement+1);
        writetoRAW("/home/plhuissier/Documents/ID15_mars_2013/AZ31_03/treatment/test/correl.raw", h_Corr, 2*displacement+1, 2*displacement+1, 2*displacement+1);
        cudaFreeHost(h_Corr);
        h_Corr = NULL;

	*/

	    int index=0;
            cublasIsamax(cbhandle, (2*displX+1)*(2*displX+1)*(2*displX+1), d_res,1,&index);
            index--;
	    cudaMemcpy(h_max, &d_res[index], sizeof(float), cudaMemcpyDeviceToHost);
    
    	    int max_z_GPU = (int)(index/((2*displX+1)*(2*displX+1)));
	    int max_y_GPU = (int)((index%((2*displX+1)*(2*displX+1)))/(2*displX+1));
	    int max_x_GPU = index%(2*displX+1);

	    depl[0]=max_x_GPU - displX;
	    depl[1]=max_y_GPU - displX;
	    depl[2]=max_z_GPU - displX;
	    depl[3]=(double)h_max[0]-1;
        }
    }
    return depl;
}

double* Correl3DGPU::correltex(){
  double* depl = new double[4];
  //texture_correl(d_res, Xold, d_olddef, d_newdef, halfPatternX, halfPatternY, halfPatternZ);

  if(ROTATION)
  { //Don't works 2019/04/30
    pixel_correl_texture_rotZ(d_res, Xold, Xnew, displX, displY, displZ, nrot, deltatheta, halfPatternX, halfPatternY, halfPatternZ);
    int index=0;
    cublasIsamax(cbhandle, (2*displX+1)*(2*displY+1)*(2*displZ+1)*(2*nrot+1), d_res,1, &index);
    index--;
    //int index = cublasIsamax((2*displacementX+1)*(2*displacementY+1)*(2*displacementZ+1)*(2*nrot+1), d_res,1)-1;
    cudaMemcpy(h_max, &d_res[index], sizeof(float), cudaMemcpyDeviceToHost);
    int max_rot_GPU = (int)(index/((2*displX+1)*(2*displY+1)*(2*displZ+1)));
    int rot = max_rot_GPU - nrot;
    LINFO << "Best rot is index " << rot;

    int max_z_GPU = (int)((index%((2*displX+1)*(2*displY+1)*(2*displZ+1)))/((2*displX+1)*(2*displY+1)));
    int max_y_GPU = (int)((index%((2*displX+1)*(2*displY+1)))/(2*displX+1));
    int max_x_GPU = index%(2*displX+1);
    depl[0]=max_x_GPU - displX;
    depl[1]=max_y_GPU - displY;
    depl[2]=max_z_GPU - displZ;
    depl[3]=(double)h_max[0]-1;
  }
  else
  { //Works 2019/04/30
    pixel_correl_texture_nodef(d_res, Xold, Xnew, displX, displY, displZ, halfPatternX, halfPatternY, halfPatternZ);
    int index=0;
    cublasIsamax(cbhandle, (2*displX+1)*(2*displY+1)*(2*displZ+1), d_res,1,&index);
    index--;
    cudaMemcpy(h_max, &d_res[index], sizeof(float), cudaMemcpyDeviceToHost);

    int max_z_GPU = (int)(index/((2*displX+1)*(2*displY+1)));
    int max_y_GPU = (int)((index%((2*displX+1)*(2*displY+1)))/(2*displX+1));
    int max_x_GPU = index%(2*displX+1);
    depl[0]=max_x_GPU - displX;
    depl[1]=max_y_GPU - displY;
    depl[2]=max_z_GPU - displZ;
    depl[3]=(double)h_max[0]-1;
  }
  return depl;
}

double* Correl3DGPU::correltexscreen(){

  double cor_max=0;
  double* depl = new double[4];

  for(int dz=-displacementZ; dz<=displacementZ; dz++){
    for(int dy=-displacementY; dy<=displacementY; dy++){
      for(int dx=-displacementX; dx<=displacementX; dx++){
        if(ROTATION){ //Works 2019/04/30 
          for(int dr=-nrot; dr<=nrot; dr++){
            float3 dX = make_float3(dx,dy,dz); 
            float3 d0 = make_float3(0,0,0);
            h_olddef[0]=d0;
            h_newdef[0]=dX;
            float3 dR = make_float3(0,dr*deltatheta*CONSTANT_PI_PER_DEG,0); 
            float3 dr0 = make_float3(0,0,0);
            h_olddef[1]=dr0;
            h_newdef[1]=dR;
            cudaMemcpy(d_olddef, h_olddef, DEFORMATIONSIZE*sizeof(deformationType), cudaMemcpyHostToDevice) ;
            cudaMemcpy(d_newdef, h_newdef, DEFORMATIONSIZE*sizeof(deformationType), cudaMemcpyHostToDevice) ;

            singlepoint_texture_correl(d_res, Xold, Xnew, d_olddef, d_newdef, halfPatternX, halfPatternY, halfPatternZ);
            
            cudaMemcpy(h_max, &d_res[0], sizeof(float), cudaMemcpyDeviceToHost);
            if((double)h_max[0]-1>cor_max)
            {    
              cudaMemcpy(h_bestdef, d_newdef, DEFORMATIONSIZE*sizeof(deformationType), cudaMemcpyDeviceToHost) ;
              cor_max = h_max[0]-1;    
            }    
          }
        }
        else
        {
          float3 dX = make_float3(dx,dy,dz); 
          float3 d0 = make_float3(0,0,0);
          h_olddef[0]=d0;
          h_newdef[0]=dX;
          cudaMemcpy(d_olddef, h_olddef, DEFORMATIONSIZE*sizeof(deformationType), cudaMemcpyHostToDevice) ;
          cudaMemcpy(d_newdef, h_newdef, DEFORMATIONSIZE*sizeof(deformationType), cudaMemcpyHostToDevice) ;

          singlepoint_texture_correl(d_res, Xold, Xnew, d_olddef, d_newdef, halfPatternX, halfPatternY, halfPatternZ);
          
          cudaMemcpy(h_max, &d_res[0], sizeof(float), cudaMemcpyDeviceToHost);

          if((double)h_max[0]-1>cor_max)
          {    
            cudaMemcpy(h_bestdef, d_newdef, DEFORMATIONSIZE*sizeof(deformationType), cudaMemcpyDeviceToHost) ;
            cor_max = h_max[0]-1;    
          }    
        }
      }

    }
  }
  depl[0]=h_bestdef[0].x;
  depl[1]=h_bestdef[0].y;
  depl[2]=h_bestdef[0].z;
  depl[3]=(double)cor_max;
  if(ROTATION)
    LINFO << format("New def : U(%f,%f,%f) R(%f,%f,%f) corr = %f", h_bestdef[0].x, h_bestdef[0].y, h_bestdef[0].z,h_bestdef[1].x, h_bestdef[1].y, h_bestdef[1].z, cor_max);

  return depl;
}


double* Correl3DGPU::correlRot(){
	    
    double* depl = new double[5];
    //pixel_correl_aniso_rotZ(d_res, d_Data, d_Kernel, displacementX, displacementY, displacementZ, halfPatternX, halfPatternY, halfPatternZ, nrot, deltatheta);
    pixel_correl_texture_rotZ(d_res, Xold, Xnew, displacementX, displacementY, displacementZ, nrot, deltatheta, halfPatternX, halfPatternY, halfPatternZ);
	int index=0;
    cublasIsamax(cbhandle, (2*displacementX+1)*(2*displacementY+1)*(2*displacementZ+1)*(2*nrot+1), d_res,1,&index);
    index--;
    //int index = cublasIsamax((2*displacementX+1)*(2*displacementY+1)*(2*displacementZ+1)*(2*nrot+1), d_res,1)-1;
    cudaMemcpy(h_max, &d_res[index], sizeof(float), cudaMemcpyDeviceToHost);
    int max_rot_GPU = (int)(index/((2*displacementX+1)*(2*displacementY+1)*(2*displacementZ+1)));
    int max_z_GPU = (int)((index%((2*displacementX+1)*(2*displacementY+1)*(2*displacementZ+1)))/((2*displacementX+1)*2*displacementY+1));
	int max_y_GPU = (int)((index%((2*displacementX+1)*(2*displacementY+1)))/(2*displacementX+1));
	int max_x_GPU = index%(2*displacementX+1);

	depl[0]=max_x_GPU - displacementX;
	depl[1]=max_y_GPU - displacementY;
	depl[2]=max_z_GPU - displacementZ;
	depl[3]=max_rot_GPU - nrot;
	depl[4]=(double)h_max[0]-1;
    return depl;
}

void Correl3DGPU::setRef(float z, float y, float x)
{
    Xold = make_float3(x,y,z);
    //LINFO << format("Old Ref is (%f,%f,%f)", x, y, z);
}	

void Correl3DGPU::setTarget(float z, float y, float x)
{
    Xnew = make_float3(x,y,z);
}/*!
*  \brief Set reference image
*
*  \param z : center of pattern in full image in z direction
*  \param y : center of pattern in full image in y direction
*  \param x : center of pattern in full image in x direction
*/

void Correl3DGPU::setImRef(int z, int y, int x)
{
    if(FFT)
    {
        //TODO in case of not fromFull
		//readfromImagePlus(img1,h_Data);
		readfromFullIm(d_Data, d_FullData, dataD, dataH, dataW, fullDataD, fullDataH, fullDataW, z, y, x);
		
		//cudaMemset(d_PaddedData,   0, mem_padded_byte );
		windowData(d_PaddedData,d_Data,domainSize, fftD, fftH, fftW  );
		
        if(normalize)
        {
            sumImRef=0;
            cublasSnrm2(cbhandle, padded_size, d_PaddedData, 1, &sumImRef);
	    }
		cufftExecR2C(fftVolumeFwd, (cufftReal *)d_PaddedData, (cufftComplex *)d_DataSpectrum);
		
		if(filters)
			filter(d_DataSpectrum, d_DataSpectrum, fftD, fftH, fftW);
    }
    else
    {
     //   cout << "readRef x=" << x << " fullW=" << fullDataW << " disp=" << displacement << " halfp=" << halfPattern << endl;
	if(aniso)
        {
            readfromFullIm_noFFT_aniso(d_Kernel, d_FullData, halfPatternX, halfPatternY, halfPatternZ, 0, 0, 0, fullDataD, fullDataH, fullDataW, z, y, x);
        }
        else
        {
            readfromFullIm_noFFT(d_Kernel, d_FullData, halfPattern, 0, fullDataD, fullDataH, fullDataW, z, y, x);
        }
	}
    cudaDeviceSynchronize();
}
	
	
/*!
*  \brief Set target image
*
*  \param z : center of pattern in full image in z direction
*  \param y : center of pattern in full image in y direction
*  \param x : center of pattern in full image in x direction
*/

void Correl3DGPU::setImTarget(int z, int y, int x)
{
  if(FFT)
  {
    //TODO in case of not fromFull
    readfromFullIm(d_Kernel, d_FullKernel, fullKernelD, fullKernelH, fullKernelW, z, y, x);
    windowKernel(d_PaddedKernel, d_Kernel, domainSize,   fftD, fftH,  fftW  );
    if(normalize)
    {
      sumImTarget=0;
      cublasSnrm2(cbhandle, padded_size, d_PaddedKernel, 1, &sumImTarget);
      //sumImTarget = cublasSnrm2(padded_size, d_PaddedKernel, 1);
    }

    cufftExecR2C(fftVolumeFwd, (cufftReal *)d_PaddedKernel, (cufftComplex *)d_KernelSpectrum);

    if(filters)
      filter(d_KernelSpectrum, d_KernelSpectrum, fftD, fftH, fftW);
  }
  else
  {
    // cout << "readKernel x=" << x << " fullW=" << fullKernelW << " disp=0 halfp=" << halfPattern << endl;
    if(aniso)
    {
      //if(usetex)
      //    readfromFullIm_tex(d_Data, d_FullKernel, halfPatternX, halfPatternY, halfPatternZ, displacementX, displacementY, displacementZ, fullKernelD, fullKernelH, fullKernelW, z, y, x);
      //else
      readfromFullIm_noFFT_aniso(d_Data, d_FullKernel, halfPatternX, halfPatternY, halfPatternZ, displX, displY, displZ, fullKernelD, fullKernelH, fullKernelW, z, y, x);
    }
    else
    {
      readfromFullIm_noFFT(d_Data, d_FullKernel, halfPatternX, displX, fullKernelD, fullKernelH, fullKernelW, z, y, x);
    }
  }
  cudaDeviceSynchronize();
}

/*!
*  \brief Set full reference image from tiff image
*
*  \param img1 : path of reference image
*/

void Correl3DGPU::setFullRef(const char* img1)
{
    setDimensions(img1, fullDataW, fullDataH, fullDataD);
    h_FullData = new unsigned char[fullDataD*fullDataH*fullDataW];
    cudaMallocHost(&h_FullData,fullDataD*fullDataH*fullDataW*sizeof(unsigned char));

    deviceFullImFree = false;
    readfromTIFF(img1,h_FullData);
    LINFO << "Dimensions (" << fullDataW << "," << fullDataH << "," << fullDataD << ")";
    
    mem_fullData_byte = fullDataD*fullDataH*fullDataW*sizeof(unsigned char);
    deviceTotalMem += mem_fullData_byte;

    if(usetex)
    {
        LDEBUG << "Binding texture!!!";
        channeloldtex=cudaCreateChannelDesc<unsigned char>(); 
        volumesizeoldtex = make_cudaExtent(fullDataW, fullDataH, fullDataD);
        cudaMalloc3DArray(&carrayoldtex,&channeloldtex,volumesizeoldtex); 
        cudaMemcpy3DParms copyparms={0};
        copyparms.extent = volumesizeoldtex;
        copyparms.dstArray = carrayoldtex;
        copyparms.kind = cudaMemcpyHostToDevice;  
        copyparms.srcPtr = make_cudaPitchedPtr((void*)h_FullData, sizeof(unsigned char)*fullDataW,fullDataW,fullDataH); 
        cudaMemcpy3D(&copyparms);

        // texture params and bind
        oldtex.filterMode=cudaFilterModeLinear;
        oldtex.normalized = false;
        oldtex.addressMode[0]=cudaAddressModeClamp; 
        oldtex.addressMode[1]=cudaAddressModeClamp; 
        oldtex.addressMode[2]=cudaAddressModeClamp;

        cudaBindTextureToArray(oldtex,carrayoldtex,channeloldtex); 
    }
    else
    {
        cudaMalloc(&d_FullData,fullDataD*fullDataH*fullDataW*sizeof(unsigned char));
        cudaMemcpy(d_FullData, h_FullData, (long)fullDataW*fullDataH*fullDataD*sizeof(unsigned char), cudaMemcpyHostToDevice);
    }
    LINFO << format("Allocated %.0f Mbytes / %.0f Mbytes (%.0f %) on GPU memory for reference image\n",(float)deviceTotalMem/1048576.0f, (float)deviceProp.totalGlobalMem/1048576.0f, (100.*deviceTotalMem)/deviceProp.totalGlobalMem);
    cudaFreeHost(h_FullData);
    h_FullData = NULL;
    cudaDeviceSynchronize();

}

/*!
*  \brief Set full target image from tiff image
*
*  \param img2 : path of target image
*/

void Correl3DGPU::setFullTarget(const char* img2)
{
    setDimensions(img2, fullKernelW, fullKernelH, fullKernelD);
    h_FullKernel= new unsigned char[fullKernelD*fullKernelH*fullKernelW];
    cudaMallocHost(&h_FullKernel,fullKernelD*fullKernelH*fullKernelW*sizeof(char));
        
    deviceFullImFree = false;
    readfromTIFF(img2, h_FullKernel);
    LINFO << "Dimensions (" << fullKernelW << "," << fullKernelH << "," << fullKernelD << ")";
    
    mem_fullKernel_byte = fullKernelD*fullKernelH*fullKernelW*sizeof(char);
    deviceTotalMem += mem_fullKernel_byte;

    if(usetex)
    {
        channelnewtex=cudaCreateChannelDesc<unsigned char>(); 
        volumesizenewtex = make_cudaExtent(fullKernelW, fullKernelH, fullKernelD);
        cudaMalloc3DArray(&carraynewtex,&channelnewtex,volumesizenewtex); 
        cudaMemcpy3DParms copyparms={0};
        copyparms.extent = volumesizenewtex;
        copyparms.dstArray = carraynewtex;
        copyparms.kind = cudaMemcpyHostToDevice;  
        copyparms.srcPtr = make_cudaPitchedPtr((void*)h_FullKernel, sizeof(unsigned char)*fullKernelW,fullKernelW,fullKernelH); 
        cudaMemcpy3D(&copyparms);

        // texture params and bind
        
        //newtex.filterMode=cudaFilterModePoint;
        newtex.addressMode[0]=cudaAddressModeClamp; 
        newtex.addressMode[1]=cudaAddressModeClamp; 
        newtex.addressMode[2]=cudaAddressModeClamp;
        newtex.filterMode=cudaFilterModeLinear;
        newtex.normalized = false;

        cudaBindTextureToArray(newtex,carraynewtex,channelnewtex); 
    }
    else
    {
        cudaMalloc(&d_FullKernel,fullKernelD*fullKernelH*fullKernelW*sizeof(char));
        cudaMemcpy(d_FullKernel, h_FullKernel, (long)fullKernelD*fullKernelH*fullKernelW*sizeof(char), cudaMemcpyHostToDevice);
    }
    LINFO << format("Allocated %.0f Mbytes / %.0f Mbytes (%.0f %) on GPU memory for target image\n",(float)deviceTotalMem/1048576.0f, (float)deviceProp.totalGlobalMem/1048576.0f, (100.*deviceTotalMem)/deviceProp.totalGlobalMem);
    cudaFreeHost(h_FullKernel);
    h_FullKernel = NULL;
    cudaDeviceSynchronize();
}

/*!
*  \brief Set full reference image from raw image
*
*  \param img1 : path of reference image
*  \param dimZ : depth of image
*  \param dimY : height of image
*  \param dimX : width of image
*/

void Correl3DGPU::setFullRef(char* img1, int dimD, int dimH, int dimW)
{
    fullDataW = dimW;
    fullDataH = dimH;
    fullDataD = dimD;
    h_FullData = new unsigned char[fullDataD*fullDataH*fullDataW];
    cudaMallocHost(&h_FullData,dimW*dimH*dimD*sizeof(char));
    cudaMalloc(&d_FullData,dimW*dimH*dimD*sizeof(char));
    deviceFullImFree = false;
    //readfromTIFF(img1,h_FullData);
    readfromImagePlus(img1,h_FullData, dimD, dimH, dimW);
    LINFO << format("Allocated %.0f Mbytes / %.0f Mbytes (%.0f %) on GPU memory\n\n",(float)deviceTotalMem/1048576.0f, (float)deviceProp.totalGlobalMem/1048576.0f, (100.*deviceTotalMem)/deviceProp.totalGlobalMem);
    cudaMemcpy(d_FullData,   h_FullData,   (long)dimW*dimH*dimD*sizeof(char), cudaMemcpyHostToDevice);
    cudaFreeHost(h_FullData);
    h_FullData = NULL;
    cudaDeviceSynchronize();
}
	
/*!
*  \brief Set full target image from raw image
*
*  \param img2 : path of target image
*  \param dimZ : depth of image
*  \param dimY : height of image
*  \param dimX : width of image
*/

void Correl3DGPU::setFullTarget(char* img2, int dimD, int dimH, int dimW)
{
    fullKernelW = dimW;
    fullKernelH = dimH;
    fullKernelD = dimD;
    h_FullKernel= new unsigned char[fullKernelD*fullKernelH*fullKernelW];
    cudaMallocHost(&h_FullKernel,dimW*dimH*dimD*sizeof(char));
    cudaMalloc(&d_FullKernel,dimW*dimH*dimD*sizeof(char));
    deviceFullImFree = false;
    //readfromTIFF(img2, h_FullKernel);
    readfromImagePlus(img2, h_FullKernel, dimD, dimH, dimW);
    LINFO << format("Allocated %.0f Mbytes / %.0f Mbytes (%.0f %) on GPU memory\n\n",(float)deviceTotalMem/1048576.0f, (float)deviceProp.totalGlobalMem/1048576.0f, (100.*deviceTotalMem)/deviceProp.totalGlobalMem);
    cudaMemcpy(d_FullKernel, h_FullKernel, (long)dimW*dimH*dimD*sizeof(char), cudaMemcpyHostToDevice);
    cudaFreeHost(h_FullKernel);
    h_FullKernel = NULL;

    cudaDeviceSynchronize();
}

/*!
*  \brief Window reference pattern if needed
*
*  \param *d_Dst : Reference data windowed (output)
*  \param *d_Src : Reference data original
*  \param a : size of windows
*  \param fftD : depth of FFT kernel
*  \param fftH : height of FFT kernel
*  \param fftW : width of FFT kernel
*/

extern "C" void Correl3DGPU::windowData(float *d_Dst, float *d_Src, float a, int fftD, int fftH, int fftW){
    dim3 threads(fftW, 1, 1);
    dim3 grid(iDivUp(fftH, threads.y), iDivUp(fftD, threads.z),1);

    if(window=="hanning")
    {
        hanning_window_kernel<<<grid,threads>>>(d_Dst, d_Src, a, fftD, fftH, fftW);
    }
    else
    {
	    if (window=="exp")
	    {
            exp_window_kernel<<<grid,threads>>>(d_Dst, d_Src, a, fftD, fftH, fftW);
	    }
	    else
	    {
            copy_kernel<<<grid,threads>>>(d_Dst, d_Src, a, fftD, fftH, fftW);
	    }
    }

    cudaDeviceSynchronize();
}

/*!
*  \brief Window target pattern if needed
*
*  \param *d_Dst : Reference data windowed (output)
*  \param *d_Src : Reference data original
*  \param a : size of windows
*  \param fftD : depth of FFT kernel
*  \param fftH : height of FFT kernel
*  \param fftW : width of FFT kernel
*/

extern "C" void Correl3DGPU::windowKernel(float *d_Dst, float *d_Src, float a, int fftD, int fftH, int fftW){
    dim3 threads(fftW, 1, 1);
    dim3 grid(iDivUp(fftH, threads.y), iDivUp(fftD, threads.z),1);

    if(reducedWindow)
    {
        if(window=="hanning")
            reduced_hanning_window_kernel<<<grid,threads>>>(d_Dst, d_Src, a, fftD, fftH, fftW);
        else
	    {
            if (window=="exp")
                reduced_exp_window_kernel<<<grid,threads>>>(d_Dst, d_Src, a, fftD, fftH, fftW);
            else
                reduced_copy_kernel<<<grid,threads>>>(d_Dst, d_Src, a, fftD, fftH, fftW);
        }
    }
    else
    {
        if(window=="hanning")
            hanning_window_kernel<<<grid,threads>>>(d_Dst, d_Src, a, fftD, fftH, fftW );
        else
        {
            if (window=="exp")
                exp_window_kernel<<<grid,threads>>>(d_Dst, d_Src, a, fftD, fftH, fftW);
            else
                copy_kernel<<<grid,threads>>>(d_Dst, d_Src, a, fftD, fftH, fftW);
        }
    }

    cudaDeviceSynchronize();
}

extern "C" void Correl3DGPU::rescaleData(float *d_Dst, float *d_Src, int fftD, int fftH, int fftW){
    dim3 threads(fftD, 1, 1);
    dim3 grid(iDivUp(fftW, threads.z), iDivUp(fftH, threads.y),1);
    scale_kernel<<<grid,threads>>>(d_Dst, d_Src, fftD, fftH, fftW);
    cudaDeviceSynchronize();
}

extern "C" void Correl3DGPU::displacementThr(float *d_Dst, int fftD, int fftH, int fftW, int dZ, int dY, int dX){
    dim3 threads(fftD, 1, 1);
    dim3 grid(iDivUp(fftW, threads.z), iDivUp(fftH, threads.y),1);
    dispThr_kernel<<<grid,threads>>>(d_Dst, fftD, fftH, fftW, dZ, dY, dX);
    cudaDeviceSynchronize();
}


////////////////////////////////////////////////////////////////////////////////
//Modulate Fourier image of padded data by conjugate Fourier image of padded kernel
//and normalize by FFT size
////////////////////////////////////////////////////////////////////////////////
extern "C" void modulateConjugateAndNormalize(fComplex *d_Src, fComplex *d_Dst,int fftD,int fftH,int fftW,int padding){
    assert( fftW % 2 == 0 );
    int dataSize = fftD * fftH * (fftW / 2 + padding);
    dim3 threads(fftD,1,1);
    dim3 grid(iDivUp(dataSize, fftD), 1 ,1);
    modulateConjugateAndNormalize_kernel<<<grid,threads>>>(d_Src, d_Dst, dataSize, 1.0f/(float)(fftD * fftW * fftH));
    cudaDeviceSynchronize();
}

extern "C" void Correl3DGPU::pixel_correl(float *d_res, float *d_Data, float *d_Kernel, int displacement, int halfPattern){
    dim3 threads(256, 1, 1);
    dim3 grid(iDivUp((2*displacement+1)*(2*displacement+1)*(2*displacement+1), threads.x),1,1);
    pixel_correl_kernel<<<grid,threads>>>(d_res, d_Data, d_Kernel, displacement, halfPattern);
    cudaDeviceSynchronize();
}

extern "C" void Correl3DGPU::pixel_correl_aniso(float *d_res, float *d_Data, float *d_Kernel, int displacementX, int displacementY, int displacementZ, int halfPatternX, int halfpatternY, int halfpatternZ){
    dim3 threads(256, 1, 1);
    dim3 grid(iDivUp((2*displacementX+1)*(2*displacementY+1)*(2*displacementZ+1), threads.x),1,1);
    pixel_correl_aniso_kernel<<<grid,threads>>>(d_res, d_Data, d_Kernel, displacementX, displacementY, displacementZ, halfPatternX, halfpatternY, halfpatternZ);
    cudaDeviceSynchronize();
}

extern "C" void Correl3DGPU::pixel_correl_aniso_crop(float *d_res, float *d_Data, float *d_Kernel, int displacementX, int displacementY, int displacementZ, int halfPatternX, int halfpatternY, int halfpatternZ, int cropHalfPatternX, int cropHalfPatternY, int cropHalfPatternZ){
    dim3 threads(256, 1, 1);
    dim3 grid(iDivUp((2*displacementX+1)*(2*displacementY+1)*(2*displacementZ+1), threads.x),1,1);
    pixel_correl_aniso_crop_kernel<<<grid,threads>>>(d_res, d_Data, d_Kernel, displacementX, displacementY, displacementZ, halfPatternX, halfpatternY, halfpatternZ, cropHalfPatternX, cropHalfPatternY, cropHalfPatternZ);
    cudaDeviceSynchronize();
}

extern "C" void Correl3DGPU::pixel_correl_texture_nodef(float *d_res, float3 Xold, float3 Xnew, int displacementX, int displacementY, int displacementZ, int halfPatternX, int halfpatternY, int halfpatternZ){
    dim3 threads(256, 1, 1);
    dim3 grid(iDivUp((2*displacementX+1)*(2*displacementY+1)*(2*displacementZ+1), threads.x),1,1);
    pixel_correl_texture_nodef_kernel<<<grid,threads>>>(d_res, Xold, Xnew, displacementX, displacementY, displacementZ, halfPatternX, halfpatternY, halfpatternZ);
    cudaDeviceSynchronize();
}

extern "C" void Correl3DGPU::pixel_correl_texture_rotZ(float *d_res, float3 Xold, float3 Xnew, int displacementX, int displacementY, int displacementZ, int nrot, float deltatheta, int halfPatternX, int halfpatternY, int halfpatternZ){
    dim3 threads(256, 1, 1);
    dim3 grid(iDivUp((2*displacementX+1)*(2*displacementY+1)*(2*displacementZ+1)*(2*nrot+1), threads.x),1,1);
    pixel_correl_texture_rotZ_kernel<<<grid,threads>>>(d_res, Xold, Xnew, displacementX, displacementY, displacementZ,nrot, deltatheta, halfPatternX, halfpatternY, halfpatternZ);
    cudaDeviceSynchronize();
}

extern "C" void Correl3DGPU::singlepoint_texture_correl(float *d_res, float3 Xold, float3 Xnew, deformationType *olddef, deformationType *newdef, int halfPatternX, int halfpatternY, int halfpatternZ){
    dim3 threads(1024, 1, 1);
    dim3 grid(1,1,1);
    //pixel_correl_texture_def_kernel<<<grid,threads, mem_shared_byte>>>(d_res, Xold, Xnew, olddef, newdef, halfPatternX, halfpatternY, halfpatternZ);
    singlepoint_correl_texture_def_kernel<<<grid,threads>>>(d_res, Xold, Xnew, olddef, newdef, halfPatternX, halfpatternY, halfpatternZ);
    cudaDeviceSynchronize();
}

extern "C" void Correl3DGPU::texture_check_value(float *d_res, float3 Xold, deformationType *olddef, deformationType *newdef){
    dim3 threads(256, 1, 1);
    dim3 grid(1,1,1);
    pixel_check_value<<<grid,threads>>>(d_res, Xold, olddef, newdef);
    cudaDeviceSynchronize();
}/*
extern "C" void Correl3DGPU::pixel_correl_tex(float *d_res, float *d_Data, float *d_Kernel, int displacementX, int displacementY, int displacementZ, int halfPatternX, int halfpatternY, int halfpatternZ){
    dim3 threads(256, 1, 1);
    dim3 grid(iDivUp((2*displacementX+1)*(2*displacementY+1)*(2*displacementZ+1), threads.x),1,1);
    pixel_correl_tex_kernel<<<grid,threads>>>(d_res, d_Data, d_Kernel, displacementX, displacementY, displacementZ, halfPatternX, halfpatternY, halfpatternZ);
    cudaDeviceSynchronize();
}
*/

extern "C" void Correl3DGPU::pixel_correl_aniso_rotZ(float *d_res, float *d_Data, float *d_Kernel, int displacementX, int displacementY, int displacementZ, int halfPatternX, int halfpatternY, int halfpatternZ, int nrot, float deltatheta){
    dim3 threads(256, 1, 1);
    dim3 grid(iDivUp((2*displacementX+1)*(2*displacementY+1)*(2*displacementZ+1)*(2*nrot+1), threads.x),1,1);
    pixel_correl_aniso_rotZ_kernel<<<grid,threads>>>(d_res, d_Data, d_Kernel, displacementX, displacementY, displacementZ, halfPatternX, halfpatternY, halfpatternZ, nrot, deltatheta);
    cudaDeviceSynchronize();
}



extern "C" void Correl3DGPU::filter(fComplex *d_Src, fComplex *d_Dst, int fftD, int fftH, int fftW){
    dim3 threads(fftD, 1, 1);
    dim3 grid(iDivUp(fftW/2+1, threads.z), iDivUp(fftH, threads.y),1);
    filter_kernel<<<grid,threads>>>(d_Src, d_Dst, fftD, fftH, fftW);
    cudaDeviceSynchronize();
}

/**
int iDivUp(int a, int b){
    return (a % b != 0) ? (a / b + 1) : (a / b);
}

//Align a to nearest higher multiple of b
int iAlignUp(int a, int b){
    return (a % b != 0) ?  (a - a % b + b) : a;
}
*/

/*!
*  \brief Read raw image
*
*  \param image_path : Path of image
*  \param pix : pointer to image array
*  \param dimZ : depth of image
*  \param dimY : height of image
*  \param dimX : width of image
*/

void Correl3DGPU::readfromRAW(const char *name, unsigned char *m, const unsigned int dim_1, const unsigned int dim_2, const unsigned int dim_3)
{
    LINFO << format("Loading from %s...\n", name);
    FILE *fo;
  	fo=fopen(name,"rb");
   	fread(m,sizeof(unsigned char),dim_1*dim_2*dim_3,fo);
   	fclose(fo);
}

/*!
*  \brief Read tiff image
*
*  \param image_path : Path of image
*  \param pix : pointer to image array
*/

void Correl3DGPU::readfromTIFF(const char * image_path, unsigned char* pix)
{
	LINFO << format("Loading from %s...\n",image_path);

	TIFF* tif = TIFFOpen(image_path, "r");
	if (tif) {
		uint32 imageWidth, imageLength;
		uint32 row;
		int j=0;
		//int slice=0;
		TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &imageWidth);
		TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &imageLength);
		//TIFFGetField(tif, TIFFTAG_IMAGEDEPTH, &imageDepth);
		//printf("Volume size checked (%d,%d,%d)\n",imageWidth,imageLength,imageDepth);
		unsigned char* buf = (unsigned char*)malloc(imageWidth *sizeof(unsigned char));
		do
		{
			//slice ++;
			//int slice_count=0;
			//buf = _TIFFmalloc(TIFFScanlineSize(tif));
			for (row = 0; row < imageLength; row++)
			{
				TIFFReadScanline(tif, buf, row);
				for (int k = 0; k < imageWidth; k++)
				{
					pix[j] = buf[k];
					j++;
					//if(buf[k]==(unsigned char)255)
					//	slice_count++;
				}
			}
		//	printf("slice %d found %d white pixels\n",slice,slice_count);
		} while (TIFFReadDirectory(tif));
		_TIFFfree(buf);
		TIFFClose(tif);
	}
}

void Correl3DGPU::writetoRAW(const char * image_path, float* pix, unsigned int imageWidth, unsigned int imageLength, unsigned int imageDepth )
{
	LINFO << format("Writing to %s...\n",image_path);
    FILE *fo;
  	fo=fopen(image_path,"wb");
   	fwrite(pix,sizeof(float),imageWidth*imageLength*imageDepth,fo);
   	fclose(fo);
}


void Correl3DGPU::writetoTIFF(const char * image_path, unsigned char* pix, unsigned int imageWidth, unsigned int imageLength, unsigned int imageDepth )
{
	LINFO << format("Writing to %s...\n",image_path);

	TIFF* tif = TIFFOpen(image_path, "w");
	if (tif) {
		for(int slice=0; slice <imageDepth; slice++)
		{
			TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, imageWidth);
					TIFFSetField(tif, TIFFTAG_IMAGELENGTH, imageLength);
					TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, 1);   // set number of channels per pixel
					TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, 8);    // set the size of the channels
					TIFFSetField(tif, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);    // set the origin of the image.
					TIFFSetField(tif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
					TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
					TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_NONE);
					TIFFSetField(tif, TIFFTAG_SUBFILETYPE, FILETYPE_PAGE);TIFFSetField(tif, TIFFTAG_PAGENUMBER, slice+1, imageDepth);

			//Now writing image to the file one strip at a time
			for (uint32 row = 0; row < imageLength; row++)
			{
				if (TIFFWriteScanline(tif, &pix[row*imageWidth+slice*imageWidth*imageLength], row, 0) != 1)
					break;
			}
			TIFFWriteDirectory(tif);

		}
		(void) TIFFClose(tif);
		//if (buf)
		//	_TIFFfree(buf);
	}
}

void Correl3DGPU::writetoTIFF(const char * image_path, float* pix, unsigned int imageWidth, unsigned int imageLength, unsigned int imageDepth )
{
	LINFO << format("Writing to %s...\n",image_path);

	TIFF* tif = TIFFOpen(image_path, "w");
	if (tif) {
		for(int slice=0; slice <imageDepth; slice++)
		{
			        TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, imageWidth);
					TIFFSetField(tif, TIFFTAG_IMAGELENGTH, imageLength);
					TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, 1);   // set number of channels per pixel
					TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, 32);    // set the size of the channels
					TIFFSetField(tif, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);    // set the origin of the image.
					TIFFSetField(tif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
					TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
					TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_NONE);
					TIFFSetField(tif, TIFFTAG_SUBFILETYPE, FILETYPE_PAGE);
                    TIFFSetField(tif, TIFFTAG_PAGENUMBER, slice+1, imageDepth);

			//Now writing image to the file one strip at a time
			for (uint32 row = 0; row < imageLength; row++)
			{
				if (TIFFWriteScanline(tif, &pix[row*imageWidth+slice*imageWidth*imageLength], row, 0) != 1)
					break;
			}
			TIFFWriteDirectory(tif);

		}
		(void) TIFFClose(tif);
		//if (buf)
		//	_TIFFfree(buf);
	}
}


void Correl3DGPU::setDimensions(const char* img1, int& fullDataW, int& fullDataH, int& fullDataD)
{
	string mode = "r";
	TIFF* tif = TIFFOpen(img1,mode.c_str());
	if (tif) {
		uint32 imageWidth, imageLength;
		TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &imageWidth);
		TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &imageLength);
		int dircount = 0;
        do {
            dircount++;
        } while (TIFFReadDirectory(tif));
        TIFFClose(tif);
		fullDataW = (int)imageWidth;
		fullDataH = (int)imageLength;
		fullDataD = (int)dircount;
	}
	else
	{
		fullDataW = 0;
		fullDataH = 0;
		fullDataD = 0;
	}
}


void Correl3DGPU::setRefDimensions(const char* img1)
{
	string mode = "r";
	TIFF* tif = TIFFOpen(img1,mode.c_str());
	if (tif) {
		uint32 imageWidth, imageLength;
		TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &imageWidth);
		TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &imageLength);
		int dircount = 0;
        do {
            dircount++;
        } while (TIFFReadDirectory(tif));
        TIFFClose(tif);
		fullDataW = (int)imageWidth;
		fullDataH = (int)imageLength;
		fullDataD = (int)dircount;
	}
	else
	{
		fullDataW = 0;
		fullDataH = 0;
		fullDataD = 0;
	}

	   	fullData_size = fullDataD * fullDataH * fullDataW;
		mem_fullData_byte = fullData_size*sizeof(unsigned char);
}


void Correl3DGPU::setTargetDimensions(const char* img1)
{
	string mode = "r";
	TIFF* tif = TIFFOpen(img1,mode.c_str());
	if (tif) {
		uint32 imageWidth, imageLength;
		TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &imageWidth);
		TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &imageLength);
		//TIFFGetField(tif, TIFFTAG_IMAGEDEPTH, &imageDepth);
		int dircount = 0;
        do {
            dircount++;
        } while (TIFFReadDirectory(tif));
        TIFFClose(tif);
		fullKernelW = (int)imageWidth;
		fullKernelH = (int)imageLength;
		fullKernelD = (int)dircount;
	}
	else
	{
		fullKernelW = 0;
		fullKernelH = 0;
		fullKernelD = 0;
	}
	   	
        fullKernel_size = fullKernelD * fullKernelH * fullKernelW;
		mem_fullKernel_byte = fullKernel_size*sizeof(unsigned char);

}

/*!
*  \brief Read raw image
*
*  \param image_path : Path of image
*  \param pix : pointer to image array
*  \param dimZ : depth of image
*  \param dimY : height of image
*  \param dimX : width of image
*/

void Correl3DGPU::readfromImagePlus(char* image_path, unsigned char* pix, int dimZ, int dimY, int dimX)
{
	LINFO <<format("Loading from %s...\n",image_path);
	FILE *fo;
	fo=fopen(image_path,"rb");
	size_t checked = fread(pix,sizeof(unsigned char),dimX*dimY*dimZ,fo);
	fclose(fo);
    LINFO << format("Data loaded.\n");
}

/*!
*  \brief Read raw image
*
*  \param image_path : Path of image
*  \param pix : pointer to image array
*  \param dimZ : depth of image
*  \param dimY : height of image
*  \param dimX : width of image
*/

void Correl3DGPU::readfromImagePlus(const char* image_path, unsigned char* pix, int dimZ, int dimY, int dimX)
{
	LINFO << format("Loading from %s...\n",image_path);
	FILE *fo;
	fo=fopen(image_path,"rb");
	size_t checked = fread(pix,sizeof(unsigned char),dimX*dimY*dimZ,fo);
	fclose(fo);
	LINFO << format("Data loaded.\n");
}

extern "C" void Correl3DGPU::readfromFullIm(float* d_Dst,unsigned char* d_Src,int dimZ,int dimY,int dimX,int z,int y,int x){
	dim3 threads(fftW, 1, 1);
	dim3 grid(iDivUp(fftH, threads.y), iDivUp(fftD, threads.z),1);
    readFromFullIm_kernel<<<grid,threads>>>(d_Dst,d_Src,fftD,fftH,fftW,dimZ,dimY,dimX,z,y,x);
    cudaDeviceSynchronize();
}

extern "C" void Correl3DGPU::readfromFullIm(float* d_Dst,unsigned char* d_Src,int dst_dimZ,int dst_dimY,int dst_dimX,int dimZ,int dimY,int dimX,int z,int y,int x){
    dim3 threads(dst_dimX, 1, 1);
    dim3 grid(iDivUp(dst_dimY, threads.y), iDivUp(dst_dimZ, threads.z),1);
    readFromFullIm_kernel<<<grid,threads>>>(d_Dst,d_Src,dst_dimZ,dst_dimY,dst_dimX,dimZ,dimY,dimX,z,y,x);
    cudaDeviceSynchronize();
}

extern "C" void Correl3DGPU::readfromFullIm_noFFT(float* d_Dst,unsigned char* d_Src,int halfpattern,int displ,int dimZ,int dimY,int dimX,int z,int y,int x){
    dim3 threads(iAlignUp((2*halfpattern + 2*displ +1),16), 1, 1);
    dim3 grid(iDivUp((2*halfpattern + 2*displ +1), threads.y), iDivUp((2*halfpattern + 2*displ +1), threads.z),1);
     
    //cout << "readfromFull x=" << x << " dimX=" << dimX << " disp=" << displ << " halfp=" << halfpattern << endl;
    //cout << "readfromFull y=" << y << " dimY=" << dimY << " disp=" << displ << " halfp=" << halfpattern << endl;
    //cout << "readfromFull z=" << z << " dimZ=" << dimZ << " disp=" << displ << " halfp=" << halfpattern << endl;
    readFromFullIm_noFFT_kernel<<<grid,threads>>>(d_Dst,d_Src,halfpattern,displ,dimZ,dimY,dimX,z,y,x);
    //cout << "readfromFull OK" << endl;
    cudaDeviceSynchronize();
}

extern "C" void Correl3DGPU::readfromFullIm_noFFT_aniso(float* d_Dst,unsigned char* d_Src,int halfpatternX, int halfpatternY, int halfpatternZ, int displX, int displY, int displZ, int dimZ,int dimY,int dimX,int z,int y,int x){
    dim3 threads(iAlignUp((2*halfpatternX + 2*displX +1),16), 1, 1);
    dim3 grid(iDivUp((2*halfpatternY + 2*displY +1), threads.y), iDivUp((2*halfpatternZ + 2*displZ +1), threads.z),1);
     
    
    readFromFullIm_noFFT_aniso_kernel<<<grid,threads>>>(d_Dst,d_Src,halfpatternX, halfpatternY, halfpatternZ, displX, displY, displZ, dimZ,dimY,dimX,z,y,x);

    cudaDeviceSynchronize();
}


/*
extern "C" void Correl3DGPU::readfromFullIm_tex(float* d_Dst,unsigned char* d_Src,int halfpatternX, int halfpatternY, int halfpatternZ, int displX, int displY, int displZ, int dimZ,int dimY,int dimX,int z,int y,int x){
    dim3 threads(iAlignUp((2*halfpatternX + 2*displX +1),16), 1, 1);
    dim3 grid(iDivUp((2*halfpatternY + 2*displY +1), threads.y), iDivUp((2*halfpatternZ + 2*displZ +1), threads.z),1);
   
    if(bindedtex)
    {
        cudaUnbindTexture(tex); 
        bindedtex=false;
    }
    readFromFullIm_noFFT_aniso_kernel<<<grid,threads>>>(d_Dst,d_Src,halfpatternX, halfpatternY, halfpatternZ, displX, displY, displZ, dimZ,dimY,dimX,z,y,x);
    if(usetex)
    {
        tex.normalized = true;                      // access with normalized texture coordinates
        tex.filterMode = cudaFilterModeLinear;      // linear interpolation
        tex.addressMode[0] = cudaAddressModeWrap;   // wrap texture coordinates
        tex.addressMode[1] = cudaAddressModeWrap;
        tex.addressMode[2] = cudaAddressModeWrap;

        cudaBindTextureToArray(tex, d_Dst, channelDesc);
        bindedtex=true;
    }

    cudaDeviceSynchronize();
}

*/


float Correl3DGPU::getValue(float* d_Src, int dimZ, int dimY, int dimX, int z, int y, int x)
{
	float* h_value = (float*)malloc(sizeof(float));
	int index = z*dimX*dimY + y*dimX + x;
	cudaMemcpy(h_value, &d_Src[index], sizeof(float), cudaMemcpyDeviceToHost);
	return h_value[0];
}

unsigned char Correl3DGPU::getValue(unsigned char* d_Src, int dimZ, int dimY, int dimX, int z, int y, int x)
{
	unsigned char* h_value = (unsigned char*)malloc(sizeof(unsigned char));
	int index = z*dimX*dimY + y*dimX + x;
	cudaMemcpy(h_value, &d_Src[index], sizeof(unsigned char), cudaMemcpyDeviceToHost);
	return h_value[0];
}


    //REQUIRED
 	/*
	Correl3DGPU::Correl3DGPU() {
 	    kernelD = FIXED;
 	    kernelH = FIXED;
 	    kernelW = FIXED;
 	    dataD = FIXED;
 	    dataH = FIXED;
 	    dataW = FIXED;
 	    fftD = FIXED;
 	    fftH = FIXED;
 	    fftW = FIXED;
	    data_size = dataD * dataH * dataW;
	    kernel_size = kernelD * kernelH * kernelW;
	    padded_size = fftD * fftH * fftW;

	    normalize = true;
	    fromFull = false;
	    window = "hanning";
	    reducedWindow = false;
	    filters = true;
	    intersect = true;

	    deviceFree = true;
	    deviceFullImFree = true;
	    hostFree = true;

		h_Data = (float*)malloc(data_size*sizeof(float));
		h_Kernel = (float*)malloc(kernel_size*sizeof(float));
		h_max = new float[0];//(float*)malloc(sizeof(float));
		hostFree = false;
	}*/


////////////////////////////////////////////////////////////////////////////////
/// Position convolution kernel center at (0, 0 , 0) in the image
////////////////////////////////////////////////////////////////////////////////
extern "C" void Correl3DGPU::padKernel(
		float *d_Dst,
		float *d_Src,
int fftD,
int fftH,
int fftW,
int kernelD,
int kernelH,
int kernelW,
int kernelZ,
int kernelY,
int kernelX
){
dim3 threads(kernelD, 1, 1);
dim3 grid(iDivUp(kernelW, threads.z), iDivUp(kernelH, threads.y),1);
padKernel_kernel<<<grid,threads>>>(
d_Dst,
d_Src,
fftD,
fftH,
fftW,
kernelD,
kernelH,
kernelW,
kernelZ,
kernelY,
kernelX
);
cudaDeviceSynchronize();

}



////////////////////////////////////////////////////////////////////////////////
//Prepare data for "pad to border" addressing mode
////////////////////////////////////////////////////////////////////////////////
extern "C" void Correl3DGPU::padDataClampToBorder(
		float *d_Dst,
		float *d_Src,
		int fftD,
		int fftH,
		int fftW,
		int dataD,
		int dataH,
		int dataW,
		int kernelD,
		int kernelW,
		int kernelH,
		int kernelZ,
		int kernelY,
		int kernelX
){
	dim3 threads(fftD, 1, 1);
	dim3 grid(iDivUp(fftW, threads.z), iDivUp(fftH, threads.y),1);

	padDataClampToBorder_kernel<<<grid,threads>>>(
d_Dst,
d_Src,
fftD,
fftH,
fftW,
dataD,
dataH,
dataW,
kernelD,
kernelH,
kernelW,
kernelZ,
kernelY,
kernelX
);

cudaDeviceSynchronize();
}

bool Correl3DGPU::validTarget(Coord target)
{
    return ((target.getX()>0)&&(target.getY()>0)&&(target.getZ()>0)&&(target.getX()<fullKernelW)&&(target.getY()<fullKernelH)&&(target.getZ()<fullKernelD));    
}


