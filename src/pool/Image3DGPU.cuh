#include <math.h>
#include <cuda.h>
#include "cuda_helper.h"
#include "cuda_runtime.h"
typedef unsigned int uint;
//typedef int imtype;

//int iDivUp(int a, int b);
//int iAlignUp(int a, int b);	



extern "C" __global__ void label_dist_kernel(
    float *d_res,
    imtype *d_Src,
    int deplX,
    int deplY,
    int deplZ,
    int fullW,
    int fullH,
    int fullD,
    float X,
    float Y,
    float Z
){
	const int x = threadIdx.x;
    const int y = blockDim.y * blockIdx.x + threadIdx.y;
    const int z = blockDim.z * blockIdx.y + threadIdx.z;
    
    float depl = (deplZ+1)*(deplZ+1)+(deplY+1)*(deplY+1)+(deplX+1)*(deplX+1);

    if(z < (2*deplZ+1) && y < (2*deplY+1) && x < (2*deplX+1)){
        int kz = __float2int_rd(Z - deplZ + z);
        int ky = __float2int_rd(Y - deplY + y);
        int kx = __float2int_rd(X - deplX + x);
        float dz = Z - floor(Z) - deplZ + z;
        float dy = Y - floor(Y) - deplY + y;
        float dx = X - floor(X) - deplX + x;
        if( (kx > 0) && (ky > 0) && (kz > 0) && (kx < fullW) &&  (ky < fullH) &&  (kz < fullD))
        {
            if(d_Src[kz * fullW * fullH + ky * fullW + kx] >= 1)
                depl = dx*dx+dy*dy+dz*dz;
        }
        else
        {
            depl += 1.;
        }
        d_res[(z * (2*deplY+1) + y)*(2*deplX+1) + x] = depl;
    }
}



