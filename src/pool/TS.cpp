#include "TS.h"

using namespace std;


template <typename Ptr, typename Pg>
TS<Ptr,Pg>::TS():TimeSerie<Ptr,Pg>()
{

}

template <typename Ptr, typename Pg> TS<Ptr,Pg>::TS(Parameters* param):TimeSerie<Ptr,Pg>(param)
{

}

template <typename Pg> TS<Particle,Pg>::TS():TimeSerie<Particle,Pg>()
{

}

template <typename Pg> TS<Particle,Pg>::TS(Parameters* param):TimeSerie<Particle,Pg>(param)
{

}



template <typename Pg>
int TS<Particle,Pg>::trackParticles()
{
    vector<vector<PSet<Particle> > > global;

    for(int i=this->scanNumber.size()-1; i>0; --i)
    {
    	vector<PSet<Particle> > checkedCorrel;
    	if(this->param->postprocess)
    	{
    		readVectorPSet(string(this->param->Directory)+"TrackingFiles/",this->param->prefix+this->scanNumberStr[i]+"_checked", this->ptrs, this->ptrsData);
    		checkedCorrel = *(&(this->ptrs));
    	}
    	else
    	{
    		LINFO << "Matching scans " << this->scanNumber[i] << " and " << this->scanNumber[i-1] << " ...";

			//Reference Particles pool (from reference scan)
			PSet<Particle>  poolref = (this->ptrs[i]).subSetVol(4);
			LINFO << "Scan " << this->scanNumber[i] << " contains " << poolref.Size() << " particles";

			//Second Particles pool (from next scan)
			PSet<Particle>  poolfinal = (this->ptrs[i-1]).subSetVol(4);
			LINFO << "Scan " << this->scanNumber[i-1] << " contains " << poolfinal.Size() << " particles";

            vector<PSet<Particle> > potentialCorrel;
            vector<PSet<Particle> > potentialCorrelInv;
            if(this->param->DIC){
            	//For each particle of reference pool look for the best correlated particles of second pool (forward correlation)
				//potentialCorrel = poolref.findBestCorrel_DICGPU(poolfinal, refPoints[i], refDeplm[i-1], thrXY, thrZ);
				potentialCorrel = poolref.findBestCorrel_DICGPU(poolfinal, (this->pgs)[i], (this->pgs)[i-1], (double)(this->param->thrXYZ), (double)(this->param->thrZ));
				//TODO no more refdepl!


				//vector<ParticlesSet> potentialCorrel = poolref.findBestCorrel_noDIC(poolfinal, deplm[i-1], thrXY, thrZ);

				//For each particle of second pool look for the best correlated particles of reference pool (backward correlation)
				//potentialCorrelInv = poolfinal.findBestCorrel_DICGPU(poolref, refPoints[i-1],refDeplp[i-1], thrXY, thrZ);
				potentialCorrelInv = poolfinal.findBestCorrel_DICGPU(poolref, this->pgs[i-1],this->pgs[i], this->param->thrXYZ, this->param->thrZ);
				//TODO no more refdepl!


				//vector<ParticlesSet> potentialCorrelInv = poolfinal.findBestCorrel_noDIC(poolref, deplp[i-1], thrXY, thrZ);
            }
            else
            {
            	//For each particle of reference pool look for the best correlated particles of second pool (forward correlation)
            	potentialCorrel = poolref.findBestCorrel_noDIC(poolfinal, this->pgs[i], this->pgs[i-1], this->param->thrXYZ, this->param->thrZ);
            	//TODO no more refdepl!

            	//vector<ParticlesSet> potentialCorrel = poolref.findBestCorrel_noDIC(poolfinal, deplm[i-1], thrXY, thrZ);

            	//For each particle of second pool look for the best correlated particles of reference pool (backward correlation)
            	potentialCorrelInv = poolfinal.findBestCorrel_noDIC(poolref, this->pgs[i-1],this->pgs[i], this->param->thrXYZ, this->param->thrZ);
            	//TODO no more refdepl!

            }

			LINFO << "Potential pairs found.";


			//debug :
			//poolref.debugCorrel(potentialCorrel,poolfinal,potentialCorrelInv);


			//Match forward and backward correlation
			checkedCorrel = poolref.matchCorrel(potentialCorrel,poolfinal,potentialCorrelInv);

			//Write the cross correlation result
			writePSetGlobal(string(this->param->Directory)+"TrackingFiles/"+this->param->prefiximage+this->scanNumberStr[i]+"_checked.dat",checkedCorrel);
			writeVectorPSet(string(this->param->Directory)+"TrackingFiles/",this->param->prefiximage+this->scanNumberStr[i]+"_checked",checkedCorrel);
    	}
	    //Keep memory of the correlation for final renumbering
	    global.push_back(checkedCorrel);
    }

    //Concatenate pairs of correlation in a global tracking
    cout << "Concatenating pairs..." << endl;
    int current=0;
    for(int i=this->numberExpe-1; i>0; --i)
    {
    	current = (this->ptrs)[i].globalNumberFromCheckedRef(global[this->numberExpe-1-i], current, this->scanNumber[i]);
    	(this->ptrs)[i-1].globalNumberFromCheckedFinal(global[this->numberExpe-1-i],(this->ptrs)[i], current, this->scanNumber[i]);
    	 //(ptrs)[i-1].globalNumberFromCheckedFinal(global[numberExpe-1-i],(*parset)[i], current, atoi(Num[i].c_str()));
    }
}

template <typename Pg>
int TS<Particle,Pg>::trackNucleateAndMergeParticles()
{
    LINFO << "Using nucleate and merge strategy for Matching";
    //vector<vector<PSet<Particle> > > global;
    int istart = 0;
    int iend = this->scanNumber.size()-1;
    if(this->param->reverse)
    {
        istart = iend;
        iend = 0;
    }
    int ind = istart;
    int indp = istart+1;
    int totalNBPart = (this->ptrs[istart]).maxGlobalColor();
    for(int i=0; i<this->scanNumber.size()-1; i++)
    {
        if(this->param->reverse)
        {
            ind = iend-i;
            indp = ind - 1;
        }
        else
        {
            ind = i;
            indp = ind + 1;
        }     
    	vector<PSet<Particle> > checkedCorrel;
    	if(this->param->postprocess)
    	{
    		readVectorPSet(string(this->param->Directory)+"PT4D_Files/",this->param->prefix+this->scanNumberStr[ind]+"_checked", this->ptrs, this->ptrsData);
    		checkedCorrel = *(&(this->ptrs));
    	}
    	else
    	{
           	LINFO << "Matching scans " << this->scanNumber[ind] << " and " << this->scanNumber[indp] << " ...";

    		//Reference Particles pool (from reference scan)
    		PSet<Particle>  poolref = (this->ptrs[ind]);
    		LINFO << "Scan " << this->scanNumber[ind] << " contains " << poolref.Size() << " particles";

	    	//Second Particles pool (from next scan)
	    	PSet<Particle>  poolfinal = (this->ptrs[indp]);
		    LINFO << "Scan " << this->scanNumber[indp] << " contains " << poolfinal.Size() << " particles";

            if(this->param->DIC){
				LINFO << "Distance to label option";
                totalNBPart = poolref.findParticle_fromLabelled(poolfinal, (this->pgs)[ind], (this->pgs)[indp], this->param->regulThrDist,  this->param->thrX, this->param->correlThr, totalNBPart);
            }
            else
            {
        		LINFO << "No DIC option";
                LINFO << "Not yet implemented";
            	//poolref.findIterativeSingleBestCorrel_noDIC(poolfinal, (this->pgs)[ind], (this->pgs)[indp], this->param->thrX, this->param->thrY, this->param->thrZ, this->param->correlThr);
            }

			LINFO << "Potential pairs found.";

       	}
        LINFO << "Current total number of objects " << totalNBPart;
    }
    return totalNBPart;

}

template <typename Pg>
int TS<Particle,Pg>::trackIncompleteParticles()
{
    LINFO << "Using missing particles strategy for Matching";
    //vector<vector<PSet<Particle> > > global;
    int istart = 0;
    int iend = this->scanNumber.size()-1;
    if(this->param->reverse)
    {
        istart = iend;
        iend = 0;
    }
    int ind = istart;
    int indp = istart+1;
    for(int i=0; i<this->scanNumber.size()-1; i++)
    {
        if(this->param->reverse)
        {
            ind = iend-i;
            indp = ind - 1;
        }
        else
        {
            ind = i;
            indp = ind + 1;
        }     
    	vector<PSet<Particle> > checkedCorrel;
    	if(this->param->postprocess)
    	{
    		readVectorPSet(string(this->param->Directory)+"PT4D_Files/",this->param->prefix+this->scanNumberStr[ind]+"_checked", this->ptrs, this->ptrsData);
    		checkedCorrel = *(&(this->ptrs));
    	}
    	else
    	{
           if(this->param->DIC){
				LINFO << "Distance to label option";
                LINFO << "Not yet implemented";
            	/*
                 LINFO << "Matching scans " << this->scanNumber[ind] << " and " << this->scanNumber[indp] << " ...";

	    		//Reference Particles pool (from reference scan)
	    		PSet<Particle>  poolref = (this->ptrs[ind]);
	    		LINFO << "Scan " << this->scanNumber[ind] << " contains " << poolref.Size() << " particles";

		    	//Second Particles pool (from next scan)
		    	PSet<Particle>  poolfinal = (this->ptrs[indp]);
			    LINFO << "Scan " << this->scanNumber[indp] << " contains " << poolfinal.Size() << " particles";

//For each particle of reference pool look for the best correlated particles of second pool (forward correlation)
                poolref.findParticle_fromLabelled(poolfinal, (this->pgs)[ind], (this->pgs)[indp], this->param->regulThrDist,  this->param->thrX, this->param->correlThr);
                //checkedCorrel = poolref.findBestCorrel_DICGPU(poolfinal, (this->pgs)[i], (this->pgs)[i+1], (double)(this->param->thrXY), (double)(this->param->thrZ));

                */
            }
            else
            {
        		LINFO << "Matching scans " << this->scanNumber[istart] << " and " << this->scanNumber[indp] << " ...";

	    		//Reference Particles pool (from reference scan)
	    		PSet<Particle>  poolref = (this->ptrs[istart]);
	    		LINFO << "Scan " << this->scanNumber[istart] << " contains " << poolref.Size() << " particles";

		    	//Second Particles pool (from next scan)
		    	PSet<Particle>  poolfinal = (this->ptrs[indp]);
			    LINFO << "Scan " << this->scanNumber[indp] << " contains " << poolfinal.Size() << " particles";

                 LINFO << "No DIC option";
            	//For each particle of reference pool look for the best correlated particles of second pool (forward correlation)
            	poolref.findIterativeSingleBestCorrel_noDIC(poolfinal, (this->pgs)[istart], (this->pgs)[indp], this->param->thrX, this->param->thrY, this->param->thrZ, this->param->correlThr);
            }

			LINFO << "Potential pairs found.";

       	}
    }
    return (this->ptrs[istart]).Size();

}

template <typename Pg>
int TS<Particle,Pg>::trackOnlyBackwardPositionParticles()
{
    LINFO << "Only estimating backward position of the particles";
    //vector<vector<PSet<Particle> > > global;
    int istart = 0;
    int iend = this->scanNumber.size()-1;
    int ind = istart;
    for(int i=istart+1; i<this->scanNumber.size(); i++)
    {
        (this->pgs)[istart].estimateLocalQuadraticCoherency((this->pgs)[i], 1.*this->param->regulThrDist);
        (this->pgs)[i].scaleCorrelFactorwithSpatialCoherency();
    }

    for(int i=0; i<this->scanNumber.size(); i++)
    {
        ind = i;
             
    	vector<PSet<Particle> > checkedCorrel;
    	if(this->param->postprocess)
    	{
    		readVectorPSet(string(this->param->Directory)+"PT4D_Files/",this->param->prefix+this->scanNumberStr[ind]+"_checked", this->ptrs, this->ptrsData);
    		checkedCorrel = *(&(this->ptrs));
    	}
    	else
    	{
			//Reference Particles pool (from reference scan)
			PSet<Particle>  poolref = (this->ptrs[ind]);
			LINFO << "Scan " << this->scanNumber[ind] << " contains " << poolref.Size() << " particles";

            if(this->param->DIC){
            	//For each particle of reference pool look for the best correlated particles of second pool (forward correlation)
		LINFO << "DIC option not yet implemented for backward position particle tracking";
                //checkedCorrel = poolref.findBestCorrel_DICGPU(poolfinal, (this->pgs)[i], (this->pgs)[i+1], (double)(this->param->thrXY), (double)(this->param->thrZ));

            }
            else
            {
                LINFO << "No DIC option";
            	//For each particle of reference pool look for the best correlated particles of second pool (forward correlation)
            	poolref.findBackwardPosition_noDIC((this->pgs)[istart], (this->pgs)[ind], this->param->correlThr, this->param->regulThrDist, this->param->regulDist);
            }

            poolref.writeParamWithBackwardPosition(this->resptrsFilesNames[ind]);
			LINFO << "Backward position written.";

       	}
    }
    return (this->ptrs[istart]).Size();

}


template <typename Pg>
int TS<Particle,Pg>::addMeshInfo()
{
    writePSetForMeshDef(this->ptrsDefFilesNames, this->ptrs);
    string FileName = (string)(this->param->Directory) + this->outputDir + "export_def_field_to_particles.dgibi";	
    ofstream fichier(FileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier
    if(fichier)  // si l'ouverture a réussi
    fichier << "OPTI ECHO 1 DIME 3 ELEM CUB8 MODE TRID;" << endl;
    fichier << "REPE BOU1 (" << this->ptrs.size() << ") ;" << endl;
    fichier << "i0 = &BOU1 ;" << endl;
    fichier << "* Lecture des donnees " << endl;
    fichier << "BOOL = i0 > 9;" << endl;
    fichier << "SI BOOL;" << endl;
    fichier << "    i1 = CHAI '0' i0;" << endl;
    fichier << "SINO;" << endl;
    fichier << "    i1 = CHAI '00' i0;" << endl;
    fichier << "FINS;" << endl;

    fichier << "OPTI ELEM CUB8;" << endl;
    fichier << "ficlec = CHAI './" << this->param->prefixmesh << "' i1 '.inp' ;" << endl;
    fichier << "OPTI LECT ficlec ;" << endl;
    fichier << "MESSAGE ficlec;" << endl;
    fichier << "TBEXT = LIRE 'AVS'; " << endl;
    fichier << "MAILLAGE = TBEXT.LEMAILLA; " << endl;
    fichier << "Deplcmt = TBEXT.LECHPOIN; " << endl;

    fichier << "* Post-traitement " << endl;
    fichier << "Mail_EF = MODELE Maillage MECANIQUE ELASTIQUE ISOTROPE;" << endl;
    fichier << "FonctUn = MANU CHML Maillage MASS 1;" << endl;
    fichier << "Defo = EPSILON Mail_EF Deplcmt;" << endl;
    fichier << "DEPL Maillage 'PLUS' Deplcmt;" << endl;
    fichier << "defN = CHANGE 'NOEUD' defo Mail_EF;" << endl;

    fichier << "ficlecP = CHAI './" << this->param->prefixptrsdef << "' i1 '.csv' ;" << endl;
    fichier << "MESSAGE ficlecP;" << endl;
    fichier << "OPTI ACQUE ficlecP;" << endl;
    fichier << "ACQUERIR NP*ENTIER;" << endl;
    fichier << "REPE B1 NP;" << endl;
    fichier << "ACQUERIR I*ENTIER J*ENTIER X*FLOTTANT Y*FLOTTANT Z*FLOTTANT;" << endl;
    fichier << "P = X Y Z;" << endl;
    fichier << "SI (&B1 EGA 1);" << endl;
    fichier << "    L = P;" << endl;
    fichier << "SINON;" << endl;
    fichier << "    L = L ET P;" << endl;
    fichier << "FINSI;" << endl;
    fichier << "FIN B1;" << endl;

    fichier << "def_P = PROI L defN;" << endl;

    fichier << "OPTI 'ECHO' 0;" << endl;
    fichier << "ficsortP = CHAI './" << this->param->prefixptrs << "strain_' i1 '.dat' ;" << endl;
    fichier << "OPTI 'IMPR' 42 'IMPR' ficsortP;" << endl;
    fichier << "REPE B3 NP;" << endl;
    fichier << "P = L POIN &B3;" << endl;
    fichier << "XX = EXTR def_P 'EPXX' P;" << endl;
    fichier << "YY = EXTR def_P 'EPYY' P;" << endl;
    fichier << "ZZ = EXTR def_P 'EPZZ' P;" << endl;
    fichier << "XY = EXTR def_P 'GAXY' P;" << endl;
    fichier << "XZ = EXTR def_P 'GAXZ' P;" << endl;
    fichier << "YZ = EXTR def_P 'GAYZ' P;" << endl;
    fichier << "MESS &B3 ' ' XX ' ' YY ' ' ZZ ' ' XY ' ' XZ ' ' YZ;" << endl;
    fichier << "FIN B3;" << endl;
    
    fichier << "OPTI 'IMPR' 6;" << endl;
    fichier << "OPTI 'ECHO' 1;" << endl;
    fichier << "FIN BOU1;" << endl;

    fichier << "fin;" << endl;

}

template <typename Pg>
vector<PSet<Particle> > TS<Particle,Pg>::rankParticles(int &current)
{
	vector<PSet<Particle> > track = vector<PSet<Particle> >();
    for(int i=1; i<=current;i++)
    {
    	PSet<Particle> * partS = new PSet<Particle> ();
    	LINFO << "Looking for particle " << i;
    	for(int j=0;j<this->scanNumber.size();j++)
    	{
    		LINFO << "Scan " << j << " ";
    		(*partS).addP((this->ptrs)[j].findGlobalColorP(i));
    	}
    	track.push_back(*partS);
    	//this->resptrs.push_back(*partS);
    }
    return track;
}

template <typename Pg>
void TS<Particle,Pg>::rankParticles()
{
    for(int i=0; i<=this->ptrs.size();i++)
    {
        this->ptrs[i].rankByGlobalNumber();
    }
}

template <typename Pg>
void TS<Particle,Pg>::recolor(vector<PSet<Particle> > track)
{
   	LINFO << "Recoloring  particles ...";
    for(int i=0; i<this->scanNumber.size(); ++i)
    {
    	int* new_color = new int[65256];
    	for(int j = 0; j<65256; j++)
    	{
    		new_color[j] = 65255;
    	}
    	for(int j=0; j<track.size(); ++j)
    	{
    		int intensFinal = j;
    		if((track[j].getP(i)) && (track[j].getP(i)->getColor()>=0))
    			new_color[track[j].getP(i)->getColor()] = intensFinal;
    	}
    	new_color[0]=0;
    	ostringstream a;
    	a << this->param->Directory << this->param->prefiximage << this->scanNumberStr[i] << "_label_color_corresp.dat";

    	writeFile(a.str(),new_color,1, 65256);
    }
}



template class TS<Particle,P>;
template class TS<P,P>;
