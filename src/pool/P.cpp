#include "P.h"
#include "PSet.h"

using namespace std;

P::P(){

  this->scan = -1;
  this->color = -1;
  this->coord = Coord();
  this->correl = 0;
  this->totalcorrel = 0;
  this->spatialCoherency = 0;
  this->roisize = 64;
  this->roiX = 0;
  this->roiY = 0;
  this->roiZ = 0;
}


P::~P(){

  this->color = -1;

}


P::P(int scan, int color, Coord coord, double correl) {

  this->scan = scan;
  this->color = color;
  this->coord = coord;
  this->correl = correl;
  this->totalcorrel = correl;
  this->spatialCoherency = 0;
  this->roisize = 64;
  this->roiX = 0;
  this->roiY = 0;
  this->roiZ = 0;
}


P::P(int scan, int color, Coord coord, int roiX, int roiY, int roiZ) {
  this->scan = scan;
  this->color = color;
  this->coord = coord;
  this->correl = 0;
  this->totalcorrel = 0;
  this->spatialCoherency = 0;
  this->roisize = 64;
  this->roiX = roiX;
  this->roiY = roiY;
  this->roiZ = roiZ;
}


P::P(int scan, int color, Coord coord) {

  this->scan = scan;
  this->color = color;
  this->coord = coord;
  this->correl = 0;
  this->totalcorrel = 0;
  this->spatialCoherency = 0;
  this->roisize = 64;
  this->roiX = 0;
  this->roiY = 0;
  this->roiZ = 0;
}


P::P(string linestr) {

  double a,b,c;
  string paramline;
  stringstream sstr;
  sstr.str(linestr);
  if(sstr.str().size()>1)
  {
     sstr >> a;
     if(!isnan(1.*a))
     {
        this->scan = (int)(1.*a);
        sstr >> a;
        this->color = (int)(1.*a);
        sstr >> a;
        sstr >> b;
        sstr >> c;
        this->coord = Coord((double)(1.*a),(double)(1.*b),(double)(1.*c));
        sstr >> a;
        this->correl = 0;
        this->totalcorrel = 0;
        this->spatialCoherency = 0;
        this->roisize = 64;
        this->roiX = 0;
        this->roiY = 0;
        this->roiZ = 0;
     }
  }
}


P::P(const string fileDirectory, int scan, int color) {
	
  this->scan = scan;
  this->color = color;
  this->coord = Coord();
  this->correl = 0;
  this->totalcorrel = 0;
  this->spatialCoherency = 0;
  this->roisize = 64;
  this->roiX = 0;
  this->roiY = 0;
  this->roiZ = 0;
  ostringstream fileName;
  char buff [200];
  sprintf(buff,"/P_%04d_%05d",scan,color);
  fileName << fileDirectory << buff;

  ifstream File (fileName.str().c_str(),ios::in);
  if (!File)
  {
    LERROR << "Error : impossible to open file " << fileName.str() << " in read only mode...";
    LERROR << "P color is " << color << " from scan " << scan;
  }
  else
  {
    LINFO << "File of P opened : color is " << color << " from scan " << scan;
  }
  double a,b,c;
  string dataline="";
  stringstream sstr;
  char schar[10000];
  File.getline(schar,10000);
  dataline+=schar;
  sstr.str(dataline);
  if(sstr.str().size()>1)
  {
    sstr >> a;
    if(!isnan(1.*a))
    {
      this->scan = (int)(1.*a);
      sstr >> a;
      this->color = (int)(1.*a);
      sstr >> a;
      sstr >> b;
      sstr >> c;
      this->coord = Coord((double)(1.*a),(double)(1.*b),(double)(1.*c));
      sstr >> a;
      this->correl = (double)(1.*a);
      this->totalcorrel = (double)(1.*a);
      sstr >> a;
      this->spatialCoherency = 0;
      this->roisize = 64;
      this->roiX = 0;
      this->roiY = 0;
      this->roiZ = 0;
    }
  }

}


P::P(const string Directory, const string pName) {
  this->scan = 0;
  this->color = 0;
  this->coord = Coord();
  this->correl = 0;
  this->totalcorrel = 0;
  this->spatialCoherency = 0;
  this->roisize = 64;
  this->roiX = 0;
  this->roiY = 0;
  this->roiZ = 0;
  ostringstream fileName;
  fileName << Directory << "/" << pName;

  ifstream File (fileName.str().c_str(),ios::in);
  if (!File)
  {
    LERROR << "Error : impossible to open file " << fileName.str() << " in read only mode...";
    //LERROR << "P color is " << color << " from scan " << scan;
  }

  double a,b,c;
  string dataline="";
  stringstream sstr;
  char schar[10000];
  File.getline(schar,10000);
  dataline+=schar;
  sstr.str(dataline);
  if(sstr.str().size()>1)
  {
    sstr >> a;
    if(!isnan(1.*a))
    {
      this->scan = (int)(1.*a);
      sstr >> a;
      this->color = (int)(1.*a);
      sstr >> a;
      sstr >> b;
      sstr >> c;
      this->coord = Coord((double)(1.*a),(double)(1.*b),(double)(1.*c));
      sstr >> a;
      this->correl = (double)(1.*a);
      this->totalcorrel = (double)(1.*a);
      sstr >> a;
      this->spatialCoherency = 0;
      this->roisize = 64;
      this->roiX = 0;
      this->roiY = 0;
      this->roiZ = 0;
    }
  }
}


void P::PUpdate(P P) {

  this->scan = P.getScan();
  this->color = P.getColor();
  this->coord = P.getCoord();
  this->correl = P.getCorrel();
  this->totalcorrel = P.getTotalCorrel();
  this->spatialCoherency = P.getSpatialCoherency();
  this->totalcorrel = P.getTotalCorrel();
  this->roiX = P.getRoiX();
  this->roiY = P.getRoiY();
  this->roiZ = P.getRoiZ();
  this->roisize = 64;

}


void P::PUpdate(const string fileDirectory, int scan, int color) {

  ostringstream fileName;
  char buff [200];
  sprintf(buff,"/P_%04d_%05d",scan,color);

  fileName << fileDirectory << buff;

  ifstream File (fileName.str().c_str(),ios::in);
  if (!File)
  {
    LERROR << "Error : impossible to open file " << fileName.str() << " in read only mode...";
  }
  else
  {
    LINFO << "File of P opened for update : color is " << color << " from scan " << scan;

  }

  string dataline="";
  stringstream sstr;
  char schar[10000];
  File.getline(schar,10000);
  dataline+=schar;
  this->PUpdate(dataline);

}


void P::PUpdate(string linestr) {
  double a,b,c;
  string paramline;
  stringstream sstr;
  sstr.str(linestr);
  if(sstr.str().size()>1)
  {
    sstr >> a;
    if(!isnan(1.*a))
    {
      this->scan = (int)(1.*a);
      sstr >> a;
      this->color = (int)(1.*a);
      sstr >> a;
      sstr >> b;
      sstr >> c;
      this->coord = Coord((double)(1.*a),(double)(1.*b),(double)(1.*c));
      sstr >> a;
      this->correl = (double)(1.*a);
      this->totalcorrel = (double)(1.*a);
      this->spatialCoherency = 0;
      this->roisize = 64;
      this->roiX = 0;
      this->roiY = 0;
      this->roiZ = 0;
    }
  }

}


string P::write() const{
  ostringstream sstr;
  sstr << this->scan << " ";
  sstr << this->color << " ";
  sstr << this->coord.toString() << " ";
  sstr << this->correl << " ";
  sstr << this->totalcorrel;
  return sstr.str();

}


string P::getFileName() const {

  char buff [200];
  sprintf(buff,"/P_%04d_%05d",this->scan,this->color);
  return (string)buff;
}

void P::writeToFile(const string fileDirectory) const
{
  ostringstream fileName;
  char buff[200];
  sprintf(buff,"/P_%04d_%05d",this->scan,this->color);

  fileName << fileDirectory << buff;

  ofstream fichier(fileName.str().c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier

  if(fichier)  // si l'ouverture a réussi
  {
    fichier << this->write() << endl;
    fichier.close();  // on referme le fichier
  }
  else  // sinon
    LERROR << "Erreur à l'ouverture !";

}

int P::getScan() const {
  return scan;
}

void P::setScan(int scan) {
  this->scan = scan;
}

int P::getColor() const {
  return color;
}

int P::getGlobalColor() const {
  return color;
}

void P::setColor(int color) {
  this->color = color;
}

Coord P::getCoord() const {
  return coord;
}

void P::setCoord(Coord coord) {
  this->coord = coord;
}

double P::getCorrel() const {
  return this->correl;
}

void P::setCorrel(double correl) {
  this->correl = correl;
}

double P::getTotalCorrel() const {
  return this->totalcorrel;
}

void P::setTotalCorrel(double correl) {
  this->totalcorrel = correl;
}

double P::getSpatialCoherency() const {
  return this->spatialCoherency;
}

void P::setSpatialCoherency(double SC) {
  this->spatialCoherency = SC;
}

int P::getRoiX() const {
  return this->roiX;
}

void P::setRoiX(int roiX) {
  this->roiX = roiX;
}

int P::getRoiY() const {
  return this->roiY;
}

void P::setRoiY(int roiY) {
  this->roiY = roiY;
}

int P::getRoiZ() const {
  return this->roiZ;
}

void P::setRoiZ(int roiZ) {
  this->roiZ = roiZ;
}


string P::tostring() const {
  ostringstream sstr;
  sstr << this->color << " ";
  sstr << this->coord.toString() << " ";
  sstr << this->correl << " ";
  sstr << this->totalcorrel;
  return sstr.str();
}

string P::tostringFull() const {
  ostringstream sstr;
  sstr << this->color << " ";
  sstr << this->scan << " ";
  sstr << this->coord.toString() << " ";
  sstr << this->correl << " ";
  sstr << this->totalcorrel;
  return sstr.str();
}

bool P::equalColor(const P& part) const {
  return (this->color == part.getColor());
}

double  P::distance(const P& part) const
{
  return this->coord.dist(part.getCoord());
}

double  P::distance(const Coord& XYZ) const
{
  return this->coord.dist(XYZ);
}



Volume P::getPSubVolume(Volume& fullIm) const
{
  fullIm.setRoi(ROI((int)this->getCoord().getX()-roisize/2,(int)this->getCoord().getY()-roisize/2,(int)this->getCoord().getZ()-roisize/2,roisize,roisize,roisize));
  int* new_color = new int[65256];
  for(int j = 0; j<65256; j++)
  {
    new_color[j] = 0;
  }
  new_color[(int)this->getColor()] = 1;

  Volume partRef = fullIm.duplicateRoi();
  partRef.recolor(new_color);
  return partRef;
}



P P::findBestCorrel_DICGPU(Correl3DGPU& correlator){ //Used? The very good one for a single solution
  int pt = this->getColor(); 
  Coord target = this->coord; 	
  double* value = new double[4]; 
  bool usetex = correlator.getusetex();  
  if(usetex)         {
    correlator.setTarget(target.getZ(), target.getY(), target.getX());  
    //value = correlator.correltex();   
    value = correlator.correltexscreen();  
  }         else         { 
    correlator.setImTarget((int)target.getZ(), (int)target.getY(), (int)target.getX()); 		
    value = correlator.correl(); 
  }  
  P *cible = new P(this->scan,this->color,target.shift(value), value[3]); 	
  if(value[3]< correlator.getCorrelThr())  
    LINFO << "P " << pt << " : dx = " << (int)value[0] << ", dy = " << (int)value[1] << ", dz = " << (int)value[2] << ", corr = " << value[3] << " not kept";
  else 
    LINFO << "P " << pt << " : dx = " << (int)value[0] << ", dy = " << (int)value[1] << ", dz = " << (int)value[2] << ", corr = " << value[3] << " OK";  
  return *cible; 	 
}



P P::findBestCorrel_DICGPU(Correl3DGPU& correlator, Coord target) { //The very good one for a single solution

  if(correlator.validTarget(target)&&(this->getCorrel()!=-1))
  {
    int pt = this->getColor();

    bool usetex = correlator.getusetex();

    double* value = new double[4];

    if(usetex)
    {
      correlator.setTarget(target.getZ(), target.getY(), target.getX());
      value = correlator.correltexscreen();
      //value = correlator.correltex();
    }
    else
    {
      correlator.setImTarget((int)target.getZ(), (int)target.getY(), (int)target.getX());
      value = correlator.correl();
    }

    P *cible = new P(this->scan,this->color,target.shift(value), value[3]);
    Coord depl = cible->getCoord().diff(this->getCoord());

    if(value[3]< correlator.getCorrelThr())
      LINFO << "P " << pt << " : local_motion  = ( " << (int)value[0] << " , " << (int)value[1] << " , " << (int)value[2] << " )  global_motion = ( " << depl.getX() << " , " << depl.getY() << " , " << depl.getZ() << " )  corr = " <<  value[3] << " not good";
    else
      LINFO << "P " << pt << " : local_motion  = ( " << (int)value[0] << " , " << (int)value[1] << " , " << (int)value[2] << " )  global_motion = ( " << depl.getX() << " , " << depl.getY() << " , " << depl.getZ() << " )  corr = " <<  value[3] << " OK";
    //LINFO << "P " << pt << " : dx_corr = " << (int)value[0] << ", dy_corr = " << (int)value[1] << ", dz_corr = " << (int)value[2] << ", corr = " << value[3] << " not kept";
    //LINFO << "P " << pt << " : dx_step = " << depl.getX() << ", dy_step = " << depl.getY() << ", dz_step = " << depl.getZ() << ", corr = " << value[3] << " not kept";
    return *cible;
  }
  else
  {
    P *cible = new P(this->scan,this->color,target, -1);
    return *cible;
  }
}

P P::optimizeCorrel_DICGPU(Correl3DGPU& correlator, Coord target){ //No more used!!!
  if(correlator.validTarget(target)&&(this->getCorrel()!=-1)){
    int pt = this->getColor();

    bool usetex = correlator.getusetex();
    double* value = new double[4];

    if(usetex)
    {
      correlator.setTarget(target.getZ(), target.getY(), target.getX());
      value = correlator.correltexscreen();
    }
    else
    {
      correlator.setImTarget((int)target.getZ(), (int)target.getY(), (int)target.getX());
      value = correlator.correl();
    }

    P *cible = new P(this->scan,this->color,target.shift(value), value[3]);
    Coord depl = cible->getCoord().diff(this->getCoord());

    if(value[3]< correlator.getCorrelThr())
      LINFO << "P " << pt << " : local_motion  = (" << (int)value[0] << "," << (int)value[1] << "," << (int)value[2] << "," << ")  global_motion = (" << depl.getX() << "," << depl.getY() << "," << depl.getZ() << ")  corr = " <<  value[3] << " not good";
    else
      LINFO << "P " << pt << " : local_motion  = (" << (int)value[0] << "," << (int)value[1] << "," << (int)value[2] << "," << ")  global_motion = (" << depl.getX() << "," << depl.getY() << "," << depl.getZ() << ")  corr = " <<  value[3] << " OK";

    return *cible;
  }
  else
  {
    P *cible = new P(this->scan,this->color,target, -1);
    return *cible;
  }

}


Volume P::getPSubVolumeSafe(Volume& src, const string volumeName) const {
  int* new_color = new int[65256];
  for(int j = 0; j<65256; j++)
  {
    new_color[j] = 0;
  }
  new_color[(int)this->getColor()] = 1;
  //stack.setRoi(new Rectangle((int)this->getCoord().getX()-roisize/2,(int)this->getCoord().getY()-roisize/2,roisize,roisize));
  ROI roi = ROI((int)this->getCoord().getX()-roisize/2,(int)this->getCoord().getY()-roisize/2,(int)this->getCoord().getZ()-roisize/2,roisize,roisize,roisize);
  ROI image = ROI(0,0,0,src.getdimX(),src.getdimY(),src.getdimZ());
  ROI intersect = roi.intersection(image);
  Volume dest;
  if(intersect.contains(roi))
  {
    src.setRoi(roi);
    dest = src.duplicateRoi();
  }
  else
  {
    printf("border ROI");
    src.setRoi(intersect);
    dest = src.duplicateRoi();
  }
  dest.recolor(new_color);
  return dest;
}


P*  P::findClosest(PSet<P>& potential) {
  if(potential.Size()>0)
  {
    double bestDist=1000000000000;
    list<P*>::iterator best = potential.getIteratorBegin();
    for (list<P*>::iterator pit=potential.getIteratorBegin(); pit!=potential.getIteratorEnd(); ++pit)
    {
      double dist = this->distance(**pit);
      if(dist<bestDist)
      {
        best=pit;
        bestDist=dist;
      }
    }
    return *best;
  }
  else
  {
    P* emptyPart(0);
    // = new Particle();
    return emptyPart;
  }
}

//template<class T>    
//template <typename T>
double P::estimateSpatialCoherency(PSet<Particle>& pi, PSet<Particle>& pf)
{
  return 1.;
}

double P::estimateSpatialCoherency(PSet<P>& pi, PSet<P>& pf)
{
  double SC = 0;
  double eps = 0.1;
  double thr = 2;
  int N = pi.Size()-1;
  if(N>0)
  {
    double u0 = 0;
    double v0 = 0;
    double w0 = 0;
    double* u = new double[N];
    double* v = new double[N];
    double* w = new double[N];
    double* ru = new double[N];
    double* rv = new double[N];
    double* rw = new double[N];
    {
      list<P*>::iterator pit;
      list<P*>::iterator pitF;
      int i;
      for (pit=pi.getIteratorBegin(), pitF = pf.getIteratorBegin(), i=0; pit!=pi.getIteratorEnd(); ++pit, ++pitF)
      {
        if(!this->equalColor((**pit)))
        {
          Coord depl = (*pitF)->getCoord().diff((*pit)->getCoord());
          u[i]=depl.getX();
          v[i]=depl.getY();
          w[i]=depl.getZ();
          i++;
        }
        else
        {
          Coord depl = (*pitF)->getCoord().diff((*pit)->getCoord());
          u0=depl.getX();
          v0=depl.getY();
          w0=depl.getZ();
        }
      }
    }
    double um = median(u,N);
    double vm = median(v,N);
    double wm = median(w,N);
    double fu = abs(u0 - um); 
    double fv = abs(v0 - vm); 
    double fw = abs(w0 - wm); 
    for(int i=0; i<N; i++)
    {
      ru[i]=abs(u[i]-um);
      rv[i]=abs(v[i]-vm);
      rw[i]=abs(w[i]-wm);
    }
    double resu = median(ru,N);
    double resv = median(rv,N);
    double resw = median(rw,N);
    double nu = fu/(resu+eps);
    double nv = fv/(resv+eps);
    double nw = fw/(resw+eps);
    double cor = sqrt(nu*nu+nv*nv+nw*nw);
    if(cor<thr)
      SC=1;
  }
  return SC;    
}

double P::estimateLocalQuadraticCoherency(PSet<Particle>& pi, PSet<Particle>& pf)
{
  return 1.;
}

double P::estimateLocalQuadraticCoherency(PSet<P>& pi, PSet<P>& pf)
{
  double SC = 0;
  double eps = 0.1;
  double thr = 0.1;
  int N = pi.Size();
  double* u = new double[N];
  double* v = new double[N];
  double* w = new double[N];
  double* nV = new double[N];       

  double* ru = new double[N];
  double* rv = new double[N];
  double* rw = new double[N];

  double* sig2 = new double[N];

  double* M = new double[10*N];
  double* X = new double[10*N];
  double* Wg = new double[N];
  int i0 = 0;

  {
    list<P*>::iterator pit;
    list<P*>::iterator pitF;
    int i;
    for (pit=pi.getIteratorBegin(), pitF = pf.getIteratorBegin(), i=0; pit!=pi.getIteratorEnd(); ++pit, ++pitF)
    {
      Coord pos = (*pit)->getCoord();
      Coord depl = (*pitF)->getCoord().diff(pos);
      u[i]=depl.getX();
      v[i]=depl.getY();
      w[i]=depl.getZ();
      nV[i] = sqrt(u[i]*u[i]+v[i]*v[i]+w[i]*w[i]);
      X[i+0*N]=1;
      X[i+1*N]=pos.getX();
      X[i+2*N]=pos.getY();
      X[i+3*N]=pos.getZ();
      X[i+4*N]=pos.getX()*pos.getY();
      X[i+5*N]=pos.getX()*pos.getZ();
      X[i+6*N]=pos.getY()*pos.getZ();
      X[i+7*N]=pos.getX()*pos.getX();
      X[i+8*N]=pos.getY()*pos.getY();
      X[i+9*N]=pos.getZ()*pos.getZ();
      if(this->equalColor((**pit)))
        i0=i;
      i++;
    }
  }
  double um = median(u,N);
  double vm = median(v,N);
  double wm = median(w,N);
  double nVm = median(nV,N);
  double K=0;
  for(int i=0; i<N; i++)
  {
    ru[i]=u[i]-um;
    rv[i]=v[i]-vm;
    rw[i]=w[i]-wm;
    sig2[i]=ru[i]*ru[i]+rv[i]*rv[i]+rw[i]*rw[i];
    K+=sqrt(sig2[i]);
  }
  K/=N;
  K+=eps;
  K*=K;
  for(int i=0; i<N; i++)
  {
    Wg[i]=exp(-sig2[i]/K);
  } 
  for(int i=0; i<N; i++)
    for(int j=0; j<10; j++)
    {
      M[i*10+j]=Wg[i]*X[j*N+i];
    } 
  double* au = new double[10];
  double* av = new double[10];
  double* aw = new double[10];
  solve_MXA_MU(M, X, u, au, N, 10);
  solve_MXA_MU(M, X, v, av, N, 10);
  solve_MXA_MU(M, X, w, aw, N, 10);

  double phiu = 0;
  double phiv = 0;
  double phiw = 0;
  for(int j=0; j<10; j++)
  {
    phiu+=au[j]*X[i0+j*N];
    phiv+=av[j]*X[i0+j*N];
    phiw+=aw[j]*X[i0+j*N];
  }
  double Cu = (phiu-u[i0])*(phiu-u[i0]);
  double Cv = (phiv-v[i0])*(phiv-v[i0]);
  double Cw = (phiw-w[i0])*(phiw-w[i0]);
  double C = (Cu+Cv+Cw)/(3.*(nVm+eps)*(nVm+eps));
  if (C<thr)
    SC=1;
  LDEBUG << "P " << this->getColor() << " Local coherency = " << C << " Correl=" << this->getCorrel();
  return SC;    
}

bool P::operator< (P p)
{
  return color<p.getColor(); 
}



