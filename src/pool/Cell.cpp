#include "Cell.h"
using namespace std;

	Cell::Cell() {
		this->number = 0;
		this->numberOfPoints=0;
		this->points = new list<P*> ();
        this->particles = new PSet<P>();
    }
	
	Cell::Cell(int number, list<P*> *points) {
		this->number = number;
		this->numberOfPoints = points->size();
		this->points = points;
        this->particles = new PSet<P>();
	}

	Cell::Cell(int number, P* p1, P* p2, P* p3, P* p4, P* p5, P* p6, P* p7, P* p8) {
		this->number = number;
		this->numberOfPoints=8;
		this->points = new list<P*> ();
		this->points->push_back(p1);
		this->points->push_back(p2);
		this->points->push_back(p3);
		this->points->push_back(p4);
		this->points->push_back(p5);
		this->points->push_back(p6);
		this->points->push_back(p7);
		this->points->push_back(p8);
        this->particles = new PSet<P>();
	}

    Cell::Cell(int number, P* p1, P* p2, P* p3, P* p4) {
		this->number = number;
		this->numberOfPoints=4;
		this->points = new list<P*> ();
		this->points->push_back(p1);
		this->points->push_back(p2);
		this->points->push_back(p3);
		this->points->push_back(p4);
        this->particles = new PSet<P>();
	}

    Cell::~Cell(){
		this->number = 0;
		this->numberOfPoints=0;
    	this->points->clear();
        delete points;
        delete particles;
    }

    void Cell::addPoint(P* p) {
        this->numberOfPoints++;
        this->points->push_back(p);
    }
   
    P* Cell::getPoint(int i) {
		int j=0;
	 	for (list<P*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit, ++j)
	 	{
			if(i==j)
				return *pit;
		}
		P* emptyPoint(0);
		return emptyPoint;
	} 

    list<Cell*> Cell::split() {
        list<Cell*>* cl = new list<Cell*>();
        if(this->numberOfPoints==8) {
            cl->push_back(new Cell(this->number, this->getPoint(1),this->getPoint(2),this->getPoint(3),this->getPoint(5)));
            cl->push_back(new Cell(this->number, this->getPoint(2),this->getPoint(3),this->getPoint(5),this->getPoint(8)));
            cl->push_back(new Cell(this->number, this->getPoint(2),this->getPoint(8),this->getPoint(5),this->getPoint(6)));
            cl->push_back(new Cell(this->number, this->getPoint(4),this->getPoint(2),this->getPoint(8),this->getPoint(3)));
            cl->push_back(new Cell(this->number, this->getPoint(7),this->getPoint(5),this->getPoint(3),this->getPoint(8)));
        }
        return *cl;
    }

    Coord Cell::getMin() {
        list<P*>::iterator pit=this->points->begin();
        Coord min = (*pit)->getCoord();
	 	++pit;
        for (; pit!=this->points->end(); ++pit)
        {
            min = min.min((*pit)->getCoord());
        }
        return min;
    }

    Coord Cell::getMax() {
        list<P*>::iterator pit=this->points->begin();
        Coord max = (*pit)->getCoord();
	 	++pit;
        for (; pit!=this->points->end(); ++pit)
        {
            max = max.max((*pit)->getCoord());
        }
        return max;
    }

    bool Cell::isInCell(Coord pt) {
        if(this->numberOfPoints==8) {
            if ((pt>=this->getMin())&&(pt<=this->getMax())) {
                list<Cell*> cell4 = this->split();
                bool inCell = false;
                for(list<Cell*>::iterator cit = cell4.begin(); cit!=cell4.end(); ++cit)
                {   
                    inCell |= (*cit)->isInCell(pt);
                    if(inCell)
                        return true;
                }
            }
        }
        if(this->numberOfPoints==4) {
            if ((pt>=this->getMin())&&(pt<=this->getMax())) {
                list<P*>::iterator pit=this->points->begin();
                Coord C1 = (*pit)->getCoord();
                ++pit;
                Coord C2 = (*pit)->getCoord();
                ++pit;
                Coord C3 = (*pit)->getCoord();
                ++pit;
                Coord C4 = (*pit)->getCoord();
                double a = C1.getX() - C4.getX();
                double b = C2.getX() - C4.getX();
                double c = C3.getX() - C4.getX();
                double d = C1.getY() - C4.getY();
                double e = C2.getY() - C4.getY();
                double f = C3.getY() - C4.getY();
                double g = C1.getZ() - C4.getZ();
                double h = C2.getZ() - C4.getZ();
                double i = C3.getZ() - C4.getZ();
                double aa = e*i-f*h;
                double bb = f*g-d*i;
                double cc = d*h-e*g;
                double dd = c*h-b*i;
                double ee = a*i-c*g;
                double ff = b*g-a*h;
                double gg = b*f-c*e;
                double hh = c*d-a*f;
                double ii = a*e-b*d;
                double detA = a*aa+b*bb+c*cc;
                double alpha = -(C4.getX()*aa+C4.getY()*bb+C4.getZ()*cc)/detA;
                double beta = -(C4.getX()*dd+C4.getY()*ee+C4.getZ()*ff)/detA;
                double gamma = -(C4.getX()*gg+C4.getY()*hh+C4.getZ()*ii)/detA;
                double delta = 1 - alpha - beta - gamma;
                if((alpha>=0)&&(beta>=0)&&(gamma>=0)&&(delta>=0))
                    return true;
            }
        }
        return false;
    }

    string Cell::toINPString()
    {
        ostringstream line;
        if(this->numberOfPoints==8)
        {
            line << this->number << " 0 hex";
            list<P*>::iterator pit=this->points->begin(); 
	     	line << " " << (*pit)->getColor();
            ++pit;
	     	line << " " << (*pit)->getColor();
            ++pit;
            ++pit;
	     	line << " " << (*pit)->getColor();
            --pit;
	     	line << " " << (*pit)->getColor();
            ++pit;
            ++pit;
	     	line << " " << (*pit)->getColor();
            ++pit;
	     	line << " " << (*pit)->getColor();
            ++pit;
            ++pit;
	     	line << " " << (*pit)->getColor();
            --pit;
	    	line << " " << (*pit)->getColor();
	    }
	    else
        {
            line << this->number << " 0 ???";
            for (list<P*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
	        {
	            if(*pit)
	     	        line << " " << (*pit)->getColor();
            } 
        }
        
	 	return line.str();
    }
