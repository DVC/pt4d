#include "Image3DGPU.h"
#include "Image3DGPU.cuh"

/*!
 * \file Image3DGPU.cu
 * \brief Image 3D
 * \author pierre.lhuissier@simap.grenoble-inp.fr
 * \version 0.1
 */

using namespace std;
   
 
/*!
*  \brief Constructor
*
*  Constructor of Image3DGPU class
*
*  \param im : path to 3D image
*/

Image3DGPU::Image3DGPU(const char * im) {
    setDimensions(im);
   	fullData_size = fullDataD * fullDataH * fullDataW;

	mem_fullData_byte = fullData_size*sizeof(imtype);
	mem_param_byte = ceil(sizeof(Parameters) / 128.0 )*128;
        
    displacementX = FIXED/2;
    displacementY = FIXED/2;
    displacementZ = FIXED/2;

    deviceFree = true;
   	deviceFullImFree = true;
   	hostFree = true;
    paramFree = true;

    deviceTotalMem = 0;	        

	v_max = new float[0];
	h_max = new int[0];
	hostFree = false;
    dev = 0;
    cudaSetDevice(dev);
    cudaDeviceReset();
    cudaGetDeviceProperties(&deviceProp, dev);
}


void Image3DGPU::setParam(Parameters &param) {
	if(paramFree)
    {
        cudaMalloc( (void**) &d_Param, mem_param_byte );
	    deviceTotalMem += mem_param_byte;
        paramFree=false;
    }
    this->h_Param=&param;
	cudaMemcpy(d_Param, &param, sizeof(Parameters), cudaMemcpyHostToDevice) ;
}


/*!
*  \brief Destructor
*
*  Destructor of Image3DGPU class
*/

void Image3DGPU::finalize()
{
		freeFullImg();
        free();

}

/*!
*  \brief Set maximal displacementX
*
*  \param displacement
*/

void Image3DGPU::setDisplacementX(int displacement) {
    this->displacementX = displacement;
}		

/*!
*  \brief Set maximal displacementY
*
*  \param displacement
*/

void Image3DGPU::setDisplacementY(int displacement) {
    this->displacementY = displacement;
}

/*!
*  \brief Set maximal displacementZ
*
*  \param displacement
*/

void Image3DGPU::setDisplacementZ(int displacement) {
    this->displacementZ = displacement;
}

/*!
*  \brief Allocate memory and initialize handle
*/
void Image3DGPU::init()
{	
	cudaMalloc((void **)&d_res, ((2*displacementX+1)*(2*displacementY+1)*(2*displacementZ+1))*sizeof(float));
    deviceTotalMem += ((2*displacementX+1)*(2*displacementY+1)*(2*displacementZ+1))*sizeof(float);
    LINFO << format("Allocated %d floats for d_res\n", (1+2*displacementX)*(1+2*displacementY)*(1+2*displacementZ));
        
    LINFO << format("Allocated %.4f Mbytes / %.0f Mbytes (%.4f %) on GPU memory\n\n",(float)deviceTotalMem/1048576.0f, (float)deviceProp.totalGlobalMem/1048576.0f, (100.*deviceTotalMem)/deviceProp.totalGlobalMem);
	        
		//cublasInit();
    cublasCreate(&cbhandle);
    deviceFree = false;
}



void Image3DGPU::freeFullImg()
{
		if(!deviceFullImFree)
		{
		    cudaFree(d_FullData);
            deviceTotalMem -= mem_fullData_byte;
		}
		deviceFullImFree = true;
}
	
/*!
*  \brief Free allocated memory on device, on host and free cufft and cublas handle
*/

void Image3DGPU::free()
{
	if(!paramFree)
    {
        cudaFree(d_Param);
	    deviceTotalMem -= mem_param_byte;
        paramFree = true;
    }
    
	if(!deviceFree)
		{
			//cublasShutdown();
            cublasDestroy(cbhandle);
			
            cudaFree(d_res);
            deviceTotalMem -= (2*displacementX+1)*(2*displacementY+1)*(2*displacementZ+1)*sizeof(float);


		    if(!deviceFullImFree)
			{
                cudaFree(d_FullData);
                deviceTotalMem -= mem_fullData_byte;
		    }
		    deviceFullImFree = true;
			deviceFree = true;
		}
		
		if(!hostFree)
	    {
			cudaFreeHost(h_FullData);
			h_FullData = NULL;
		    h_max = NULL;
		    hostFree = true;
		}
}
	
/*!
*  \brief Write Image options to string
*
*  \return string containing Image options
*/

string Image3DGPU::optionsToString()
{
		ostringstream s;
    	s << "direct Image\n";
        s << "Maximal X displacement  " << displacementX << "\n";
        s << "Maximal Y displacement  " << displacementY << "\n";
        s << "Maximal Z displacement  " << displacementZ << "\n";
		return s.str();
}

/*!
*  \brief Perform closest label extraction
*
*  \return label value (-1 if out of displThr)
*/

int Image3DGPU::label_dist(Coord X){
	    
	int label = 0;
    if( (X.getX()>0) && (X.getY()>0) && (X.getZ()>0) && (X.getX()<fullDataW) && (X.getY()<fullDataH) && (X.getZ()<fullDataD)){
    //LDEBUG << "Target " << X.display();        
    dim3 threads(iAlignUp((2*displacementX +1),16), 1, 1);
    dim3 grid(iDivUp((2*displacementY +1), threads.y), iDivUp((2*displacementZ +1), threads.z),1);
    label_dist_kernel<<<grid,threads>>>(d_res, d_FullData, displacementX, displacementY, displacementZ, fullDataW, fullDataH, fullDataD, X.getX(), X.getY(), X.getZ());
    cudaDeviceSynchronize();
    int index=0;
    cublasIsamin(cbhandle, (2*displacementX+1)*(2*displacementY+1)*(2*displacementZ+1), d_res,1,&index);
    index--;
	cudaMemcpy(v_max, &d_res[index], sizeof(float), cudaMemcpyDeviceToHost);
    //LDEBUG << "Min index = " << index << " with dist=" << v_max[0]; 		    
	//cudaMemcpy(v_max, &d_res[index+1], sizeof(float), cudaMemcpyDeviceToHost);
    //LDEBUG << "At index+1 dist=" << v_max[0]; 		    
	//cudaMemcpy(v_max, &d_res[index+2], sizeof(float), cudaMemcpyDeviceToHost);
    //LDEBUG << "At index+2 dist=" << v_max[0]; 		    
    
    int min_z_GPU = (int)(index/((2*displacementX+1)*(2*displacementY+1)));
	int min_y_GPU = (int)((index%((2*displacementX+1)*(2*displacementY+1)))/(2*displacementX+1));
	int min_x_GPU = index%(2*displacementX+1);

	int xl = min_x_GPU - displacementX + X.getX();
    int yl = min_y_GPU - displacementY + X.getY();
    int zl = min_z_GPU - displacementZ + X.getZ();

    index = xl + fullDataW * ( yl + fullDataH * zl ); 
    //LDEBUG << "Min global index = " << index; 		    
    //LDEBUG << "Min global target (" << xl << "," << yl << "," << zl << ")"; 		    

	if((index < fullDataW*fullDataH*fullDataD) && (index>0))
    {
        cudaMemcpy(h_max, &d_FullData[index], sizeof(int), cudaMemcpyDeviceToHost);
        label = (int)h_max[0];
    }
    //LDEBUG << "Label = " << label; 		    
    //LINFO << "Image value on GPU  0 = " << this->getValue(d_FullData, fullDataD, fullDataH, fullDataH, 687, 604, 595);
    //LINFO << "Image value on GPU 20 = " << this->getValue(d_FullData, fullDataD, fullDataH, fullDataH, 687, 604, 596);
    }

    return label;
}



	
/*!
*  \brief Set full reference image from tiff image
*
*  \param img1 : path of reference image
*/

void Image3DGPU::setFullIm(const char* img1)
{
    setDimensions(img1, fullDataW, fullDataH, fullDataD);
    LINFO << "Dimensions (" << fullDataW << "," << fullDataH << "," << fullDataD << ")";
    h_FullData = new imtype[fullDataD*fullDataH*fullDataW];
    cudaMallocHost(&h_FullData,fullDataD*fullDataH*fullDataW*sizeof(imtype));

    deviceFullImFree = false;
    readfromTIFF(img1,h_FullData);
    
    mem_fullData_byte = fullDataD*fullDataH*fullDataW*sizeof(imtype);
    deviceTotalMem += mem_fullData_byte;
        
    cudaMalloc(&d_FullData,fullDataD*fullDataH*fullDataW*sizeof(imtype));
    cudaMemcpy(d_FullData, h_FullData, (long)fullDataW*fullDataH*fullDataD*sizeof(imtype), cudaMemcpyHostToDevice);
    
    LINFO << format("Allocated %.0f Mbytes / %.0f Mbytes (%.0f %) on GPU memory\n\n",(float)deviceTotalMem/1048576.0f, (float)deviceProp.totalGlobalMem/1048576.0f, (100.*deviceTotalMem)/deviceProp.totalGlobalMem);
    cudaFreeHost(h_FullData);
    h_FullData = NULL;
    cudaDeviceSynchronize();
}


/*!
*  \brief Set full image from raw image
*
*  \param img1 : path of reference image
*  \param dimZ : depth of image
*  \param dimY : height of image
*  \param dimX : width of image
*/

void Image3DGPU::setFullIm(char* img1, int dimD, int dimH, int dimW)
{
    fullDataW = dimW;
    fullDataH = dimH;
    fullDataD = dimD;
    h_FullData = new imtype[fullDataD*fullDataH*fullDataW];
    cudaMallocHost(&h_FullData,dimW*dimH*dimD*sizeof(char));
    cudaMalloc(&d_FullData,dimW*dimH*dimD*sizeof(char));
    deviceFullImFree = false;
    //readfromTIFF(img1,h_FullData);
    readfromImagePlus(img1,h_FullData, dimD, dimH, dimW);
    LINFO << format("Allocated %.0f Mbytes / %.0f Mbytes (%.0f %) on GPU memory\n\n",(float)deviceTotalMem/1048576.0f, (float)deviceProp.totalGlobalMem/1048576.0f, (100.*deviceTotalMem)/deviceProp.totalGlobalMem);
    cudaMemcpy(d_FullData,   h_FullData,   (long)dimW*dimH*dimD*sizeof(char), cudaMemcpyHostToDevice);
    cudaFreeHost(h_FullData);
    h_FullData = NULL;
    cudaDeviceSynchronize();
}

/**

int iDivUp(int a, int b){
    return (a % b != 0) ? (a / b + 1) : (a / b);
}

//Align a to nearest higher multiple of b
int iAlignUp(int a, int b){
    return (a % b != 0) ?  (a - a % b + b) : a;
}
*/

/*!
*  \brief Read raw image
*
*  \param image_path : Path of image
*  \param pix : pointer to image array
*  \param dimZ : depth of image
*  \param dimY : height of image
*  \param dimX : width of image
*/

void Image3DGPU::readfromRAW(const char *name, imtype *m, const unsigned int dim_1, const unsigned int dim_2, const unsigned int dim_3)
{
    LINFO << format("Loading from %s...\n", name);
    FILE *fo;
  	fo=fopen(name,"rb");
   	fread(m,sizeof(imtype),dim_1*dim_2*dim_3,fo);
   	fclose(fo);
}

/*!
*  \brief Read tiff image
*
*  \param image_path : Path of image
*  \param pix : pointer to image array
*/

void Image3DGPU::readfromTIFF(const char * image_path, imtype* pix)
{
	LINFO << format("Loading from %s...\n",image_path);
		imtype maxval=0;

	TIFF* tif = TIFFOpen(image_path, "r");
	if (tif) {
		uint32 imageWidth, imageLength;
		uint32 row;
		int j=0;
		//int slice=0;
		TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &imageWidth);
		TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &imageLength);
		//TIFFGetField(tif, TIFFTAG_IMAGEDEPTH, &imageDepth);
		//printf("Volume size checked (%d,%d,%d)\n",imageWidth,imageLength,imageDepth);
        unsigned short int* buf = (unsigned short int*)malloc(imageWidth * sizeof(unsigned short int));
		do
		{
			//slice ++;
			//int slice_count=0;
			//buf = _TIFFmalloc(TIFFScanlineSize(tif));
			for (row = 0; row < imageLength; row++)
			{
				TIFFReadScanline(tif, buf, row);
				for (int k = 0; k < imageWidth; k++)
				{
					pix[j] = (imtype)buf[k];
					if(pix[j]>maxval)
                        maxval=pix[j];
                    j++;
					//if(buf[k]==(imtype)255)
					//	slice_count++;
				}
			}
		//	printf("slice %d found %d white pixels\n",slice,slice_count);
		} while (TIFFReadDirectory(tif));
		_TIFFfree(buf);
		TIFFClose(tif);
	}
    //LINFO << "Max value in image is "  << maxval;
    //LINFO << "Image value 0 = " << pix[(fullDataH * 687 + 604) *  fullDataW + 595];
    //LINFO << "Image value 20 = " << pix[(fullDataH * 687 + 604) *  fullDataW + 596];
}

void Image3DGPU::writetoRAW(const char * image_path, float* pix, unsigned int imageWidth, unsigned int imageLength, unsigned int imageDepth )
{
	LINFO << format("Writing to %s...\n",image_path);
    FILE *fo;
  	fo=fopen(image_path,"wb");
   	fwrite(pix,sizeof(float),imageWidth*imageLength*imageDepth,fo);
   	fclose(fo);
}


void Image3DGPU::writetoTIFF(const char * image_path, imtype* pix, unsigned int imageWidth, unsigned int imageLength, unsigned int imageDepth )
{
	LINFO << format("Writing to %s...\n",image_path);

	TIFF* tif = TIFFOpen(image_path, "w");
	if (tif) {
		for(int slice=0; slice <imageDepth; slice++)
		{
			TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, imageWidth);
					TIFFSetField(tif, TIFFTAG_IMAGELENGTH, imageLength);
					TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, 1);   // set number of channels per pixel
					TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, 32);    // set the size of the channels
					TIFFSetField(tif, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);    // set the origin of the image.
					TIFFSetField(tif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
					TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
					TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_NONE);
					TIFFSetField(tif, TIFFTAG_SUBFILETYPE, FILETYPE_PAGE);TIFFSetField(tif, TIFFTAG_PAGENUMBER, slice+1, imageDepth);

			//Now writing image to the file one strip at a time
			for (uint32 row = 0; row < imageLength; row++)
			{
				if (TIFFWriteScanline(tif, &pix[row*imageWidth+slice*imageWidth*imageLength], row, 0) != 1)
					break;
			}
			TIFFWriteDirectory(tif);

		}
		(void) TIFFClose(tif);
		//if (buf)
		//	_TIFFfree(buf);
	}
}

void Image3DGPU::writetoTIFF(const char * image_path, float* pix, unsigned int imageWidth, unsigned int imageLength, unsigned int imageDepth )
{
	LINFO << format("Writing to %s...\n",image_path);

	TIFF* tif = TIFFOpen(image_path, "w");
	if (tif) {
		for(int slice=0; slice <imageDepth; slice++)
		{
			        TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, imageWidth);
					TIFFSetField(tif, TIFFTAG_IMAGELENGTH, imageLength);
					TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, 1);   // set number of channels per pixel
					TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, 32);    // set the size of the channels
					TIFFSetField(tif, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);    // set the origin of the image.
					TIFFSetField(tif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
					TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
					TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_NONE);
					TIFFSetField(tif, TIFFTAG_SUBFILETYPE, FILETYPE_PAGE);
                    TIFFSetField(tif, TIFFTAG_PAGENUMBER, slice+1, imageDepth);

			//Now writing image to the file one strip at a time
			for (uint32 row = 0; row < imageLength; row++)
			{
				if (TIFFWriteScanline(tif, &pix[row*imageWidth+slice*imageWidth*imageLength], row, 0) != 1)
					break;
			}
			TIFFWriteDirectory(tif);

		}
		(void) TIFFClose(tif);
		//if (buf)
		//	_TIFFfree(buf);
	}
}


void Image3DGPU::setDimensions(const char* img1, int& fullDataW, int& fullDataH, int& fullDataD)
{
	string mode = "r";
	TIFF* tif = TIFFOpen(img1,mode.c_str());
	if (tif) {
		uint32 imageWidth, imageLength;
		TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &imageWidth);
		TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &imageLength);
		int dircount = 0;
        do {
            dircount++;
        } while (TIFFReadDirectory(tif));
        TIFFClose(tif);
		fullDataW = (int)imageWidth;
		fullDataH = (int)imageLength;
		fullDataD = (int)dircount;
	}
	else
	{
		fullDataW = 0;
		fullDataH = 0;
		fullDataD = 0;
	}
}


void Image3DGPU::setDimensions(const char* img1)
{
	string mode = "r";
	TIFF* tif = TIFFOpen(img1,mode.c_str());
	if (tif) {
		uint32 imageWidth, imageLength;
		TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &imageWidth);
		TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &imageLength);
		int dircount = 0;
        do {
            dircount++;
        } while (TIFFReadDirectory(tif));
        TIFFClose(tif);
		fullDataW = (int)imageWidth;
		fullDataH = (int)imageLength;
		fullDataD = (int)dircount;
	}
	else
	{
		fullDataW = 0;
		fullDataH = 0;
		fullDataD = 0;
	}

	   	fullData_size = fullDataD * fullDataH * fullDataW;
		mem_fullData_byte = fullData_size*sizeof(imtype);
}




/*!
*  \brief Read raw image
*
*  \param image_path : Path of image
*  \param pix : pointer to image array
*  \param dimZ : depth of image
*  \param dimY : height of image
*  \param dimX : width of image
*/

void Image3DGPU::readfromImagePlus(char* image_path, imtype* pix, int dimZ, int dimY, int dimX)
{
	LINFO <<format("Loading from %s...\n",image_path);
	FILE *fo;
	fo=fopen(image_path,"rb");
	size_t checked = fread(pix,sizeof(imtype),dimX*dimY*dimZ,fo);
	fclose(fo);
    LINFO << format("Data loaded.\n");
}

/*!
*  \brief Read raw image
*
*  \param image_path : Path of image
*  \param pix : pointer to image array
*  \param dimZ : depth of image
*  \param dimY : height of image
*  \param dimX : width of image
*/

void Image3DGPU::readfromImagePlus(const char* image_path, imtype* pix, int dimZ, int dimY, int dimX)
{
	LINFO << format("Loading from %s...\n",image_path);
	FILE *fo;
	fo=fopen(image_path,"rb");
	size_t checked = fread(pix,sizeof(imtype),dimX*dimY*dimZ,fo);
	fclose(fo);
	LINFO << format("Data loaded.\n");
}

float Image3DGPU::getValue(float* d_Src, int dimZ, int dimY, int dimX, int z, int y, int x)
{
	float* h_value = (float*)malloc(sizeof(float));
	int index = z*dimX*dimY + y*dimX + x;
	cudaMemcpy(h_value, &d_Src[index], sizeof(float), cudaMemcpyDeviceToHost);
	return h_value[0];
}

imtype Image3DGPU::getValue(imtype* d_Src, int dimZ, int dimY, int dimX, int z, int y, int x)
{
	imtype* h_value = (imtype*)malloc(sizeof(imtype));
	int index = z*dimX*dimY + y*dimX + x;
	cudaMemcpy(h_value, &d_Src[index], sizeof(imtype), cudaMemcpyDeviceToHost);
	return h_value[0];
}


