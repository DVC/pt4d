#include "PSet.h"

using namespace std;


//Constructor and destructor
template<class T>
PSet<T>::PSet() {
		this->name = "";
		this->StackdimX=0;
		this->StackdimY=0;
		this->StackdimZ=0;
		this->ps = new list<T*> ();
	}

template<class T>
PSet<T>::PSet(string name) {
		this->name = name;
		this->StackdimX=0;
		this->StackdimY=0;
		this->StackdimZ=0;
		this->setDimensions();
		this->ps = new list<T*> ();
	}

template<class T>
PSet<T>::PSet(string name, list<T*> *points) {
		this->name = name;
		this->StackdimX=0;
		this->StackdimY=0;
		this->StackdimZ=0;
		this->setDimensions();
		this->ps = points;
	}

template<class T>
PSet<T>::PSet(const string fileDirectory, const string pointsSetName, const vector<PSet<T> > & raw) {
		 this->ps = new list<T*>();
		 ostringstream fileName;
		 fileName << fileDirectory << pointsSetName;
		 ifstream fichier(fileName.str().c_str(), ios::in);  //déclaration du flux et ouverture du fichier
		 if(fichier)  // si l'ouverture a réussi
	     {

		    	 string psName = "";
		    	 char schar[10000];
		    	 fichier.getline(schar,10000);
		    	 psName+=schar;
		    	 stringstream sstr;
		    	 sstr.clear();
		    	 sstr.str(psName);
		    	 string s;
		    	 int a;
		    	 if(sstr.str().size()>1)
		    	 {
		    		 sstr >> s;
		    		 this->name=s;
		    	 }
		    	 psName = "";
		    	 fichier.getline(schar,10000);
		    	 psName+=schar;
		    	 sstr.clear();
		    	 sstr.str(psName);
		    	 if(sstr.str().size()>1)
		    	 {
		    		 sstr >> a;
			    	 this->StackdimX = (int)a;
		    	 }
		    	 psName = "";
		    	 fichier.getline(schar,10000);
		    	 psName+=schar;
		    	 sstr.clear();
		    	 sstr.str(psName);
		    	 if(sstr.str().size()>1)
		    	 {
		    		 sstr >> a;
			    	 this->StackdimY = (int)a;
		    	 }
		    	 psName = "";
		    	 fichier.getline(schar,10000);
		    	 psName+=schar;
		    	 sstr.clear();
		    	 sstr.str(psName);
		    	 if(sstr.str().size()>1)
		    	 {
		    		 sstr >> a;
			    	 this->StackdimZ = (int)a;
		    	 }

		    	 LDEBUG << "Updated PointsSet " << pointsSetName;
		    	 LDEBUG << "Name = " << this->name;
		    	 LDEBUG << "dim = (" << this->StackdimX << "," << this->StackdimY << "," << this->StackdimZ << ")";
		    	 string dataline="";
		    	 while (fichier.good())
		    	 {
		    	 dataline="";
		         fichier.getline(schar,10000);
		    	 dataline+=schar;
		    	 sstr.clear();
		    	 sstr.str(dataline);
		    	 if(sstr.str().size()>1)
		    	 {
		    		sstr >> a;
		    		if(!isnan(1.*a))
		    		{
		    			int scan = (int)(1.*a);
		    			sstr >> a;
		    			int color = (int)(1.*a);
		    			T* part = raw[scan].findP(color);
		    			part->PUpdate(fileDirectory,scan,color);
		    			this->ps->push_back(part);
			      }
		    	 }
		     }
			 fichier.close();
		}
		 else  // sinon
	     {
	    	 LERROR << "Erreur à l'ouverture !";
	     	 LERROR << "File " << fileName.str() << " not found!";
	     }
	}

template<class T>
PSet<T>::PSet(const string fileDirectory, const string pointsSetName, vector<T*> & raw) {
		 this->ps = new list<T*>();
		 ostringstream fileName;
		 fileName << fileDirectory << pointsSetName;
		 ifstream fichier(fileName.str().c_str(), ios::in);  //déclaration du flux et ouverture du fichier
		 if(fichier)  // si l'ouverture a réussi
	     {

		    	 string psName = "";
		    	 char schar[10000];
		    	 fichier.getline(schar,10000);
		    	 psName+=schar;
		    	 stringstream sstr;
		    	 sstr.clear();
		    	 sstr.str(psName);
		    	 string s;
		    	 int a;
		    	 if(sstr.str().size()>1)
		    	 {
		    		 sstr >> s;
		    		 this->name=s;
		    	 }
		    	 psName = "";
		    	 fichier.getline(schar,10000);
		    	 psName+=schar;
		    	 sstr.clear();
		    	 sstr.str(psName);
		    	 if(sstr.str().size()>1)
		    	 {
		    		 sstr >> a;
			    	 this->StackdimX = (int)a;
		    	 }
		    	 psName = "";
		    	 fichier.getline(schar,10000);
		    	 psName+=schar;
		    	 sstr.clear();
		    	 sstr.str(psName);
		    	 if(sstr.str().size()>1)
		    	 {
		    		 sstr >> a;
			    	 this->StackdimY = (int)a;
		    	 }
		    	 psName = "";
		    	 fichier.getline(schar,10000);
		    	 psName+=schar;
		    	 sstr.clear();
		    	 sstr.str(psName);
		    	 if(sstr.str().size()>1)
		    	 {
		    		 sstr >> a;
			    	 this->StackdimZ = (int)a;
		    	 }

		    	 LDEBUG << "Updated PointsSet " << pointsSetName;
		    	 LDEBUG << "Name = " << this->name;
		    	 LDEBUG << "dim = (" << this->StackdimX << "," << this->StackdimY << "," << this->StackdimZ << ")";
		    	 string pName="";
		    	 while (fichier.good())
		    	 {

		    		 pName="";
		    		 fichier.getline(schar,10000);
		    		 pName+=schar;

		    		 ostringstream fullpName;
		    		 fullpName << fileDirectory << "/" << pName;
		    		 if(ifstream(fullpName.str().c_str()) && pName!=""){
		    			 T* part = new T(fileDirectory, pName);
		    			 raw.push_back(part);
		    			 this->ps->push_back(part);
		    		 }
		    	 }
		    	 fichier.close();
		}
		 else  // sinon
	     {
	    	 LERROR << "Erreur à l'ouverture !";
	     	 LERROR << "File " << fileName.str() << " not found!";
	     }
	}

/*
template<typename T>
PSet<T>::PSet(const string fileDirectory, const string pointsSetName) {
			 this->ps = new list<T*>();
			 ostringstream fileName;
			 fileName << fileDirectory << pointsSetName;
			 ifstream fichier(fileName.str().c_str(), ios::in);  //déclaration du flux et ouverture du fichier
			 if(fichier)  // si l'ouverture a réussi
		     {

			    	 string psName = "";
			    	 char schar[10000];
			    	 fichier.getline(schar,10000);
			    	 psName+=schar;
			    	 stringstream sstr;
			    	 sstr.clear();
			    	 sstr.str(psName);
			    	 string s;
			    	 int a;
			    	 if(sstr.str().size()>1)
			    	 {
			    		 sstr >> s;
			    		 this->name=s;
			    	 }
			    	 psName = "";
			    	 fichier.getline(schar,10000);
			    	 psName+=schar;
			    	 sstr.clear();
			    	 sstr.str(psName);
			    	 if(sstr.str().size()>1)
			    	 {
			    		 sstr >> a;
				    	 this->StackdimX = (int)a;
			    	 }
			    	 psName = "";
			    	 fichier.getline(schar,10000);
			    	 psName+=schar;
			    	 sstr.clear();
			    	 sstr.str(psName);
			    	 if(sstr.str().size()>1)
			    	 {
			    		 sstr >> a;
				    	 this->StackdimY = (int)a;
			    	 }
			    	 psName = "";
			    	 fichier.getline(schar,10000);
			    	 psName+=schar;
			    	 sstr.clear();
			    	 sstr.str(psName);
			    	 if(sstr.str().size()>1)
			    	 {
			    		 sstr >> a;
				    	 this->StackdimZ = (int)a;
			    	 }
			    	 LDEBUG << "New PointsSet " << pointsSetName;
			    	 LDEBUG << "Name = " << this->name;
			    	 LDEBUG << "dim = (" << this->StackdimX << "," << this->StackdimY << "," << this->StackdimZ << ")";

			    	string dataline="";
			    	while (fichier.good())
			    	{
			    	 dataline="";
			    	 fichier.getline(schar,10000);
			    	 dataline+=schar;
			    	 sstr.clear();
			    	 sstr.str(dataline);
			    	 if(sstr.str().size()>1)
			    	 {
			    		sstr >> a;
			    		if(!isnan(1.*a))
			    		{
			    			int scan = (int)(1.*a);
			    			sstr >> a;
			    			int color = (int)(1.*a);
			    			Point* part = new Point(fileDirectory,scan,color);
			    			this->ps->push_back(part);
				      }
			    	 }
			     }
				 fichier.close();
			}
			 else  // sinon
		     {
		    	 LERROR << "Erreur à l'ouverture !";
		     	 LERROR << "File " << fileName.str() << " not found!";
		     }
		}
*/

template<class T>
PSet<T>::PSet(const PSet<T>& pointsSet)
{
		this->name = pointsSet.getName();
		this->StackdimX = pointsSet.getStackdimX();
		this->StackdimY = pointsSet.getStackdimY();
		this->StackdimZ = pointsSet.getStackdimZ();
		this->ps = pointsSet.getP();
}

template<class T>
PSet<T>::PSet(const string name, const vector<T*>& partraw)
{
		this->name = name;
		this->StackdimX=0;
		this->StackdimY=0;
		this->StackdimZ=0;
		this->setDimensions();
		LINFO << "New PointsSet of " << partraw.size() << " points with image " << name << " (" << StackdimX << "," << StackdimY << "," << StackdimZ << ")";
		this->ps = new list<T*>();
		for(int i=0; i<partraw.size(); i++)
		{
			T* pt;
			pt = partraw[i];
			this->ps->push_back(pt);
		}
}

template<class T>
PSet<T>::~PSet()
{
	this->name="";
	this->StackdimX=0;
	this->StackdimY=0;
	this->StackdimZ=0;
	this->ps->clear();
    delete ps;
}


template<class T>
void PSet<T>::cleanAll()
{
		for (typename list<T*>::iterator pit=this->ps->end(); pit!=this->ps->begin(); --pit)
		{
				delete *pit;
                *pit=NULL;
		}
		
}

template<class T>
PSet<T> & PSet<T>::operator = (const PSet<T> &pointsSet)
	{
		this->name = pointsSet.getName();
		this->StackdimX = pointsSet.getStackdimX();
		this->StackdimY = pointsSet.getStackdimY();
		this->StackdimZ = pointsSet.getStackdimZ();
		this->ps = pointsSet.getP();
		return *this;
	}




//Getter and setter
template<class T>
string  PSet<T>::getName() const {
		return name;
}

template<class T>
void PSet<T>::setName(string name) {
		this->name = name;
	}

template<class T>
list<T*>*  PSet<T>::getP() const
{
		list<T*> *toto = new list<T*>();
		for (typename list<T*>::const_iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
		{
				toto->push_back(&**pit);
		}
		return toto;
}

template<class T>
void PSet<T>::setP(list<T*> *points) {
		this->ps = points;
	}

template<class T>
typename list<T*>::iterator PSet<T>::getIteratorBegin(){
		return this->ps->begin();
	}

template<class T>
typename list<T*>::iterator PSet<T>::getIteratorEnd(){
		return this->ps->end();
	}

template<class T>
typename list<T*>::iterator  PSet<T>::getIteratorBegin() const {
		return this->ps->begin();
	}

template<class T>
typename list<T*>::iterator PSet<T>::getIteratorEnd() const {
		return this->ps->end();
	}

template<class T>
void PSet<T>::addP(T* point) {
		this->ps->push_back(point);
	}

template<class T>
int PSet<T>::Size() const {
		return ps->size();
	}

template<class T>
void PSet<T>::removeP(T* part)
	{
        this->ps->remove(part);
	}

template<class T>
void PSet<T>::removeP(int i)
	{
		typename list<T*>::iterator pit=this->ps->begin();
		int j=0;
		while(j<i)
		{
			++pit;
		}
		this->ps->erase(pit);
	}

template<class T>
int PSet<T>::getPos(const T& part)
{
		 int i=0;
		 for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit, ++i)
		{
			if((*pit)->equalColor(part))
				return i;
		}
		return -1;
}

template<class T>
int PSet<T>::removeUnmatched()
{
		int i=0;
		for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end();)
		{
			if((*pit)->getGlobalColor()==0)
            {
               LDEBUG << "Remove part " << (*pit)->tostring();
               pit = this->ps->erase(pit);
			   i++;
            }	
            else
                pit++;
		}
		return i;
}



template <typename T>
bool PSet<T>::contains(const T& part)
{
	for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
	{
		if( *pit && (*pit)->equalColor(part))
			return true;
	}
	return false;
}

template<class T>
void PSet<T>::setDimensions()
{
		const char* img1 = this->name.c_str();
	 	string mode = "r";
	 	TIFF* tif = TIFFOpen(img1,mode.c_str());
	 	if (tif) {
	 		uint32 imageWidth, imageLength;
	 		TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &imageWidth);
	 		TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &imageLength);
	 		//TIFFGetField(tif, TIFFTAG_IMAGEDEPTH, &imageDepth);
	 		int dircount = 0;
	         do {
	             dircount++;
	         } while (TIFFReadDirectory(tif));
	         TIFFClose(tif);
	         this->StackdimX = (int)imageWidth;
	         this->StackdimY = (int)imageLength;
	         this->StackdimZ = (int)dircount;
	 	}
	 	else
	 	{
	 		this->StackdimX = 0;
	 		this->StackdimY = 0;
	 		this->StackdimZ = 0;
	 	}
}

template<class T>
void PSet<T>::setScan(int scan)
{
    for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
    {
        (*pit)->setScan(scan);
    }
}

template<class T>
int PSet<T>::getStackdimX() const {
	return this->StackdimX;
}

template<class T>
void PSet<T>::setStackdimX(int stackdimX) {
	StackdimX = stackdimX;
}

template<class T>
int PSet<T>::getStackdimY() const {
	return StackdimY;
}

template<class T>
void PSet<T>::setStackdimY(int stackdimY) {
	StackdimY = stackdimY;
}

template<class T>
int PSet<T>::getStackdimZ() const {
	return StackdimZ;
}

template<class T>
void PSet<T>::setStackdimZ(int stackdimZ) {
	StackdimZ = stackdimZ;
}

template<>
//template<class T>
void PSet<Particle>::initGlobalColor()
{
		for (list<Particle*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
		{
			//LDEBUG << "before GlobalColor init " << (*pit)->tostring();
            (*pit)->setGlobalColor((*pit)->getColor());
			//LDEBUG << "after GlobalColor init " << (*pit)->tostring();
		}
}

template<>
//template<class T>
void PSet<P>::initGlobalColor()
{
}

template<>
void PSet<Particle>::resetGlobalColor()
{
		for (list<Particle*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
        {
            (*pit)->setGlobalColor(0);
        }
}

template<>
void PSet<P>::resetGlobalColor()
{
}

template<>
int PSet<Particle>::fillGlobalColor(int currentMax)
{
    for (list<Particle*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
	{
		if((*pit)->getGlobalColor()==0)
        {
                currentMax++;
                (*pit)->setGlobalColor(currentMax);
		}
    }
    return currentMax;
}

template<>
int PSet<P>::fillGlobalColor(int currentMax)
{
    return currentMax;
}


template<class T>
void PSet<T>::initCorrel(int correl)
{
		for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
		{
			(*pit)->setCorrel(correl);
		}
}

template<class T>
void PSet<T>::initTotalCorrel(int correl)
{
		for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
		{
			(*pit)->setTotalCorrel(correl);
		}
}


//Extractor


template<class T>
T* PSet<T>::findP(int color)
{
		for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
		{
			if((*pit)->getColor()==color)
				return *pit;
		}
		T* emptyPart(0);
		return emptyPart;
}

template<class T>
T* PSet<T>::findP(int color) const
{
		for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
		{
			if((*pit)->getColor()==color)
				return *pit;
		}
		T* emptyPart(0);
		return emptyPart;
}

template<class T>
T* PSet<T>::getP(int i){
		int j=0;
	 	for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit, ++j)
	 	{
			if(i==j)
				return *pit;
		}
		T* emptyPart(0);
		return emptyPart;
}


template<typename T>
bool PSet<T>::hasGlobalColor(int color)
{
		for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
		{
			if((*pit)->getGlobalColor()==color)
				return true;
		}
		return false;
}

template<typename T>
int PSet<T>::maxGlobalColor() const
{
        int max = 0 ;
		for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
		{
			if((*pit)->getGlobalColor() > max)
				max = (*pit)->getGlobalColor();
		}
		return max;
}

template<typename T>
T* PSet<T>::findGlobalColorP(int color)
{
		for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
		{
			if((*pit)->getGlobalColor()==color)
				return *pit;
		}
		T* emptyPart(0);
		return emptyPart;
}

template<typename T>
T* PSet<T>::findGlobalColorP(int color) const
{
		for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
		{
			if((*pit)->getGlobalColor()==color)
				return *pit;
		}
		T* emptyPart(0);
		return emptyPart;
}

//Subsets from single parameter or distance
template<class T>
PSet<T> PSet<T>::subSet(const Coord& coord, double distX, double distY, double distZ) const
{
		PSet<T>* subset = new PSet<T>(this->name);
		int nbpart = 0;
		int visited = 0;
		for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
		{
			visited++;
			if(((*pit)->getCoord().distX(coord)<distX)&&((*pit)->getCoord().distY(coord)<distY)&&((*pit)->getCoord().distZ(coord)<distZ))
			{
				subset->addP(*pit);
				nbpart++;
			}
		}
		return *subset;
}
//Subsets from single parameter or distance
template<class T>
PSet<T> PSet<T>::subSet(const Coord& coord, double distXY, double distZ) const
{
		PSet<T>* subset = new PSet<T>(this->name);
		int nbpart = 0;
		int visited = 0;
		for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
		{
			visited++;
			if(((*pit)->getCoord().distXY(coord)<distXY)&&((*pit)->getCoord().distZ(coord)<distZ))
			{
				subset->addP(*pit);
				nbpart++;
			}
		}
		return *subset;
}

template<class T>
PSet<T> PSet<T>::subSet(const Coord& coord, double dist) const
{
		PSet<T>* subset = new PSet<T>(this->name);
		int nbpart = 0;
		int visited = 0;
		for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
		{
			visited++;
			if(((*pit)->getCoord().dist(coord)<dist))
			{
				subset->addP(*pit);
				nbpart++;
			}
		}
		return *subset;
}

template<class T>
PSet<T>  PSet<T>::subSet(const PSet<T>& pst) const
{
		PSet<T>* subset = new PSet<T>(this->name);
		int nbpart = 0;
		//int visited = 0;
		for(typename list<T*>::iterator pitF=pst.getIteratorBegin(); pitF!=pst.getIteratorEnd(); ++pitF)
		{
	            int i = (*pitF)->getColor();
				//cout << "Final points " << i << endl;
                if(this->findP(i))
                {
				    //cout << "Found" << endl;
                    subset->addP(this->findP(i));
                    //cout << "Added" << endl;
				}
                nbpart++;
		}
		//cout << "Final subset contains " << nbpart << " points" << endl;
		return *subset;
}

template<class T>
PSet<T> PSet<T>::subSetCorrel(double thr) const
{
		PSet<T>* subset = new PSet<T>(this->name);
		int nbpart = 0;
		int visited = 0;
		for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
		{
            //cout << "Point "  << (*pit)->getColor() << " Correl=" << (*pit)->getCorrel() << endl;
			visited++;
			if((*pit)->getCorrel()>thr)
			{
				subset->addP(*pit);
				nbpart++;
			}
		}
	return *subset;
}

template<class T>
PSet<T> PSet<T>::subSetTotalCorrel(double thr) const
{
		PSet<T>* subset = new PSet<T>(this->name);
		int nbpart = 0;
		int visited = 0;
		for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
		{
            //cout << "Point "  << (*pit)->getColor() << " Correl=" << (*pit)->getCorrel() << endl;
			visited++;
			if((*pit)->getTotalCorrel()>thr)
			{
				subset->addP(*pit);
				nbpart++;
			}
		}
	return *subset;
}

template<class T>
PSet<T> PSet<T>::subSetSpatialCoherency(double thr) const
{
		PSet<T>* subset = new PSet<T>(this->name);
		for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
		{
			if((*pit)->getSpatialCoherency()>thr)
			{
				subset->addP(*pit);
			}
		}
	return *subset;
}

template<typename T>
PSet<T> PSet<T>::subSetVol(double vMin) const
{
	PSet<T>* subset = new PSet<T>(*this);
	return *subset;
}

template<>
PSet<Particle> PSet<Particle>::subSetVol(double vMin) const
{
	PSet<Particle>* subset = new PSet<Particle>(this->name);
	//cout << "Extracting subset " << this->name << "subset" << endl;
	int nbpart = 0;
	int visited = 0;
	for (list<Particle*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
	{
			visited++;
			if(((*pit)->getVolume()>vMin))
			{
				subset->addP(*pit);
				nbpart++;
			}
	}
	LINFO << "The subset has been created and contains " << nbpart << " particles";
	return *subset;
}

template<class T>
T* PSet<T>::findClosest(const Coord& XYZ)
{
		if(this->Size()>0)
		{
			double bestDist=1000000000000;
			typename list<T*>::iterator best = this->ps->begin();
			for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
			{
				double dist = (*pit)->distance(XYZ);
				if(dist<bestDist)
				{
					best=pit;
					bestDist=dist;
				}
			}
			return *best;
		}
		else
		{

			T* emptyPart(0);
			return emptyPart;
		}
}

template<class T>
PSet<T> PSet<T>::closest(PSet<T> pfinal, double distXY, double distZ)
{
	PSet<T>* potentialCorrel = new PSet<T>();
	for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
	{
		PSet<T> subset =  pfinal.subSet((*pit)->getCoord(), distXY, distZ);
		T* potential = (*pit)->findClosest(subset);
		potentialCorrel->addP(potential);
	}
	return *potentialCorrel;
}

template<>
PSet<Particle> PSet<Particle>::findLost(PSet<Particle> pool)
{
	PSet<Particle> lost = PSet<Particle>();
	for (list<Particle*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
	{
		if(!pool.contains(**pit))
			lost.addP(*pit);
	}
	return lost;
}

/// TO BE THE REFERENCE

template<class T>
PSet<T> PSet<T>::findBestCorrel_DICGPU(PSet<T> &pfinal, Parameters &param) //The new one 2019/04/26
{
    Volume imRef = Volume(this->name, Coord(this->StackdimX, this->StackdimY, this->StackdimZ));
    Volume imFinal = Volume(pfinal.getName(), Coord(pfinal.getStackdimX(), pfinal.getStackdimY(), pfinal.getStackdimZ()));

    Coord offset = Coord(0,0,0);
 
    Correl3DGPU corr = Correl3DGPU(imRef.getName().c_str(), imFinal.getName().c_str());
    corr.setParam(param);
    corr.setOptimize(false);
    
    LINFO << "Correlation parameters:";
    LINFO << corr.optionsToString();
    
    corr.init();

    LINFO << "Setting Ref";
    corr.setFullRef(imRef.getName().c_str());
    LINFO << "Setting Target";
    corr.setFullTarget(imFinal.getName().c_str());
    clock_t startTime = clock();
    clock_t currentTime = clock();
    int globalTime = diffclock(currentTime,startTime);
    int deltaTime = diffclock(clock(),currentTime);
    if(param.rigidguiding)
    {
 	LINFO << "Start precorrelation for rigid estimation";
        typename list<T*>::iterator pit0=this->ps->begin(); 
        if(param.usetex)
          corr.setRef((*pit0)->getCoord().getZ(), (*pit0)->getCoord().getY(), (*pit0)->getCoord().getX());
        else
          corr.setImRef((int)(*pit0)->getCoord().getZ(), (int)(*pit0)->getCoord().getY(), (int)(*pit0)->getCoord().getX());
        if((*pit0)->getRoiX()>0)
            corr.setCropHalfPatternX((*pit0)->getRoiX());
        if((*pit0)->getRoiY()>0)
            corr.setCropHalfPatternY((*pit0)->getRoiY());
        if((*pit0)->getRoiZ()>0)
            corr.setCropHalfPatternZ((*pit0)->getRoiZ());

 	LINFO << "Using point 1 (" << (*pit0)->getCoord().getX() << "," << (*pit0)->getCoord().getY() << "," << (*pit0)->getCoord().getZ()  << ")";
        Coord optipos = Coord(0,0,0);
        double offsetcor = -1;
        for(int k=-param.initThrZfactor+1; k<param.initThrZfactor; k+=2){
            for(int j=-param.initThrYfactor+1; j<param.initThrYfactor; j+=2){
                for(int l=-param.initThrXfactor+1; l<param.initThrXfactor; l+=2){
                    Coord offsetactu = Coord(l*param.thrX,j*param.thrY,k*param.thrZ); 
                    LINFO << offsetactu.toString();
                    T guidingP = (*pit0)->findBestCorrel_DICGPU(corr,(*pit0)->getCoord().shift(offsetactu));
                    if(guidingP.getCorrel()>offsetcor)
                    {
                        offsetcor=guidingP.getCorrel();
                        optipos=guidingP.getCoord();
                    }
                }
            }
        }
       
        Coord offsetf = optipos.diff((*pit0)->getCoord());
        offset = offsetf.getInt();
		
        LINFO << "Applied offset will be (" << offset.getX() << "," << offset.getY() << "," << offset.getZ() << ")";
    }

    LINFO << "Ready to start correlations";
    list<T*>* pool = this->rankLocality((param.thrX+param.thrY+param.thrZ));
                
    int i=0;
    for (typename list<T*>::iterator pit= pool->begin(); pit!=pool->end(); ++pit, ++i)
    {
      if(i%100 == 0)
      {
  	  deltaTime = diffclock(clock(),currentTime);
	  currentTime = clock();
	  globalTime = diffclock(currentTime,startTime);
	  LINFO << "Proceeded 100 points in " << deltaTime << "ms ; " << i <<"/" << this->ps->size() <<" in " << globalTime/1000 << "s";
      }
    
      if(param.usetex)
          corr.setRef((*pit)->getCoord().getZ(), (*pit)->getCoord().getY(), (*pit)->getCoord().getX());
      else
          corr.setImRef((int)(*pit)->getCoord().getZ(), (int)(*pit)->getCoord().getY(), (int)(*pit)->getCoord().getX());
      
      if((*pit)->getRoiX()>0)
                corr.setCropHalfPatternX((*pit)->getRoiX());
      if((*pit)->getRoiY()>0)
                corr.setCropHalfPatternY((*pit)->getRoiY());
      if((*pit)->getRoiZ()>0)
                corr.setCropHalfPatternZ((*pit)->getRoiZ());
      T potential;
      if(i==0)
      {
          potential = (*pit)->findBestCorrel_DICGPU(corr, (*pit)->getCoord().shift(offset));
      }
      else
      {
          LDEBUG << "Point " << (*pit)->getColor() << " start position estimate";
          Coord target = this->estimatePositionGauss((*pit)->getCoord(), pfinal, param.correlThr, (param.correlDomainX+param.correlDomainY+param.correlDomainZ)/3+(param.thrX+param.thrY+param.thrZ)/3, param.correlDomainX+param.correlDomainY+param.correlDomainZ+param.thrX+param.thrY+param.thrZ , true);
              //Coord target = this->estimatePositionGauss((*pit)->getCoord(), pfinal, correlThr, 100, 300 , true);
          Coord targetInt = target.getInt();
                //LINFO << "Offset (" << targetInt.getX()-(int)(*pit)->getCoord().getX() << "," << targetInt.getY()-(int)(*pit)->getCoord().getY() << "," << targetInt.getZ()-(int)(*pit)->getCoord().getZ() << ")";   
          potential = (*pit)->findBestCorrel_DICGPU(corr, targetInt);
      }
      T* pitF = pfinal.findP((*pit)->getColor());
      (pitF)->PUpdate(potential);
    }

    pfinal.countValid(param.correlThr);
    corr.freeFullImg();
    corr.free();
    return pfinal;

}

//IDEM 

template<class T>
PSet<T> PSet<T>::optimizeCorrel_DICGPU(PSet<T> &pfinal, Parameters &param) //The new one 2019/04/26
{
    Volume imRef = Volume(this->name, Coord(this->StackdimX, this->StackdimY, this->StackdimZ));
    Volume imFinal = Volume(pfinal.getName(), Coord(pfinal.getStackdimX(), pfinal.getStackdimY(), pfinal.getStackdimZ()));

    //Coord offset = Coord(0,0,0);
 
    Correl3DGPU corr = Correl3DGPU(imRef.getName().c_str(), imFinal.getName().c_str());
    corr.setParam(param);
    corr.setOptimize(true);
    
    LINFO << "Correlation parameters:";
    LINFO << corr.optionsToString();

    corr.init();

    LINFO << "Setting Ref";
    corr.setFullRef(imRef.getName().c_str());
    LINFO << "Setting Target";
    corr.setFullTarget(imFinal.getName().c_str());
    clock_t startTime = clock();
    clock_t currentTime = clock();
    int globalTime = diffclock(currentTime,startTime);
    int deltaTime = diffclock(clock(),currentTime);
		
    LINFO << "Estimate local coherency with distance " << param.regulThrDist ;
    
    this->estimateLocalQuadraticCoherency(pfinal, 1.*param.regulThrDist);
    pfinal.scaleCorrelFactorwithSpatialCoherency();

    int i=0;
    LINFO << "Ready to start correlations";
    typename list<T*>::iterator pitF=(pfinal).getIteratorBegin();
    for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit, ++i)
    {
        if(param.usetex)
          corr.setRef((*pit)->getCoord().getZ(), (*pit)->getCoord().getY(), (*pit)->getCoord().getX());
        else
          corr.setImRef((int)(*pit)->getCoord().getZ(), (int)(*pit)->getCoord().getY(), (int)(*pit)->getCoord().getX());
        PSet<T> pthr=this->subSetCorrel(param.correlThr);
        Coord target = pthr.estimatePositionfromQuadraticFit((*pit)->getCoord(), pfinal, 1.*param.regulThrDist);
        Coord targetInt = target.getInt();
        T potential = (*pit)->findBestCorrel_DICGPU(corr, targetInt);
        (*pitF)->PUpdate(potential);
        ++pitF;
    }
    this->estimateLocalQuadraticCoherency(pfinal, 1.*param.regulThrDist);
    pfinal.scaleCorrelFactorwithSpatialCoherency();
    corr.freeFullImg();
    corr.free();
    return pfinal;
}


// TO BE REMOVED


//Subsets from correlation or multicriteria
template<class T>
PSet<T> PSet<T>::findBestCorrel_DICGPU(PSet<T> &pfinal, int correlDomain, double distXY, double distZ, double correlThr)
{
		Volume imRef = Volume(this->name, Coord(this->StackdimX, this->StackdimY, this->StackdimZ));
		Volume imFinal = Volume(pfinal.getName(), Coord(pfinal.getStackdimX(), pfinal.getStackdimY(), pfinal.getStackdimZ()));
		LINFO << "Initialize GPU";
		Correl3DGPU corr = Correl3DGPU(imRef.getName().c_str(), imFinal.getName().c_str());
		corr.setfromFull(true);
		corr.setFilter(false);
		corr.setNormalize(true);
		corr.setIntersect(false);
		corr.setReducedWindow(false);
		corr.setdisplThr(false, distZ, distXY, distXY);
		corr.setDomain(correlDomain);
        corr.setWindow("none");
        //corr.setWindow("hanning");

		corr.setFFT(false);
        corr.setHalfPattern(correlDomain);
        corr.setDisplacement(distXY);
        corr.setCorrelThr(correlThr);

        corr.init();

		LINFO << "Correlation parameters:";
        LINFO << corr.optionsToString();
        LINFO << "Setting Ref";
		corr.setFullRef(imRef.getName().c_str());
		LINFO << "Setting Target";
		corr.setFullTarget(imFinal.getName().c_str());
		clock_t startTime = clock();
		clock_t currentTime = clock();
		int globalTime = diffclock(currentTime,startTime);
		int deltaTime = diffclock(clock(),currentTime);
		int i=0;
		LINFO << "Ready to start correlations";
		typename list<T*>::iterator pitF=(pfinal).getP()->begin();
        for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit, ++i)
		{
          	if(i%100 == 0)
			{
				deltaTime = diffclock(clock(),currentTime);
				currentTime = clock();
				globalTime = diffclock(currentTime,startTime);
				LINFO << "Proceeded 100 points in " << deltaTime << "ms ; " << i <<"/" << this->ps->size() <<" in " << globalTime/1000 << "s";
			}
            corr.setImRef((int)(*pit)->getCoord().getZ(), (int)(*pit)->getCoord().getY(), (int)(*pit)->getCoord().getX());
            T potential = (*pit)->findBestCorrel_DICGPU(corr);
			(*pitF)->PUpdate(potential);
	        ++pitF;
	    }
		pfinal.countValid(correlThr);
        corr.freeFullImg();
		corr.free();
		return pfinal;
	}

template<class T>
PSet<T> PSet<T>::findBestCorrel_DICGPU_aniso(PSet<T> &pfinal, int correlDomainX, int correlDomainY, int correlDomainZ, double distX, double distY, double distZ, double correlThr)
	{
		Volume imRef = Volume(this->name, Coord(this->StackdimX, this->StackdimY, this->StackdimZ));
		Volume imFinal = Volume(pfinal.getName(), Coord(pfinal.getStackdimX(), pfinal.getStackdimY(), pfinal.getStackdimZ()));
		LINFO << "Initialize GPU";
		Correl3DGPU corr = Correl3DGPU(imRef.getName().c_str(), imFinal.getName().c_str());
		corr.setfromFull(true);
		corr.setFilter(false);
		corr.setNormalize(true);
		corr.setIntersect(false);
		corr.setReducedWindow(false);
		corr.setdisplThr(false, distZ, distX, distY);
		corr.setDomain(correlDomainX);
        corr.setWindow("none");
        //corr.setWindow("hanning");

		corr.setFFT(false);
        corr.setaniso(true);
        corr.setHalfPatternX(correlDomainX);
        corr.setHalfPatternY(correlDomainY);
        corr.setHalfPatternZ(correlDomainZ);
        corr.setDisplacementX(distX);
        corr.setDisplacementY(distY);
        corr.setDisplacementZ(distZ);
        corr.setCorrelThr(correlThr);

        corr.init();

		LINFO << "Correlation parameters:";
        LINFO << corr.optionsToString();
        LINFO << "Setting Ref";
		corr.setFullRef(imRef.getName().c_str());
		LINFO << "Setting Target";
		corr.setFullTarget(imFinal.getName().c_str());
		clock_t startTime = clock();
		clock_t currentTime = clock();
		int globalTime = diffclock(currentTime,startTime);
		int deltaTime = diffclock(clock(),currentTime);
		int i=0;
		LINFO << "Ready to start correlations";
		typename list<T*>::iterator pitF=(pfinal).getP()->begin();
        for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit, ++i)
		{
          	if(i%100 == 0)
			{
				deltaTime = diffclock(clock(),currentTime);
				currentTime = clock();
				globalTime = diffclock(currentTime,startTime);
				LINFO << "Proceeded 100 points in " << deltaTime << "ms ; " << i <<"/" << this->ps->size() <<" in " << globalTime/1000 << "s";
			}
            corr.setImRef((int)(*pit)->getCoord().getZ(), (int)(*pit)->getCoord().getY(), (int)(*pit)->getCoord().getX());
            T potential = (*pit)->findBestCorrel_DICGPU(corr);
			(*pitF)->PUpdate(potential);
	        ++pitF;
	    }
		pfinal.countValid(correlThr);
        corr.freeFullImg();
		corr.free();
		return pfinal;
	}

template<class T>
PSet<T> PSet<T>::findBestCorrel_DICGPU_aniso_guided(PSet<T> &pfinal, int correlDomainX, int correlDomainY, int correlDomainZ, double distX, double distY, double distZ, double correlThr, int initThrXfactor, int initThrYfactor, int initThrZfactor)
	{
        Volume imRef = Volume(this->name, Coord(this->StackdimX, this->StackdimY, this->StackdimZ));
		Volume imFinal = Volume(pfinal.getName(), Coord(pfinal.getStackdimX(), pfinal.getStackdimY(), pfinal.getStackdimZ()));
	    Correl3DGPU corr2 = Correl3DGPU(imRef.getName().c_str(), imFinal.getName().c_str());
		corr2.setfromFull(true);
		corr2.setFilter(false);
		corr2.setNormalize(true);
		corr2.setIntersect(false);
		corr2.setReducedWindow(false);
		corr2.setdisplThr(false, distZ, distX, distY);
		corr2.setDomain(correlDomainX);
        corr2.setWindow("none");
        //corr.setWindow("hanning");

		corr2.setFFT(false);
        corr2.setaniso(true);
        corr2.setHalfPatternX(correlDomainX);
        corr2.setHalfPatternY(correlDomainY);
        corr2.setHalfPatternZ(correlDomainZ);
        corr2.setDisplacementX(distX);
        corr2.setDisplacementY(distY);
        corr2.setDisplacementZ(distZ);
        corr2.setCorrelThr(correlThr);

        corr2.init();

		LINFO << "Correlation parameters:";
        LINFO << corr2.optionsToString();
        LINFO << "Setting Ref";
		corr2.setFullRef(imRef.getName().c_str());
		LINFO << "Setting Target";
		corr2.setFullTarget(imFinal.getName().c_str());
		clock_t startTime = clock();
		clock_t currentTime = clock();
		int globalTime = diffclock(currentTime,startTime);
		int deltaTime = diffclock(clock(),currentTime);
		int i=0;
		LINFO << "Start precorrelation for rigid estimation";
        typename list<T*>::iterator pit0=this->ps->begin(); 
        corr2.setImRef((int)(*pit0)->getCoord().getZ(), (int)(*pit0)->getCoord().getY(), (int)(*pit0)->getCoord().getX());
        if((*pit0)->getRoiX()>0)
            corr2.setCropHalfPatternX((*pit0)->getRoiX());
        if((*pit0)->getRoiY()>0)
            corr2.setCropHalfPatternY((*pit0)->getRoiY());
        if((*pit0)->getRoiZ()>0)
            corr2.setCropHalfPatternZ((*pit0)->getRoiZ());

        Coord optipos = Coord(0,0,0);
        double offsetcor = -1;
        for(int k=-initThrZfactor+1; k<initThrZfactor; k+=2){
            for(int j=-initThrYfactor+1; j<initThrYfactor; j+=2){
                for(int l=-initThrXfactor+1; l<initThrXfactor; l+=2){
                    Coord offsetactu = Coord(l*distX,j*distY,k*distZ); 
                    LINFO << offsetactu.toString();
                    T guidingP = (*pit0)->findBestCorrel_DICGPU(corr2,(*pit0)->getCoord().shift(offsetactu));
                    if(guidingP.getCorrel()>offsetcor)
                    {
                        offsetcor=guidingP.getCorrel();
                        optipos=guidingP.getCoord();
                    }
                }
            }
        }
       
        Coord offsetf = optipos.diff((*pit0)->getCoord());
        Coord offset = offsetf.getInt();
		
        LINFO << "Applied offset will be (" << offset.getX() << "," << offset.getY() << "," << offset.getZ() << ")";
 
		LINFO << "Ready to start correlations";
		//typename list<T*>::iterator pitF=(pfinal).getP()->begin();
        list<T*>* pool = this->rankLocality((distX+distY+distZ));
                
        for (typename list<T*>::iterator pit= pool->begin(); pit!=pool->end(); ++pit, ++i)
		{
          	if(i%100 == 0)
			{
				deltaTime = diffclock(clock(),currentTime);
				currentTime = clock();
				globalTime = diffclock(currentTime,startTime);
				LINFO << "Proceeded 100 points in " << deltaTime << "ms ; " << i <<"/" << this->ps->size() <<" in " << globalTime/1000 << "s";
			}
            corr2.setImRef((int)(*pit)->getCoord().getZ(), (int)(*pit)->getCoord().getY(), (int)(*pit)->getCoord().getX());
            if((*pit)->getRoiX()>0)
                corr2.setCropHalfPatternX((*pit)->getRoiX());
            if((*pit)->getRoiY()>0)
                corr2.setCropHalfPatternY((*pit)->getRoiY());
            if((*pit)->getRoiZ()>0)
                corr2.setCropHalfPatternZ((*pit)->getRoiZ());
            T potential;
            if(i==0)
            {
                potential = (*pit)->findBestCorrel_DICGPU(corr2, (*pit)->getCoord().shift(offset));
            }
            else
            {
                LDEBUG << "Point " << (*pit)->getColor() << " start position estimate";
                Coord target = this->estimatePositionGauss((*pit)->getCoord(), pfinal, correlThr, (correlDomainX+correlDomainY+correlDomainZ)/3+(distX+distY+distZ)/3, correlDomainX+correlDomainY+correlDomainZ+distX+distY+distZ , true);
                //Coord target = this->estimatePositionGauss((*pit)->getCoord(), pfinal, correlThr, 100, 300 , true);
                Coord targetInt = target.getInt();
                //LINFO << "Offset (" << targetInt.getX()-(int)(*pit)->getCoord().getX() << "," << targetInt.getY()-(int)(*pit)->getCoord().getY() << "," << targetInt.getZ()-(int)(*pit)->getCoord().getZ() << ")";   
                potential = (*pit)->findBestCorrel_DICGPU(corr2, targetInt);
			}
            T* pitF = pfinal.findP((*pit)->getColor());
            (pitF)->PUpdate(potential);
	    }
		pfinal.countValid(correlThr);
        corr2.freeFullImg();
		corr2.free();
	    return pfinal;
	}


template<class T>
PSet<T> PSet<T>::findBestCorrel_DICGPU_aniso_guided(PSet<T> &pfinal, int correlDomainX, int correlDomainY, int correlDomainZ, double distX, double distY, double distZ, double correlThr)
	{
		Volume imRef = Volume(this->name, Coord(this->StackdimX, this->StackdimY, this->StackdimZ));
		Volume imFinal = Volume(pfinal.getName(), Coord(pfinal.getStackdimX(), pfinal.getStackdimY(), pfinal.getStackdimZ()));
	    Correl3DGPU corr2 = Correl3DGPU(imRef.getName().c_str(), imFinal.getName().c_str());
		corr2.setfromFull(true);
		corr2.setFilter(false);
		corr2.setNormalize(true);
		corr2.setIntersect(false);
		corr2.setReducedWindow(false);
		corr2.setdisplThr(false, distZ, distX, distY);
		corr2.setDomain(correlDomainX);
        corr2.setWindow("none");
        //corr.setWindow("hanning");

		corr2.setFFT(false);
        corr2.setaniso(true);
        corr2.setHalfPatternX(correlDomainX);
        corr2.setHalfPatternY(correlDomainY);
        corr2.setHalfPatternZ(correlDomainZ);
        corr2.setDisplacementX(distX);
        corr2.setDisplacementY(distY);
        corr2.setDisplacementZ(distZ);
        corr2.setCorrelThr(correlThr);

        corr2.init();

		LINFO << "Correlation parameters:";
        LINFO << corr2.optionsToString();
        LINFO << "Setting Ref";
		corr2.setFullRef(imRef.getName().c_str());
		LINFO << "Setting Target";
		corr2.setFullTarget(imFinal.getName().c_str());
		clock_t startTime = clock();
		clock_t currentTime = clock();
		int globalTime = diffclock(currentTime,startTime);
		int deltaTime = diffclock(clock(),currentTime);
		int i=0;
		LINFO << "Start precorrelation for rigid estimation";
        typename list<T*>::iterator pit0=this->ps->begin(); 
        corr2.setImRef((int)(*pit0)->getCoord().getZ(), (int)(*pit0)->getCoord().getY(), (int)(*pit0)->getCoord().getX());
        if((*pit0)->getRoiX()>0)
            corr2.setCropHalfPatternX((*pit0)->getRoiX());
        if((*pit0)->getRoiY()>0)
            corr2.setCropHalfPatternY((*pit0)->getRoiY());
        if((*pit0)->getRoiZ()>0)
            corr2.setCropHalfPatternZ((*pit0)->getRoiZ());

        Coord optipos = Coord(0,0,0);
        double offsetcor = -1;
        for(int k=-2; k<3; k++){
            for(int j=-2; j<3; j++){
                for(int l=-2; l<3; l++){
                    Coord offsetactu = Coord(l*2*distX,j*2*distY,k*2*distZ); 
                    LINFO << offsetactu.toString();
                    T guidingP = (*pit0)->findBestCorrel_DICGPU(corr2,(*pit0)->getCoord().shift(offsetactu));
                    if(guidingP.getCorrel()>offsetcor)
                    {
                        offsetcor=guidingP.getCorrel();
                        optipos=guidingP.getCoord();
                    }
                }
            }
        }
       
        Coord offsetf = optipos.diff((*pit0)->getCoord());
        Coord offset = offsetf.getInt();
		
        LINFO << "Applied offset will be (" << offset.getX() << "," << offset.getY() << "," << offset.getZ() << ")";
 
		LINFO << "Ready to start correlations";
		//typename list<T*>::iterator pitF=(pfinal).getP()->begin();
        list<T*>* pool = this->rankLocality((distX+distY+distZ));
                
        for (typename list<T*>::iterator pit= pool->begin(); pit!=pool->end(); ++pit, ++i)
		{
          	if(i%100 == 0)
			{
				deltaTime = diffclock(clock(),currentTime);
				currentTime = clock();
				globalTime = diffclock(currentTime,startTime);
				LINFO << "Proceeded 100 points in " << deltaTime << "ms ; " << i <<"/" << this->ps->size() <<" in " << globalTime/1000 << "s";
			}
            corr2.setImRef((int)(*pit)->getCoord().getZ(), (int)(*pit)->getCoord().getY(), (int)(*pit)->getCoord().getX());
            if((*pit)->getRoiX()>0)
                corr2.setCropHalfPatternX((*pit)->getRoiX());
            if((*pit)->getRoiY()>0)
                corr2.setCropHalfPatternY((*pit)->getRoiY());
            if((*pit)->getRoiZ()>0)
                corr2.setCropHalfPatternZ((*pit)->getRoiZ());
            T potential;
            if(i==0)
            {
                potential = (*pit)->findBestCorrel_DICGPU(corr2, (*pit)->getCoord().shift(offset));
            }
            else
            {
                LDEBUG << "Point " << (*pit)->getColor() << " start position estimate";
                Coord target = this->estimatePositionGauss((*pit)->getCoord(), pfinal, correlThr, (correlDomainX+correlDomainY+correlDomainZ)/3+(distX+distY+distZ)/3, correlDomainX+correlDomainY+correlDomainZ+distX+distY+distZ , true);
                //Coord target = this->estimatePositionGauss((*pit)->getCoord(), pfinal, correlThr, 100, 300 , true);
                Coord targetInt = target.getInt();
                //LINFO << "Offset (" << targetInt.getX()-(int)(*pit)->getCoord().getX() << "," << targetInt.getY()-(int)(*pit)->getCoord().getY() << "," << targetInt.getZ()-(int)(*pit)->getCoord().getZ() << ")";   
                potential = (*pit)->findBestCorrel_DICGPU(corr2, targetInt);
			}
            T* pitF = pfinal.findP((*pit)->getColor());
            (pitF)->PUpdate(potential);
	    }
		pfinal.countValid(correlThr);
        corr2.freeFullImg();
		corr2.free();
	    return pfinal;
	}

template<class T>
PSet<T> PSet<T>::findBestCorrel_DICGPU_aniso_guided_bak(PSet<T> &pfinal, int correlDomainX, int correlDomainY, int correlDomainZ, double distX, double distY, double distZ, double correlThr)
	{
		Volume imRef = Volume(this->name, Coord(this->StackdimX, this->StackdimY, this->StackdimZ));
		Volume imFinal = Volume(pfinal.getName(), Coord(pfinal.getStackdimX(), pfinal.getStackdimY(), pfinal.getStackdimZ()));
		/*
 * LINFO << "Initialize GPU";
		Correl3DGPU corr = Correl3DGPU(imRef.getName().c_str(), imFinal.getName().c_str());
		corr.setfromFull(true);
		corr.setFilter(false);
		corr.setNormalize(true);
		corr.setIntersect(false);
		corr.setReducedWindow(false);
		corr.setdisplThr(false, distZ, distX, distY);
		corr.setDomain(correlDomainX);
        corr.setWindow("none");
        //corr.setWindow("hanning");

		corr.setFFT(false);
        corr.setaniso(true);
        corr.setHalfPatternX(correlDomainX);
        corr.setHalfPatternY(correlDomainY);
        corr.setHalfPatternZ(correlDomainZ);
        corr.setDisplacementX(distX*5);
        corr.setDisplacementY(distY*5);
        corr.setDisplacementZ(distZ*5);
        corr.setCorrelThr(correlThr);

        corr.init();

		corr.setFullRef(imRef.getName().c_str());
		corr.setFullTarget(imFinal.getName().c_str());
		LINFO << "Start precorrelation for rigid estimation";
		
        typename list<T*>::iterator pit0=this->ps->begin(); 
        corr.setImRef((int)(*pit0)->getCoord().getZ(), (int)(*pit0)->getCoord().getY(), (int)(*pit0)->getCoord().getX());
        T guidingP = (*pit0)->findBestCorrel_DICGPU(corr);
        Coord offsetf = guidingP.getCoord().diff((*pit0)->getCoord());
        Coord offset = offsetf.getInt();
        corr.freeFullImg();
		corr.free();
		
        LINFO << "Applied offset will be (" << offset.getX() << "," << offset.getY() << "," << offset.getZ() << ")";
*/
        Correl3DGPU corr2 = Correl3DGPU(imRef.getName().c_str(), imFinal.getName().c_str());
		corr2.setfromFull(true);
		corr2.setFilter(false);
		corr2.setNormalize(true);
		corr2.setIntersect(false);
		corr2.setReducedWindow(false);
		corr2.setdisplThr(false, distZ, distX, distY);
		corr2.setDomain(correlDomainX);
        corr2.setWindow("none");
        //corr.setWindow("hanning");

		corr2.setFFT(false);
        corr2.setaniso(true);
        corr2.setHalfPatternX(correlDomainX);
        corr2.setHalfPatternY(correlDomainY);
        corr2.setHalfPatternZ(correlDomainZ);
        corr2.setDisplacementX(distX);
        corr2.setDisplacementY(distY);
        corr2.setDisplacementZ(distZ);
        corr2.setCorrelThr(correlThr);

        corr2.init();

		LINFO << "Correlation parameters:";
        LINFO << corr2.optionsToString();
        LINFO << "Setting Ref";
		corr2.setFullRef(imRef.getName().c_str());
		LINFO << "Setting Target";
		corr2.setFullTarget(imFinal.getName().c_str());
		clock_t startTime = clock();
		clock_t currentTime = clock();
		int globalTime = diffclock(currentTime,startTime);
		int deltaTime = diffclock(clock(),currentTime);
		int i=0;
		LINFO << "Start precorrelation for rigid estimation";
        typename list<T*>::iterator pit0=this->ps->begin(); 
        corr2.setImRef((int)(*pit0)->getCoord().getZ(), (int)(*pit0)->getCoord().getY(), (int)(*pit0)->getCoord().getX());
        if((*pit0)->getRoiX()>0)
            corr2.setCropHalfPatternX((*pit0)->getRoiX());
        if((*pit0)->getRoiY()>0)
            corr2.setCropHalfPatternY((*pit0)->getRoiY());
        if((*pit0)->getRoiZ()>0)
            corr2.setCropHalfPatternZ((*pit0)->getRoiZ());

        Coord optipos = Coord(0,0,0);
        double offsetcor = -1;
        for(int k=-2; k<3; k++){
            for(int j=-2; j<3; j++){
                for(int l=-2; l<3; l++){
                    Coord offsetactu = Coord(l*2*distX,j*2*distY,k*2*distZ); 
                    LINFO << offsetactu.toString();
                    T guidingP = (*pit0)->findBestCorrel_DICGPU(corr2,(*pit0)->getCoord().shift(offsetactu));
                    if(guidingP.getCorrel()>offsetcor)
                    {
                        offsetcor=guidingP.getCorrel();
                        optipos=guidingP.getCoord();
                    }
                }
            }
        }
       
        Coord offsetf = optipos.diff((*pit0)->getCoord());
        Coord offset = offsetf.getInt();
		
        LINFO << "Applied offset will be (" << offset.getX() << "," << offset.getY() << "," << offset.getZ() << ")";
 
		LINFO << "Ready to start correlations";
		typename list<T*>::iterator pitF=(pfinal).getP()->begin();
        for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit, ++i)
		{
          	if(i%100 == 0)
			{
				deltaTime = diffclock(clock(),currentTime);
				currentTime = clock();
				globalTime = diffclock(currentTime,startTime);
				LINFO << "Proceeded 100 points in " << deltaTime << "ms ; " << i <<"/" << this->ps->size() <<" in " << globalTime/1000 << "s";
			}
            corr2.setImRef((int)(*pit)->getCoord().getZ(), (int)(*pit)->getCoord().getY(), (int)(*pit)->getCoord().getX());
            if((*pit)->getRoiX()>0)
                corr2.setCropHalfPatternX((*pit)->getRoiX());
            if((*pit)->getRoiY()>0)
                corr2.setCropHalfPatternY((*pit)->getRoiY());
            if((*pit)->getRoiZ()>0)
                corr2.setCropHalfPatternZ((*pit)->getRoiZ());
            T potential;
            if(i==0)
            {
                potential = (*pit)->findBestCorrel_DICGPU(corr2, (*pit)->getCoord().shift(offset));
            }
            else
            {
                Coord target = this->estimatePositionGauss((*pit)->getCoord(), pfinal, correlThr, (distX+distY+distZ)/3, distX+distY+distZ , true);
                //Coord target = this->estimatePositionGauss((*pit)->getCoord(), pfinal, correlThr, 100, 300 , true);
                Coord targetInt = target.getInt();
                //LINFO << "Offset (" << targetInt.getX()-(int)(*pit)->getCoord().getX() << "," << targetInt.getY()-(int)(*pit)->getCoord().getY() << "," << targetInt.getZ()-(int)(*pit)->getCoord().getZ() << ")";   
                potential = (*pit)->findBestCorrel_DICGPU(corr2, targetInt);
			}
            (*pitF)->PUpdate(potential);
	        ++pitF;
	    }
		pfinal.countValid(correlThr);
        corr2.freeFullImg();
		corr2.free();
	    return pfinal;
	}

/*
template<class T>
PSet<T> PSet<T>::findBestCorrel_DICGPU_texture(PSet<T> &pfinal, int correlDomainX, int correlDomainY, int correlDomainZ, double distX, double distY, double distZ, double correlThr)
	{
		Volume imRef = Volume(this->name, Coord(this->StackdimX, this->StackdimY, this->StackdimZ));
		Volume imFinal = Volume(pfinal.getName(), Coord(pfinal.getStackdimX(), pfinal.getStackdimY(), pfinal.getStackdimZ()));
		LINFO << "Initialize GPU";
		Correl3DGPU corr = Correl3DGPU(imRef.getName().c_str(), imFinal.getName().c_str());
		corr.setfromFull(true);
		corr.setusetex(true);
		corr.setFilter(false);
		corr.setNormalize(true);
		corr.setIntersect(false);
		corr.setReducedWindow(false);
		corr.setdisplThr(false, distZ, distX, distY);
		corr.setDomain(correlDomainX);
        corr.setWindow("none");
        //corr.setWindow("hanning");

		corr.setFFT(false);
        corr.setaniso(true);
        corr.setHalfPatternX(correlDomainX);
        corr.setHalfPatternY(correlDomainY);
        corr.setHalfPatternZ(correlDomainZ);
        corr.setDisplacementX(distX);
        corr.setDisplacementY(distY);
        corr.setDisplacementZ(distZ);
        corr.setCorrelThr(correlThr);

        corr.init();

		LINFO << "Correlation parameters:";
        LINFO << corr.optionsToString();
        LINFO << "Setting Ref";
		corr.setFullRef(imRef.getName().c_str());
		LINFO << "Setting Target";
		corr.setFullTarget(imFinal.getName().c_str());
		clock_t startTime = clock();
		clock_t currentTime = clock();
		int globalTime = diffclock(currentTime,startTime);
		int deltaTime = diffclock(clock(),currentTime);
		int i=0;
		LINFO << "Ready to start correlations";
		typename list<T*>::iterator pitF=(pfinal).getP()->begin();
        for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit, ++i)
		{
          	if(i%100 == 0)
			{
				deltaTime = diffclock(clock(),currentTime);
				currentTime = clock();
				globalTime = diffclock(currentTime,startTime);
				LINFO << "Proceeded 100 points in " << deltaTime << "ms ; " << i <<"/" << this->ps->size() <<" in " << globalTime/1000 << "s";
			}
            corr.setRef((*pit)->getCoord().getZ(), (*pit)->getCoord().getY(), (*pit)->getCoord().getX());
            T potential = (*pit)->findBestCorrel_DICGPU(corr);
			(*pitF)->PUpdate(potential);
	        ++pitF;
	    }
		pfinal.countValid(correlThr);
        corr.freeFullImg();
		corr.free();
		return pfinal;
	}
*/

/*
template<class T>
PSet<T> PSet<T>::findBestCorrel_DICGPU_texture_guided(PSet<T> &pfinal, int correlDomainX, int correlDomainY, int correlDomainZ, double distX, double distY, double distZ, double correlThr, int initThrXfactor, int initThrYfactor, int initThrZfactor)
	{
        	Volume imRef = Volume(this->name, Coord(this->StackdimX, this->StackdimY, this->StackdimZ));
		Volume imFinal = Volume(pfinal.getName(), Coord(pfinal.getStackdimX(), pfinal.getStackdimY(), pfinal.getStackdimZ()));
		LINFO << "Initialize GPU";
	    	Correl3DGPU corr2 = Correl3DGPU(imRef.getName().c_str(), imFinal.getName().c_str());
		corr2.setusetex(true);
		corr2.setfromFull(true);
		corr2.setFilter(false);
		corr2.setNormalize(true);
		corr2.setIntersect(false);
		corr2.setReducedWindow(false);
		corr2.setdisplThr(false, distZ, distX, distY);
		corr2.setDomain(correlDomainX);
        	corr2.setWindow("none");
        	//corr.setWindow("hanning");

		corr2.setFFT(false);
        	corr2.setaniso(true);
        	corr2.setHalfPatternX(correlDomainX);
        	corr2.setHalfPatternY(correlDomainY);
        	corr2.setHalfPatternZ(correlDomainZ);
        	corr2.setDisplacementX(distX);
        	corr2.setDisplacementY(distY);
        	corr2.setDisplacementZ(distZ);
        	corr2.setCorrelThr(correlThr);

        	corr2.init();

		LINFO << "Correlation parameters:";
        	LINFO << corr2.optionsToString();
        	LINFO << "Setting Ref";
		corr2.setFullRef(imRef.getName().c_str());
		LINFO << "Setting Target";
		corr2.setFullTarget(imFinal.getName().c_str());
		clock_t startTime = clock();
		clock_t currentTime = clock();
		int globalTime = diffclock(currentTime,startTime);
		int deltaTime = diffclock(clock(),currentTime);
		int i=0;
		LINFO << "Start precorrelation for rigid estimation";
        	typename list<T*>::iterator pit0=this->ps->begin(); 
                corr2.setRef((*pit0)->getCoord().getZ(), (*pit0)->getCoord().getY(), (*pit0)->getCoord().getX());
          	if((*pit0)->getRoiX()>0)
            		corr2.setCropHalfPatternX((*pit0)->getRoiX());
        	if((*pit0)->getRoiY()>0)
            		corr2.setCropHalfPatternY((*pit0)->getRoiY());
        	if((*pit0)->getRoiZ()>0)
            		corr2.setCropHalfPatternZ((*pit0)->getRoiZ());

        Coord optipos = Coord(0,0,0);
        double offsetcor = -1;
        for(int k=-initThrZfactor+1; k<initThrZfactor; k+=2){
            for(int j=-initThrYfactor+1; j<initThrYfactor; j+=2){
                for(int l=-initThrXfactor+1; l<initThrXfactor; l+=2){
                    Coord offsetactu = Coord(l*distX,j*distY,k*distZ); 
                    LINFO << offsetactu.toString();
     		    T guidingP = (*pit0)->findBestCorrel_DICGPU(corr2,(*pit0)->getCoord().shift(offsetactu));
                    if(guidingP.getCorrel()>offsetcor)
                    {
                        offsetcor=guidingP.getCorrel();
                        optipos=guidingP.getCoord();
                    }
                }
            }
        }
       
        Coord offsetf = optipos.diff((*pit0)->getCoord());
        Coord offset = offsetf.getInt();
		
        LINFO << "Applied offset will be (" << offset.getX() << "," << offset.getY() << "," << offset.getZ() << ")";
 
		LINFO << "Ready to start correlations";
		//typename list<T*>::iterator pitF=(pfinal).getP()->begin();
        list<T*>* pool = this->rankLocality((distX+distY+distZ));
                
        for (typename list<T*>::iterator pit= pool->begin(); pit!=pool->end(); ++pit, ++i)
		{
          	if(i%100 == 0)
			{
				deltaTime = diffclock(clock(),currentTime);
				currentTime = clock();
				globalTime = diffclock(currentTime,startTime);
				LINFO << "Proceeded 100 points in " << deltaTime << "ms ; " << i <<"/" << this->ps->size() <<" in " << globalTime/1000 << "s";
			}
            corr2.setRef((*pit)->getCoord().getZ(), (*pit)->getCoord().getY(), (*pit)->getCoord().getX());
            if((*pit)->getRoiX()>0)
                corr2.setCropHalfPatternX((*pit)->getRoiX());
            if((*pit)->getRoiY()>0)
                corr2.setCropHalfPatternY((*pit)->getRoiY());
            if((*pit)->getRoiZ()>0)
                corr2.setCropHalfPatternZ((*pit)->getRoiZ());
            T potential;
            if(i==0)
            {
                potential = (*pit)->findBestCorrel_DICGPU(corr2, (*pit)->getCoord().shift(offset));
            }
            else
            {
                LDEBUG << "Point " << (*pit)->getColor() << " start position estimate";
                Coord target = this->estimatePositionGauss((*pit)->getCoord(), pfinal, correlThr, (correlDomainX+correlDomainY+correlDomainZ)/3+(distX+distY+distZ)/3, correlDomainX+correlDomainY+correlDomainZ+distX+distY+distZ , true);
                //Coord target = this->estimatePositionGauss((*pit)->getCoord(), pfinal, correlThr, 100, 300 , true);
                Coord targetInt = target.getInt();
                //LINFO << "Offset (" << targetInt.getX()-(int)(*pit)->getCoord().getX() << "," << targetInt.getY()-(int)(*pit)->getCoord().getY() << "," << targetInt.getZ()-(int)(*pit)->getCoord().getZ() << ")";   
                potential = (*pit)->findBestCorrel_DICGPU(corr2, targetInt);
			}
            T* pitF = pfinal.findP((*pit)->getColor());
            (pitF)->PUpdate(potential);
	    }
		pfinal.countValid(correlThr);
        corr2.freeFullImg();
		corr2.free();
	    return pfinal;
	}

*/

template<class T>
PSet<T> PSet<T>::findBestCorrel_DICGPU(PSet<T> &pfinal, int correlDomain, double distXY, double distZ, double correlThr, vector<vector<double> > depl)
	{
		Volume imRef = Volume(this->name, Coord(this->StackdimX, this->StackdimY, this->StackdimZ));
		Volume imFinal = Volume(pfinal.getName(), Coord(pfinal.getStackdimX(), pfinal.getStackdimY(), pfinal.getStackdimZ()));
		LINFO << "Initialize GPU";
		Correl3DGPU corr = Correl3DGPU(imRef.getName().c_str(), imFinal.getName().c_str());
		corr.setfromFull(true);
		corr.setFilter(false);
		corr.setNormalize(true);
		corr.setIntersect(false);
		corr.setReducedWindow(false);
		corr.setdisplThr(false, distZ, distXY, distXY);
		corr.setDomain(correlDomain);
        corr.setWindow("none");
        //corr.setWindow("hanning");

		corr.setFFT(false);
        corr.setHalfPattern(correlDomain);
        corr.setDisplacement(distXY);
        corr.setCorrelThr(correlThr);

        corr.init();

		LINFO << "Correlation parameters:";
        LINFO << corr.optionsToString();
        LINFO << "Setting Ref";
		corr.setFullRef(imRef.getName().c_str());
		LINFO << "Setting Target";
		corr.setFullTarget(imFinal.getName().c_str());
		clock_t startTime = clock();
		clock_t currentTime = clock();
		int globalTime = diffclock(currentTime,startTime);
		int deltaTime = diffclock(clock(),currentTime);
		int i=0;
		LINFO << "Ready to start correlations";
		typename list<T*>::iterator pitF=(pfinal).getP()->begin();
        for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit, ++i)
		{
          	if(i%100 == 0)
			{
				deltaTime = diffclock(clock(),currentTime);
				currentTime = clock();
				globalTime = diffclock(currentTime,startTime);
				LINFO << "Proceeded 100 points in " << deltaTime << "ms ; " << i <<"/" << this->ps->size() <<" in " << globalTime/1000 << "s";
			}
            corr.setImRef((int)(*pit)->getCoord().getZ(), (int)(*pit)->getCoord().getY(), (int)(*pit)->getCoord().getX());
            T potential = (T)((*pit)->findBestCorrel_DICGPU(corr, (*pit)->getCoord().shift(depl)));
			(*pitF)->PUpdate(potential);
	        ++pitF;
	    }
		pfinal.countValid(correlThr);
        corr.freeFullImg();
		corr.free();
		return pfinal;
	}

template<class T>
PSet<T>  PSet<T>::optimizeCorrel_DICGPU(PSet<T> &pfinal, int correlDomain, int distThr, double correlThr, int distLocal)
	{
		Volume imRef = Volume(this->name, Coord(this->StackdimX, this->StackdimY, this->StackdimZ));
		Volume imFinal = Volume(pfinal.getName(), Coord(pfinal.getStackdimX(), pfinal.getStackdimY(), pfinal.getStackdimZ()));
		LINFO << "Initialize GPU";
		Correl3DGPU corr = Correl3DGPU(imRef.getName().c_str(), imFinal.getName().c_str());
		corr.setfromFull(true);
		corr.setFilter(false);
		corr.setNormalize(true);
		corr.setIntersect(false);
		corr.setReducedWindow(false);
		corr.setdisplThr(false, distThr, distThr, distThr);
		corr.setDomain(correlDomain);
        corr.setWindow("none");
        //corr.setWindow("hanning");

		corr.setFFT(false);
        corr.setHalfPattern(correlDomain);
        corr.setDisplacement(distThr);
        corr.setCorrelThr(correlThr);

        corr.init();

		LINFO << "Correlation parameters:";
        LINFO << corr.optionsToString();
        LINFO << "Setting Ref";
		corr.setFullRef(imRef.getName().c_str());
		LINFO << "Setting Target";
		corr.setFullTarget(imFinal.getName().c_str());
		clock_t startTime = clock();
		clock_t currentTime = clock();
		int globalTime = diffclock(currentTime,startTime);
		int deltaTime = diffclock(clock(),currentTime);
		int i=0;
		LINFO << "Ready to start correlations";
		typename list<T*>::iterator pitF=(pfinal).getIteratorBegin();
        for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit, ++i)
		{
			//if((*pitF)->getCorrel()<correlThr)
            //{
	            //LINFO << "Point " << (i+1);
                corr.setImRef((int)(*pit)->getCoord().getZ(), (int)(*pit)->getCoord().getY(), (int)(*pit)->getCoord().getX());
                Coord target = this->estimatePositionEq((*pit)->getCoord(), pfinal, correlThr, 1.*distLocal);
                Coord targetInt = target.getInt();
                //cout << "x=" << (*pit)->getCoord().getX() << " y=" << (*pit)->getCoord().getY() <<" z=" << (*pit)->getCoord().getZ() << endl;
                //cout << "x=" << target.getX() << " y= " << target.getY() <<" z=" << target.getZ() << endl;
                T potential = (*pit)->findBestCorrel_DICGPU(corr, targetInt);
			    (*pitF)->PUpdate(potential);
	        //}
            ++pitF;
	    }
		corr.freeFullImg();
		corr.free();
		return pfinal;
	}

template<class T>
PSet<T> PSet<T>::optimizeCorrel_DICGPU_aniso(PSet<T> &pfinal, int correlDomainX, int correlDomainY, int correlDomainZ, int distThrX, int distThrY, int distThrZ, double correlThr, int distLocal)
	{
		Volume imRef = Volume(this->name, Coord(this->StackdimX, this->StackdimY, this->StackdimZ));
		Volume imFinal = Volume(pfinal.getName(), Coord(pfinal.getStackdimX(), pfinal.getStackdimY(), pfinal.getStackdimZ()));
		LINFO << "Initialize GPU";
		Correl3DGPU corr = Correl3DGPU(imRef.getName().c_str(), imFinal.getName().c_str());
		corr.setfromFull(true);
		corr.setFilter(false);
		corr.setNormalize(true);
		corr.setIntersect(false);
		corr.setReducedWindow(false);
		corr.setdisplThr(false, distThrZ, distThrX, distThrY);
		corr.setDomain(correlDomainX);
        corr.setWindow("none");
        //corr.setWindow("hanning");

		corr.setFFT(false);
        corr.setaniso(true);
        corr.setHalfPatternX(correlDomainX);
        corr.setHalfPatternY(correlDomainY);
        corr.setHalfPatternZ(correlDomainZ);
        corr.setDisplacementX(distThrX);
        corr.setDisplacementY(distThrY);
        corr.setDisplacementZ(distThrZ);
        corr.setCorrelThr(correlThr);

        corr.init();

		LINFO << "Correlation parameters:";
        LINFO << corr.optionsToString();
        LINFO << "Setting Ref";
		corr.setFullRef(imRef.getName().c_str());
		LINFO << "Setting Target";
		corr.setFullTarget(imFinal.getName().c_str());
		clock_t startTime = clock();
		clock_t currentTime = clock();
		int globalTime = diffclock(currentTime,startTime);
		int deltaTime = diffclock(clock(),currentTime);
		int i=0;
		this->estimateLocalQuadraticCoherency(pfinal, 1.*distLocal);
        pfinal.scaleCorrelFactorwithSpatialCoherency();
        
        LINFO << "Ready to start correlations";
		typename list<T*>::iterator pitF=(pfinal).getIteratorBegin();
        for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit, ++i)
		{
			//if((*pitF)->getCorrel()<correlThr)
            //{
	            //LINFO << "Point " << (i+1);
                corr.setImRef((int)(*pit)->getCoord().getZ(), (int)(*pit)->getCoord().getY(), (int)(*pit)->getCoord().getX());
                PSet<T> pthr=this->subSetCorrel(correlThr);
                Coord target = pthr.estimatePositionfromQuadraticFit((*pit)->getCoord(), pfinal, 1.*distLocal);
                // Coord target = this->estimatePositionEq((*pit)->getCoord(), pfinal, correlThr, 1.*distLocal);
                Coord targetInt = target.getInt();
                //cout << "x=" << (*pit)->getCoord().getX() << " y=" << (*pit)->getCoord().getY() <<" z=" << (*pit)->getCoord().getZ() << endl;
                //cout << "x=" << target.getX() << " y= " << target.getY() <<" z=" << target.getZ() << endl;
                T potential = (*pit)->findBestCorrel_DICGPU(corr, targetInt);
			    (*pitF)->PUpdate(potential);
	        //}
            ++pitF;
	    }
		this->estimateLocalQuadraticCoherency(pfinal, 1.*distLocal);
        pfinal.scaleCorrelFactorwithSpatialCoherency();
		corr.freeFullImg();
		corr.free();
		return pfinal;
	}

template<class T>
PSet<T> PSet<T>::optimizeCorrel_DICGPU_texture(PSet<T> &pfinal, int correlDomainX, int correlDomainY, int correlDomainZ, int distThrX, int distThrY, int distThrZ, double correlThr, int distLocal)
	{
		Volume imRef = Volume(this->name, Coord(this->StackdimX, this->StackdimY, this->StackdimZ));
		Volume imFinal = Volume(pfinal.getName(), Coord(pfinal.getStackdimX(), pfinal.getStackdimY(), pfinal.getStackdimZ()));
		LINFO << "Initialize GPU";
		Correl3DGPU corr = Correl3DGPU(imRef.getName().c_str(), imFinal.getName().c_str());
		corr.setusetex(true);
		corr.setfromFull(true);
		corr.setFilter(false);
		corr.setNormalize(true);
		corr.setIntersect(false);
		corr.setReducedWindow(false);
		corr.setdisplThr(false, distThrZ, distThrX, distThrY);
		corr.setDomain(correlDomainX);
        corr.setWindow("none");
        //corr.setWindow("hanning");

		corr.setFFT(false);
        corr.setaniso(true);
        corr.setHalfPatternX(correlDomainX);
        corr.setHalfPatternY(correlDomainY);
        corr.setHalfPatternZ(correlDomainZ);
        corr.setDisplacementX(distThrX);
        corr.setDisplacementY(distThrY);
        corr.setDisplacementZ(distThrZ);
        corr.setCorrelThr(correlThr);

        corr.init();

		LINFO << "Correlation parameters:";
        LINFO << corr.optionsToString();
        LINFO << "Setting Ref";
		corr.setFullRef(imRef.getName().c_str());
		LINFO << "Setting Target";
		corr.setFullTarget(imFinal.getName().c_str());
		clock_t startTime = clock();
		clock_t currentTime = clock();
		int globalTime = diffclock(currentTime,startTime);
		int deltaTime = diffclock(clock(),currentTime);
		int i=0;
		LINFO << "Ready to start correlations";
		typename list<T*>::iterator pitF=(pfinal).getIteratorBegin();
        for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit, ++i)
		{
			//if((*pitF)->getCorrel()<correlThr)
            //{
	            //LINFO << "Point " << (i+1);
                corr.setRef((*pit)->getCoord().getZ(), (*pit)->getCoord().getY(), (*pit)->getCoord().getX());
                Coord target = this->estimatePositionEq((*pit)->getCoord(), pfinal, correlThr, 1.*distLocal);
                T potential = (*pit)->optimizeCorrel_DICGPU(corr, target);
			    (*pitF)->PUpdate(potential);
	        //}
            ++pitF;
	    }
		corr.freeFullImg();
		corr.free();
		return pfinal;
	}

template<class T>
void PSet<T>::findBackwardPosition_noDIC(const PSet<P>& pinit, PSet<P>& pfinal, double correlThr, double thrdist, double interactdist)
    {
        typename list<T*>::iterator pit=this->ps->begin(); 
        //#pragma omp parallel for
        //for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
        for (int i=0; i<this->ps->size(); i++)
		{
            //Coord target = pfinal.estimatePositionGaussInv((*pit)->getCoord(), pinit, correlThr, 1.*thrdist, 1.*interactdist, true);
            Coord target = pfinal.estimatePositionfromQuadraticFit((*pit)->getCoord(), pinit, thrdist);
            (*pit)->setCoord(target);   
            ++pit;
        }
       	
    }


//Coord estimator

template<class T>
Coord PSet<T>::estimateDepl(const Coord& ca, const PSet<T>& pfinal, double correlThr, double dist)
    {
        PSet<T> initsubsetdist = this->subSet(ca, dist);
        PSet<T> finalsubsetdist = pfinal.subSet(initsubsetdist);
        PSet<T> finalsubset = finalsubsetdist.subSetCorrel(correlThr);
        PSet<T> subset = this->subSet(finalsubset);
        Coord target = Coord(0,0,0);
        double norm = 0;
        int points=0;
        typename list<T*>::iterator pitF=finalsubset.getIteratorBegin();
        for (typename list<T*>::iterator pit=subset.getIteratorBegin(); pit!=subset.getIteratorEnd(); ++pit)
		{
            points++;
			double dista = ca.dist((*pit)->getCoord());
            Coord ref = (*pit)->getCoord().mult(-1.);
            Coord final = (*pitF)->getCoord();
            Coord depl = final.shift(ref);
            double coeff =exp(-4*dista*dista/(dist*dist));
            Coord add = depl.mult(coeff);
            norm += coeff;
            target = target.shift(add);
	        ++pitF;
        }
        if(points>0)
        {
            target = target.mult(1./norm);
        }
        else
        {
            target = this->estimateDepl(ca, pfinal, correlThr, 2*dist);
        }
        //cout << "result " << "dx=" << target.getX() << " dy=" << target.getY() << " dz=" << target.getZ() << endl;
        return target;
   }

template<class T>
Coord PSet<T>::estimateDepl(const Coord& ca, const PSet<T>& pfinal, double correlThr, double thrdist, double interactdist)
    {
		PSet<T> initsubsetdist = this->subSet(ca, thrdist);
		PSet<T> finalsubsetdist = pfinal.subSet(initsubsetdist);
		PSet<T> finalsubset = finalsubsetdist.subSetCorrel(correlThr);
		PSet<T> subset = this->subSet(finalsubset);
        Coord target = Coord(0,0,0);
        double norm = 0;
        int points=0;
        typename list<T*>::iterator pitF=finalsubset.getIteratorBegin();
        for (typename list<T*>::iterator pit=subset.getIteratorBegin(); pit!=subset.getIteratorEnd(); ++pit)
		{
            points++;
			double dista = ca.dist((*pit)->getCoord());
            Coord ref = (*pit)->getCoord().mult(-1.);
            Coord final = (*pitF)->getCoord();
            Coord depl = final.shift(ref);
            double coeff =exp(-dista*dista/(interactdist*interactdist));
            Coord add = depl.mult(coeff);
            norm += coeff;
            target = target.shift(add);
	        ++pitF;
        }
        if(points>0)
        {
            target = target.mult(1./norm);
        }
        else
        {
            target = this->estimateDepl(ca, pfinal, correlThr, 2*thrdist, interactdist);
        }
        //cout << "result " << "dx=" << target.getX() << " dy=" << target.getY() << " dz=" << target.getZ() << endl;
        return target;
   }

template<class T>
Coord PSet<T>::estimateLargeDepl(const Coord& ca, const PSet<T>& pfinal, double correlThr, double thrdist, double interactdist)
{
	PSet<T> finalsubsetdist = pfinal.subSet(ca, thrdist);
	PSet<T> finalsubset = finalsubsetdist.subSetCorrel(correlThr);
	PSet<T> subset = this->subSet(finalsubset);
        Coord target = Coord(0,0,0);
        double norm = 0;
        int points=0;
        typename list<T*>::iterator pit=subset.getIteratorBegin();
        for (typename list<T*>::iterator pitF=finalsubset.getIteratorBegin(); pitF!=finalsubset.getIteratorEnd(); ++pitF)
		{
            points++;
			double dista = ca.dist((*pitF)->getCoord());
            Coord final = (*pitF)->getCoord();
            Coord ref = (*pit)->getCoord().mult(-1);
            Coord depl = final.shift(ref);
            double coeff =exp(-dista*dista/(interactdist*interactdist));
            Coord add = depl.mult(coeff);
            norm += coeff;
            target = target.shift(add);
	        ++pit;
        }
        if(points>0)
        {
            target = target.mult(1./norm);
        }
        else
        {
            target = this->estimateLargeDepl(ca, pfinal, correlThr, 2*thrdist, interactdist);
        }
        //cout << "result " << "dx=" << target.getX() << " dy=" << target.getY() << " dz=" << target.getZ() << endl;
        return target;
   }

template<class T>
Coord PSet<T>::estimatePosition(const Coord& ca, const PSet<T>& pfinal, double correlThr, double dist)
    {
	    //cout << "Estimate position" << endl;
	    //cout << "Search distance subset" << endl;
	PSet<T> initsubsetdist = this->subSet(ca, dist);
	    //cout << "size=" << initsubsetdist.Size() << endl;
        //cout << "Search final subset dist" << endl;
	PSet<T> finalsubsetdist = pfinal.subSet(initsubsetdist);
        //cout << "Done" << endl;
	    //cout << "size=" << finalsubsetdist.Size() << endl;
	    //cout << "Search correl subset" << endl;
	PSet<T> finalsubset = finalsubsetdist.subSetCorrel(correlThr);
	    //cout << "size=" << finalsubset.Size() << endl;
        //cout << "Search init subset correl" << endl;
	PSet<T> subset = this->subSet(finalsubset);
	    //cout << "size=" << subset.Size() << endl;
        Coord target = Coord(0,0,0);
        double norm = 0;
        int points=0;
        typename list<T*>::iterator pitF=finalsubset.getIteratorBegin();
        for (typename list<T*>::iterator pit=subset.getIteratorBegin(); pit!=subset.getIteratorEnd(); ++pit)
		{
            points++;
	        //cout << "Dist" << endl;
			double dista = ca.dist((*pit)->getCoord());
	        //cout << "Dist = " << dista << endl;
            Coord ref = (*pit)->getCoord().mult(-1.);
            Coord final = (*pitF)->getCoord();
            Coord depl = final.shift(ref);
            Coord add = depl.mult(1./dista);
            norm += 1./dista;
            target = target.shift(add);
            //cout << "pt " << (*pit)->getColor() << " dx=" << depl.getX() << " dy=" << depl.getY() << " dz=" << depl.getZ() << endl;
	        ++pitF;
        }
        if(points>0)
        {
            target = target.mult(1./norm);
            target = ca.shift(target);
        }
        else
        {
            target = this->estimatePosition(ca, pfinal, correlThr, 2*dist);
        }
        //cout << "result " << "dx=" << target.getX() << " dy=" << target.getY() << " dz=" << target.getZ() << endl;
        return target;
   }

template<class T>
Coord PSet<T>::estimatePositionGauss(const Coord& ca, const PSet<T>& pfinal, double correlThr, double distThr, double interactdist, bool useTotalCorrel)
    {
	    //cout << "Estimate position" << endl;
	    //cout << "Search distance subset" << endl;
	PSet<T> initsubsetdist = this->subSet(ca, distThr);
	LDEBUG << "subset size is " << initsubsetdist.Size();
        //cout << "Search final subset dist" << endl;
	PSet<T> finalsubsetdist = pfinal.subSet(initsubsetdist);
        //cout << "Done" << endl;
	    //cout << "size=" << finalsubsetdist.Size() << endl;
	    //cout << "Search correl subset" << endl;
	PSet<T> finalsubset;

        if(useTotalCorrel)
            finalsubset = finalsubsetdist.subSetTotalCorrel(correlThr);
	    else
            finalsubset = finalsubsetdist.subSetCorrel(correlThr);

	LDEBUG << "final subset size is " << finalsubsetdist.Size();
        //cout << "size=" << finalsubset.Size() << endl;
        //cout << "Search init subset correl" << endl;
        PSet<T> subset = this->subSet(finalsubset);
	    //cout << "size=" << subset.Size() << endl;
        Coord target = Coord(0,0,0);
        double norm = 0;
        int points=0;
        typename list<T*>::iterator pitF=finalsubset.getIteratorBegin();
        for (typename list<T*>::iterator pit=subset.getIteratorBegin(); pit!=subset.getIteratorEnd(); ++pit)
		{
            points++;
	        //cout << "Dist" << endl;
			double dista = ca.dist((*pit)->getCoord());
	        //cout << "Dist = " << dista << endl;
            Coord ref = (*pit)->getCoord().mult(-1.);
            Coord final = (*pitF)->getCoord();
            Coord depl = final.shift(ref);
            double coeff =exp(-dista*dista/(interactdist*interactdist));
            Coord add = depl.mult(coeff);
            norm += coeff;
            target = target.shift(add);
            //cout << "pt " << (*pit)->getColor() << " dx=" << depl.getX() << " dy=" << depl.getY() << " dz=" << depl.getZ() << endl;
	        ++pitF;
        }
        LDEBUG << "Gauss position estimate with distThr=" << distThr << " found " << points << " points";
        if(points>0)
        {
            target = target.mult(1./norm);
            target = ca.shift(target);
        }
        else
        {
            target = this->estimatePositionGauss(ca, pfinal, correlThr, 2*distThr, interactdist, useTotalCorrel);
        }
        //cout << "result " << "dx=" << target.getX() << " dy=" << target.getY() << " dz=" << target.getZ() << endl;
        return target;
   }

template<class T>
Coord PSet<T>::estimatePositionGauss(const Coord& ca, const PSet<T>& pfinal, double correlThr, double distThr, bool useTotalCorrel)
{
	    //cout << "Estimate position" << endl;
	    //cout << "Search distance subset" << endl;
	PSet<T> initsubsetdist = this->subSet(ca, distThr);
	LDEBUG << "subset size is " << initsubsetdist.Size();
        //cout << "Search final subset dist" << endl;
	PSet<T> finalsubsetdist = pfinal.subSet(initsubsetdist);
        //cout << "Done" << endl;
	    //cout << "size=" << finalsubsetdist.Size() << endl;
	    //cout << "Search correl subset" << endl;
	PSet<T> finalsubset;

    if(useTotalCorrel)
            finalsubset = finalsubsetdist.subSetTotalCorrel(correlThr);
	else
            finalsubset = finalsubsetdist.subSetCorrel(correlThr);

	LDEBUG << "final subset size is " << finalsubsetdist.Size();
    PSet<T> subset = this->subSet(finalsubset);
    Coord target = Coord(0,0,0);
    int points=subset.Size();
    if(points>0)
    {
        double norm = 0;
        double sigma=0;
        for (typename list<T*>::iterator pit=subset.getIteratorBegin(); pit!=subset.getIteratorEnd(); ++pit)
		{
			sigma += ca.dist((*pit)->getCoord());
        }
        sigma *= 1.24/points;
        {
        typename list<T*>::iterator pit;
        typename list<T*>::iterator pitF;
        for (pit=subset.getIteratorBegin(), pitF=finalsubset.getIteratorBegin(); pit!=subset.getIteratorEnd(); ++pit, ++pitF)
		{
			double dista = ca.dist((*pit)->getCoord());
            Coord ref = (*pit)->getCoord().mult(-1.);
            Coord final = (*pitF)->getCoord();
            Coord depl = final.shift(ref);
            double coeff =exp(-dista*dista/(sigma*sigma));
            Coord add = depl.mult(coeff);
            norm += coeff;
            target = target.shift(add);
        }
        }
        LDEBUG << "Gauss position estimate with distThr=" << distThr << " found " << points << " points";
        target = target.mult(1./norm);
        target = ca.shift(target);
    }
    else
    {
        target = this->estimatePositionGauss(ca, pfinal, correlThr, 2*distThr, useTotalCorrel);
    }
        //cout << "result " << "dx=" << target.getX() << " dy=" << target.getY() << " dz=" << target.getZ() << endl;
        return target;
}

template<class T>
void PSet<T>::estimateLocalQuadraticCoherency(const PSet<T>& pfinal, double distThr)
{
    typename list<T*>::iterator pit = this->ps->begin();
    typename list<T*>::iterator pitF =  pitF=pfinal.getIteratorBegin();
    #pragma omp parallel for
    for(int i=0; i<this->ps->size(); i++)
    {   
        PSet<T> piss = this->subSet((*pit)->getCoord(),distThr);
        PSet<T> pfss = pfinal.subSet(piss);
        while(piss.Size()<9)
        {
            distThr = distThr*1.5;
            piss = this->subSet((*pit)->getCoord(),distThr);
            pfss = pfinal.subSet(piss);
        }
        double SC = (*pitF)->estimateLocalQuadraticCoherency(piss, pfss);
        (*pitF)->setSpatialCoherency(SC);    	
    ++pit;
    ++pitF;
    }
}

template<class T>
Coord PSet<T>::estimatePositionfromQuadraticFit(const Coord& ca, const PSet<T>& pfinal, double distThr)
{
    Coord target = Coord(0,0,0);
    PSet<T> pi = this->subSet(ca,distThr);
    PSet<T> pf = pfinal.subSet(pi);
    while(pi.Size()<20)
    {
        distThr = distThr*1.5;
        pi = this->subSet(ca,distThr);
        pf = pfinal.subSet(pi);
    }
        double eps = 0.1;
        int N = pi.Size();
        double* u = new double[N];
        double* v = new double[N];
        double* w = new double[N];
        double* nV = new double[N];       
 
        double* ru = new double[N];
        double* rv = new double[N];
        double* rw = new double[N];

        double* sig2 = new double[N];
        
        double* M = new double[10*N];
        double* X = new double[10*N];
        double* Wg = new double[N];
        double* X0 = new double[10];
        X0[0]=1;
        X0[1]=ca.getX();
        X0[2]=ca.getY();
        X0[3]=ca.getZ();
        X0[4]=ca.getX()*ca.getY();
        X0[5]=ca.getX()*ca.getZ();
        X0[6]=ca.getY()*ca.getZ();
        X0[7]=ca.getX()*ca.getX();
        X0[8]=ca.getY()*ca.getY();
        X0[9]=ca.getZ()*ca.getZ();
         
	    {
        typename list<T*>::iterator pit;
        typename list<T*>::iterator pitF;
        int i;
        for (pit=pi.getIteratorBegin(), pitF = pf.getIteratorBegin(), i=0; pit!=pi.getIteratorEnd(); ++pit, ++pitF)
        {
            Coord pos = (*pit)->getCoord();
            Coord depl = (*pitF)->getCoord().diff(pos);
            u[i]=depl.getX();
            v[i]=depl.getY();
            w[i]=depl.getZ();
            nV[i] = sqrt(u[i]*u[i]+v[i]*v[i]+w[i]*w[i]);
            X[i+0*N]=1;
            X[i+1*N]=pos.getX();
            X[i+2*N]=pos.getY();
            X[i+3*N]=pos.getZ();
            X[i+4*N]=pos.getX()*pos.getY();
            X[i+5*N]=pos.getX()*pos.getZ();
            X[i+6*N]=pos.getY()*pos.getZ();
            X[i+7*N]=pos.getX()*pos.getX();
            X[i+8*N]=pos.getY()*pos.getY();
            X[i+9*N]=pos.getZ()*pos.getZ();
            i++;
        }
        }
        double um = median(u,N);
        double vm = median(v,N);
        double wm = median(w,N);
        double nVm = median(nV,N);
        double K=0;
        for(int i=0; i<N; i++)
        {
            ru[i]=u[i]-um;
            rv[i]=v[i]-vm;
            rw[i]=w[i]-wm;
            sig2[i]=ru[i]*ru[i]+rv[i]*rv[i]+rw[i]*rw[i];
            K+=sqrt(sig2[i]);
        }
        K/=N;
        K+=eps;
        K*=K;
        for(int i=0; i<N; i++)
        {
            Wg[i]=exp(-sig2[i]/K);
        } 
        for(int i=0; i<N; i++)
            for(int j=0; j<10; j++)
            {
                M[i*10+j]=Wg[i]*X[j*N+i];
            } 
        double* au = new double[10];
        double* av = new double[10];
        double* aw = new double[10];
        solve_MXA_MU(M, X, u, au, N, 10);
        solve_MXA_MU(M, X, v, av, N, 10);
        solve_MXA_MU(M, X, w, aw, N, 10);
        
        double phiu = 0;
        double phiv = 0;
        double phiw = 0;
        for(int j=0; j<10; j++)
        {
            phiu+=au[j]*X0[j];
            phiv+=av[j]*X0[j];
            phiw+=aw[j]*X0[j];
        }

        target.setX(phiu+X0[1]);
        target.setY(phiv+X0[2]);
        target.setZ(phiw+X0[3]);

        LDEBUG << "Coord " << ca.display() << " new estimated pos is (" << target.display();
        return target;    
 }


template<class T>
Coord PSet<T>::estimatePositionGaussInv(const Coord& ca, const PSet<T>& pfinal, double correlThr, double distThr, double interactdist, bool useTotalCorrel)
    {
	    //cout << "Estimate position" << endl;
	    //cout << "Search distance subset" << endl;
	PSet<T> initsubsetdist = this->subSet(ca, distThr);
	LDEBUG << "subset size is " << initsubsetdist.Size();
	PSet<T> initsubset;
    if(useTotalCorrel)
            initsubset = initsubsetdist.subSetTotalCorrel(correlThr);
	else
            initsubset = initsubsetdist.subSetCorrel(correlThr);
        //cout << "Search final subset dist" << endl;
	LDEBUG << "subset size is " << initsubset.Size() << " after thr on correl";
	PSet<T> finalsubset = pfinal.subSet(initsubset);
        //cout << "Done" << endl;
	    //cout << "size=" << finalsubsetdist.Size() << endl;
	    //cout << "Search correl subset" << endl;


	LDEBUG << "final subset size is " << finalsubset.Size();
        //cout << "size=" << finalsubset.Size() << endl;
        //cout << "Search init subset correl" << endl;
    PSet<T> subset = initsubset.subSet(finalsubset);
	    //cout << "size=" << subset.Size() << endl;
    Coord target = Coord(0,0,0);
    double norm = 0;
    int points=0;
    typename list<T*>::iterator pitF=finalsubset.getIteratorBegin();
    for (typename list<T*>::iterator pit=subset.getIteratorBegin(); pit!=subset.getIteratorEnd(); ++pit)
    {
            points++;
	        //cout << "Dist" << endl;
			double dista = ca.dist((*pit)->getCoord());
	        //cout << "Dist = " << dista << endl;
            Coord ref = (*pit)->getCoord().mult(-1.);
            Coord final = (*pitF)->getCoord();
            Coord depl = final.shift(ref);
            double coeff =exp(-dista*dista/(interactdist*interactdist));
            Coord add = depl.mult(coeff);
            norm += coeff;
            target = target.shift(add);
            //cout << "pt " << (*pit)->getColor() << " dx=" << depl.getX() << " dy=" << depl.getY() << " dz=" << depl.getZ() << endl;
	        ++pitF;
        }
        LDEBUG << "Gauss position estimate with distThr=" << distThr << " found " << points << " points";
        if(points>0)
        {
            target = target.mult(1./norm);
            target = ca.shift(target);
        }
        else
        {
            target = this->estimatePositionGauss(ca, pfinal, correlThr, 2*distThr, interactdist, useTotalCorrel);
        }
        //cout << "result " << "dx=" << target.getX() << " dy=" << target.getY() << " dz=" << target.getZ() << endl;
        return target;
   }

template<class T>
void PSet<T>::estimateSpatialCoherency(const PSet<T>& pinit, double distThr, double SCThr)
{
	PSet<T> pfinalsubset = this->subSetSpatialCoherency(SCThr);
	PSet<T> pinitsubset = pinit.subSet(pfinalsubset);
    typename list<T*>::iterator pit=pinitsubset.getIteratorBegin(); 
    #pragma omp parallel for
    for (int i=0; i<pinitsubset.Size(); i++)
    {   
        PSet<T> piss = pinitsubset.subSet((*pit)->getCoord(),distThr);
        PSet<T> pfss = pfinalsubset.subSet(piss);
        double SC = (*pit)->estimateSpatialCoherency(piss, pfss);
        (*pit)->setSpatialCoherency(SC);   
        ++pit;
  }
}

template<class T>
void PSet<T>::scaleCorrelFactorwithSpatialCoherency()
{
    typename list<T*>::iterator pit=this->ps->begin();
    //for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
    #pragma omp parallel for
    for (int i=0; i<this->ps->size(); i++)
    {   
        (*pit)->setCorrel((*pit)->getCorrel()*(*pit)->getSpatialCoherency());    	
        ++pit;
    }
}

 
template<class T>
Coord PSet<T>::estimatePositionEq(const Coord& ca, const PSet<T>& pfinal, double correlThr, double dist)
    {
	    //cout << "Estimate position" << endl;
	    //cout << "Search distance subset" << endl;
	PSet<T> initsubsetdist = this->subSet(ca, dist);
	    //cout << "size=" << initsubsetdist.Size() << endl;
        //cout << "Search final subset dist" << endl;
	PSet<T> finalsubsetdist = pfinal.subSet(initsubsetdist);
        //cout << "Done" << endl;
	    //cout << "size=" << finalsubsetdist.Size() << endl;
	    //cout << "Search correl subset" << endl;
	PSet<T> finalsubset = finalsubsetdist.subSetCorrel(correlThr);
	    //cout << "size=" << finalsubset.Size() << endl;
        //cout << "Search init subset correl" << endl;
	PSet<T> subset = this->subSet(finalsubset);
	    //cout << "size=" << subset.Size() << endl;
        Coord target = Coord(0,0,0);
        double norm = 0;
        int points=0;
        typename list<T*>::iterator pitF=finalsubset.getIteratorBegin();
        for (typename list<T*>::iterator pit=subset.getIteratorBegin(); pit!=subset.getIteratorEnd(); ++pit)
		{
            points++;
	        //cout << "Dist" << endl;
			double dista = ca.dist((*pit)->getCoord());
	        //cout << "Dist = " << dista << endl;
            Coord ref = (*pit)->getCoord().mult(-1.);
            Coord final = (*pitF)->getCoord();
            Coord depl = final.shift(ref);
            Coord add = depl.mult(1.);
            norm += 1.;
            target = target.shift(add);
            //cout << "pt " << (*pit)->getColor() << " dx=" << depl.getX() << " dy=" << depl.getY() << " dz=" << depl.getZ() << endl;
	        ++pitF;
        }
        if(points>0)
        {
            target = target.mult(1./norm);
            target = ca.shift(target);
        }
        else
        {
            target = this->estimatePositionEq(ca, pfinal, correlThr, 2*dist);
        }
        //cout << "result " << "dx=" << target.getX() << " dy=" << target.getY() << " dz=" << target.getZ() << endl;
        return target;
   }




//Matching functions

template<class T>
vector<PSet<T> > PSet<T>::findCorrel(PSet<T> pfinal, double refSlices[], double refSlicesFinal[])
	{
		vector<PSet<T> > potentialCorrel = vector<PSet<T> >();
		for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
		{

			double dZ=1;
			double dXY=1;
			int subsetsize=0;
			PSet<T> subset;
			while(subsetsize==0)
			{
				subset = pfinal.subSet((*pit)->getCoord().Interpolate(refSlicesFinal, refSlices), dXY, dZ);
				subsetsize = subset.Size();
			}
			PSet<T> partS = PSet<T>();
			partS.addP(*pit);
			partS.addP(*subset.getIteratorBegin());
			potentialCorrel.push_back(partS);
		}
		return potentialCorrel;
	}

template<class T>
void PSet<T>::totalCorrel(std::vector<PSet<T> > VpS)
{
    for (typename list<T*>::iterator pit=VpS[0].getIteratorBegin(); pit!=VpS[0].getIteratorEnd(); ++pit)
    {
            (*pit)->setTotalCorrel((*pit)->getCorrel());
        }
   for(int i=1; i<VpS.size(); i++)
        {
	   typename list<T*>::iterator pitP=VpS[i-1].getIteratorBegin();
            for (typename list<T*>::iterator pit=VpS[i].getIteratorBegin(); pit!=VpS[i].getIteratorEnd(); ++pit)
            {
                double oldC = (*pitP)->getTotalCorrel();
                double newC = (*pit)->getCorrel();
                if(oldC<newC)
                    newC=oldC;
                (*pit)->setTotalCorrel(newC);
                ++pitP;
            }
        }
    }


template<>
vector<PSet<Particle> > PSet<Particle>::findBestCorrel_DICGPU(PSet<Particle> &pfinal, PSet<P> refPoints, PSet<P> finalPoints, double distXY, double distZ)
{
	vector<PSet<Particle> >* potentialCorrel = new vector<PSet<Particle> >();
	Volume imRef = Volume(this->name, Coord(this->StackdimX, this->StackdimY, this->StackdimZ));
	Volume imFinal = Volume(pfinal.getName(), Coord(pfinal.getStackdimX(), pfinal.getStackdimY(), pfinal.getStackdimZ()));
	cout << "Initialize GPU" << endl;
	Correl3DGPU corr = Correl3DGPU(imRef.getName().c_str(), imFinal.getName().c_str());
	corr.setfromFull(true);
	corr.setFilter(false);
	corr.setNormalize(true);
	corr.setIntersect(false);
	corr.setReducedWindow(true);
	corr.setWindow("none");
	corr.init();
			//corr.setFullRef(imRef.getName().c_str(), imRef.getdimZ(), imRef.getdimY(), imRef.getdimX());
			//corr.setFullTarget(imFinal.getName().c_str(), imFinal.getdimZ(), imFinal.getdimY(), imFinal.getdimX());
	cout << "Setting Ref" << endl;
			//cout << "Ref is " << imRef.getName().c_str() << endl;
	corr.setFullRef(imRef.getName().c_str());
			//cout << "Ref is set" << endl;
	cout << "Setting Target" << endl;
	corr.setFullTarget(imFinal.getName().c_str());
			//cout << "Target is set" << endl;
	clock_t startTime = clock();
	clock_t currentTime = clock();
	int globalTime = diffclock(currentTime,startTime);
	int deltaTime = diffclock(clock(),currentTime);
	int i=0;
	LINFO << "Ready to start correlations";
			//cout << this->getParticle(4)->fullInfoTostring() << endl;
	for (list<Particle*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit, ++i)
	{
				//i++;
				//cout << i << endl;
				//cout << this->getParticle(i)->tostring() << endl;
				//cout << (*pit)->tostring() << endl;
				if(i%100 == 0)
				{
					deltaTime = diffclock(clock(),currentTime);
					currentTime = clock();
					globalTime = diffclock(currentTime,startTime);
					LINFO << "Proceeded 100 pores in " << deltaTime << "ms ; " << i <<"/" << this->ps->size() <<" in " << globalTime/1000 << "s";
				}

	            corr.setImRef((int)(*pit)->getCoord().getZ(), (int)(*pit)->getCoord().getY(), (int)(*pit)->getCoord().getX());
	            Coord guestPos = refPoints.estimatePositionGauss((*pit)->getCoord(),finalPoints, 0.1, sqrt(distXY*distXY+distZ*distZ)*3, sqrt(distXY*distXY+distZ*distZ), true);
	            PSet<Particle> subset =  pfinal.subSet(guestPos, distXY, distZ);

	            PSet<Particle> potential = (*pit)->findBestCorrel_DICGPU(subset,corr,guestPos);

	        	potential.setName("potential for particle "+i);
				(*potentialCorrel).push_back(potential);

	}
	corr.freeFullImg();
	corr.free();
	return *potentialCorrel;
}

template<>
vector<PSet<Particle> > PSet<Particle>::findBestCorrel_noDIC(PSet<Particle> &pfinal, PSet<P> refPoints, PSet<P> finalPoints, double distXY, double distZ)
{
	vector<PSet<Particle> >* potentialCorrel = new vector<PSet<Particle> >();
	Volume imRef = Volume(this->name, Coord(this->StackdimX, this->StackdimY, this->StackdimZ));
	Volume imFinal = Volume(pfinal.getName(), Coord(pfinal.getStackdimX(), pfinal.getStackdimY(), pfinal.getStackdimZ()));
	clock_t startTime = clock();
	clock_t currentTime = clock();
	int globalTime = diffclock(currentTime,startTime);
	int deltaTime = diffclock(clock(),currentTime);
	int i=0;
	cout << "Ready to start correlations" << endl;
		//cout << this->getParticle(4)->fullInfoTostring() << endl;
	for (list<Particle*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit, ++i)
	{
			//i++;
			//cout << i << endl;
			//cout << this->getParticle(i)->tostring() << endl;
			//cout << (*pit)->tostring() << endl;
			if(i%100 == 0)
			{
				deltaTime = diffclock(clock(),currentTime);
				currentTime = clock();
				globalTime = diffclock(currentTime,startTime);
				LINFO << "Proceeded 100 pores in " << deltaTime << "ms ; " << i <<"/" << this->ps->size() <<" in " << globalTime/1000 << "s";
			}

			Coord guestPos = refPoints.estimatePositionGauss((*pit)->getCoord(),finalPoints, 0.1, sqrt(distXY*distXY+distZ*distZ)*3, sqrt(distXY*distXY+distZ*distZ), true);
			PSet<Particle> subset =  pfinal.subSet(guestPos, distXY, distZ);

            PSet<Particle> potential = (*pit)->findBestCorrel_noDIC(subset,guestPos);

        	potential.setName("potential for particle "+i);
			(*potentialCorrel).push_back(potential);

	}
	return *potentialCorrel;
}

template<>
vector<PSet<Particle> > PSet<Particle>::findBestCorrel_noDIC_safe(PSet<Particle> &pfinal, PSet<P> refPoints, PSet<P> finalPoints, double distX, double distY, double distZ, double correlThr)
{
	LINFO << "Ready to start correlations in safe mode";

	vector<PSet<Particle> >* potentialCorrel = new vector<PSet<Particle> >();
	//Volume imRef = Volume(this->name, Coord(this->StackdimX, this->StackdimY, this->StackdimZ));
	//Volume imFinal = Volume(pfinal.getName(), Coord(pfinal.getStackdimX(), pfinal.getStackdimY(), pfinal.getStackdimZ()));
	//clock_t startTime = clock();
	//clock_t currentTime = clock();
	//int globalTime = diffclock(currentTime,startTime);
	//int deltaTime = diffclock(clock(),currentTime);
	int i=0;
		//cout << this->getParticle(4)->fullInfoTostring() << endl;
	for (list<Particle*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit, ++i)
	{
			//i++;
			LDEBUG << "Part " << i ;
			//LDEBUG << this->getP(i)->tostring();
			LDEBUG << (*pit)->tostring() ;
			/*
            if(i%100 == 0)
			{
				deltaTime = diffclock(clock(),currentTime);
				currentTime = clock();
				globalTime = diffclock(currentTime,startTime);
				LINFO << "Proceeded 100 pores in " << deltaTime << "ms ; " << i <<"/" << this->ps->size() <<" in " << globalTime/1000 << "s";
			}*/
            Coord guestPos = refPoints.estimatePositionGauss((*pit)->getCoord(),finalPoints, -1, (distX+distY+distZ)/3, distX+distY+distZ, false);
			PSet<Particle> subset =  pfinal.subSet(guestPos, distX, distY, distZ);
            LDEBUG << "subset size " << subset.Size();
            PSet<Particle> potential = (*pit)->findBestCorrel_noDIC_safe(subset,guestPos);
            if(potential.Size()==0)
            {
                LDEBUG << "unused required";
                    potential.addUnused(pfinal, **pit, guestPos);
        	}
            potential.setName("potential for particle "+i);
			//potential.setGlobalColor((*pit)->getGlobalColor());
            (*potentialCorrel).push_back(potential);

	}
	return *potentialCorrel;
}

template<>
void PSet<Particle>::findIterativeSingleBestCorrel_noDIC(PSet<Particle> &pfinal, PSet<P> refPoints, PSet<P> finalPoints, double distX, double distY, double distZ, double correlThr)
{
	LINFO << "Ready to start correlations in safe mode";

	int i=0;
    int round1=0;
    int round2=0;
    int nbunused=0;
    PSet<Particle>* matched = new PSet<Particle>();
    for (list<Particle*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit, ++i)
    {      
        if((*pit)->getGlobalColor()!=0)
        {
			LDEBUG << "Part " << (*pit)->tostring() ;
            Coord guestPos = refPoints.estimatePositionGauss((*pit)->getCoord(),finalPoints, -1, (distX+distY+distZ)/3, distX+distY+distZ, false);
   	        LDEBUG << "Guest pos " << guestPos.toString();
            PSet<Particle> subset =  pfinal.subSet(guestPos, 2*distX, 2*distY, 2*distZ);
            LDEBUG << "subset size " << subset.Size();
		    if(subset.Size()>0)
            {
                Particle *cible = subset.findClosest(guestPos);
                LDEBUG << "Best matching part is " << cible->tostring() ;
                if(cible->getGlobalColor()==0)
                {
                    cible->setGlobalColor((*pit)->getGlobalColor());
                    cible->setCorrel(1);
                    matched->addP(*pit);
                    round1++;
                }
            }
        }
    }

    LINFO << "Matched " << round1 << "/" << this->Size() << "on first round";

    for (list<Particle*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit, ++i)
    {      
        if(((*pit)->getGlobalColor()!=0)&&(!matched->hasGlobalColor((*pit)->getGlobalColor())))
        {
			LDEBUG << "Part "  << (*pit)->tostring() ;
            Coord guestPos = refPoints.estimatePositionGauss((*pit)->getCoord(),finalPoints, -1, (distX+distY+distZ)/3, distX+distY+distZ, false);
   	        LDEBUG << "Guest pos " << guestPos.toString();
            PSet<Particle> subset =  pfinal.subSet(guestPos, 4*distX, 4*distY, 4*distZ);
            LDEBUG << "subset size " << subset.Size();
		    if(subset.Size()>0)
            {
                Particle *cible = subset.findClosest(guestPos);
                LDEBUG << "Best matching part is " << cible->tostring() ;
                if(cible->getGlobalColor()==0)
                {
                    cible->setGlobalColor((*pit)->getGlobalColor());
                    cible->setCorrel(1);
                    matched->addP(*pit);
                    round2++;
                }
            }
        }
    }
 
    LINFO << "Matched " << (round1+round2) << "/" << this->Size() << "on second round";

    for (list<Particle*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit, ++i)
    {      
        if(((*pit)->getGlobalColor()!=0)&&(!matched->hasGlobalColor((*pit)->getGlobalColor())))
        {
			LDEBUG << "Part " << (*pit)->tostring() ;
            Coord guestPos = refPoints.estimatePositionGauss((*pit)->getCoord(),finalPoints, -1, (distX+distY+distZ)/3, distX+distY+distZ, false);
		    LDEBUG << "Part " << (*pit)->getColor() << " (global Color " << (*pit)->getGlobalColor() << ") unused required";
            pfinal.updateUnmatched(**pit, guestPos);
            nbunused++;
         }
    }
   
 
    LINFO << "Add " << nbunused << " particles ";
    int removed = pfinal.removeUnmatched(); 
    LINFO << "removed " << removed << " extra particles ";
    LINFO << "Matched " << (round1 + round2 + nbunused) << "/" << this->Size();
}


template<>
void PSet<Particle>::findSingleBestCorrel_noDIC_safe(PSet<Particle> &pfinal, PSet<P> refPoints, PSet<P> finalPoints, double distX, double distY, double distZ, double correlThr)
{
	LINFO << "Ready to start correlations in safe mode";

	int i=0;
    int nbmissing=0;
    for (list<Particle*>::iterator pit=pfinal.getIteratorBegin(); pit!=pfinal.getIteratorEnd(); ++pit, ++i)
    {
			LDEBUG << "Part " << i << " " << (*pit)->tostring() ;
			if(!(*pit)->getCoord().isZero())
            {
                Coord guestPos = finalPoints.estimatePositionGauss((*pit)->getCoord(),refPoints, -1, (distX+distY+distZ)/3, distX+distY+distZ, false);
			    LDEBUG << "Guest pos " << guestPos.toString();
                //PSet<Particle> subset =  this->subSet(guestPos, 5*distX, 5*distY, 5*distZ);
                //LDEBUG << "subset size " << subset.Size();
		        Particle *cible = this->findClosest(guestPos);
                //PSet<Particle> potential = (*pit)->findBestCorrel_noDIC_safe(*this,guestPos);
			    //LDEBUG << "Best matching part is " << potential.getP(0)->tostring() ;
                //(*pit)->setGlobalColor(potential.getP(0)->getGlobalColor());
                //(*pit)->setCorrel(1);
                //LDEBUG << "Global color of particle " << (*pit)->getColor() << " of scan " << (*pit)->getScan() << " is set to " << potential.getP(0)->getGlobalColor(); 
    			LDEBUG << "Best matching part is " << cible->tostring() ;
                (*pit)->setGlobalColor(cible->getGlobalColor());
                (*pit)->setCorrel(1);
                LDEBUG << "Global color of particle " << (*pit)->getColor() << " of scan " << (*pit)->getScan() << " is set to " << cible->getGlobalColor(); 
            }
            else
            {
                nbmissing++;
            }
    }

	i=0;
    int nbunused=0;
    for (list<Particle*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit, ++i)
    {      
        if(!pfinal.hasGlobalColor((*pit)->getGlobalColor()))
        {  
            Coord guestPos = refPoints.estimatePositionGauss((*pit)->getCoord(),finalPoints, -1, (distX+distY+distZ)/3, distX+distY+distZ, false);
		    LDEBUG << "Part " << i << " (global Color " << (*pit)->getGlobalColor() << ") unused required";
            pfinal.updateUnused(**pit, guestPos);
            nbunused++;
        }
        else
        {
            LDEBUG << "Part " << i << " already matched";
        }
    }
    LINFO << "Add " << nbunused << " particles for " << nbmissing << " missing";
}

template<>
int PSet<Particle>::findParticle_fromLabelled(PSet<Particle> &pfinal, PSet<P> refPoints, PSet<P> finalPoints, double distLocal, double distLabel, double correlThr, int currentMaxGlobalColor)
{
    LINFO << "Ready to start correlations based on labelled images";

	Volume imFinal = Volume(pfinal.getName(), Coord(pfinal.getStackdimX(), pfinal.getStackdimY(), pfinal.getStackdimZ()));
	LINFO << "Initialize GPU";
	Image3DGPU im = Image3DGPU(imFinal.getName().c_str());
    im.setDisplacementX(distLabel);
    im.setDisplacementY(distLabel);
    im.setDisplacementZ(distLabel);
    im.init();

	LINFO << "Tracking parameters:";
    LINFO << im.optionsToString();
    LINFO << "Setting Im";
	im.setFullIm(imFinal.getName().c_str());
	clock_t startTime = clock();
	clock_t currentTime = clock();
	int globalTime = diffclock(currentTime,startTime);
	int deltaTime = diffclock(clock(),currentTime);
	refPoints.estimateLocalQuadraticCoherency(finalPoints, 1.*distLocal);
    finalPoints.scaleCorrelFactorwithSpatialCoherency();
        
    LINFO << "Ready to start Tracking";
    int i=0;
    int nbmissing=0;
    for (list<Particle*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit, ++i)
    {
	    LDEBUG << "Part " << i << " " << (*pit)->tostring() ;
        PSet<P> pfthr=finalPoints.subSetCorrel(correlThr);
        PSet<P> pthr=refPoints.subSet(pfthr);
        Coord guestPos = pthr.estimatePositionfromQuadraticFit((*pit)->getCoord(), pfthr, distLocal);
	    LDEBUG << "Guest pos " << guestPos.toString();
        int label =  im.label_dist(guestPos);
        if(label>0)
        {
            Particle *cible = pfinal.findP(label);
       	    LDEBUG << "Best matching part is " << cible->tostring() ;
            (*pit)->setFather(cible->getColor());
            cible->setCorrel(1);
            if((cible->getGlobalColor()==0)||((*pit)->getVolume()>((this->findGlobalColorP(cible->getGlobalColor()))->getVolume())))
            {
                cible->setGlobalColor((*pit)->getGlobalColor());
            }
            LDEBUG << "Global color of particle " << cible->getColor() << " of scan " << cible->getScan() << " is set to " << (*pit)->getGlobalColor(); 
        }
        else
        {
            nbmissing++;
            LDEBUG << "No match found";
        }
    }

    im.freeFullImg();
    im.free();
	
    i=0;
    int nbconnect=0;
    for (list<Particle*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit, ++i)
    {      
        if((*pit)->getFather()>-1)
        {
            (*pit)->setFather((pfinal.findP((*pit)->getFather()))->getGlobalColor());
            if((*pit)->getFather()!=(*pit)->getGlobalColor())
                nbconnect++;
        }
    }
    int oldMax = currentMaxGlobalColor;
    currentMaxGlobalColor = pfinal.fillGlobalColor(currentMaxGlobalColor);
    LINFO << "Found " << currentMaxGlobalColor - oldMax << " nucleations, " << nbconnect << " merges and " << nbmissing << " closures";
    return currentMaxGlobalColor;
}


template<class T>
void PSet<T>::addUnused(PSet<Particle> &partSet, Particle &part, Coord &coord)
{
    for (list<Particle*>::iterator pit=partSet.getIteratorBegin(); pit!=partSet.getIteratorEnd(); ++pit)
    {
			if((*pit)->getCoord().isZero())
			{
				(*pit)->PUpdate(part);
                (*pit)->setCorrel(0);
                (*pit)->setCoord(coord);
                (*pit)->setGlobalColor(part.getGlobalColor());
			    this->addP(*pit);
                return;
			}
	}
    return;
}

template<class T>
void PSet<T>::updateUnmatched(Particle &part, Coord &coord)
{
    this->updateUnmatched(*this, part, coord);
}

template<class T>
void PSet<T>::updateUnmatched(PSet<Particle> &partSet, Particle &part, Coord &coord)
{
    for (list<Particle*>::iterator pit=partSet.getIteratorBegin(); pit!=partSet.getIteratorEnd(); ++pit)
    {
			if((*pit)->getGlobalColor()==0)
			{
				(*pit)->PUpdate(part);
                (*pit)->setCorrel(0);
                (*pit)->setCoord(coord);
                (*pit)->setGlobalColor(part.getGlobalColor());
                return;
			}
	}
    return;
}

template<class T>
void PSet<T>::updateUnmatched(PSet<P> &partSet, Particle &part, Coord &coord)
{
    for (list<P*>::iterator pit=partSet.getIteratorBegin(); pit!=partSet.getIteratorEnd(); ++pit)
    {
			if((*pit)->getCorrel()==0)
			{
				(*pit)->PUpdate(part);
                (*pit)->setCorrel(0);
                (*pit)->setCoord(coord);
                return;
			}
	}
    return;
}

template<class T>
void PSet<T>::updateUnused(Particle &part, Coord &coord)
{
    this->updateUnused(*this, part, coord);
}

template<class T>
void PSet<T>::updateUnused(PSet<Particle> &partSet, Particle &part, Coord &coord)
{
    for (list<Particle*>::iterator pit=partSet.getIteratorBegin(); pit!=partSet.getIteratorEnd(); ++pit)
    {
			if((*pit)->getCoord().isZero())
			{
				(*pit)->PUpdate(part);
                (*pit)->setCorrel(0);
                (*pit)->setCoord(coord);
                (*pit)->setGlobalColor(part.getGlobalColor());
                return;
			}
	}
    return;
}

template<class T>
void PSet<T>::updateUnused(PSet<P> &partSet, Particle &part, Coord &coord)
{
    for (list<P*>::iterator pit=partSet.getIteratorBegin(); pit!=partSet.getIteratorEnd(); ++pit)
    {
			if((*pit)->getCoord().isZero())
			{
				(*pit)->PUpdate(part);
                (*pit)->setCorrel(0);
                (*pit)->setCoord(coord);
                return;
			}
	}
    return;
}

template <>
int PSet<Particle>::setGlobalNumbers(int current)
{
		 for (list<Particle*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
		 {
			if(((*pit)->getGlobalColor()==0))
			{
				current++;
				(*pit)->setGlobalColor(current);
			}
		}
	return current;
}

template <>
void PSet<Particle>::matchGlobalNumberIntra(PSet<Particle> partSet)
{
		 list<Particle*>::iterator pitSet = partSet.getIteratorBegin();
		 for (list<Particle*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
		 {
			 (*pit)->setGlobalColor((*pitSet)->getGlobalColor());
		 }
}

template <>
int PSet<Particle>::globalNumberFromCheckedRef(vector<PSet<Particle> > & VpS, int current, int scan)
{
		for(int i=0; i<VpS.size();i++)
		{
			int pos = this->getPos(**VpS[i].getIteratorBegin());
			if((pos>=0)&&(this->getP(pos)->getGlobalColor()<=0))
			{
				current++;
				Particle* pit = this->getP(pos);
				pit->setGlobalColor(current);
				pit->setFather(current);
				pit->setGrandfather(current);
				pit->setFatherconnect(scan);
				pit->setGrandfatherconnect(scan);
				pit->setBorn(scan);
				pit->setDead(scan);
			}
		}
	return current;
}

template <>
int PSet<Particle>::globalNumberFromCheckedFinal(vector<PSet<Particle> > & VpS, PSet<Particle> & ref, int current, int scan )
	{
		for(int i=0; i<VpS.size();i++)
		{
			int pos = this->getPos(*VpS[i].getP(1));
			int posref=ref.getPos(*VpS[i].getP(0));

			if((pos>=0)&&(posref>=0)&&(ref.getP(posref)->getGlobalColor()>0))
			{
				Particle* pit = this->getP(pos);
				pit->setGlobalColor(ref.getP(posref)->getGlobalColor());
				pit->setFather(ref.getP(posref)->getFather());
				pit->setGrandfather(ref.getP(posref)->getGrandfather());
				pit->setFatherconnect(ref.getP(posref)->getFatherconnect());
				pit->setGrandfatherconnect(ref.getP(posref)->getGrandfatherconnect());
				pit->setBorn(scan);
				pit->setDead(ref.getP(posref)->getDead());
			}
			for(int j=2; j<VpS[i].Size(); j++)
			{
				pos = this->getPos(*VpS[i].getP(j));
				if((pos>=0)&&(posref>=0)&&(ref.getP(posref)->getGlobalColor()>0)&&(this->getP(pos)->getGlobalColor()<=0))
				{
					current++;
					Particle* pit = this->getP(pos);
					pit->setGlobalColor(current);
					pit->setFather(ref.getP(posref)->getGlobalColor());
					pit->setGrandfather(ref.getP(posref)->getGrandfather());
					pit->setFatherconnect(scan);
					pit->setGrandfatherconnect(ref.getP(posref)->getGrandfatherconnect());
					pit->setBorn(scan);
					pit->setDead(scan);
				}
			}
		}
		return current;
}
/**
template <>
int PSet<Particle>::globalNumberFromSafe(vector<PSet<Particle> > & VpS, PSet<Particle> & ref, int scan )
	{
        for ( list<Particle*>::iterator pit=partSet.getIteratorBegin(); pit!=partSet.getIteratorEnd(); ++pit)
		for(int i=0; i<VpS.size();i++)
		{
			int pos = this->getPos(*VpS[i].getP(1));
			int posref=ref.getPos(*VpS[i].getP(0));

			if((pos>=0)&&(posref>=0)&&(ref.getP(posref)->getGlobalColor()>0))
			{
				Particle* pit = this->getP(pos);
				pit->setGlobalColor(ref.getP(posref)->getGlobalColor());
				pit->setFather(ref.getP(posref)->getFather());
				pit->setGrandfather(ref.getP(posref)->getGrandfather());
				pit->setFatherconnect(ref.getP(posref)->getFatherconnect());
				pit->setGrandfatherconnect(ref.getP(posref)->getGrandfatherconnect());
				pit->setBorn(scan);
				pit->setDead(ref.getP(posref)->getDead());
			}
			for(int j=2; j<VpS[i].Size(); j++)
			{
				pos = this->getPos(*VpS[i].getP(j));
				if((pos>=0)&&(posref>=0)&&(ref.getP(posref)->getGlobalColor()>0)&&(this->getP(pos)->getGlobalColor()<=0))
				{
					current++;
					Particle* pit = this->getP(pos);
					pit->setGlobalColor(current);
					pit->setFather(ref.getP(posref)->getGlobalColor());
					pit->setGrandfather(ref.getP(posref)->getGrandfather());
					pit->setFatherconnect(scan);
					pit->setGrandfatherconnect(ref.getP(posref)->getGrandfatherconnect());
					pit->setBorn(scan);
					pit->setDead(scan);
				}
			}
		}
		return current;
}
*/
template <>
vector<PSet<Particle> > PSet<Particle>::checkCorrel(vector<PSet<Particle> > potential, PSet<Particle> pfinal, vector<PSet<Particle> > potentialInv)
{
	vector<PSet<Particle> >* checkedCorrel = new vector<PSet<Particle> >();
	int ck=1;
	int round=0;
	while(ck>0)
	{
		round++;
		cout << "Check correl round " << round  << "\n" << endl;
		ck=0;
		int i=0;
		for (list<Particle*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit, ++i)
		{
			//printf("Current particle : " + i +" ,color is " + this->particles[i].getColor() + "\n");
			int indpotInv = -1;
			indpotInv = pfinal.getPos(**potential[i].getIteratorBegin());
			//printf("indpotInv : " + indpotInv+"\n");
			if((indpotInv>=0)&&((*pit)->equalColor(**potentialInv[indpotInv].getIteratorBegin())))
			{
				ck++;
				//printf("found\n");
				PSet<Particle> checked = PSet<Particle>();
				checked.addP(*pit);
				checked.addP(*potential[i].getIteratorBegin());
				//potential[i] = ParticlesSet();
				for(int j=0; j<potentialInv.size();j++)
				{
					if(potentialInv[j].contains(**pit))
					{
						potentialInv[j].removeP(potentialInv[j].getPos(**pit));
					}
				}
				//printf("removed\n");
			}
		}
	}
	return *checkedCorrel;
}

template <>
void PSet<Particle>::debugCorrel(vector<PSet<Particle> > & potential, PSet<Particle> & pfinal, vector<PSet<Particle> > & potentialInv)
{
	int i=0;
	int ck=0;

   int nref=this->ps->size();
	//for each particle **pit of reference volume
   for (list<Particle*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit, ++i)
	{
		cout << "Check correl particle " << (i+1) << "/" << nref << endl;
       if(*pit)
               cout << "pit is ok and color is " << (*pit)->getColor() << endl;

       if(potential[i].getP(0))
       {
           cout << potential[i].Size() << " potential particles" << endl;
           int indpotInv = -1;
		    indpotInv = pfinal.getPos(*potential[i].getP(0));
		    cout << "Index of potInv " << indpotInv << endl;
		    Particle* potInv = potentialInv[indpotInv].getP(0);

           if(potInv){
               cout << "potInv is ok" << endl;
               cout << "Refcolor = " << (*pit)->getColor() << " and Found Color = " << potInv->getColor() << endl;
           }
       }
       else
       {
           cout << "No potential correl for this particle" << endl;
       }
	}
}


template <>
vector<PSet<Particle> > PSet<Particle>::matchCorrel(vector<PSet<Particle> > & potential, PSet<Particle> & pfinal, vector<PSet<Particle> > & potentialInv)
{
	vector<PSet<Particle> >* checkedMain = new vector<PSet<Particle> >();
	int i=0;
	int ck=0;

   int nref=this->ps->size();
	//for each particle **pit of reference volume
   for (list<Particle*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit, ++i)
	{
		//cout << "Check correl particle " << (i+1) << "/" << nref << endl;
       if(potential[i].getP(0))
		{
           int indpotInv = -1;
		    indpotInv = pfinal.getPos(*potential[i].getP(0));
		    Particle* potInv = potentialInv[indpotInv].getP(0);
           //if(potInv){
           //    cout << "Refcolor = " << (*pit)->getColor() << " and Found Color = " << potInv->getColor() << endl;
           //}

           //if reference particule is the best correlation of its best correlation (both particles matches)
           if((indpotInv>=0) && (potInv) && ((*pit)->equalColor(*potInv)))
		    {
			    ck++;
			    //cout << "Checked++ (" << ck << "," << i << ")" << endl;
			    PSet<Particle>* checked = new PSet<Particle>(this->name);
			    //cout << "Particle " << (*pit)->getColor() << " and " << potential[i].getParticle(0)->getColor() << endl;
               checked->addP(*pit);
			    checked->addP(potential[i].getP(0));
               int j=0;
               for (list<Particle*>::iterator pitF=pfinal.getIteratorBegin(); pitF!=pfinal.getIteratorEnd(); ++pitF, ++j)
	            {
				    if(potentialInv[j].contains(**pit))
				    {
                       //cout << " Add particle " << (*pitF)->getColor() << endl;
					    checked->addP(*pitF);
                       potentialInv[j].removeP(potentialInv[j].getPos(**pit));
				    }
			    }
			    checkedMain->push_back(*checked);
		    }
       }
       else
       {
           cout << "No potential correl for this particle" << endl;
       }
	}
   cout << "Found " << ck << "/" << nref << "particles matching" << endl;
	return *checkedMain;
}




//Ordering and renumbering

template<class T>
void PSet<T>::renumber()
{
    int i=1;
    for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
    {
        (*pit)->setColor(i);
        i++;
    }
}

template<typename T>
inline bool larger_volume(T* part1, T* part2){ return (part1->getVolume()<part2->getVolume()); }

template<typename T>
PSet<T> PSet<T>::rankByVolume()
{
		PSet<T> ranked = PSet<T>(*this);
		//TODO ranked.ps->sort(larger_volume);
		return ranked;
}

template<typename T>
void PSet<T>::rankByGlobalNumber()
{
	this->ps->sort();
}

template<class T>
list<T*>*  PSet<T>::rankLocality( int Thr ) const
{
		list<T*> *pool = new list<T*>();
		for (typename list<T*>::const_iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
		{
				 pool->push_back(&**pit);
		}
		
        list<T*> *ranked = new list<T*>();
                    
        ranked->push_back(&**(pool->begin()));
        pool->remove(&**(pool->begin()));
	    	
        for (typename list<T*>::iterator pit=ranked->begin(); pit!=ranked->end(); ++pit)
		{ 
            bool found = false;
            double dist = 100000000000;
            typename list<T*>::iterator pitpn = pool->begin();
            for (typename list<T*>::iterator pitp=pool->begin(); pitp!=pool->end();)
            {
                double locdist =  (*pit)->getCoord().dist((*pitp)->getCoord());
                if((locdist < dist)&&(locdist>Thr))
                {
                        dist = locdist;
                        pitpn = pitp;
                }
                if(locdist < Thr)
                {
                    ranked->push_back(&**pitp);
                    pitp = pool->erase(pitp);
                    found = true;
                }
                else
                    ++pitp;
            }
            if((!found)&&!(pool->empty()))
            {
                LDEBUG << "Used closest";
                ranked->push_back(&**pitpn);
                pool->erase(pitpn);
            }
        }
             
		return ranked;
}


//Counter

template<class T>
int PSet<T>::countValid(double thr) const
{
    int valid = 0;
    for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
    {
        if((*pit)&&((*pit)->getCorrel()>thr))
            valid++;
    }
    return valid;
}



//Mesh tools

template<class T>
PSet<P> PSet<T>::computeMesh(PSet<P>& newMesh, PSet<T>& pInit, PSet<T>& pFinal, double correlThr, double distThr, double distLocal)
{
    LINFO << "Computing Mesh";
    int i=0;
    list<P*>::iterator pitNM=(newMesh).getIteratorBegin();
    typename list<T*>::iterator pit=this->ps->begin();
    //#pragma omp parallel for
    for (int i=0; i<this->ps->size(); i++)
    {
        //LINFO << "Point " << i << " (" << (*pit)->getCoord().getX() << "," << (*pit)->getCoord().getY() << "," << (*pit)->getCoord().getZ() << ")";
        PSet<T> pthr=pInit.subSetCorrel(correlThr);
        Coord target = pthr.estimatePositionfromQuadraticFit((*pit)->getCoord(), pFinal, 1.*distLocal);
        //Coord target = pInit.estimatePositionGauss((*pit)->getCoord(),pFinal, correlThr, distThr, distLocal, false);
        LINFO << "Point " << i << " (" << (*pit)->getCoord().getX() << "," << (*pit)->getCoord().getY() << "," << (*pit)->getCoord().getZ() << ") => (" << target.getX() << "," << target.getY() << "," << target.getZ() << ")";
        //i++;
        (*pitNM)->setCoord(target);
	    ++pitNM;
        ++pit;
    }
    return newMesh;
}

template<class T>
void PSet<T>::computeSafeMesh(PSet<P>& newMesh, PSet<T>& pInit, PSet<T>& pFinal, double correlThr, double distThr, double distLocal)
{
    LINFO << "Computing Mesh";
    list<P*>::iterator pitNM=(newMesh).getIteratorBegin();
    //typename list<T*>::iterator pit=this->ps->begin();
    
    #pragma omp parallel private(pitNM)
    for( pitNM=(newMesh).getIteratorBegin(); pitNM!=(newMesh).getIteratorEnd(); ++pitNM)
    {
        #pragma omp single nowait
        {
          P* pit = this->findP((*pitNM)->getColor());
          PSet<T> pthr=pInit.subSetTotalCorrel(correlThr);
          Coord target = pthr.estimatePositionfromQuadraticFit(pit->getCoord(), pFinal, 1.*distLocal);
          LINFO << "Point " << " (" << (pit)->getCoord().getX() << "," << (pit)->getCoord().getY() << "," << (pit)->getCoord().getZ() << ") => (" << target.getX() << "," << target.getY() << "," << target.getZ() << ")";
          (*pitNM)->setCoord(target);
        }
    }
}

template<class T>
void PSet<T>::writeVTKMesh(const string fileName, const PSet<T> reference, const PSet<T> lastStep)
    {
            LDEBUG << "try pset writeVTKMesh";
			 ofstream fichier(fileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier

	        if(fichier)  // si l'ouverture a réussi
	        {
                LDEBUG << "file opened";
                fichier << "# vtk DataFile Version 3.1" << endl;
                fichier << "Mesh points" << endl;
                fichier << "ASCII" << endl;
                fichier << "DATASET STRUCTURED_GRID" << endl;
                fichier << "DIMENSIONS " << this->StackdimX << " " << this->StackdimY << " " << this->StackdimZ << endl;
                fichier << "POINTS " << this->Size() << " FLOAT" << endl;
	        	for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
	        	{
                    fichier << (*pit)->getCoord().getX() << " ";
                    fichier << (*pit)->getCoord().getY() << " ";
                    fichier << (*pit)->getCoord().getZ() << endl;
	        	}
                fichier << endl;
                LDEBUG << "points written";
                fichier << "POINT_DATA " << this->Size() << endl;
                //fichier << "SCALARS CorrelFactor FLOAT 1" << endl;
                //fichier << "LOOKUP_TABLE default" << endl;
	        	//for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
                //{
                //    fichier << (*pit)->getCorrel() << endl;
                //}
                //fichier << endl;
                fichier << "VECTORS GlobalDisplacement FLOAT" << endl;
                typename list<T*>::iterator pitRef=reference.getIteratorBegin();
            	for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
	        	{
                    fichier << ((*pit)->getCoord().getX() - (*pitRef)->getCoord().getX()) << " ";
                    fichier << ((*pit)->getCoord().getY() - (*pitRef)->getCoord().getY()) << " ";
                    fichier << ((*pit)->getCoord().getZ() - (*pitRef)->getCoord().getZ()) << endl;
                    ++pitRef;
	        	}
                //fichier << endl;
                LDEBUG << "displacement written";
                fichier << "VECTORS IncrementalDisplacement FLOAT" << endl;
                typename list<T*>::iterator pitL=lastStep.getIteratorBegin();
            	for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
	        	{
                    fichier << ((*pit)->getCoord().getX() - (*pitL)->getCoord().getX()) << " ";
                    fichier << ((*pit)->getCoord().getY() - (*pitL)->getCoord().getY()) << " ";
                    fichier << ((*pit)->getCoord().getZ() - (*pitL)->getCoord().getZ()) << endl;
                    ++pitL;
	        	}
                fichier << endl;

                fichier.close();  // on referme le fichier
	        }
	        else  // sinon
	                LERROR << "Erreur à l'ouverture !";
            LDEBUG << "vtk written";

	}



//Output to string

template<>
string PSet<Particle>::colorGlobalTostring()
	{
		 ostringstream colorLine;

		for (list<Particle*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
		{
				if(*pit)
                    colorLine << (*pit)->getGlobalColor() << " ";
				else
					colorLine << -1 << " ";
		}
		return colorLine.str();
}

template<>
string PSet<Particle>::volumeTostring()
	{
		 ostringstream colorLine;

		for (list<Particle*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
		{
				if(*pit)
                    colorLine << (*pit)->getVolume() << " ";
				else
					colorLine << -1 << " ";
		}
		return colorLine.str();
}

template<class T>
string PSet<T>::fullInfoTostring()
	{
		ostringstream colorLine;
		string header = "";
		for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
		{
				//if(header=="")
				//{
				//	header = (*pit)->writeHeader();
				//	colorLine << header << " ";
				//}
				colorLine << (*pit)->tostring() << " ";
		}
		return colorLine.str();
}

template<class T>
string PSet<T>::colorTostring()
	{
		 ostringstream colorLine;

		for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
		{
				if(*pit)
                    colorLine << (*pit)->getColor() << " ";
				else
					colorLine << -1 << " ";
		}
		return colorLine.str();
}

template<class T>
string PSet<T>::PosTostring()
{
		 ostringstream colorLine;

	for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
	     	{
	     		if(*pit)
	     			colorLine << (*pit)->getCoord().getX() << " " << (*pit)->getCoord().getY() << " " << (*pit)->getCoord().getZ() <<" ";
	     		else
	     			colorLine << -1 << " " << -1 << " " << -1 << " ";


		}
		return colorLine.str();
}

template<class T>
string PSet<T>::correlTostring()
	{
		 ostringstream colorLine;

     	for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
     	{
     		if(*pit)
     						      colorLine << (*pit)->getCorrel() << " ";
     						else
     							  colorLine << 0 << " ";
		}
		return colorLine.str();
	}


//Output to file

template<class T>
void  PSet<T>::write(const string fileName) const
	 {

			 ofstream fichier(fileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier

	        if(fichier)  // si l'ouverture a réussi
	        {
	        	for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
	        	{
	        		fichier << (*pit)->write() << endl;

	        	}
	            fichier.close();  // on referme le fichier
	        }
	        else  // sinon
	                LERROR << "Erreur à l'ouverture !";

}

template<class T>
void PSet<T>::writeToFile(const string fileDirectory, const string pointsSetName) const
	{
		    ostringstream fileName;
			fileName << fileDirectory << "/" << pointsSetName;
			 ofstream fichier(fileName.str().c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier

	        if(fichier)  // si l'ouverture a réussi
	        {
	        	fichier << this->name << endl;
	        	fichier << this->StackdimX << endl;
	    		fichier << this->StackdimY << endl;
	    		fichier << this->StackdimZ << endl;
	        	for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
	        	{
	        		if(*pit){
	        			fichier << (*pit)->getScan() << " " << (*pit)->getColor() << endl;
	        			(*pit)->writeToFile(fileDirectory);
	        		}
	        	}
	            fichier.close();  // on referme le fichier
	        }
	        else  // sinon
	                LERROR << "Erreur à l'ouverture !";
}

template<class T>
void PSet<T>::writeToFile(const string fileName)
	{

			 ofstream fichier(fileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier

	        if(fichier)  // si l'ouverture a réussi
	        {
	        	for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
	        	{
	        		fichier << (*pit)->tostring() << endl;

	        	}
	            fichier.close();  // on referme le fichier
	        }
	        else  // sinon
	                LERROR << "Erreur à l'ouverture !";

}

template<class T>
void PSet<T>::writeToCorrelManuPtsFile(const string fileName)
{

			 ofstream fichier(fileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier

	        if(fichier)  // si l'ouverture a réussi
	        {
                fichier << this->Size() << " " << this->Size() << " " << 1 << " " << 1 << endl;
	        	for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
	        	{
	        		fichier << (*pit)->getColor() << " ";
                    fichier << 1 << " ";
                    fichier << (*pit)->getCoord().getX() << " ";
                    fichier << (*pit)->getCoord().getY()*(-1) << " ";
                    fichier << (*pit)->getCoord().getZ()*(-1) << endl;
	        	}
	            fichier.close();  // on referme le fichier
	        }
	        else  // sinon
	                LERROR << "Erreur à l'ouverture !";

}

template<class T>
void PSet<T>::writeToPtsFile(const string fileName)
		{

			 ofstream fichier(fileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier

	        if(fichier)  // si l'ouverture a réussi
	        {
                fichier << this->Size() << " " << this->Size() << " " << 1 << " " << 1 << endl;
	        	for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
	        	{
	        		fichier << (*pit)->getColor() << " ";
                    fichier << (*pit)->getScan() << " ";
                    fichier << (*pit)->getCoord().getX() << " ";
                    fichier << (*pit)->getCoord().getY()*(-1) << " ";
                    fichier << (*pit)->getCoord().getZ()*(-1) << " ";
                    fichier << (*pit)->getCorrel() << endl;
	        	}
	            fichier.close();  // on referme le fichier
	        }
	        else  // sinon
	                LERROR << "Erreur à l'ouverture !";

}

template<class T>
void PSet<T>::writeVTKDisplacement(const string fileName, const PSet<T> reference, const PSet<T> lastStep)
    {
			 ofstream fichier(fileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier

	        if(fichier)  // si l'ouverture a réussi
	        {
                fichier << "# vtk DataFile Version 3.1" << endl;
                fichier << "Correl points" << endl;
                fichier << "ASCII" << endl;
                fichier << "DATASET UNSTRUCTURED_GRID" << endl;
                fichier << "POINTS " << this->Size() << " FLOAT" << endl;
	        	for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
	        	{
                    fichier << (*pit)->getCoord().getX() << " ";
                    fichier << (*pit)->getCoord().getY() << " ";
                    fichier << (*pit)->getCoord().getZ() << endl;
	        	}
                fichier << endl;
                fichier << "POINT_DATA " << this->Size() << endl;
                fichier << "SCALARS CorrelFactor FLOAT 1" << endl;
                fichier << "LOOKUP_TABLE default" << endl;
	        	for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
                {
                    fichier << (*pit)->getCorrel() << endl;
                }
                fichier << "SCALARS TotalCorrelFactor FLOAT 1" << endl;
                fichier << "LOOKUP_TABLE default" << endl;
	        	for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
                {
                    fichier << (*pit)->getTotalCorrel() << endl;
                }//fichier << endl;
                fichier << "VECTORS GlobalDisplacement FLOAT" << endl;
                typename list<T*>::iterator pitRef=reference.getIteratorBegin();
            	for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
	        	{
                    fichier << ((*pit)->getCoord().getX() - (*pitRef)->getCoord().getX()) << " ";
                    fichier << ((*pit)->getCoord().getY() - (*pitRef)->getCoord().getY()) << " ";
                    fichier << ((*pit)->getCoord().getZ() - (*pitRef)->getCoord().getZ()) << endl;
                    ++pitRef;
	        	}
                //fichier << endl;
                fichier << "VECTORS IncrementalDisplacement FLOAT" << endl;
                typename list<T*>::iterator pitL=lastStep.getIteratorBegin();
            	for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
	        	{
                    fichier << ((*pit)->getCoord().getX() - (*pitL)->getCoord().getX()) << " ";
                    fichier << ((*pit)->getCoord().getY() - (*pitL)->getCoord().getY()) << " ";
                    fichier << ((*pit)->getCoord().getZ() - (*pitL)->getCoord().getZ()) << endl;
                    ++pitL;
	        	}
                fichier << endl;

                fichier.close();  // on referme le fichier
	        }
	        else  // sinon
	                LERROR << "Erreur à l'ouverture !";

	}

template<>
void PSet<P>::writeVTKParticle(const string fileName)
{
}
	
template<>
void PSet<Particle>::writeVTKParticle(const string fileName)
{
			 ofstream fichier(fileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier

	        if(fichier)  // si l'ouverture a réussi
	        {
                fichier << "# vtk DataFile Version 3.1" << endl;
                fichier << "Particle scan " << this->name << endl;
                fichier << "ASCII" << endl;
                fichier << "DATASET UNSTRUCTURED_GRID" << endl;
                fichier << "POINTS " << this->Size() << " FLOAT" << endl;
	        	for (list<Particle*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
	        	{
                    fichier << (*pit)->getCoord().getX() << " ";
                    fichier << (*pit)->getCoord().getY() << " ";
                    fichier << (*pit)->getCoord().getZ() << endl;
	        	}
                fichier << endl;
                fichier << "POINT_DATA " << this->Size() << endl;
                fichier << "SCALARS GlobalColor INT 1" << endl;
                fichier << "LOOKUP_TABLE default" << endl;
	        	for (list<Particle*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
                {
                    fichier << (*pit)->getGlobalColor() << endl;
                }
                fichier << "SCALARS Color INT 1" << endl;
                fichier << "LOOKUP_TABLE default" << endl;
	        	for (list<Particle*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
                {
                    fichier << (*pit)->getColor() << endl;
                }
                fichier << "SCALARS CorrelFactor FLOAT 1" << endl;
                fichier << "LOOKUP_TABLE default" << endl;
	        	for (list<Particle*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
                {
                    fichier << (*pit)->getCorrel() << endl;
                }
                fichier << "SCALARS TotalCorrelFactor FLOAT 1" << endl;
                fichier << "LOOKUP_TABLE default" << endl;
	        	for (list<Particle*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
                {
                    fichier << (*pit)->getTotalCorrel() << endl;
                }
                fichier << "SCALARS Volume FLOAT 1" << endl;
                fichier << "LOOKUP_TABLE default" << endl;
	        	for (list<Particle*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
                {
                    fichier << (*pit)->getVolume() << endl;
                }
                fichier << "SCALARS ShapeFactor1 FLOAT 1" << endl;
                fichier << "LOOKUP_TABLE default" << endl;
	        	for (list<Particle*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
                {
                    fichier << (*pit)->getShapeFact() << endl;
                }
                fichier << "SCALARS ShapeFactor2 FLOAT 1" << endl;
                fichier << "LOOKUP_TABLE default" << endl;
	        	for (list<Particle*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
                {
                    fichier << (*pit)->getShapeFact2() << endl;
                }
                fichier << endl;
                fichier << "SCALARS zAngle FLOAT 1" << endl;
                fichier << "LOOKUP_TABLE default" << endl;
	        	for (list<Particle*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
                {
                    fichier << (*pit)->getZAngle() << endl;
                }
                fichier << endl;
                 fichier << "SCALARS Sphericity FLOAT 1" << endl;
                fichier << "LOOKUP_TABLE default" << endl;
	        	for (list<Particle*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
                {
                    fichier << (*pit)->getSphericity() << endl;
                }
                fichier << endl;
                fichier << "SCALARS Boundary INT 1" << endl;
                fichier << "LOOKUP_TABLE default" << endl;
	        	for (list<Particle*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
                {
                    fichier << (int)((*pit)->isBoundary()) << endl;
                }
                fichier << endl;

                fichier.close();  // on referme le fichier
	        }
	        else  // sinon
	                LERROR << "Erreur à l'ouverture !";

	}

template<>
void PSet<P>::writeVTKParticleSorted(const string fileName, int nb)
{
}

template<>
void PSet<Particle>::writeVTKParticleSorted(const string fileName, int nb)
{
			 ofstream fichier(fileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier

	        if(fichier)  // si l'ouverture a réussi
	        {
                fichier << "# vtk DataFile Version 3.1" << endl;
                fichier << "Particle scan " << this->name << endl;
                fichier << "ASCII" << endl;
                fichier << "DATASET UNSTRUCTURED_GRID" << endl;
                fichier << "POINTS " << nb << " FLOAT" << endl;
                for(int i=1; i<=nb; i++)
                {
                    if(this->hasGlobalColor(i))
                    {
                        fichier << this->findGlobalColorP(i)->getCoord().getX() << " ";
                        fichier << this->findGlobalColorP(i)->getCoord().getY() << " ";
                        fichier << this->findGlobalColorP(i)->getCoord().getZ() << endl;
	        	    }
                    else
                        fichier << "0. 0. 0." << endl;
                }
                fichier << endl;
                fichier << "POINT_DATA " << nb << endl;
                fichier << "SCALARS GlobalColor INT 1" << endl;
                fichier << "LOOKUP_TABLE default" << endl;
                for(int i=1; i<=nb; i++)
                {
                    if(this->hasGlobalColor(i))
                        fichier << this->findGlobalColorP(i)->getGlobalColor() << endl;
                    else
                        fichier << "-1" << endl;
                }
                fichier << "SCALARS Color INT 1" << endl;
                fichier << "LOOKUP_TABLE default" << endl;
                for(int i=1; i<=nb; i++)
                {
                    if(this->hasGlobalColor(i))
                        fichier << this->findGlobalColorP(i)->getColor() << endl;
                    else
                        fichier << "0" << endl;
                }
                fichier << "SCALARS CorrelFactor FLOAT 1" << endl;
                fichier << "LOOKUP_TABLE default" << endl;
                for(int i=1; i<=nb; i++)
                {
                    if(this->hasGlobalColor(i))
                        fichier << this->findGlobalColorP(i)->getCorrel() << endl;
                    else
                        fichier << "0." << endl;
                }
                fichier << "SCALARS TotalCorrelFactor FLOAT 1" << endl;
                fichier << "LOOKUP_TABLE default" << endl;
                for(int i=1; i<=nb; i++)
                {
                    if(this->hasGlobalColor(i))
                        fichier << this->findGlobalColorP(i)->getTotalCorrel() << endl;
                    else
                        fichier << "0." << endl;
                }
                fichier << "SCALARS Volume FLOAT 1" << endl;
                fichier << "LOOKUP_TABLE default" << endl;
                for(int i=1; i<=nb; i++)
                {
                    if(this->hasGlobalColor(i))
                        fichier << this->findGlobalColorP(i)->getVolume() << endl;
                    else
                        fichier << "0." << endl;
                }
                fichier << "SCALARS ShapeFactor1 FLOAT 1" << endl;
                fichier << "LOOKUP_TABLE default" << endl;
                for(int i=1; i<=nb; i++)
                {
                    if(this->hasGlobalColor(i))
                        fichier << this->findGlobalColorP(i)->getShapeFact() << endl;
                    else
                        fichier << "0." << endl;
                }
                fichier << "SCALARS ShapeFactor2 FLOAT 1" << endl;
                fichier << "LOOKUP_TABLE default" << endl;
                for(int i=1; i<=nb; i++)
                {
                    if(this->hasGlobalColor(i))
                        fichier << this->findGlobalColorP(i)->getShapeFact2() << endl;
                    else
                        fichier << "0." << endl;
                }
                fichier << endl;
                fichier << "SCALARS zAngle FLOAT 1" << endl;
                fichier << "LOOKUP_TABLE default" << endl;
                for(int i=1; i<=nb; i++)
                {
                    if(this->hasGlobalColor(i))
                        fichier << this->findGlobalColorP(i)->getZAngle() << endl;
                    else
                        fichier << "0." << endl;
                }
                fichier << endl;
                 fichier << "SCALARS Sphericity FLOAT 1" << endl;
                fichier << "LOOKUP_TABLE default" << endl;
                for(int i=1; i<=nb; i++)
                {
                    if(this->hasGlobalColor(i))
                        fichier << this->findGlobalColorP(i)->getSphericity() << endl;
                    else
                        fichier << "0." << endl;
                }
                fichier << endl;
                fichier << "SCALARS Boundary INT 1" << endl;
                fichier << "LOOKUP_TABLE default" << endl;
                for(int i=1; i<=nb; i++)
                {
                    if(this->hasGlobalColor(i))
                        fichier << (int)(this->findGlobalColorP(i)->isBoundary()) << endl;
                    else
                        fichier << "0" << endl;
                }
                fichier << endl;
                fichier << "SCALARS ConnectingTo INT 1" << endl;
                fichier << "LOOKUP_TABLE default" << endl;
                for(int i=1; i<=nb; i++)
                {
                    if(this->hasGlobalColor(i))
                        fichier << this->findGlobalColorP(i)->getFather() << endl;
                    else
                        fichier << "-1" << endl;
                }
 
                fichier.close();  // on referme le fichier
	        }
	        else  // sinon
	                LERROR << "Erreur à l'ouverture !";
}


template<class T>
void PSet<T>::colorToFile(string fileName)
	{

		 ofstream fichier(fileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier

        if(fichier)  // si l'ouverture a réussi
        {
        	for (typename list<T*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
        	{
        		fichier << (*pit)->getColor() << endl;
        	}

                fichier.close();  // on referme le fichier
        }
        else  // sinon
                LERROR << "Erreur à l'ouverture !";

}
template<class T>
void PSet<T>::writeCastemINP(const string directory, const string nameprefix, PSet<T> &ref0) {

        ostringstream inpName;
        inpName << directory << nameprefix << ".inp";
        LDEBUG << "Try to write CASTEM INP file " << inpName.str();

        ofstream fichier(inpName.str().c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier

	    if(fichier)  // si l'ouverture a réussi
	    {
            LDEBUG << "file opened";
            fichier << this->Size() << " 0 3 0 0" << endl;
	        	//for (list<P*>::const_iterator pit=this->points->getIteratorBegin(); pit!=this->points->getIteratorEnd(); ++pit)
	        	for (typename list<T*>::const_iterator pit=ref0.getIteratorBegin(); pit!=ref0.getIteratorEnd(); ++pit)
	        	{
                    fichier << (*pit)->getColor() << " ";
                    fichier << (*pit)->getCoord().getX() << " ";
                    fichier << (*pit)->getCoord().getY() << " ";
                    fichier << (*pit)->getCoord().getZ() << endl;
	        	}
                fichier << "3 1 1 1" << endl;
                fichier << "UX," << endl;
                fichier << "UY," << endl;
                fichier << "UZ," << endl;
                typename list<T*>::const_iterator pitRef=ref0.getIteratorBegin();
	        	for (typename list<T*>::const_iterator pit=this->getIteratorBegin(); pit!=this->getIteratorEnd(); ++pit)
	        	{
                    fichier << (*pit)->getColor() << " ";
                    fichier << ((*pit)->getCoord().getX() - (*pitRef)->getCoord().getX()) << " ";
                    fichier << ((*pit)->getCoord().getY() - (*pitRef)->getCoord().getY()) << " ";
                    fichier << ((*pit)->getCoord().getZ() - (*pitRef)->getCoord().getZ()) << endl;
                    ++pitRef;
	        	}

                fichier.close();  
    
          }
    }

template<>
void PSet<Particle>::writeParamWithBackwardPosition(const string fileName)
{

			 ofstream fichier(fileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier

	        if(fichier)  // si l'ouverture a réussi
	        {
	        	for (list<Particle*>::iterator pit=this->ps->begin(); pit!=this->ps->end(); ++pit)
	        	{
	        		fichier << (*pit)->getParam() << " ";
                    fichier << (*pit)->getCoord().getX() << " ";
                    fichier << (*pit)->getCoord().getY() << " ";
                    fichier << (*pit)->getCoord().getZ() << endl;

	        	}
	            fichier.close();  // on referme le fichier
	        }
	        else  // sinon
	                LERROR << "Erreur à l'ouverture !";


}

template<>
void PSet<P>::writeParamWithBackwardPosition(const string fileName)
{

}

template class PSet<P>;
//template class PSet<Point>;
template class PSet<Particle>;


