#include "Param.h"

#include "conf_reader.hpp"

using namespace std;


void generateInputFile(const char *prog, const char *inputName) {

LDEBUG << "Generate configuration file";
LDEBUG << "Input file " << inputName;

ofstream fichier(inputName, ios::out | ios::app);  //déclaration du flux et ouverture du fichier
if(fichier)  // si l'ouverture a réussi
{

//inputOuput
fichier << "[inputOutput]\n";
fichier << "# Global working directory\n";
fichier << "directory=./\n";
fichier << "# Directory containing input files\n";
fichier << "inputDirectory=./input/\n";
fichier << "# Directory that will contain output files\n";
fichier << "outputDirectory=./output/\n";
fichier << "# Main prefix (used for extra file saving)\n";
fichier << "mainPrefix=\n";
fichier << "# 3D images prefix\n";
fichier << "prefixImage=\n";
fichier << "# 3D images suffix\n";
fichier << "suffixImage=\n";
fichier << "# Points to track file prefix\n";
fichier << "prefixPtrs=\n";
fichier << "# Points to track file suffix\n";
fichier << "suffixPtrs=\n";
//fichier << "# Points to track file prefix\n";
//fichier << "prefixDepl=";
//fichier << "suffixDepl=";
//fichier << "prefixMask=";
//fichier << "suffixMask=";
fichier << "# Mesh file prefix\n";
fichier << "prefixMesh=\n";
fichier << "# Mesh file suffix\n";
fichier << "suffixMesh=\n";
fichier << "# Guiding points file prefix (already performed correlation)\n";
fichier << "prefixPgs=\n";
fichier << "# Guiding points file suffix (already performed correlation)\n";
fichier << "suffixPgs=\n";
//fichier << "meshName=";
fichier << "# Theoretical skeleton filename\n";
fichier << "numericalSqueleton=\n";
fichier << "# Deformation of points to track file prefix (output)\n";
fichier << "prefixPtrsDef=\n";
fichier << "# Deformation of points to track file suffix (output)\n";
fichier << "suffixPtrsDef=\n\n";

//volumes
fichier << "[volumes]\n";
fichier << "# Number of fisrt volume\n";
fichier << "firstVol=1\n";
fichier << "# Number of last volume\n";
fichier << "lastVol=31\n";
fichier << "# Step between volumes numbers\n";
fichier << "interVol=2\n";
fichier << "# Skipped volumes numbers (number, space separated)\n";
fichier << "skippedVol=\n\n";

//Options
fichier << "[options]\n";
fichier << "# Appends results on the fly in result files (1=append; 0=write new file after computation)\n";
fichier << "append=0\n";
if(prog=="PT")
{
    fichier << "# Do particles tracking using DIC (1=DIC; 0=Use parameters file and/or already performed correlation)\n";
    fichier << "DIC=1\n";
}
fichier << "# Use an already performed correlation to estimate starting guest position (1=yes; 0=use point position in first volume as guest position in second volume)\n";
fichier << "preCorrel=0\n";
fichier << "# Use surounding points with already performed tracking to estimate starting guest position (1=yes; 0=use point position in first volume as guest position in second volume)\n";
fichier << "rigidGuiding=1\n";
if(prog=="PT")
{
    fichier << "# Safest tracking : just find backward position in initial state of each particle (1=yes; 0=other approach)\n";
    fichier << "backwardPosition=0\n";
    fichier << "# Some particles might disappear in some volumes (1=yes; 0=constant number of particles)\n";
    fichier << "missingParticles=0\n";
}
fichier << "# Remove points for which correlation factor do not exceed correlThr (1=yes; 0=keep all points)\n";
fichier << "rmbad=0\n";
if((prog=="Tracking")||(prog=="TrackingR"))
{
fichier << "# Use parameter file as starting points for correlation (1=tracked points are located at center of mass of particles; 0=use other points with .pts format)\n";
fichier << "initFromParticles=0\n";
}
fichier << "# Just perform post-processing operations (1=just post-process; 0=perform computation and then post-process)\n";
fichier << "postProcess=0\n";
//fichier << "# Use anisotropic correlation window (1=anisotropic window; 0=cubic window)\n";
//fichier << "aniso=1\n";
fichier << "# Account for rotation around one axis for correlation (1=extra degree of freedom with rotation; 0=translation only)\n";
fichier << "rota=0\n";
fichier << "# Restart computation at a given volume number (1=start from begining; integer i=restart from i)\n";
fichier << "restart=1\n";
fichier << " # Reverse order for correlation (1=start from final volume; 0=start from initial volume)\n";
fichier << "reverseOrder=0\n";
//fichier << "# Interpolate displacement field and generate 3D images (1=yes; 0=no)\n";
//fichier << "deplfield=0\n";
//fichier << "# Compute various strain field and generate 3D images (1=yes; 0=no)\n";
//fichier << "exx=0\n";
//fichier << "eyy=0\n";
//fichier << "ezz=0\n";
fichier << "# Compute mesh evolution (1=yes; 0=no)\n";
fichier << "mesh=1\n\n";

//GPU
fichier << "[GPU]\n";
fichier << "# Use texture on GPU (1/0 ; required when rota=1)\n";
fichier << "usetex=1\n\n";

//Correlation
fichier << "[correlation]\n";
//fichier << "# Max admissible motion in between guest position and optimized position \n";
//fichier << "thrXYZ=10\n";
fichier << "# Max admissible motion among X direction between guest position and best position\n";
fichier << "thrX=10\n";
fichier << "# Max admissible motion among Y direction between guest position and best position\n";
fichier << "thrY=10\n";
fichier << "# Max admissible motion among Z direction between guest position and best position\n";
fichier << "thrZ=10\n";
fichier << "# Multiply thrX for first point research (guest position is point position)\n";
fichier << "initThrXfactor=5\n";
fichier << "# Multiply thrY for first point research (guest position is point position)\n";
fichier << "initThrYfactor=5\n";
fichier << "# Multiply thrZ for first point research (guest position is point position)\n";
fichier << "initThrZfactor=5\n";
fichier << "# Threshold on correlation factor for confidence in the correlation (0<correlThr<1)\n";
fichier << "correlThr=0.5\n";
//fichier << "# Size of correlation window in case of isotropic window (aniso=0)\n";
//fichier << "correlDomain=7\n";
fichier << "# Dimension among X direction of correlation window\n";
fichier << "correlDomainX=7\n";
fichier << "# Dimension among Y direction of correlation window\n";
fichier << "correlDomainY=7\n";
fichier << "# Dimension among Z direction of correlation window\n";
fichier << "correlDomainZ=7\n";
fichier << "# Rotation axis (0=X, 1=Y, 2=Z)\n";
fichier << "rotAxis=2\n";
fichier << "# Number of rotation steps when accounting for rotation (rota=1)\n";
fichier << "nrot=10\n";
fichier << "# Step of the rotation in degree when accounting for rotation (rota=1)\n";
fichier << "deltaTheta=2.5\n";
//fichier << "# Minimum distance between generated sub-nodes in lattice structures\n";
//fichier << "nodesSpacing=20\n\n";

//Regularisation
fichier << "[regularisation]\n";
fichier << "# Optimize the position (use a regularisation loop for guest position using interpolation of valid points) \n";
fichier << "optimize=0\n";
//fichier << "# Max admissible motion in between guest position and optimized position \n";
//fichier << "optiThrXYZ=10\n";
fichier << "# Max admissible motion among X direction between guest position and optimized position\n";
fichier << "optiThrX=10\n";
fichier << "# Max admissible motion among Y direction between guest position and optimized position\n";
fichier << "optiThrY=10\n";
fichier << "# Max admissible motion among Z direction between guest position and optimized position\n";
fichier << "optiThrZ=10\n";
fichier << "# Max radius for regularisation\n";
fichier << "regularisationDistanceThreshold=150\n";
fichier << "# Usually regularisationDistanceThreshold/3\n";
fichier << "regularisationDistance=50\n";

}
else  // sinon
   LERROR << "Erreur à l'ouverture !";
}



void readConfig (const char *fileName, Parameters *param) {

LDEBUG << "Read configuration file";
LDEBUG << "Input from " << fileName;

//conf_reader::File file("../test2/in.conf");
LDEBUG << "Create conf_reader";
conf_reader::File file(fileName);

LDEBUG << "Read file";
file.read_file();

LDEBUG << "Display file";
file.show_in_log();
std::string une_chaine ;//dummy string

LDEBUG << "Parse file";

//inputOuput
une_chaine = file.get_string("inputOutput","directory");
strcpy ( param->Directory, une_chaine.c_str() );
une_chaine = file.get_string("inputOutput","inputDirectory");
strcpy ( param->inputDir, une_chaine.c_str() );
une_chaine = file.get_string("inputOutput","outputDirectory");
strcpy ( param->outputDir, une_chaine.c_str() );
une_chaine = file.get_string("inputOutput","mainPrefix");
strcpy ( param->prefix, une_chaine.c_str() );
une_chaine = file.get_string("inputOutput","prefixImage");
strcpy ( param->prefiximage, une_chaine.c_str() );
une_chaine = file.get_string("inputOutput","suffixImage");
strcpy ( param->suffiximage, une_chaine.c_str() );
une_chaine = file.get_string("inputOutput","prefixPtrs");
strcpy ( param->prefixptrs, une_chaine.c_str() );
une_chaine = file.get_string("inputOutput","suffixPtrs");
strcpy ( param->suffixptrs, une_chaine.c_str() );
//une_chaine = file.get_string("inputOutput","prefixDepl");
//strcpy ( param->prefixdepl, une_chaine.c_str() );
//une_chaine = file.get_string("inputOutput","suffixDepl");
//strcpy ( param->suffixdepl, une_chaine.c_str() );
//une_chaine = file.get_string("inputOutput","prefixMask");
//strcpy ( param->prefixmask, une_chaine.c_str() );
//une_chaine = file.get_string("inputOutput","suffixMask");
//strcpy ( param->suffixmask, une_chaine.c_str() );
une_chaine = file.get_string("inputOutput","prefixMesh");
strcpy ( param->prefixmesh, une_chaine.c_str() );
une_chaine = file.get_string("inputOutput","suffixMesh");
strcpy ( param->suffixmesh, une_chaine.c_str() );
une_chaine = file.get_string("inputOutput","prefixPgs");
strcpy ( param->prefixpgs, une_chaine.c_str() );
une_chaine = file.get_string("inputOutput","suffixPgs");
strcpy ( param->suffixpgs, une_chaine.c_str() );
//une_chaine = file.get_string("inputOutput","meshName");
//strcpy ( param->meshname, une_chaine.c_str() );
une_chaine = file.get_string("inputOutput","numericalSqueleton");
strcpy ( param->theoreticalSqueleton, une_chaine.c_str() );
une_chaine = file.get_string("inputOutput","prefixPtrsDef");
strcpy ( param->prefixptrsdef, une_chaine.c_str() );
une_chaine = file.get_string("inputOutput","suffixPtrsDef");
strcpy ( param->suffixptrsdef, une_chaine.c_str() );
LDEBUG << "inputOutut ok";

//volumes
param->firstvol = file.get_int("volumes","firstVol");
param->lastvol = file.get_int("volumes","lastVol");
param->intervol = file.get_int("volumes","interVol");
param->skippedvol = file.get_int_vector("volumes","skippedVol");
LINFO << param->skippedvol.size()  << " skipped volumes";
LDEBUG << "Volumes ok";

//Options
param->append = (bool)(file.get_int("options","append"));
param->DIC = (bool)(file.get_int("options","DIC"));
param->precorrel = (bool)(file.get_int("options","preCorrel"));
param->rigidguiding = (bool)(file.get_int("options","rigidGuiding"));
param->backwardposition = (bool)(file.get_int("options","backwardPosition"));
param->missingparticles = (bool)(file.get_int("options","missingParticles"));
param->rmbad = (bool)(file.get_int("options","rmbad"));
param->initfromparam = (bool)(file.get_int("options","initFromParticles"));
param->postprocess = (bool)(file.get_int("options","postProcess"));
//param->aniso = (bool)(file.get_int("options","aniso"));
param->aniso = true;
param->rota = (bool)(file.get_int("options","rota"));
param->restart = file.get_int("options","restart");
param->reverse = (bool)(file.get_int("options","reverseOrder"));
//param->deplfield = (bool)(file.get_int("options","deplfield"));
//param->deffield = (bool)(file.get_int("options","deffield"));
//param->exx = (bool)(file.get_int("options","exx"));
//param->eyy = (bool)(file.get_int("options","eyy"));
//param->ezz = (bool)(file.get_int("options","ezz"));
param->mesh = (bool)(file.get_int("options","mesh"));
LDEBUG << "Options ok";

//GPU
param->usetex = (bool)(file.get_int("GPU","usetex"));
LDEBUG << "GPU ok";

//Correlation
//param->thrXYZ = file.get_double("correlation","thrXYZ");
param->thrX = file.get_double("correlation","thrX");
param->thrY = file.get_double("correlation","thrY");
param->thrZ = file.get_double("correlation","thrZ");
param->thrXYZ = param->thrX;
param->initThrXfactor = file.get_int("correlation","initThrXfactor");
param->initThrYfactor = file.get_int("correlation","initThrYfactor");
param->initThrZfactor = file.get_int("correlation","initThrZfactor");
param->correlThr = file.get_double("correlation","correlThr");
//param->correlDomain = file.get_int("correlation","correlDomain");
param->correlDomainX = file.get_int("correlation","correlDomainX");
param->correlDomainY = file.get_int("correlation","correlDomainY");
param->correlDomainZ = file.get_int("correlation","correlDomainZ");
param->correlDomain = param->correlDomainX;
param->rotAxis = file.get_int("correlation","rotAxis");
param->nrot = file.get_int("correlation","nrot");
param->deltaTheta = file.get_double("correlation","deltaTheta");
param->nodesSpacing = file.get_double("correlation","nodesSpacing");
LDEBUG << "Correlation ok";

//Regularisation
param->optimize = (bool)(file.get_int("regularisation","optimize"));
//param->optiThrXYZ = file.get_double("regularisation","optiThrXYZ");
param->optiThrX = file.get_double("regularisation","optiThrX");
param->optiThrY = file.get_double("regularisation","optiThrY");
param->optiThrZ = file.get_double("regularisation","optiThrZ");
param->optiThrXYZ = param->optiThrX;
param->regulThrDist = file.get_double("regularisation","regularisationDistanceThreshold");
param->regulDist = file.get_double("regularisation","regularisationDistance");
LDEBUG << "Regularisation ok";

LDEBUG << "Params seem ok";

}


void init(string Directory)
{

    time_t rawtime;
    struct tm * timeinfo;
    char buffer [80];

    time (&rawtime);
    timeinfo = localtime (&rawtime);

    strftime(buffer,80,"PT4D_%Y_%m_%d_%H_%M_%S",timeinfo);

    log4cpp::Appender *appender1 = new log4cpp::OstreamAppender("console", &std::cout);
    log4cpp::PatternLayout *defaultLayout = new log4cpp::PatternLayout();
    defaultLayout->setConversionPattern("%m%n");
    appender1->setLayout(defaultLayout);
    //appender1.layout.setConversionPattern("%m")

    string logfile = Directory + buffer + ".log";
    string lastlogfile = Directory + "out.log";
    string debuglogfile = Directory + buffer + "_debug.log";
    string lastdebuglogfile = Directory + "debug.log";
    ifstream f(lastlogfile.c_str());
    if(f.good())
        std::remove(lastlogfile.c_str());
    ifstream g(lastdebuglogfile.c_str());
    if(g.good())
        std::remove(lastdebuglogfile.c_str());

    log4cpp::Appender *appender2 = new log4cpp::FileAppender("default", logfile);
    appender2->setLayout(defaultLayout);
    log4cpp::Appender *appender5 = new log4cpp::FileAppender("default", lastlogfile);
    appender5->setLayout(defaultLayout);

    log4cpp::Appender *appender3 = new log4cpp::FileAppender("debug", debuglogfile);
    log4cpp::PatternLayout *dateLayout = new log4cpp::PatternLayout();
    dateLayout->setConversionPattern("%d [%p] %m%n");
    appender3->setLayout(dateLayout);

    log4cpp::Appender *appender4 = new log4cpp::FileAppender("debug", lastdebuglogfile);
    appender4->setLayout(dateLayout);


     //log4cpp::Category& debuglogger = log4cpp::Category::getRoot();
    //log4cpp::Category& debuglogger = log4cpp::Category::getInstance(std::string("debuglogger"));
    //debuglogger = log4cpp::Category::getInstance(std::string("debuglogger"));
    debuglogger.setPriority(log4cpp::Priority::DEBUG);
    debuglogger.addAppender(appender3);
    debuglogger.addAppender(appender4);

    //log4cpp::Category& logger = log4cpp::Category::getRoot();
    //logger = log4cpp::Category::getInstance(std::string("debuglogger.logger"));
    logger.setPriority(log4cpp::Priority::INFO);
    logger.addAppender(appender1);
    logger.addAppender(appender2);
    logger.addAppender(appender5);

 // use of functions for logging messages
    PDEBUG("Debug log file");
    LINFO << "Log saved in file " <<  logfile;

    TIFFSetWarningHandler(DummyHandler);

}

void finalize()
{
    cout << "Close log files" << endl;
    log4cpp::Category::shutdown();
    //logger.shutdown();
    
    //debuglogger.shutdown();
    cout << "Log files closed" << endl;
}

