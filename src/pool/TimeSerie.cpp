#include "TimeSerie.h"

using namespace std;


template <typename Ptr, typename Pg>
TimeSerie<Ptr,Pg>::TimeSerie() {
    this->inputDir = "./";
    this->outputDir = "./";

    this->scanNumber = vector<int>();
    this->scanNumberStr = vector<string>();
    
    this->names = vector<string>();
	
    this->pgsDir = "";
    this->pgsPrefix = vector<string>();
    this->pgsFilesNames = vector<string>();
    this->pgsData = vector<vector<Pg*> >();
    this->pgs = vector<PSet<Pg> >();

    this->ptrsDir = "";
    this->ptrsPrefix = vector<string>();
    this->ptrsFilesNames = vector<string>();
    this->ptrsDefDir = "";
    this->ptrsDefPrefix = vector<string>();
    this->ptrsDefFilesNames = vector<string>();
    this->ptrsData = vector<vector<Ptr*> >();
    this->ptrs = vector<PSet<Ptr> >();

    this->resptrscorDir = "";
    this->resptrscorPrefix = vector<string>();
    this->resptrscorFilesNames = vector<string>();

    this->resptrsDir = "";
    this->resptrsPrefix = vector<string>();
    this->resptrsFilesNames = vector<string>();
    this->resptrs = vector<PSet<Ptr> >();

    this->meshesDir = "";
    this->meshesPrefix = vector<string>();
    this->meshesFilesNames = vector<string>();
    this->mesh = vector<Mesh*>();

    this->resmeshesDir = "";
    this->resmeshesPrefix = vector<string>();
    this->resmeshesFilesNames = vector<string>();
    this->resmesh = vector<Mesh*>();
     
    this->preCorrelDeplZ = vector<vector<vector<double> > >();

    this->imagesPrefix = vector<string>();
    this->imagesFilesNames = vector<string>();
    this->maskFilesNames = vector<string>();

    this->outputprefixFilesNames = vector<string>();
    
    this->param = new Parameters();
}


template <typename Ptr, typename Pg>
TimeSerie<Ptr,Pg>::TimeSerie(Parameters* param) {
    
    LDEBUG << "Initate create TimeSerie";
    this->inputDir = param->inputDir;
    this->outputDir = param->outputDir;
    //checkAndCreateDir(string(this->param->Directory) + this->param->outputDir);
    
    this->scanNumber = vector<int>();
    this->scanNumberStr = vector<string>();
    
    this->names = vector<string>();
	
    this->pgsDir = "";
    this->pgsPrefix = vector<string>();
    this->pgsFilesNames = vector<string>();
    this->pgsData = vector<vector<Pg*> >();
    this->pgs = vector<PSet<Pg> >();

    this->ptrsDir = "";
    this->ptrsPrefix = vector<string>();
    this->ptrsFilesNames = vector<string>();
    this->ptrsDefDir = "";
    this->ptrsDefPrefix = vector<string>();
    this->ptrsDefFilesNames = vector<string>();
    this->ptrsData = vector<vector<Ptr*> >();
    this->ptrs = vector<PSet<Ptr> >();
    
    this->resptrscorDir = "";
    this->resptrscorPrefix = vector<string>();
    this->resptrscorFilesNames = vector<string>();

    this->resptrsDir = "";
    this->resptrsPrefix = vector<string>();
    this->resptrsFilesNames = vector<string>();
    this->resptrs = vector<PSet<Ptr> >();

    this->meshesDir = "";
    this->meshesPrefix = vector<string>();
    this->meshesFilesNames = vector<string>();
    this->mesh = vector<Mesh*>();
   
    this->resmeshesDir = "";
    this->resmeshesPrefix = vector<string>();
    this->resmeshesFilesNames = vector<string>();
    this->resmesh = vector<Mesh*>();
    
 
    this->preCorrelDeplZ = vector<vector<vector<double> > >();
    
    this->imagesPrefix = vector<string>();
    this->imagesFilesNames = vector<string>();
    this->maskFilesNames = vector<string>();
    
    this->outputprefixFilesNames = vector<string>();
    
    LDEBUG << "Initated field";
    
    this->param = param;
    
    LDEBUG << "Set param";
   
    for(int i=this->param->firstvol; i!=this->param->lastvol+this->param->intervol; i+=this->param->intervol)
    {
      char numscan[3];
      sprintf(numscan,"%03d",i);
      bool skip=false;
      for (vector<int>::iterator it = this->param->skippedvol.begin(); it!=this->param->skippedvol.end(); ++it) {
        if(i==*it)
          skip=true;
      }
      //if(find(this->param->skippedvol.begin(), this->param->skippedvol.end(),i)!=this->param->skippedvol.end())
      if(skip)
      {
            LINFO << "Skipping scan " << numscan;
      }
      else
      {
            this->scanNumberStr.push_back(string(numscan));
            this->scanNumber.push_back(i);
      }
    }
	
    this->startExpe = ceil((1.*this->param->restart-this->param->firstvol)/this->param->intervol);
    this->numberExpe = this->scanNumber.size();
    
    LDEBUG << "Get numbers";

    for(int i=0; i<this->scanNumber.size(); ++i)
    {
        string name = string(this->param->prefix) + this->scanNumberStr[i];
        this->names.push_back(name);
        
        this->pgsDir = string(this->param->Directory) + this->param->inputDir;

        string pgsNamePrefix = this->param->prefixpgs + this->scanNumberStr[i];
        this->pgsPrefix.push_back(pgsNamePrefix);

        string pgsName = string(this->pgsDir) + pgsNamePrefix + this->param->suffixpgs;
        this->pgsFilesNames.push_back(pgsName);
        
        this->ptrsDir = string(this->param->Directory) + this->param->inputDir;

        string ptrsNamePrefix = this->param->prefixptrs + this->scanNumberStr[i];
        this->ptrsPrefix.push_back(ptrsNamePrefix);
        
        string ptrsName = string(this->ptrsDir) + ptrsNamePrefix + this->param->suffixptrs;
        this->ptrsFilesNames.push_back(ptrsName);
        
        this->ptrsDefDir = string(this->param->Directory) + this->param->outputDir;

        string ptrsDefNamePrefix = this->param->prefixptrsdef + this->scanNumberStr[i];
        this->ptrsDefPrefix.push_back(ptrsDefNamePrefix);

        string ptrsDefName = string(this->ptrsDefDir) + ptrsDefNamePrefix + this->param->suffixptrsdef;
        this->ptrsDefFilesNames.push_back(ptrsDefName);
         
        this->resptrscorDir = string(this->param->Directory) + this->param->outputDir;

        string resptrscorNamePrefix = "";
        string resptrscorName = "";
        if((i>0)&&(i<=this->startExpe))
        {
            resptrscorNamePrefix = string(this->param->prefixptrs) + this->scanNumberStr[i] + string("_cor");
            this->resptrscorPrefix.push_back(resptrscorNamePrefix);
            resptrscorName = string(this->resptrscorDir) + resptrscorNamePrefix + this->param->suffixptrs;
        }
        else
        {
            resptrscorNamePrefix = string(this->param->prefixptrs) + this->scanNumberStr[i];
            this->resptrscorPrefix.push_back(resptrscorNamePrefix);
            resptrscorName = string(this->resptrscorDir) + resptrscorNamePrefix + this->param->suffixptrs;
        }

        this->resptrscorFilesNames.push_back(resptrscorName);

        this->resptrsDir = string(this->param->Directory) + this->param->outputDir;

        string resptrsNamePrefix = this->param->prefixptrs + this->scanNumberStr[i];
        this->resptrsPrefix.push_back(ptrsNamePrefix);
        
        string resptrsName = string(this->resptrsDir) + resptrsNamePrefix + this->param->suffixptrs;
        this->resptrsFilesNames.push_back(resptrsName);
        
        this->meshesDir = string(this->param->Directory) + this->param->inputDir;
        
        string meshesNamePrefix = this->param->prefixmesh + this->scanNumberStr[i];
        this->meshesPrefix.push_back(meshesNamePrefix);

        string meshName = string(this->meshesDir) + meshesNamePrefix + this->param->suffixmesh;
        this->meshesFilesNames.push_back(meshName);
        
        this->resmeshesDir = string(this->param->Directory) + this->param->outputDir;
        
        string resmeshesNamePrefix = this->param->prefixmesh + this->scanNumberStr[i];
        this->resmeshesPrefix.push_back(resmeshesNamePrefix);

        string resmeshName = string(this->resmeshesDir) + resmeshesNamePrefix + this->param->suffixmesh;
        this->resmeshesFilesNames.push_back(resmeshName);
         
        string imageName = string(this->param->Directory) + this->param->inputDir +  this->param->prefiximage + this->scanNumberStr[i] + this->param->suffiximage;
        this->imagesFilesNames.push_back(imageName);
        string maskName = string(this->param->Directory) + this->param->inputDir + this->param->prefixmask + this->scanNumberStr[i] + this->param->suffixmask;
        this->maskFilesNames.push_back(maskName);
        string outputprefixName = string(this->param->Directory) + this->param->outputDir + this->param->prefiximage + this->scanNumberStr[i];
        this->outputprefixFilesNames.push_back(imageName);
    }
    LDEBUG << "Set all";
}


template <typename Ptr, typename Pg>
void TimeSerie<Ptr,Pg>::readPgs() {
	this->readP(this->pgs, this->pgsData, this->pgsFilesNames, true);
}

template <typename Ptr, typename Pg>
void TimeSerie<Ptr,Pg>::readPtrs() {
	this->readP(this->ptrs, this->ptrsData, this->ptrsFilesNames, false);
}

template <typename Ptr, typename Pg>
void TimeSerie<Ptr,Pg>::readP(vector<PSet< Particle> > & particles, vector<vector<Particle*> > & particlesData, vector<string> & particlesFilesNames, bool Pgs)
{
    if(this->param->postprocess)
    {
	        LINFO << "Read particles with already performed correlation";
	        LINFO << "Reading particles files ...";
			readVectorPSet(string(this->param->Directory)+"TrackingFiles/", string(this->param->prefix)+"_param", particles, particlesData);
    }
    else
    {
        LINFO << "Reading particles files ...";
	if(this->param->missingparticles)
        {
   	    LINFO << "Looking for scan " << this->scanNumberStr[0] << " parameters in file " <<  particlesFilesNames[0];
	    vector<Particle*> raw = readParamFile(particlesFilesNames[0],0);
	    particlesData.push_back(raw);
	    PSet<Particle> * rawps = new PSet<Particle> (this->imagesFilesNames.at(0), raw);
	    rawps->initGlobalColor();
	    rawps->initCorrel(1);
            particles.push_back((*rawps).rankByVolume());
            int nbpart=raw.size();    
	    for(int i=1; i<this->scanNumber.size(); ++i)
	    {   
    	      LINFO << "Looking for scan " << this->scanNumberStr[i] << " parameters in file " <<  particlesFilesNames[i];
	      vector<Particle*> raw = readParamFileConstrained(particlesFilesNames[i],i,nbpart);
	      particlesData.push_back(raw);
	      PSet<Particle> * rawps = new PSet<Particle> (this->imagesFilesNames.at(i), raw);
	      rawps->initCorrel(0);
	      particles.push_back((*rawps).rankByVolume());
	    }
       }
       else
       {
          for(int i=0; i<this->scanNumber.size(); ++i)
          {   
  	      LINFO << "Looking for scan " << this->scanNumberStr[i] << " parameters in file " <<  particlesFilesNames[i];
	      vector<Particle*> raw = readParamFile(particlesFilesNames[i],i);
	      particlesData.push_back(raw);
	      PSet<Particle> * rawps = new PSet<Particle> (this->imagesFilesNames.at(i), raw);
	      if(i==0)
              {
                rawps->initGlobalColor();
	        rawps->initCorrel(1);
	      }
              else
              {
                 rawps->resetGlobalColor();
              }
              particles.push_back((*rawps).rankByVolume());
	  }
        }
	//writeVectorParticlesSet(string(this->param->Directory)+"TrackingFiles/", string(this->param->prefix)+"_param", this->particlesData);
    }
}


template <typename Ptr, typename Pg>
void TimeSerie<Ptr,Pg>::readP(vector<PSet< P> > & points, vector<vector<P*> > & pointsData, vector<string> & pointsFilesNames, bool Pgs)
{
	if(param->postprocess||Pgs)
    {
        LINFO << "Read points with already performed correlation";
        LINFO << "Reading pgs files ...";
   		vector<P*> raw1 = readCorrelManuPtsFile(pointsFilesNames[0],0);
		pointsData.push_back(raw1);
		PSet<P> * rawps1 = new PSet<P> (this->imagesFilesNames[0], raw1);
	    points.push_back(*rawps1);
        for(int i=1; i<this->scanNumber.size(); ++i)
        {
			vector<P*> raw = readPtsFile(pointsFilesNames[i],this->scanNumber[i]);
			pointsData.push_back(raw);
			PSet<P> * rawps = new PSet<P> (this->imagesFilesNames.at(i), raw);
	        points.push_back(*rawps);
        }
        //????global[0].totalCorrel(global);  TOCHECK
    }
    else
    {   
   	    if(param->initfromparam)
        {
		    vector<P*> raw1 = readPFromParamFile(pointsFilesNames[0],0);
		    pointsData.push_back(raw1);
            PSet<P> * rawps1 = new PSet<P> (this->imagesFilesNames[0], raw1);
	        rawps1->initCorrel(1);
            points.push_back(*rawps1);
            for(int i=1; i<=this->startExpe; ++i)
            {
			    vector<P*> raw = readPtsFile(pointsFilesNames[i],this->scanNumber[i]);
			    pointsData.push_back(raw);
			    PSet<P> * rawps = new PSet<P> (this->imagesFilesNames.at(i), raw);
	            points.push_back(*rawps);
            }
            for(int i=this->startExpe+1; i<this->scanNumber.size(); ++i)
            {
   		        vector<P*> raw = readPFromParamFile(pointsFilesNames[0],this->scanNumber[i]);
			    pointsData.push_back(raw);
			    PSet<P> * rawps = new PSet<P> (this->imagesFilesNames.at(i), raw);
	            rawps->setScan(this->scanNumber[i]);
	            rawps->initCorrel(0);
	            rawps->initTotalCorrel(0);
                //dataparam->push_back(*rawps); TOCHECK
	            points.push_back(*rawps);
            }
        }
        else
        {
    	    vector<P*> raw1 = readCorrelManuPtsFile(pointsFilesNames[0],0);
		    pointsData.push_back(raw1);
            PSet<P> * rawps1 = new PSet<P> (this->imagesFilesNames[0], raw1);
	        rawps1->initCorrel(1);
	        points.push_back(*rawps1);
            for(int i=1; i<=this->startExpe; ++i)
            {
			    vector<P*> raw = readPtsFile(pointsFilesNames[i],this->scanNumber[i]);
			    pointsData.push_back(raw);
			    PSet<P> * rawps = new PSet<P> (this->imagesFilesNames.at(i), raw);
	            points.push_back(*rawps);
            }
            for(int i=this->startExpe+1; i<this->scanNumber.size(); ++i)
            {
   		        vector<P*> raw = readCorrelManuPtsFile(pointsFilesNames[0],this->scanNumber[i]);
			    pointsData.push_back(raw);
			    PSet<P> * rawps = new PSet<P> (this->imagesFilesNames.at(i), raw);
	            rawps->setScan(this->scanNumber[i]);
	            rawps->initCorrel(0);
	            rawps->initTotalCorrel(0);
                //dataparam->push_back(*rawps); TOCHECK
	            points.push_back(*rawps);
            }
        }
        points[0].writeToCorrelManuPtsFile(this->resptrsFilesNames[0]);
   	    points[0].writeToCorrelManuPtsFile(this->resptrscorFilesNames[0]);
        points[0].writeVTKDisplacement(string(this->resptrsDir) + this->resptrsPrefix[0]+"_displ.vtk",points[0],points[0]);
    }
}


template <typename Ptr, typename Pg>
void TimeSerie<Ptr,Pg>::readMesh() {
    for(int i=0; i<this->names.size(); i++)
    {   
        Mesh* newMesh = new Mesh();
        newMesh->read(this->meshesFilesNames[i],i);
        this->mesh.push_back(newMesh);
	}
}


template <typename Ptr, typename Pg>
void TimeSerie<Ptr,Pg>::computeMesh()
{
    Mesh* initMesh = new Mesh();
    initMesh->read(this->meshesFilesNames[0],0);
    this->mesh.push_back(initMesh);
    LDEBUG << "Mesh " <<  1  << " loaded";
    (this->mesh[0])->writeVTK(this->resmeshesFilesNames[0],*(this->mesh[0]),*(this->mesh[0]));
    (this->mesh[0])->writeCastemINP(this->resmeshesDir, this->resmeshesPrefix[0],*(this->mesh[0]));
    (this->ptrs[0]).writeCastemINP(this->resmeshesDir, this->resptrsPrefix[0],(this->ptrs[0]));
    LINFO << "Mesh state " << 1;
    for(int i=1; i<scanNumber.size(); ++i)
    {
        Mesh* newMesh = new Mesh();
        newMesh->read(this->meshesFilesNames[0],0);
        this->mesh.push_back(newMesh);
        (this->mesh[i-1])->computeSafe(*(this->mesh[i]),(PSet<P>)(this->ptrs[i-1]),(PSet<P>)(this->ptrs[i]),param->correlThr, param->regulThrDist, param->regulDist);
        LDEBUG << "Mesh " << i + 1  << " computed";
        (this->mesh[i])->writeVTK(this->resmeshesFilesNames[i],*(this->mesh[0]),*(this->mesh[i-1]));
        (this->mesh[i])->writeCastemINP(this->resmeshesDir, this->resmeshesPrefix[i],*(this->mesh[0]));
        (this->ptrs[i]).writeCastemINP(this->resmeshesDir, this->resptrsPrefix[i], this->ptrs[0]);
        LINFO << "Mesh state " << i +1;
    }
}


template <typename Ptr, typename Pg>
void TimeSerie<Ptr,Pg>::computeDeplField()
{
    LINFO << "Computing displacement field on whole image";
    for(int i=1; i<scanNumber.size(); ++i)
    {
        LINFO << "Scan " << this->scanNumber[i];
        PostProc postproc = PostProc(this->param, this->maskFilesNames[i],(PSet<P>&)((ptrs)[0]),(PSet<P>&)((ptrs)[i]));
        postproc.init();
        postproc.calcDeplField();
        postproc.applyMask();
        //postproc.checkDeplField();
        //postproc.writeDeplField(string(param->Directory)+param->prefiximage+Num[i]+"_depl_" ,".raw",3);
        postproc.writeEpsField(this->outputprefixFilesNames[i]+"_ezz.raw");
        postproc.finalize();
     }
}


template <typename Ptr, typename Pg>
void TimeSerie<Ptr,Pg>::writePgs()
{
	string ptype = "_pgs_";
	this->writeP(this->pgs, ptype);
}


template <typename Ptr, typename Pg>
void TimeSerie<Ptr,Pg>::writePtrs()
{
	string ptype = "_ptrs_";
	this->writeP(this->ptrs, ptype);
}


template <typename Ptr, typename Pg>
void TimeSerie<Ptr,Pg>::writeP(vector<PSet< P> > & points, string ptype)
{
	string globalName = string(this->param->outputDir)+this->param->prefix + ptype + "track_global_info.dat";
    writePSetGlobal(globalName,points);
	string psetName = string(this->param->prefix) + ptype;
    writeVectorPSet(string(this->param->outputDir)+"/P_Files", psetName,points);
}

template <typename Ptr, typename Pg>
void TimeSerie<Ptr,Pg>::writeP(vector<PSet< Particle> > & particles, string ptype)
{
	string globalName = string(this->param->outputDir)+this->param->prefix + ptype + "track_global_info.dat";
    writePSetGlobal(globalName,particles);
    string globalNameTrans = string(this->param->outputDir)+this->param->prefix + ptype + "track_global_info_transpose.dat";
    //writePSetGlobalTranspose(globalNameTrans,particles);
    int nbpart = writePSetGlobalTransposeNucleateAndMerge(globalNameTrans,particles);
	string psetName = string(this->param->prefix) + ptype;
    //checkAndCreateDir(string(this->param->Directory) + this->param->outputDir+"/Particles_Files");
    writeVectorPSet(string(this->param->outputDir)+"/Particles_Files",psetName,particles);
    //int nbpart=ptrs[0].Size();
    for(int i=0; i<scanNumber.size(); ++i)
    {
        ptrs[i].writeVTKParticleSorted(string(this->outputDir)+this->resptrsPrefix[i] + "particles.vtk", nbpart); 
    }
}

/*
template <typename Pg>
void TimeSerie<Particle,Pg>::removeUntracked()
{
    for(int i=scanNumber.size() - 2; i>=0; --i)
    {
        LDEBUG << "Scan " << scanNumber[i];
        ptrs[i] = (ptrs[i]).subSet(ptrs[numberExpe-1]);
        ptrs[i].renumber();
   	    ptrs[i].writeToCorrelManuPtsFile(outputprefixFilesNames[i]+"_post"+param->suffixptrs);
    }
    LDEBUG << "Scan " << scanNumber[numberExpe-1];
    ptrs[numberExpe-1].renumber();
   	ptrs[numberExpe-1].writeToCorrelManuPtsFile(outputprefixFilesNames[numberExpe-1]+"_post"+param->suffixptrs);

}
*/
template <typename Ptr, typename Pg>
void TimeSerie<Ptr,Pg>::readPreCorrel()
{
    LINFO << "precorrel mode is on";
    for(int i=0; i<scanNumber.size()-1; ++i)
    {
        preCorrelDeplZ.push_back(readFile(string(param->Directory)+param->prefixdepl+scanNumberStr[i]+"_"+scanNumberStr[i+1]+param->suffixdepl));
    	LINFO << "Read " << param->prefixdepl << scanNumberStr[i] << "_" << scanNumberStr[i+1] << param->suffixdepl;
    }
}

template <typename Ptr, typename Pg>
void TimeSerie<Ptr,Pg>::correl()
{
    int numberExpe = scanNumber.size();
    for(int i=this->startExpe; i<numberExpe-1; ++i)
    {
        PSet<Ptr>  checkedCorrel;
        LINFO << "Matching scans " << scanNumber[i] << " and " << scanNumber[i+1] ;
    
        LINFO << "--------------------------------\n\n" ;

        //Reference Points pool (from reference scan)
        PSet<Ptr>  poolref = (ptrs)[i];
        LINFO << "Scan " << scanNumber[i] << " contains " << poolref.Size() << " points";

        //Second Points pool (from next scan)
        PSet<Ptr>  poolfinal = (ptrs)[i+1];
        LINFO << "Scan " << scanNumber[i+1] << " contains " << poolfinal.Size() << " points\n";

        if(param->precorrel)
            checkedCorrel = poolref.findBestCorrel_DICGPU(poolfinal, param->correlDomain, param->thrXYZ, param->thrZ, param->correlThr, preCorrelDeplZ[i]);
        else
            checkedCorrel = poolref.findBestCorrel_DICGPU(poolfinal, *param);
    
        LINFO << "\nFirst iteration : positions found." ;
    
        LINFO << checkedCorrel.countValid(param->correlThr) << "/" << checkedCorrel.Size() << " points ketp";

        if(param->optimize)
        {
          LINFO << "Optimizing positions.";
          LINFO << "--------------------------------\n\n" ;
          checkedCorrel = poolref.optimizeCorrel_DICGPU(checkedCorrel, *param);
          LINFO << "New positions found.";
        }

        checkedCorrel.setScan(scanNumber[i+1]);

        if(param->rmbad)
        {
          LINFO << "Removing wrong points";
          checkedCorrel = checkedCorrel.subSetCorrel(param->correlThr);
          (ptrs)[i+1] = (ptrs)[i+1].subSet(checkedCorrel);
        }
   
        checkedCorrel.writeToCorrelManuPtsFile(this->resptrsFilesNames[i+1]);
        checkedCorrel.writeToPtsFile(this->resptrscorFilesNames[i+1]);
        resptrs.push_back(checkedCorrel);
        checkedCorrel.writeVTKDisplacement(string(this->resptrsDir) + this->resptrsPrefix[i+1]+"_displ.vtk",ptrs[0],ptrs[i]);
        //writePointsFiles(string(param->Directory)+param->prefixptrs+"point_", param->suffixptrs, ptrs,param->append);
    }
}

/*
template <typename Ptr, typename Pg>
void TimeSerie<Ptr,Pg>::correl_bakup()
{
    int numberExpe = scanNumber.size();
    for(int i=this->startExpe; i<numberExpe-1; ++i)
    {
        PSet<Ptr>  checkedCorrel;
        LINFO << "Matching scans " << scanNumber[i] << " and " << scanNumber[i+1] << " ...";

        //Reference Points pool (from reference scan)
        PSet<Ptr>  poolref = (ptrs)[i];
        LINFO << "Scan " << scanNumber[i] << " contains " << poolref.Size() << " points";

        //Second Points pool (from next scan)
        PSet<Ptr>  poolfinal = (ptrs)[i+1];
        LINFO << "Scan " << scanNumber[i+1] << " contains " << poolfinal.Size() << " points";

    if(param->precorrel)
    {
        checkedCorrel = poolref.findBestCorrel_DICGPU(poolfinal, param->correlDomain, param->thrXY, param->thrZ, param->correlThr, preCorrelDeplZ[i]);
    }
    else
    {
        if(param->usetex)
        {
            LDEBUG << "Use texture";
            checkedCorrel = poolref.findBestCorrel_DICGPU_texture(poolfinal, param->correlDomainX, param->correlDomainY, param->correlDomainZ, param->thrX, param->thrY, param->thrZ, param->correlThr);
        }
        else
        {
            if(param->aniso)
            {
                if(param->rigidguiding)
                    //checkedCorrel = poolref.findBestCorrel_DICGPU_aniso_guided(poolfinal, param->correlDomainX, param->correlDomainY, param->correlDomainZ, param->thrX, param->thrY, param->thrZ, param->correlThr);
                    checkedCorrel = poolref.findBestCorrel_DICGPU_aniso_guided(poolfinal, param->correlDomainX, param->correlDomainY, param->correlDomainZ, param->thrX, param->thrY, param->thrZ, param->correlThr, param->initThrXfactor, param->initThrYfactor, param->initThrZfactor);
                else
                    checkedCorrel = poolref.findBestCorrel_DICGPU_aniso(poolfinal, param->correlDomainX, param->correlDomainY, param->correlDomainZ, param->thrX, param->thrY, param->thrZ, param->correlThr);
            }
            else
                checkedCorrel = poolref.findBestCorrel_DICGPU(poolfinal, param->correlDomain, param->thrXY, param->thrZ, param->correlThr);
        }
    }
    LINFO << "First iteration : positions found." ;
    
    LINFO << checkedCorrel.countValid(param->correlThr) << "/" << checkedCorrel.Size() << " points ketp";

    if(param->optimize)
    {
        LINFO << "Optimizing positions.";
        if(param->usetex)
        {
            LDEBUG << "Use texture";
            checkedCorrel = poolref.optimizeCorrel_DICGPU_texture(checkedCorrel, param->correlDomainX, param->correlDomainY, param->correlDomainZ, param->thrX/5, param->thrY/5, param->thrZ/5, param->correlThr, param->regulThrDist);
        }
        else
        {
            if(param->aniso)
                checkedCorrel = poolref.optimizeCorrel_DICGPU_aniso(checkedCorrel, param->correlDomainX, param->correlDomainY, param->correlDomainZ, param->thrX/5,param->thrY/5,param->thrZ/5, param->correlThr, param->regulThrDist);
            else
                checkedCorrel = poolref.optimizeCorrel_DICGPU(checkedCorrel, param->correlDomain, param->thrXY/5, param->correlThr, param->regulThrDist);
        }
        LINFO << "New positions found.";
    }

	checkedCorrel.setScan(scanNumber[i+1]);

    if(param->rmbad)
    {
        LINFO << "Removing wrong points";
        checkedCorrel = checkedCorrel.subSetCorrel(param->correlThr);
        (ptrs)[i+1] = (ptrs)[i+1].subSet(checkedCorrel);
    }
   
    checkedCorrel.writeToCorrelManuPtsFile(this->resptrsFilesNames[i+1]);
   checkedCorrel.writeToPtsFile(this->resptrscorFilesNames[i+1]);
    resptrs.push_back(checkedCorrel);
    checkedCorrel.writeVTKDisplacement(string(this->resptrsDir) + this->resptrsPrefix[i+1]+"_displ.vtk",ptrs[0],ptrs[i]);
    //writePointsFiles(string(param->Directory)+param->prefixptrs+"point_", param->suffixptrs, ptrs,param->append);
}
}
*/

template <typename Ptr, typename Pg>
void TimeSerie<Ptr,Pg>::removeUntracked()
{
    for(int i=this->scanNumber.size() - 2; i>=0; --i)
    {
        LDEBUG << "Scan " << this->scanNumber[i];
        this->ptrs[i] = (this->ptrs[i]).subSet(this->ptrs[this->numberExpe-1]);
        this->ptrs[i].renumber();
        this->ptrs[i].writeToCorrelManuPtsFile(this->outputprefixFilesNames[i]+"_post"+this->param->suffixptrs);
    }
    LDEBUG << "Scan " << this->scanNumber[this->numberExpe-1];
    this->ptrs[this->numberExpe-1].renumber();
    this->ptrs[this->numberExpe-1].writeToCorrelManuPtsFile(this->outputprefixFilesNames[this->numberExpe-1]+"_post"+this->param->suffixptrs);

}

/*

template <typename Pg>
int TimeSerie<Particle,Pg>::trackParticles()
{
    vector<vector<PSet<Particle> > > global;

    for(int i=scanNumber.size()-1; i>0; --i)
    {
    	vector<PSet<Particle> > checkedCorrel;
    	if(param->postprocess)
    	{
    		readVectorPSet(string(param->Directory)+"TrackingFiles/",param->prefix+scanNumberStr[i]+"_checked", ptrs, ptrsData);
    		checkedCorrel = *(&(this->ptrs));
    	}
    	else
    	{
    		LINFO << "Matching scans " << scanNumber[i] << " and " << scanNumber[i-1] << " ...";

			//Reference Particles pool (from reference scan)
			PSet<Particle>  poolref = (ptrs[i]).subSetVol(4);
			LINFO << "Scan " << scanNumber[i] << " contains " << poolref.Size() << " particles";

			//Second Particles pool (from next scan)
			PSet<Particle>  poolfinal = (ptrs[i-1]).subSetVol(4);
			LINFO << "Scan " << scanNumber[i-1] << " contains " << poolfinal.Size() << " particles";

            vector<PSet<Particle> > potentialCorrel;
            vector<PSet<Particle> > potentialCorrelInv;
            if(param->DIC){
            	//For each particle of reference pool look for the best correlated particles of second pool (forward correlation)
				//potentialCorrel = poolref.findBestCorrel_DICGPU(poolfinal, refPoints[i], refDeplm[i-1], thrXY, thrZ);
				potentialCorrel = poolref.findBestCorrel_DICGPU(poolfinal, (pgs)[i], (pgs)[i-1], (double)(param->thrXY), (double)(param->thrZ));
				//TODO no more refdepl!


				//vector<ParticlesSet> potentialCorrel = poolref.findBestCorrel_noDIC(poolfinal, deplm[i-1], thrXY, thrZ);

				//For each particle of second pool look for the best correlated particles of reference pool (backward correlation)
				//potentialCorrelInv = poolfinal.findBestCorrel_DICGPU(poolref, refPoints[i-1],refDeplp[i-1], thrXY, thrZ);
				potentialCorrelInv = poolfinal.findBestCorrel_DICGPU(poolref, pgs[i-1],pgs[i], param->thrXY, param->thrZ);
				//TODO no more refdepl!


				//vector<ParticlesSet> potentialCorrelInv = poolfinal.findBestCorrel_noDIC(poolref, deplp[i-1], thrXY, thrZ);
            }
            else
            {
            	//For each particle of reference pool look for the best correlated particles of second pool (forward correlation)
            	potentialCorrel = poolref.findBestCorrel_noDIC(poolfinal, pgs[i], pgs[i-1], param->thrXY, param->thrZ);
            	//TODO no more refdepl!

            	//vector<ParticlesSet> potentialCorrel = poolref.findBestCorrel_noDIC(poolfinal, deplm[i-1], thrXY, thrZ);

            	//For each particle of second pool look for the best correlated particles of reference pool (backward correlation)
            	potentialCorrelInv = poolfinal.findBestCorrel_noDIC(poolref, pgs[i-1],pgs[i], param->thrXY, param->thrZ);
            	//TODO no more refdepl!

            }

			LINFO << "Potential pairs found.";


			//debug :
			//poolref.debugCorrel(potentialCorrel,poolfinal,potentialCorrelInv);


			//Match forward and backward correlation
			checkedCorrel = poolref.matchCorrel(potentialCorrel,poolfinal,potentialCorrelInv);

			//Write the cross correlation result
			writePSetGlobal(string(param->Directory)+"TrackingFiles/"+this->param->prefiximage+scanNumberStr[i]+"_checked.dat",checkedCorrel);
			writeVectorPSet(string(param->Directory)+"TrackingFiles/",this->param->prefiximage+scanNumberStr[i]+"_checked",checkedCorrel);
    	}
	    //Keep memory of the correlation for final renumbering
	    global.push_back(checkedCorrel);
    }

    //Concatenate pairs of correlation in a global tracking
    cout << "Concatenating pairs..." << endl;
    int current=0;
    for(int i=numberExpe-1; i>0; --i)
    {
    	current = (ptrs)[i].globalNumberFromCheckedRef(global[numberExpe-1-i], current, scanNumber[i]);
    	(ptrs)[i-1].globalNumberFromCheckedFinal(global[numberExpe-1-i],(ptrs)[i], current, scanNumber[i]);
    	 //(ptrs)[i-1].globalNumberFromCheckedFinal(global[numberExpe-1-i],(*parset)[i], current, atoi(Num[i].c_str()));
    }
}

template <typename Pg>
vector<PSet<Particle> > TimeSerie<Particle,Pg>::rankParticles(int &current)
{
	vector<PSet<Particle> > track = vector<PSet<Particle> >();
    for(int i=1; i<=current;i++)
    {
    	PSet<Particle> * partS = new PSet<Particle> ();
    	LINFO << "Looking for particle " << i;
    	for(int j=0;j<scanNumber.size();j++)
    	{
    		LINFO << "Scan " << j << " ";
    		(*partS).addP((ptrs)[j].findGlobalColorP(i));
    	}
    	resptrs.push_back(*partS);
    }
}


template <typename Pg>
void TimeSerie<Particle,Pg>::recolor(vector<PSet<Particle> > track)
{
   	LINFO << "Recoloring  particles ...";
    for(int i=0; i<scanNumber.size(); ++i)
    {
    	int* new_color = new int[65256];
    	for(int j = 0; j<65256; j++)
    	{
    		new_color[j] = 65255;
    	}
    	for(int j=0; j<track.size(); ++j)
    	{
    		int intensFinal = j;
    		if((track[j].getP(i)) && (track[j].getP(i)->getColor()>=0))
    			new_color[track[j].getP(i)->getColor()] = intensFinal;
    	}
    	new_color[0]=0;
    	ostringstream a;
    	a << this->param->Directory << this->param->prefiximage << scanNumberStr[i] << "_label_color_corresp.dat";

    	writeFile(a.str(),new_color,1, 65256);
    }
}

*/




template class TimeSerie<Particle,P>;
template class TimeSerie<P,P>;
