#include "IO.h"

using namespace std;

/*
 * void checkAndCreateDir(string Directory)
{
    filesystem::path dir=Directory.c_str();
    if (!filesystem::exists(dir))
    {
        LINFO << "Creating directory " << Directory;
        filesystem::create_directory(Directory.c_str());
    }
}
*/

/**
void writeVectorPointsSet(string Directory, string VectorPointSetName, vector<PSet<Point> > & vector)
{
	 ostringstream fileName;
	 fileName << Directory << "/VectorPointsSet_" << VectorPointSetName;
     ofstream fichier(fileName.str().c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier
     if(fichier)  // si l'ouverture a réussi
	 {
	 		 for(int j=0; j<vector.size(); j++)
	 	 	 {
	 			 	 ostringstream psName;
	 			 	 char buff[4];
	 			 	 sprintf(buff,"%04d",j);
	 			     psName << "PointsSet_" << VectorPointSetName << buff;
	 				fichier <<  psName.str() <<endl;
	 				vector[j].writeToFile(Directory,psName.str());
	 	 	 }
	 	 	 fichier.close();
	 	 }
	 	 else  // sinon
	 	 LERROR << "Erreur à l'ouverture !";
}

void readVectorPointsSet(string Directory, string VectorPointSetName, vector<PSet<Point> > & raw, vector<PSet<Point> > & v)
{
	 //vector<PSet<Point> >* v = new vector<PSet<Point> >();
	 ostringstream fileName;
	 fileName << Directory << "/VectorPointsSet_" << VectorPointSetName;
     ifstream fichier(fileName.str().c_str(), ios::in);  //déclaration du flux et ouverture du fichier
     if(fichier)  // si l'ouverture a réussi
	 {
    	 while (fichier.good())
    	 {
    	 	string psName = "";
    	 	char schar[10000];
    	 	fichier.getline(schar,10000);
    	    psName+=schar;
    	    ostringstream fullpsName;
    	    fullpsName << Directory << "/" << psName;
    	    if(ifstream(fullpsName.str().c_str()) && psName!=""){
    	    	PSet<Point> * toto = new PSet<Point> (Directory,psName,raw);
    	    	v.push_back(*toto);
    	    }
    	 }
	 	 fichier.close();
	 }
     else  // sinon
     {
         	 LERROR << "Erreur à l'ouverture !";
          	 LERROR << "File " << fileName.str() << " not found!";
      }
     //return *v;
}

void readDataPointsSet(string Directory, string VectorPointSetName, vector<PSet<Point> > & v)
{
	 //vector<PSet<Point> >* v = new vector<PSet<Point> >();
	 ostringstream fileName;
	 fileName << Directory << "/VectorPointsSet_" << VectorPointSetName;
	 ifstream fichier(fileName.str().c_str(), ios::in);  //déclaration du flux et ouverture du fichier
     if(fichier)  // si l'ouverture a réussi
	 {
    	 while (fichier.good())
    	 {
    	 	string psName = "";
    	 	char schar[10000];
    	 	fichier.getline(schar,10000);
    	    psName+=schar;
    	    PSet<Point> * toto = new PSet<Point> (Directory,psName);
    	    v.push_back(*toto);

    	 }
	 	 fichier.close();
	 }
     else  // sinon
     {
    	 LERROR << "Erreur à l'ouverture !";
     	 LERROR << "File " << fileName.str() << " not found!";
     }
     //return *v;
}



vector<Point*> readMeshPtsFile(string fileName, int scan, int& dimx, int& dimy, int& dimz)
 {
    LDEBUG << "Will try to read MeshPts type file " << fileName;
    vector<Point*> *points = new vector<Point*>();
    ifstream File (fileName.c_str(),ios::in);
    if (!File)
    {
    	LERROR << "Error : impossible to open file " << fileName << " in read only mode...";
    }
    else
    {
	    //cout << "File opened " << endl;
    }
    vector<double> line;
    double a;
    string paramline;
    int nbColonne;
    nbColonne=3;
    stringstream sstr;
    char schar[10000];
    File.getline(schar,10000);
    File.getline(schar,10000);
    File.getline(schar,10000);
    File.getline(schar,10000);
    File.getline(schar,10000);
	paramline = "";
    paramline+=schar;
    sscanf(schar,"DIMENSIONS %d %d %d\n", &dimx, &dimy, &dimz);

    LINFO << dimx*dimy*dimz << "  points in mesh (" << dimx << "," << dimy << "," << dimz << ")";

    File.getline(schar,10000);
    int pn=0;
    int wp=0;
    while (File.good())
    {
	paramline = "";
	File.getline(schar,10000);
    	paramline+=schar;
	    sstr.clear();
    	sstr.str(paramline);
    	if(sstr.str().size()>1)
        {
    		for(int i=0; i<nbColonne; i++)
    		{
			sstr >> a;
			line.push_back(1.*a);
		}
		if(!isnan(line[0]))
		{
			pn++;
			points->push_back(new Point(scan,
						(int)pn,
						Coord((double)line[0],(double)line[1],(double)line[2])
						));
			}
		else
		{
			wp++;
		}
		line.clear();
        }
    }
    File.close();
    return *points;
}


*/

void writeFile(string filePath, int* values, int dimi, int dimj)
 {
 	 ofstream fichier(filePath.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier
 	 if(fichier)  // si l'ouverture a réussi
 	 {
 		 for(int j=0; j<dimj; j++)
 	 	 {
 			for(int i=0; i<dimi; i++)
 			{
 				fichier << values[j*dimi+i] << endl;
 			}
 	 	 }
 	 	 fichier.close();  // on referme le fichier
 	 }
 	 else  // sinon
 	 LERROR << "Erreur à l'ouverture !";
}
/*
void writeFile(string filePath, int values[], int dim)
{
	 ofstream fichier(filePath.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier
	 if(fichier)  // si l'ouverture a réussi
	 {
		 for(int j=0; j<dim; j++)
	 	 {
			 fichier << values[j] << endl;
	 	 }
	 	 fichier.close();  // on referme le fichier
	 }
	 else  // sinon
	 LERROR << "Erreur à l'ouverture !";
}

void writevectorPointsSet(string FileName, vector<PSet<Point> > vector)
{
	 ofstream fichier(FileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier
	 	 if(fichier)  // si l'ouverture a réussi
	 	 {
	 		 for(int j=0; j<vector.size(); j++)
	 	 	 {

	 				fichier << vector[j].colorTostring() << endl;
	 	 	 }
	 	 	 fichier.close();  // on referme le fichier
	 	 }
	 	 else  // sinon
	 	 LERROR << "Erreur à l'ouverture !";
}

void writevectorPointsSetGlobal(string FileName, vector<PSet<Point> > vector)
{

	 ofstream fichier(FileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier
		 	 if(fichier)  // si l'ouverture a réussi
		 	 {
		 		 for(int j=0; j<vector.size(); j++)
		 	 	 {

		 				fichier << vector[j].colorTostring();
		 				fichier << vector[j].correlTostring();
		 				fichier << vector[j].PosTostring() << endl;
		 	 	 }
		 	 	 fichier.close();  // on referme le fichier
		 	 }
		 	 else  // sinon
		 	 LERROR << "Erreur à l'ouverture !";
}




void readVectorParticlesSet(string Directory, string VectorParticleSetName, vector<PSet<Particle> > & raw, vector<PSet<Particle> > & v)
{
	 //vector<PSet<Particle> >* v = new vector<PSet<Particle> >();
	 ostringstream fileName;
	 fileName << Directory << "/VectorParticleSet_" << VectorParticleSetName;
     ifstream fichier(fileName.str().c_str(), ios::in);  //déclaration du flux et ouverture du fichier
     if(fichier)  // si l'ouverture a réussi
	 {
    	 while (fichier.good())
    	 {
    	 	string psName = "";
    	 	char schar[10000];
    	 	fichier.getline(schar,10000);
    	    psName+=schar;
    	    ostringstream fullpsName;
    	    fullpsName << Directory << "/" << psName;
    	    if(ifstream(fullpsName.str().c_str()) && psName!=""){
    	    	PSet<Particle> * toto = new PSet<Particle> (Directory,psName,raw);
    	    	v.push_back(*toto);
    	    }
    	 }
	 	 fichier.close();
	 }
     else  // sinon
     {
         	 cerr << "Erreur à l'ouverture !" << endl;
          	 cerr << "File " << fileName.str() << " not found!" << endl;
      }
     //return *v;
}

void readDataParticlesSet(string Directory, string VectorParticleSetName, vector<PSet<Particle> > & v  )
{
	 //vector<PSet<Particle> >* v = new vector<PSet<Particle> >();
	 ostringstream fileName;
	 fileName << Directory << "/VectorParticleSet_" << VectorParticleSetName;
	 ifstream fichier(fileName.str().c_str(), ios::in);  //déclaration du flux et ouverture du fichier
     if(fichier)  // si l'ouverture a réussi
	 {
    	 while (fichier.good())
    	 {
    	 	string psName = "";
    	 	char schar[10000];
    	 	fichier.getline(schar,10000);
    	    psName+=schar;
    	    PSet<Particle> * toto = new PSet<Particle> (Directory,psName);
    	    v.push_back(*toto);

    	 }
	 	 fichier.close();
	 }
     else  // sinon
     {
    	 cerr << "Erreur à l'ouverture !" << endl;
     	 cerr << "File " << fileName.str() << " not found!" << endl;
     }
     //return *v;
}




void writevectorParticlesSet(string FileName, vector<PSet<Particle> > vector)
{
	 ofstream fichier(FileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier
	 	 if(fichier)  // si l'ouverture a réussi
	 	 {
	 		 for(int j=0; j<vector.size(); j++)
	 	 	 {

	 				fichier << vector[j].colorTostring() << endl;
	 	 	 }
	 	 	 fichier.close();  // on referme le fichier
	 	 }
	 	 else  // sinon
	 	 cerr << "Erreur à l'ouverture !" << endl;
}

void writevectorParticlesSetGlobal(string FileName, vector<PSet<Particle> > vector)
{

	 ofstream fichier(FileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier
		 	 if(fichier)  // si l'ouverture a réussi
		 	 {
		 		 for(int j=0; j<vector.size(); j++)
		 	 	 {

		 				fichier << vector[j].colorTostring();
		 				fichier << vector[j].colorGlobalTostring();
		 				fichier << vector[j].volumeTostring();
		 				fichier << vector[j].PosTostring() << endl;
		 	 	 }
		 	 	 fichier.close();  // on referme le fichier
		 	 }
		 	 else  // sinon
		 	 cerr << "Erreur à l'ouverture !" << endl;
}



vector<Coord> readPtsFile(string filePath)
{

    ifstream file(filePath.c_str(), ios::in | ios::out);
    cout << "Readding " << filePath << endl;

    if(!file.is_open()) {
        cerr << "Could not open " << filePath << endl;
        return vector<Coord >();
    }

    vector<Coord > data;
    string line;
    std::getline(file, line, '\n');
    while(!std::getline(file, line, '\n').eof()) {
        istringstream reader(line);
        vector<double> lineData;

        string::const_iterator i = line.begin();
        while(!reader.eof()) {
            double val;
            reader >> val;

            if(reader.fail())
                break;

            lineData.push_back(val);
        }

        data.push_back(Coord(lineData[2],lineData[3],lineData[4]));
    }
    return data;
}







*/

vector<P*> readCorrelManuPtsFile(string fileName, int scan)
 {
    LDEBUG << "Will try to read CorrelManuPts type file " << fileName;
    vector<P*> *points = new vector<P*>();
    ifstream File (fileName.c_str(),ios::in);
    if (!File)
    {
    	LERROR << "Error : impossible to open file " << fileName << " in read only mode...";
    }
    else
    {
	    //cout << "File opened " << endl;
    }
    vector<double> line;
    double a;
    string paramline;
    int nbColonne;
    nbColonne=0;
    stringstream sstr;
    char schar[10000];
    File.getline(schar,10000);
    File.getline(schar,10000);
    paramline+=schar;
    sstr.str(paramline);
    while(sstr >> a)
    {
        nbColonne++;
    }

    File.seekg(0, ios::beg);
    File.getline(schar,10000);
    int pn=0;
    int wp=0;
    while (File.good())
    {
	paramline = "";
	File.getline(schar,10000);
    	paramline+=schar;
	sstr.clear();
    	sstr.str(paramline);
    	if(sstr.str().size()>1)
        {
    		for(int i=0; i<nbColonne; i++)
    		{
			sstr >> a;
			line.push_back(1.*a);
		}
		if(!isnan(line[0]))
		{
			pn++;
            if(scan>1)
            {
            	points->push_back(new P(scan,
						(int)line[0],
						Coord((double)line[2],-(double)line[3],-(double)line[4]),
                        0.0
						));
            }
            else
            {
    			points->push_back(new P(scan,
						(int)line[0],
						Coord((double)line[2],-(double)line[3],-(double)line[4]),
                        1.0
						));
            }
        LDEBUG << "P " << pn << " (" << (double)line[2] << "," << -(double)line[3] << "," <<-(double)line[4] << ")";
		}
		else
		{
			wp++;
		}
		line.clear();
        }
    }
    File.close();
    return *points;
}

vector<P*> readPtsFile(string fileName, int scan)
 {
    LDEBUG << "Will try to read Pts type file " << fileName;
    vector<P*> *points = new vector<P*>();
    ifstream File (fileName.c_str(),ios::in);
    if (!File)
    {
    	LERROR << "Error : impossible to open file " << fileName << " in read only mode...";
    }
    else
    {
    LDEBUG << "File opened ";
    }
    vector<double> line;
    double a;
    string paramline;
    int nbColonne;
    nbColonne=0;
    stringstream sstr;
    char schar[10000];
    File.getline(schar,10000);
    File.getline(schar,10000);
    paramline+=schar;
    sstr.str(paramline);
    while(sstr >> a)
    {
        nbColonne++;
    }

    File.seekg(0, ios::beg);
    File.getline(schar,10000);
    int pn=0;
    int wp=0;
    while (File.good())
    {
	paramline = "";
	File.getline(schar,10000);
    	paramline+=schar;
	sstr.clear();
    	sstr.str(paramline);
    	if(sstr.str().size()>1)
        {
    		for(int i=0; i<nbColonne; i++)
    		{
			sstr >> a;
			line.push_back(1.*a);
		}
		if(!isnan(line[0]))
		{
			pn++;
			points->push_back(new P(scan,
						(int)line[0],
						Coord((double)line[2],-(double)line[3],-(double)line[4]),
						(double)line[5]));
            LDEBUG << "P " << pn << " (" << (double)line[2] << "," << -(double)line[3] << "," <<-(double)line[4] << ")";
		}
		else
		{
			wp++;
		}
		line.clear();
        }
    }
    File.close();
    return *points;
}

vector<Particle*> readParamFile(string fileName, int scan)
 {
    vector<Particle*> *particles = new vector<Particle*>();
    ifstream File (fileName.c_str(),ios::in);
    if (!File)
    {
    	cerr << "Error : impossible to open file " << fileName << " in read only mode..." << endl;
    }
    else
    {
	//cout << "File opened " << endl;
    }
    vector<double> line;
    double a;
    string paramline;
    int nbColonne;
    nbColonne=0;
    stringstream sstr;
    char schar[10000];
    File.getline(schar,10000);
    File.getline(schar,10000);
    paramline+=schar;
    sstr.str(paramline);
    while(sstr >> a)
    {
        nbColonne++;
	//cout << "Column " << nbColonne << " : " << a << endl;
    }

    File.seekg(0, ios::beg);
    File.getline(schar,10000);
    LDEBUG << nbColonne << " columns";
    int pn=0;
    int wp=0;
    while (File.good())
    {
	paramline = "";
	File.getline(schar,10000);
    	paramline+=schar;
	sstr.clear();
    	sstr.str(paramline);
    	if(sstr.str().size()>1)
        {
        //cout << paramline << endl;
    		for(int i=0; i<nbColonne; i++)
    		{
			sstr >> a;
			line.push_back(1.*a);
		}
		if(!isnan(line[0]))
		{
			pn++;
			//cout << "Add one more particle (total " << pn << ")" <<  endl;

			particles->push_back(new Particle(scan,
						(int)line[0],
						Coord((double)line[1],(double)line[2],(double)line[3]),
						(double)line[5],
						(bool)(line[36]==1),
						(double)line[8],
						((double)line[35])/((double)line[33]),
						((double)line[35])/((double)line[34]),
						(double)line[20],
						paramline
						));
			}
		else
		{
			wp++;
			//cout << "Skipped particle ( total " << wp << ")" << endl;
		}
		line.clear();
        }
    }
    File.close();
    LINFO << fileName << " successfully loaded";
    LINFO << "Found " << pn << " valid objects";
    LINFO << "Skipped " << wp << " objects";
    return *particles;
}

vector<Particle*> readParamFileConstrained(string fileName, int scan, int nbpartcons)
 {
    vector<Particle*> *particles = new vector<Particle*>();
    ifstream File (fileName.c_str(),ios::in);
    if (!File)
    {
    	cerr << "Error : impossible to open file " << fileName << " in read only mode..." << endl;
    }
    else
    {
	//cout << "File opened " << endl;
    }
    vector<double> line;
    double a;
    string paramline;
    int nbColonne;
    nbColonne=0;
    stringstream sstr;
    char schar[10000];
    File.getline(schar,10000);
    File.getline(schar,10000);
    paramline+=schar;
    sstr.str(paramline);
    while(sstr >> a)
    {
        nbColonne++;
	//cout << "Column " << nbColonne << " : " << a << endl;
    }

    File.seekg(0, ios::beg);
    File.getline(schar,10000);
    //cout << nbColonne << " columns" << endl;
    int pn=0;
    int wp=0;
    while (File.good())
    {
	paramline = "";
	File.getline(schar,10000);
    	paramline+=schar;
	sstr.clear();
    	sstr.str(paramline);
    	if(sstr.str().size()>1)
        {
        //cout << paramline << endl;
    		for(int i=0; i<nbColonne; i++)
    		{
			sstr >> a;
			line.push_back(1.*a);
		}
		if(!isnan(line[0]))
		{
			pn++;
			//cout << "Add one more particle (total " << pn << ")" <<  endl;

			particles->push_back(new Particle(scan,
						(int)line[0],
						Coord((double)line[1],(double)line[2],(double)line[3]),
						(double)line[5],
						(bool)(line[36]==1),
						(double)line[8],
						((double)line[35])/((double)line[33]),
						((double)line[35])/((double)line[34]),
						(double)line[20],
						paramline
						));
			}
		else
		{
			wp++;
			//cout << "Skipped particle ( total " << wp << ")" << endl;
		}
		line.clear();
        }
    }
    File.close();
    for(int i=0; i<nbpartcons-pn;i++)
    {
   			particles->push_back(new Particle(scan,
						pn+i+1,
						Coord(0,0,0),
						0,
                        false,
                        0,
                        1.,
                        1.,
                        0.,
                        ""));
	 }
    LINFO << fileName << " successfully loaded";
    LINFO << "Found " << pn << " valid objects";
    LINFO << "Skipped " << wp << " objects";
    LINFO << "Added " << nbpartcons-pn << " empty objects to match constrains";
    return *particles;
}

vector<P*> readPFromParamFile(string fileName, int scan)
 {
    vector<P*> *p = new vector<P*>();
    ifstream File (fileName.c_str(),ios::in);
    if (!File)
    {
    	cerr << "Error : impossible to open file " << fileName << " in read only mode..." << endl;
    }
    else
    {
	LDEBUG << "File opened ";
    }
    vector<double> line;
    double a;
    string paramline;
    int nbColonne;
    nbColonne=0;
    stringstream sstr;
    char schar[10000];
    File.getline(schar,10000);
    File.getline(schar,10000);
    paramline+=schar;
    sstr.str(paramline);
    while(sstr >> a)
    {
        nbColonne++;
	//cout << "Column " << nbColonne << " : " << a << endl;
    }

    File.seekg(0, ios::beg);
    File.getline(schar,10000);
    LDEBUG << nbColonne << " columns";
    int pn=0;
    int wp=0;
    while (File.good())
    {
	paramline = "";
	File.getline(schar,10000);
    	paramline+=schar;
	sstr.clear();
    	sstr.str(paramline);
    	if(sstr.str().size()>1)
        {
        //cout << paramline << endl;
    		for(int i=0; i<nbColonne; i++)
    		{
			sstr >> a;
			line.push_back(1.*a);
		}
		if(!isnan(line[0]))
		{
			pn++;
			//cout << "Add one more particle (total " << pn << ")" <<  endl;

			p->push_back(new P(scan,
						(int)line[0],
						Coord((double)line[1],(double)line[2],(double)line[3]),
						(int)((line[28]-line[27])/2+5),
						(int)((line[30]-line[29])/2+5),
						(int)((line[32]-line[31])/2+5)
						));
            if(scan==0)
                LDEBUG << "P " << line[0] << "(" << line[1] << "," << line[2] << "," << line[3] << ") bbox (" << (int)((line[28]-line[27])/2+5) << "," << (int)((line[30]-line[29])/2+5) <<  "," << (int)((line[32]-line[31])/2+5) << ")"; 
			}
		else
		{
			wp++;
			//cout << "Skipped particle ( total " << wp << ")" << endl;
		}
		line.clear();
        }
    }
    File.close();
    LINFO << fileName << " successfully loaded";
    LINFO << "Found " << pn << " valid objects";
    LINFO << "Skipped " << wp << " objects";
    return *p;
}

vector<vector<double> > readFile(string filePath)
 {

     ifstream file(filePath.c_str(), ios::in | ios::out);

     if(!file.is_open()) {
         LERROR << "Could not open " << filePath;
         return vector<vector<double> >();
     }

     vector<vector<double> > data;
     string line;
     while(!std::getline(file, line, '\n').eof()) {
         istringstream reader(line);
         vector<double> lineData;

         string::const_iterator i = line.begin();
         while(!reader.eof()) {
             double val;
             reader >> val;

             if(reader.fail())
                 break;

             lineData.push_back(val);
         }

         data.push_back(lineData);
     }
     return data;
 }



template<typename T>
void writeVectorPSet(string Directory, string VectorPSetName, vector<PSet<T> > & vector)
{
	 ostringstream fileName;
	 fileName << Directory << "/VectorPSet_" << VectorPSetName;
     ofstream fichier(fileName.str().c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier
     if(fichier)  // si l'ouverture a réussi
	 {
	 		 for(int j=0; j<vector.size(); j++)
	 	 	 {
	 			 	 ostringstream psName;
	 			 	 char buff[4];
	 			 	 sprintf(buff,"%04d",j);
	 			     psName << "PSet_" << VectorPSetName << buff;
	 				fichier <<  psName.str() <<endl;
	 				vector[j].writeToFile(Directory,psName.str());
	 	 	 }
	 	 	 fichier.close();
	 	 }
	 	 else  // sinon
	 	 cerr << "Erreur à l'ouverture !" << endl;
}

template<typename T>
void readVectorPSet(string Directory, string VectorPSetName, vector<PSet<T> > & p, vector<vector<T> > & pData)
{
	 //vector<PSet<Point> >* v = new vector<PSet<Point> >();
	 ostringstream fileName;
	 fileName << Directory << "/VectorPSet_" << VectorPSetName;
     ifstream fichier(fileName.str().c_str(), ios::in);  //déclaration du flux et ouverture du fichier
     if(fichier)  // si l'ouverture a réussi
	 {
    	 while (fichier.good())
    	 {
    	 	string psName = "";
    	 	char schar[10000];
    	 	fichier.getline(schar,10000);
    	    psName+=schar;
    	    ostringstream fullpsName;
    	    fullpsName << Directory << "/" << psName;
    	    if(ifstream(fullpsName.str().c_str()) && psName!=""){
    	    	PSet<T> * toto = new PSet<T> (Directory,psName,pData);
    	    	p.push_back(*toto);
    	    }
    	 }
	 	 fichier.close();
	 }
     else  // sinon
     {
         	 LERROR << "Erreur à l'ouverture !";
          	 LERROR << "File " << fileName.str() << " not found!";
      }
     //return *v;
}

//template<>
void writePSetGlobalTranspose(string FileName, vector<PSet<Particle> > vector)
{

	 ofstream fichier(FileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier
     if(fichier)  // si l'ouverture a réussi
	 {
        int nb = vector[0].Size();
        for(int j=0; j<vector.size(); j++)
		{
		    fichier << "color_scan" << vector[j].getP(0)->getScan() << " ";
    	}
        for(int j=0; j<vector.size(); j++)
		{
		    fichier << "valid_scan" << vector[j].getP(0)->getScan() << " ";
    	}
        for(int j=0; j<vector.size(); j++)
		{
		    fichier << "Xg_scan" << vector[j].getP(0)->getScan() << " ";
    	}
        for(int j=0; j<vector.size(); j++)
		{
		    fichier << "Yg_scan" << vector[j].getP(0)->getScan() << " ";
    	}
         for(int j=0; j<vector.size(); j++)
		{
		    fichier << "Zg_scan" << vector[j].getP(0)->getScan() << " ";
    	}
         for(int j=0; j<vector.size(); j++)
		{
		    fichier << "volume_scan" << vector[j].getP(0)->getScan() << " ";
    	}
        for(int j=0; j<vector.size(); j++)
		{
		    fichier << "sphericity_scan" << vector[j].getP(0)->getScan() << " ";
    	}
        for(int j=0; j<vector.size(); j++)
		{
		    fichier << "shapeFact_scan" << vector[j].getP(0)->getScan() << " ";
    	}
        for(int j=0; j<vector.size(); j++)
		{
		    fichier << "shapeFact2_scan" << vector[j].getP(0)->getScan() << " ";
    	}
        fichier << endl;
        for(int i=1; i<=nb; i++)
        {
		 	for(int j=0; j<vector.size(); j++)
		 	{
		 		fichier << vector[j].findGlobalColorP(i)->getColor() << " ";
    	 	}
		 	for(int j=0; j<vector.size(); j++)
		 	{
		 		fichier << vector[j].findGlobalColorP(i)->getCorrel() << " ";
    	 	}
			 	for(int j=0; j<vector.size(); j++)
		 	{
		 		fichier << vector[j].findGlobalColorP(i)->getCoord().getX() << " ";
    	 	}
		 	for(int j=0; j<vector.size(); j++)
		 	{
		 		fichier << vector[j].findGlobalColorP(i)->getCoord().getY() << " ";
    	 	}
		 	for(int j=0; j<vector.size(); j++)
		 	{
		 		fichier << vector[j].findGlobalColorP(i)->getCoord().getZ() << " ";
    	 	}
		 	for(int j=0; j<vector.size(); j++)
		 	{
		 		fichier << vector[j].findGlobalColorP(i)->getVolume() << " ";
    	 	}
		 	for(int j=0; j<vector.size(); j++)
		 	{
		 		fichier << vector[j].findGlobalColorP(i)->getSphericity() << " ";
    	 	}
		 	for(int j=0; j<vector.size(); j++)
		 	{
		 		fichier << vector[j].findGlobalColorP(i)->getShapeFact() << " ";
    	 	}
		 	for(int j=0; j<vector.size(); j++)
		 	{
		 		fichier << vector[j].findGlobalColorP(i)->getShapeFact2() << " ";
    	 	}
		fichier << endl;
        }
        
		fichier.close();  // on referme le fichier
		 	 }
		 	 else  // sinon
		 	 cerr << "Erreur à l'ouverture !" << endl;
}

//template<>
void writePSetGlobalTranspose(string FileName, vector<PSet<P> > vector)
{

	 ofstream fichier(FileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier
     if(fichier)  // si l'ouverture a réussi
	 {
        int nb = vector[0].Size();
        for(int j=0; j<vector.size(); j++)
		{
		    fichier << "color_scan" << vector[j].getP(0)->getScan() << " ";
    	}
        for(int j=0; j<vector.size(); j++)
		{
		    fichier << "Xg_scan" << vector[j].getP(0)->getScan() << " ";
    	}
        for(int j=0; j<vector.size(); j++)
		{
		    fichier << "Yg_scan" << vector[j].getP(0)->getScan() << " ";
    	}
         for(int j=0; j<vector.size(); j++)
		{
		    fichier << "Zg_scan" << vector[j].getP(0)->getScan() << " ";
    	}
        fichier << endl;
        for(int i=1; i<=nb; i++)
        {
		 	for(int j=0; j<vector.size(); j++)
		 	{
		 		fichier << vector[j].findGlobalColorP(i)->getColor() << " ";
    	 	}
		 	for(int j=0; j<vector.size(); j++)
		 	{
		 		fichier << vector[j].findGlobalColorP(i)->getCoord().getX() << " ";
    	 	}
		 	for(int j=0; j<vector.size(); j++)
		 	{
		 		fichier << vector[j].findGlobalColorP(i)->getCoord().getY() << " ";
    	 	}
		 	for(int j=0; j<vector.size(); j++)
		 	{
		 		fichier << vector[j].findGlobalColorP(i)->getCoord().getZ() << " ";
    	 	}
		fichier << endl;
        }
        
		fichier.close();  // on referme le fichier
		 	 }
		 	 else  // sinon
		 	 cerr << "Erreur à l'ouverture !" << endl;
}

int writePSetGlobalTransposeNucleateAndMerge(string FileName, vector<PSet<Particle> > vector)
{
    int nb = 0;
    for(int j=0; j<vector.size(); j++)
    {
        int max = vector[j].maxGlobalColor();
        if(max>nb)
            nb=max;
    }
    LINFO << "Total number of objects is " << nb;
    
	 ofstream fichier(FileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier
     if(fichier)  // si l'ouverture a réussi
	 {
        //int nb = vector[0].Size();
        for(int j=0; j<vector.size(); j++)
		{
		    fichier << "color_scan" << vector[j].getP(0)->getScan() << " ";
    	}
        for(int j=0; j<vector.size(); j++)
		{
		    fichier << "valid_scan" << vector[j].getP(0)->getScan() << " ";
    	}
        for(int j=0; j<vector.size(); j++)
		{
		    fichier << "Xg_scan" << vector[j].getP(0)->getScan() << " ";
    	}
        for(int j=0; j<vector.size(); j++)
		{
		    fichier << "Yg_scan" << vector[j].getP(0)->getScan() << " ";
    	}
         for(int j=0; j<vector.size(); j++)
		{
		    fichier << "Zg_scan" << vector[j].getP(0)->getScan() << " ";
    	}
         for(int j=0; j<vector.size(); j++)
		{
		    fichier << "volume_scan" << vector[j].getP(0)->getScan() << " ";
    	}
        for(int j=0; j<vector.size(); j++)
		{
		    fichier << "sphericity_scan" << vector[j].getP(0)->getScan() << " ";
    	}
        for(int j=0; j<vector.size(); j++)
		{
		    fichier << "shapeFact_scan" << vector[j].getP(0)->getScan() << " ";
    	}
        for(int j=0; j<vector.size(); j++)
		{
		    fichier << "shapeFact2_scan" << vector[j].getP(0)->getScan() << " ";
    	}
        for(int j=0; j<vector.size(); j++)
		{
		    fichier << "connectingto_scan" << vector[j].getP(0)->getScan() << " ";
    	}
        fichier << endl;
        for(int i=1; i<=nb; i++)
        {
		 	for(int j=0; j<vector.size(); j++)
		 	{
		 		if(vector[j].hasGlobalColor(i))
                    fichier << vector[j].findGlobalColorP(i)->getColor() << " ";
                else
                    fichier << "0 "; 
    	 	}
		 	for(int j=0; j<vector.size(); j++)
		 	{
		 		if(vector[j].hasGlobalColor(i))
		 		    fichier << vector[j].findGlobalColorP(i)->getCorrel() << " ";
                else
                    fichier << "0 "; 
    	 	}
			 	for(int j=0; j<vector.size(); j++)
		 	{
		 		if(vector[j].hasGlobalColor(i))
		 		    fichier << vector[j].findGlobalColorP(i)->getCoord().getX() << " ";
                else
                    fichier << "0 "; 
    	 	}
		 	for(int j=0; j<vector.size(); j++)
		 	{
		 		if(vector[j].hasGlobalColor(i))
		 		    fichier << vector[j].findGlobalColorP(i)->getCoord().getY() << " ";
                else
                    fichier << "0 "; 
    	 	}
		 	for(int j=0; j<vector.size(); j++)
		 	{
		 		if(vector[j].hasGlobalColor(i))
		 		    fichier << vector[j].findGlobalColorP(i)->getCoord().getZ() << " ";
                else
                    fichier << "0 "; 
    	 	}
		 	for(int j=0; j<vector.size(); j++)
		 	{
		 		if(vector[j].hasGlobalColor(i))
		 		    fichier << vector[j].findGlobalColorP(i)->getVolume() << " ";
                else
                    fichier << "0 "; 
    	 	}
		 	for(int j=0; j<vector.size(); j++)
		 	{
		 		if(vector[j].hasGlobalColor(i))
		 		    fichier << vector[j].findGlobalColorP(i)->getSphericity() << " ";
                else
                    fichier << "0 "; 
    	 	}
		 	for(int j=0; j<vector.size(); j++)
		 	{
		 		if(vector[j].hasGlobalColor(i))
		 		    fichier << vector[j].findGlobalColorP(i)->getShapeFact() << " ";
                else
                    fichier << "0 "; 
    	 	}
		 	for(int j=0; j<vector.size(); j++)
		 	{
		 		if(vector[j].hasGlobalColor(i))
		 		    fichier << vector[j].findGlobalColorP(i)->getShapeFact2() << " ";
                else
                    fichier << "0 "; 
    	 	}
		 	for(int j=0; j<vector.size(); j++)
		 	{
		 		if(vector[j].hasGlobalColor(i))
		 		    fichier << vector[j].findGlobalColorP(i)->getFather() << " ";
                else
                    fichier << "-1 "; 
    	 	}
		fichier << endl;
        }
        
		fichier.close();  // on referme le fichier
		 	 }
		 	 else  // sinon
		 	 cerr << "Erreur à l'ouverture !" << endl;

    return nb;
}


void writePSetForMeshDef(vector<string> FileName, vector<PSet<Particle> > vector)
{
    int nbscan = FileName.size();
    for(int j=0; j<nbscan; j++)
    {
	    ofstream fichier(FileName[j].c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier
        if(fichier)  // si l'ouverture a réussi
	    {
            int nb = vector[0].Size();
            fichier << nb << endl;   
            for(int i=1; i<=nb; i++)
            {
		 		fichier << vector[j].findGlobalColorP(i)->getGlobalColor() << " ";
		 		fichier << vector[j].findGlobalColorP(i)->getCorrel() << " ";
		 		fichier << vector[j].findGlobalColorP(i)->getCoord().getX() << " ";
		 		fichier << vector[j].findGlobalColorP(i)->getCoord().getY() << " ";
		 		fichier << vector[j].findGlobalColorP(i)->getCoord().getZ();
                fichier << endl;
    	    }
		fichier.close();  // on referme le fichier
        }
	 	else  // sinon
	 	    cerr << "Erreur à l'ouverture !" << endl;
    }
}

void writePSetGlobal(string FileName, vector<PSet<Particle> > vector)
{

	 ofstream fichier(FileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier
		 	 if(fichier)  // si l'ouverture a réussi
		 	 {
		 		 for(int j=0; j<vector.size(); j++)
		 	 	 {

		 				fichier << vector[j].colorTostring();
		 				fichier << vector[j].colorGlobalTostring();
		 				fichier << vector[j].volumeTostring();
		 				fichier << vector[j].PosTostring() << endl;
		 	 	 }
		 	 	 fichier.close();  // on referme le fichier
		 	 }
		 	 else  // sinon
		 	 cerr << "Erreur à l'ouverture !" << endl;
}

void writePSetGlobal(string FileName, vector<PSet<P> > vector)
{

	 ofstream fichier(FileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier
		 	 if(fichier)  // si l'ouverture a réussi
		 	 {
		 		 for(int j=0; j<vector.size(); j++)
		 	 	 {

		 				fichier << vector[j].PosTostring() << endl;
		 	 	 }
		 	 	 fichier.close();  // on referme le fichier
		 	 }
		 	 else  // sinon
		 	 cerr << "Erreur à l'ouverture !" << endl;
}



template <typename T>
void writePointsFiles(string FileNamePrefix, string FileNameSuffix, vector<PSet<T> > vector, bool append)
{
    if(!append){
        LINFO << "Write single point files";
	    for(int i=0; i<vector[0].Size(); i++)
        {
		    char numpts[5];
            int color =vector[0].getP(i)->getColor();
		    sprintf(numpts,"%05d",color);
            string FileName = FileNamePrefix + string(numpts) +FileNameSuffix;
	        ofstream fichier(FileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier
		    if(fichier)  // si l'ouverture a réussi
	 	    {
			    for(int j=0; j<vector.size(); j++)
		 	    {
		 	    	fichier << vector[j].findP(color)->tostringFull() << endl;
		 	    }
		 	    fichier.close();  // on referme le fichier
		    }
		    else  // sinon
		 	    LERROR << "Erreur à l'ouverture !";
        }
    }
    else
    {
        int current=vector.size()-1;
	    for(int i=0; i<vector[current].Size(); i++)
        {
            char numpts[5];
            int color =vector[current].getP(i)->getColor();
		    sprintf(numpts,"%05d",color);
            string FileName = FileNamePrefix + string(numpts) +FileNameSuffix;
	        ofstream fichier(FileName.c_str(), ios::out | ios::app);  //déclaration du flux et ouverture du fichier
		    if(fichier)  // si l'ouverture a réussi
	 	    {
		 	   	fichier << vector[current].findP(color)->tostringFull() << endl;
		 	    fichier.close();  // on referme le fichier
		    }
		    else  // sinon
		 	    LERROR << "Erreur à l'ouverture !";
        }
    }
}


template < typename T >
void readVectorPSet(string Directory, string VectorPSetName, vector<PSet<T> > & v, vector<vector<T*> > & vData)
{
	 //vector<PSet<Particle> >* v = new vector<PSet<Particle> >();
	 ostringstream fileName;
	 fileName << Directory << "/VectorPSet_" << VectorPSetName;
     ifstream fichier(fileName.str().c_str(), ios::in);  //déclaration du flux et ouverture du fichier
     if(fichier)  // si l'ouverture a réussi
	 {
    	 while (fichier.good())
    	 {
    	 	string psName = "";
    	 	char schar[10000];
    	 	fichier.getline(schar,10000);
    	    psName+=schar;
    	    ostringstream fullpsName;
    	    fullpsName << Directory << "/" << psName;
    	    if(ifstream(fullpsName.str().c_str()) && psName!=""){
    	    	vector<T*> * totoData = new vector<T*>();
    	    	PSet<T> * toto = new PSet<T> (Directory,psName,*totoData);
    	    	vData.push_back(*totoData);
    	    	v.push_back(*toto);
    	    }
    	 }
	 	 fichier.close();
	 }
     else  // sinon
     {
         	 cerr << "Erreur à l'ouverture !" << endl;
          	 cerr << "File " << fileName.str() << " not found!" << endl;
      }
     //return *v;
}


template void writeVectorPSet(string Directory, string VectorPSetName, vector<PSet<P> > & vector);
template void writeVectorPSet(string Directory, string VectorPSetName, vector<PSet<Particle> > & vector);

template void writePointsFiles(std::string FileNamePrefix, std::string FileNameSuffix, std::vector<PSet<P> > vector, bool append);
template void writePointsFiles(std::string FileNamePrefix, std::string FileNameSuffix, std::vector<PSet<Particle> > vector, bool append);

template void readVectorPSet(std::string dir, std::string VectorPSetName, std::vector<PSet<P> > & particles, std::vector<std::vector<P*> > & particlesData);
template void readVectorPSet(std::string dir, std::string VectorPSetName, std::vector<PSet<Particle> > & particles, std::vector<std::vector<Particle*> > & particlesData);


