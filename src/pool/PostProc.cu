#include "PostProc.h"
#include "PostProc.cuh"


/*!
 * \file PostProc.cpp
 * \brief Correlation 3D post processing
 * \author pierre.lhuissier@simap.grenoble-inp.fr
 * \version 0.1
 */

using namespace std;
   
 
/*!
*  \brief Constructor
*
*  Constructor of PostProc class
*
*  \param param : param of the correlation
*/


PostProc::PostProc(Parameters* paramglob, const string mask, PSet<P> &ptsI, PSet<P> &ptsF) {
    h_Param=paramglob;
    correlThr = h_Param->correlThr;
    regulDist  = h_Param->regulDist;
    regulDistThr  = h_Param->regulThrDist;
    deviceTotalMem = 0;
    imMask=mask;
    LINFO << imMask;
    setDimensions(imMask.c_str());
    dev = 0;
    cudaSetDevice(dev);
    cudaGetDeviceProperties(&deviceProp, dev);
    ptsInit=ptsI;
    ptsFinal=ptsF;
}


void PostProc::setPointsInit(PSet<P> &pts){
    ptsInit=pts;
}

void PostProc::setPointsFinal(PSet<P> &pts){
    ptsFinal=pts;
}

/*!
*  \brief Destructor
*
*  Destructor of PostProc class
*/

void PostProc::finalize()
{
		
        free();

}

void PostProc::setDimensions(const char* img1, unsigned long& fullDataW, unsigned long& fullDataH, unsigned long& fullDataD)
{
	string mode = "r";
	TIFF* tif = TIFFOpen(img1,mode.c_str());
	if (tif) {
		uint32 imageWidth, imageLength;
		TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &imageWidth);
		TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &imageLength);
		int dircount = 0;
        do {
            dircount++;
        } while (TIFFReadDirectory(tif));
        TIFFClose(tif);
		fullDataW = (unsigned long)imageWidth;
		fullDataH = (unsigned long)imageLength;
		fullDataD = (unsigned long)dircount;
	}
	else
	{
		fullDataW = 0;
		fullDataH = 0;
		fullDataD = 0;
	}
}


void PostProc::setDimensions(const char* img1)
{
	string mode = "r";
	TIFF* tif = TIFFOpen(img1,mode.c_str());
	if (tif) {
		uint32 imageWidth, imageLength;
		TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &imageWidth);
		TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &imageLength);
		int dircount = 0;
        do {
            dircount++;
        } while (TIFFReadDirectory(tif));
        TIFFClose(tif);
		fullDataW = (unsigned long)imageWidth;
		fullDataH = (unsigned long)imageLength;
		fullDataD = (unsigned long)dircount;
	}
	else
	{
		fullDataW = 0;
		fullDataH = 0;
		fullDataD = 0;
	}

	   	fullData_size = fullDataD * fullDataH * fullDataW;
		mem_fullData_byte = fullData_size*sizeof(unsigned char);
}


//void PostProc::init(const char* imMask)
void PostProc::init()
{
    LINFO << "Initiate device for post processor";
    LINFO << "Use device : " << deviceProp.name;
    LINFO << "device Mem = " << (long)deviceProp.totalGlobalMem;

    setDimensions(imMask.c_str(), fullDataW, fullDataH, fullDataD);
    cout << "Dimensions (" << fullDataW << "," << fullDataH << "," << fullDataD << ")" << endl;
    mem_fullData_byte = fullDataD*fullDataH*fullDataW*sizeof(float)*3;
    h_Mask = new unsigned char[fullDataD*fullDataH*fullDataW];
    h_Depl = new float[((long)fullDataD)*fullDataH*fullDataW*3];
    hostFree = false;
    readfromTIFF(imMask.c_str(),h_Mask);
    
    setPoints();
    cudaMalloc((void **)&d_Points,  nbPoints*6*sizeof(float));
    deviceTotalMem += nbPoints*6*sizeof(float);
    checkCudaErrors(cudaMemcpy(d_Points, h_Points, (long)nbPoints*6*sizeof(float), cudaMemcpyHostToDevice));
   
    //LINFO << format("Allocated %d floats for d_Points\n", nbPoints*6);
    //LINFO << "device Mem Used = " << deviceTotalMem;

    fullDatasubD = (unsigned long)(((long)(0.8*deviceProp.totalGlobalMem)-nbPoints*6*sizeof(float))/(4*fullDataW*fullDataH*sizeof(float)))-1;
    cudaMalloc((void **)&d_Depl,  3*fullDatasubD*fullDataH*fullDataW*sizeof(float));
    cudaMallocHost(&h_Depl, 3*fullDataD*fullDataH*fullDataW*sizeof(float));
    deviceTotalMem += (3*fullDatasubD*fullDataH*fullDataW*sizeof(float));
    //LINFO << format("Allocated %d floats for d_Depl\n", 3*fullDatasubD*fullDataH*fullDataW);
    //LINFO << "device Mem Used = " << deviceTotalMem;
    LINFO << "Size of sub volume = " << fullDatasubD << " slices";

    cudaMalloc((void **)&d_Coeff,  fullDatasubD*fullDataH*fullDataW*sizeof(float));
    deviceTotalMem += (fullDatasubD*fullDataH*fullDataW*sizeof(float));
    //LINFO << format("Allocated %d floats for d_Coeff\n", fullDatasubD*fullDataH*fullDataW);
    //LINFO << "device Mem Used = " << deviceTotalMem;
    LINFO << format("Allocated %.4f Mbytes / %.0f Mbytes (%.4f %) on GPU memory\n\n",(float)deviceTotalMem/1048576.0f, (float)deviceProp.totalGlobalMem/1048576.0f, (100.*deviceTotalMem)/deviceProp.totalGlobalMem);        
    deviceFree = false;

    if(h_Param->ezz)
    {
        h_ezz = new float[((long)fullDataD)*fullDataH*fullDataW];
        cudaMallocHost(&h_ezz, fullDataD*fullDataH*fullDataW*sizeof(float));
    }
}


/*!
*  \brief Free allocated memory 
*/

void PostProc::free()
{
    
     if(!deviceFree)
    {
	    cudaFree(d_Depl);
        cudaFree(d_Coeff);
        cudaFree(d_Points);
        deviceFree = true;
    }

    if(!hostFree)
    {
	    cudaFreeHost(h_Depl);
        h_Depl = NULL;
	    cudaFreeHost(h_ezz);
        h_ezz = NULL;
	    //cudaFreeHost(h_Mask);
        h_Mask = NULL;
	    cudaFreeHost(h_Points);
        h_Points = NULL;
        hostFree = true;
    }

}



void PostProc::setPoints(PSet<P> &ptsInit, PSet<P> &ptsFinal)
{
    setPointsInit(ptsInit);
    setPointsFinal(ptsFinal);
    setPoints();

}

void PostProc::setPoints()
{
    LINFO << "Set points data in post processor";
    unsigned long points=0;
    list<P*>::iterator pit=ptsInit.getIteratorBegin();
    for (list<P*>::iterator pitF=ptsFinal.getIteratorBegin(); pitF!=ptsFinal.getIteratorEnd(); ++pitF)
	{
        if((*pitF)->getCorrel()>correlThr)
            points++;
	}
    nbPoints=points;
    LINFO << "Found " << nbPoints << " valid points";    
    points=0;
    h_Points = new float[6*nbPoints];
    cudaMallocHost(&h_Points, nbPoints*6*sizeof(float));
    hostFree=false;
    deviceFree=false;
    for (list<P*>::iterator pitF=ptsFinal.getIteratorBegin(); pitF!=ptsFinal.getIteratorEnd(); ++pitF)
	{
        if((*pitF)->getCorrel()>correlThr)
        {
            h_Points[6*points]=(*pitF)->getCoord().getX();
            h_Points[6*points+1]=(*pitF)->getCoord().getY();
            h_Points[6*points+2]=(*pitF)->getCoord().getZ();
            h_Points[6*points+3]=(*pitF)->getCoord().getX()-(*pit)->getCoord().getX();
            h_Points[6*points+4]=(*pitF)->getCoord().getY()-(*pit)->getCoord().getY();
            h_Points[6*points+5]=(*pitF)->getCoord().getZ()-(*pit)->getCoord().getZ();
            points++;            
            ++pit;
        }
    }
}

void PostProc::checkValue(float *d_Data, unsigned long index)
{
    float* h_value = new float[0];
    cudaMemcpy(h_value, &d_Data[index], sizeof(float), cudaMemcpyDeviceToHost);
    LINFO << format("%s[%u]=%f", "d_Data", index, h_value[0]);
    h_value=NULL;
}

extern "C" void PostProc::calcDeplField(float *d_Depl, float *d_Coeff, float *d_Points, float regulDistThr, float regulDist, unsigned long fullDataD, unsigned long fullDataH, unsigned long fullDataW, unsigned long fullDatasubD, unsigned long nbPoints){
    dim3 threads(256, 1, 1);
    dim3 grid(iDivUp(2*regulDistThr, threads.x), 2*regulDistThr, 2*regulDistThr);
    int nbround = iDivUp(fullDataD,fullDatasubD-2); 
    int nbremains = fullDataD%(fullDatasubD-2);
    //checkValue(d_Points, 0);
    //checkValue(d_Points, 1);
    //checkValue(d_Points, 2);
    //checkValue(d_Points, 3);
    //checkValue(d_Points, 4);
    //checkValue(d_Points, 5);
    //LINFO << "regulDistThr=" << regulDistThr;

    for(unsigned long j=0; j<nbround; j++)
    {
        LINFO << "Step " << (j+1) << "/" << nbround;

        //unsigned long index = (unsigned long)(h_Points[0]+h_Points[1]*fullDataW+ ((unsigned long)h_Points[2])%fullDatasubD*fullDataW*fullDataH);
        LDEBUG << "round " << j;
        //checkValue(d_Depl,3*index);
        //checkValue(d_Coeff,index);
        
        LDEBUG << "Reset displacement";
        dim3 gridreset(iDivUp(fullDataW*3,threads.x),fullDataH,fullDatasubD);
        LDEBUG << format("grid(%d,%d,%d) block(%d,%d,%d)",gridreset.x,gridreset.y, gridreset.z,threads.x,threads.y,threads.z);

        kernel_resetData<<<gridreset,threads>>>(d_Depl, fullDatasubD*fullDataH*fullDataW*3);
        getLastCudaError("Kernel execution failed");
        cudaDeviceSynchronize();

        //checkValue(d_Depl,3*index);
        //checkValue(d_Coeff,index);        

        LDEBUG << "Reset weighting factor";
        dim3 gridreset2(iDivUp(fullDataW,threads.x),fullDataH,fullDatasubD);
        kernel_resetData<<<gridreset2,threads>>>(d_Coeff, fullDatasubD*fullDataH*fullDataW);
        getLastCudaError("Kernel execution failed");            
        cudaDeviceSynchronize();

        //checkValue(d_Depl,3*index);
        //checkValue(d_Coeff,index);
        

        LDEBUG << "Interpolates displacement field";
        LDEBUG << "Uses " << nbPoints << " points";
        for(unsigned long i=0; i<nbPoints; i++)
        {
            //LINFO << " Point " << (i+1) << "/" << nbPoints;
            //checkValue(d_Points,6*i+0);
            //checkValue(d_Points,6*i+1);
            //checkValue(d_Points,6*i+2);
            kernel_calcDeplField<<<grid,threads>>>(d_Depl, d_Coeff, d_Points, regulDistThr, regulDist, fullDataD, fullDataH, fullDataW, fullDatasubD, j, i);
            getLastCudaError("Kernel execution failed");
            cudaDeviceSynchronize();
        }
        
        LDEBUG << "Normalize by weighting factor";
        kernel_div<<<gridreset2,threads>>>(d_Depl, d_Coeff, fullDatasubD*fullDataH*fullDataW);
        getLastCudaError("Kernel execution failed");
        kernel_div<<<gridreset2,threads>>>(&(d_Depl[1]), d_Coeff, fullDatasubD*fullDataH*fullDataW);
        getLastCudaError("Kernel execution failed");
        kernel_div<<<gridreset2,threads>>>(&(d_Depl[2]), d_Coeff, fullDatasubD*fullDataH*fullDataW);
        getLastCudaError("Kernel execution failed");
        cudaDeviceSynchronize();

        if(h_Param->ezz)
        {
            LDEBUG << "Compute strain field";
            kernel_resetData<<<gridreset2,threads>>>(d_Coeff, fullDatasubD*fullDataH*fullDataW);
            getLastCudaError("Kernel execution failed");            
            cudaDeviceSynchronize();
            kernel_calcDefField<<<gridreset2,threads>>>(d_Depl, d_Coeff, fullDataD, fullDataH, fullDataW, fullDatasubD, j);
            getLastCudaError("Kernel execution failed");
            cudaDeviceSynchronize();
            LDEBUG << "Copies back data";
		    if(j<nbround-1  )  
            {
                LDEBUG << "Slices from " << (j*(fullDatasubD-1)+1) << " to " << ((j*(fullDatasubD-1)+1)+ fullDatasubD-2);     
                checkCudaErrors(cudaMemcpy(&(h_ezz[(j*(fullDatasubD-2)+1)*fullDataH*fullDataW]), &(d_Coeff[fullDataH*fullDataW]), (fullDatasubD-2)*fullDataH*fullDataW*sizeof(float), cudaMemcpyDeviceToHost));
                cudaDeviceSynchronize();
            }
            else
            {
                int nbremains2 = (fullDataD-2)%(fullDatasubD-2)-nbround+1;
                LDEBUG << "Slices from " << (j*(fullDatasubD-1)+1) << " to " << ((j*(fullDatasubD-1)+1) + nbremains2);     
                cudaMemcpy(&(h_ezz[(j*(fullDatasubD-1)+1)*fullDataH*fullDataW]), &(d_Coeff[fullDataH*fullDataW]), nbremains2*fullDataH*fullDataW*sizeof(float), cudaMemcpyDeviceToHost);
                cudaDeviceSynchronize();
            }
        }
        else
        {
            LDEBUG << "Copies back data";
		    if(j<nbround-1  )           
                checkCudaErrors(cudaMemcpy(&(h_Depl[j*(fullDatasubD-2)*fullDataH*fullDataW*3]), d_Depl, (fullDatasubD-2)*fullDataH*fullDataW*3*sizeof(float), cudaMemcpyDeviceToHost));
            else
                cudaMemcpy(&(h_Depl[j*(fullDatasubD-2)*fullDataH*fullDataW*3]), d_Depl, nbremains*fullDataH*fullDataW*3*sizeof(float), cudaMemcpyDeviceToHost);
            cudaDeviceSynchronize();
        }
    }
}
/*!
*  \brief Compute depl field
*
*  \param dimY : height of image
*  \param dimX : width of image
*/

void PostProc::calcDeplField()
{
    LINFO << "Computing strain field";
    calcDeplField(d_Depl, d_Coeff, d_Points, regulDistThr, regulDist, fullDataD, fullDataH, fullDataW, fullDatasubD, nbPoints);
}



/**
    int index=0;
    for(int k=0; k<fullDataD; k++){
    for(int j=0; j<fullDataH; j++){
    LINFO << "slice " << k << " line " << j;
    for(int i=0; i<fullDataW; i++){
        if (h_Mask[index]>0)
        {
            Coord depl = ptsInit.estimateLargeDepl(Coord(i,j,k), ptsFinal, h_Param->correlThr, h_Param->regulThrDist, h_Param->regulDist);
            h_FullData[index]=depl.getX();
            h_FullData[fullDataD*fullDataH*fullDataW+index]=depl.getY();
            h_FullData[fullDataD*fullDataH*fullDataW*2+index]=depl.getZ();
        }
        else
        {
            h_FullData[index]=0.;
            h_FullData[fullDataD*fullDataH*fullDataW+index]=0.;
            h_FullData[fullDataD*fullDataH*fullDataW*2+index]=0.;
        }
    }
    }
    }
*/
void PostProc::applyMask()
{
    LINFO << "Apply mask";
    unsigned long length = fullDataD*fullDataH*fullDataW;
    for(unsigned long i=0; i<length; i++)
    {
        if(h_Mask[i]<128)
        {
           h_ezz[i]=-100;
        }
    }
}

void PostProc::checkDeplField()
{
    LINFO << "Chech displacement field";
    unsigned long length = fullDataD*fullDataH*fullDataW*3;
    float dmax=-10000000.;   	
    for(unsigned long i=0; i<length; i++)
    {
        if(h_Depl[i]>dmax)
        {
            dmax=h_Depl[i];
        }
    }
    LINFO << "Max value of displ field is " << dmax;        
}

void PostProc::writeDeplField(const string image_path)
{
    //writetoTIFF(image_path.c_str(), h_Depl, fullDataD, fullDataH, fullDataW ,3);
    writetoRAW(image_path.c_str(), h_Depl, fullDataD, fullDataH, fullDataW ,3);
}

void PostProc::writeDeplField(const string prefix, const string suffix)
{
    writetoRAW(prefix, suffix, h_Depl, fullDataD, fullDataH, fullDataW ,3);
}

void PostProc::writeDeplField(const string prefix, const string suffix, int dim)
{
    writetoRAW(prefix, suffix, h_Depl, fullDataD, fullDataH, fullDataW ,3, dim);
}

void PostProc::writeEpsField(const string image_path)
{
    writetoRAW(image_path.c_str(), h_ezz, fullDataD, fullDataH, fullDataW);
}


/*!
*  \brief Read raw image
*
*  \param image_path : Path of image
*  \param pix : pointer to image array
*  \param dimZ : depth of image
*  \param dimY : height of image
*  \param dimX : width of image
*/

void PostProc::readfromRAW(const char *name, unsigned char *m, const unsigned long dim_1, const unsigned long dim_2, const unsigned long dim_3)
{
    LINFO << format("Loading from %s...\n", name);
    FILE *fo;
  	fo=fopen(name,"rb");
   	fread(m,sizeof(unsigned char),dim_1*dim_2*dim_3,fo);
   	fclose(fo);
}

/*!
*  \brief Read tiff image
*
*  \param image_path : Path of image
*  \param pix : pointer to image array
*/

void PostProc::readfromTIFF(const char * image_path, unsigned char* pix)
{
	LINFO << format("Loading from %s...\n",image_path);

	TIFF* tif = TIFFOpen(image_path, "r");
	if (tif) {
		uint32 imageWidth, imageLength;
		uint32 row;
		int j=0;
		//int slice=0;
		TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &imageWidth);
		TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &imageLength);
		//TIFFGetField(tif, TIFFTAG_IMAGEDEPTH, &imageDepth);
		//printf("Volume size checked (%d,%d,%d)\n",imageWidth,imageLength,imageDepth);
		unsigned char* buf = (unsigned char*)malloc(imageWidth *sizeof(unsigned char));
		do
		{
			//slice ++;
			//int slice_count=0;
			//buf = _TIFFmalloc(TIFFScanlineSize(tif));
			for (row = 0; row < imageLength; row++)
			{
				TIFFReadScanline(tif, buf, row);
				for (int k = 0; k < imageWidth; k++)
				{
					pix[j] = buf[k];
					j++;
					//if(buf[k]==(unsigned char)255)
					//	slice_count++;
				}
			}
		//	printf("slice %d found %d white pixels\n",slice,slice_count);
		} while (TIFFReadDirectory(tif));
		_TIFFfree(buf);
		TIFFClose(tif);
	}
}

void PostProc::writetoRAW(const char * image_path, float* pix, unsigned long imageWidth, unsigned long imageLength, unsigned long imageDepth )
{
	LINFO << format("Writing to %s...\n",image_path);
    FILE *fo;
  	fo=fopen(image_path,"wb");
   	fwrite(pix,sizeof(float),imageWidth*imageLength*imageDepth,fo);
   	fclose(fo);
}

//void PostProc::writetoRAW(const char * image_path, float* pix, unsigned int imageWidth, unsigned int imageLength, unsigned int imageDepth, unsigned int channel )

void PostProc::writetoRAW(const char * image_path, float* pix, unsigned long imageWidth, unsigned long imageLength, unsigned long imageDepth, unsigned long channel )
{
	LINFO << format("Writing to %s (%d,%d,%d,%d)...\n",image_path,imageWidth,imageLength,imageDepth,channel );
    //LINFO << format("size of float is %ld byte", (long)(sizeof(float)));
    //LINFO << format("size should be %ld byte", (long)(((long)imageWidth)*imageLength*imageDepth*channel*sizeof(float)));
  	 //TODO large file here I divided by 4 to have a result
    unsigned long length = imageWidth*imageLength*imageDepth*channel;
   	FILE *fo;
    fo=fopen(image_path,"wb");
    fwrite(pix,sizeof(float),length,fo);
    fclose(fo);
  	LDEBUG << "writing finished"; 
   
}

void PostProc::writetoRAW(const string prefix, const string suffix, float* pix, unsigned long imageWidth, unsigned long imageLength, unsigned long imageDepth, unsigned long channel )
{
	LINFO << format("Writing to %s (%d,%d,%d,%d)...\n",prefix.c_str(),imageWidth,imageLength,imageDepth,channel );
    //LINFO << format("size of float is %ld byte", (long)(sizeof(float)));
    //LINFO << format("size should be %ld byte", (long)(((long)imageWidth)*imageLength*imageDepth*channel*sizeof(float)));
  	 //TODO large file here I divided by 4 to have a result
    unsigned long length = imageWidth*imageLength*imageDepth*channel;
   	unsigned long packsize = length/2;
    for(int step=0; step<2; step++)
    {
        LINFO << format("Writing %s%d%s", prefix.c_str(), step+1, suffix.c_str());
        FILE *fo;
        string image_path = format("%s%d%s", prefix.c_str(), step+1, suffix.c_str());
        fo=fopen(image_path.c_str(),"wb");
        fwrite(&(pix[packsize*step]),sizeof(float),packsize,fo);
        fclose(fo);
    }
  	LDEBUG << "writing finished";     
}

void PostProc::writetoRAW(const string prefix, const string suffix, float* pix, unsigned long imageWidth, unsigned long imageLength, unsigned long imageDepth, unsigned long channel, int dim )
{
	LINFO << format("Writing to %s (%d,%d,%d,%d)...\n",prefix.c_str(),imageWidth,imageLength,imageDepth,1 );
    //LINFO << format("size of float is %ld byte", (long)(sizeof(float)));
    //LINFO << format("size should be %ld byte", (long)(((long)imageWidth)*imageLength*imageDepth*channel*sizeof(float)));
  	 //TODO large file here I divided by 4 to have a result
    unsigned long length = imageWidth*imageLength*imageDepth;
   	unsigned long packsize = length/2;
    for(int step=0; step<2; step++)
    {
        LINFO << format("Writing %s_dim%d_%d%s", prefix.c_str(), dim, step+1, suffix.c_str());
        FILE *fo;
        string image_path = format("%s_dim%d_%d%s", prefix.c_str(), dim, step+1, suffix.c_str());
        fo=fopen(image_path.c_str(),"wb");
        for(unsigned long j=0; j<packsize; j++)
        {
            fwrite(&(pix[packsize*channel*step+dim-1+channel*j]),sizeof(float),1,fo);
        }
        fclose(fo);
    }
  	LDEBUG << "writing finished";     
}

void PostProc::writetoTIFF(const char * image_path, unsigned char* pix, unsigned long imageWidth, unsigned long imageLength, unsigned long imageDepth )
{
	LINFO << format("Writing to %s...\n",image_path);

	TIFF* tif = TIFFOpen(image_path, "w");
	if (tif) {
		for(int slice=0; slice <imageDepth; slice++)
		{
			TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, imageWidth);
					TIFFSetField(tif, TIFFTAG_IMAGELENGTH, imageLength);
					TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, 1);   // set number of channels per pixel
					TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, 8);    // set the size of the channels
					TIFFSetField(tif, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);    // set the origin of the image.
					TIFFSetField(tif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
					TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
					TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_NONE);
					TIFFSetField(tif, TIFFTAG_SUBFILETYPE, FILETYPE_PAGE);TIFFSetField(tif, TIFFTAG_PAGENUMBER, slice+1, imageDepth);

			//Now writing image to the file one strip at a time
			for (uint32 row = 0; row < imageLength; row++)
			{
				if (TIFFWriteScanline(tif, &pix[row*imageWidth+slice*imageWidth*imageLength], row, 0) != 1)
					break;
			}
			TIFFWriteDirectory(tif);

		}
		(void) TIFFClose(tif);
		//if (buf)
		//	_TIFFfree(buf);
	}
}

void PostProc::writetoTIFF(const char * image_path, float* pix, unsigned long imageWidth, unsigned long imageLength, unsigned long imageDepth )
{
	LINFO << format("Writing to %s...\n",image_path);

	TIFF* tif = TIFFOpen(image_path, "w");
	if (tif) {
		for(int slice=0; slice <imageDepth; slice++)
		{
			        TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, imageWidth);
					TIFFSetField(tif, TIFFTAG_IMAGELENGTH, imageLength);
					TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, 1);   // set number of channels per pixel
					TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, 32);    // set the size of the channels
					TIFFSetField(tif, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);    // set the origin of the image.
					TIFFSetField(tif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
					TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
					TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_NONE);
					TIFFSetField(tif, TIFFTAG_SUBFILETYPE, FILETYPE_PAGE);
                    TIFFSetField(tif, TIFFTAG_PAGENUMBER, slice+1, imageDepth);

			//Now writing image to the file one strip at a time
			for (uint32 row = 0; row < imageLength; row++)
			{
				if (TIFFWriteScanline(tif, &pix[row*imageWidth+slice*imageWidth*imageLength], row, 0) != 1)
					break;
			}
			TIFFWriteDirectory(tif);

		}
		(void) TIFFClose(tif);
		//if (buf)
		//	_TIFFfree(buf);
	}
}

void PostProc::writetoTIFF(const char * image_path, float* pix, unsigned long imageWidth, unsigned long imageLength, unsigned long imageDepth , unsigned long channel)
{
	LINFO << format("Writing to %s...\n",image_path);

	TIFF* tif = TIFFOpen(image_path, "w");
	if (tif) {
		for(int slice=0; slice <imageDepth; slice++)
		{
			        TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, imageWidth);
					TIFFSetField(tif, TIFFTAG_IMAGELENGTH, imageLength);
					TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, channel);   // set number of channels per pixel
					TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, 32);    // set the size of the channels
					TIFFSetField(tif, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT);    // set the origin of the image.
					TIFFSetField(tif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
					TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_MINISBLACK);
					TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_NONE);
					TIFFSetField(tif, TIFFTAG_SUBFILETYPE, FILETYPE_PAGE);
                    TIFFSetField(tif, TIFFTAG_PAGENUMBER, slice+1, imageDepth);

			//Now writing image to the file one strip at a time
			for (uint32 row = 0; row < imageLength; row++)
			{
				if (TIFFWriteScanline(tif, &pix[row*imageWidth*channel+slice*imageWidth*imageLength*channel], row, 0) != 1)
					break;
			}
			TIFFWriteDirectory(tif);

		}
		(void) TIFFClose(tif);
		//if (buf)
		//	_TIFFfree(buf);
	}
}


/*!
*  \brief Read raw image
*
*  \param image_path : Path of image
*  \param pix : pointer to image array
*  \param dimZ : depth of image
*  \param dimY : height of image
*  \param dimX : width of image
*/

void PostProc::readfromImagePlus(char* image_path, unsigned char* pix, unsigned long dimZ, unsigned long dimY, unsigned long dimX)
{
	LINFO <<format("Loading from %s...\n",image_path);
	FILE *fo;
	fo=fopen(image_path,"rb");
	size_t checked = fread(pix,sizeof(unsigned char),dimX*dimY*dimZ,fo);
	fclose(fo);
    LINFO << format("Data loaded.\n");
}

/*!
*  \brief Read raw image
*
*  \param image_path : Path of image
*  \param pix : pointer to image array
*  \param dimZ : depth of image
*  \param dimY : height of image
*  \param dimX : width of image
*/

void PostProc::readfromImagePlus(const char* image_path, unsigned char* pix, unsigned long dimZ, unsigned long dimY, unsigned long dimX)
{
	LINFO << format("Loading from %s...\n",image_path);
	FILE *fo;
	fo=fopen(image_path,"rb");
	size_t checked = fread(pix,sizeof(unsigned char),dimX*dimY*dimZ,fo);
	fclose(fo);
	LINFO << format("Data loaded.\n");
}

