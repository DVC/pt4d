
#include "ROI.h"
using namespace std;

ROI::ROI()
{
}

ROI::ROI(int x, int y, int z, int dimX, int dimY, int dimZ)
{
	this->x=x;
	this->y=y;
	this->z=z;
	this->dimX = dimX;
	this->dimY = dimY;
	this->dimZ = dimZ;
}

int ROI::getX()
{
	return this->x;
}

void ROI::setX(int x)
{
	this->x = x;
}

int ROI::getY()
{
	return this->y;
}

void ROI::setY(int y)
{
	this->y = y;
}

int ROI::getZ()
{
	return this->z;
}

void ROI::setZ(int z)
{
	this->z = z;
}

int ROI::getdimX()
{
	return this->dimX;
}

void ROI::setdimX(int dimX)
{
	this->dimX = dimX;
}

int ROI::getdimY()
{
	return this->dimY;
}

void ROI::setdimY(int dimY)
{
	this->dimY = dimY;
}

int ROI::getdimZ()
{
	return this->dimZ;
}

void ROI::setdimZ(int dimZ)
{
	this->dimZ = dimZ;
}

ROI ROI::intersection(ROI roi)
{
	int Xmin = max(this->x,roi.getX());
	int Ymin = max(this->y,roi.getY());
	int Zmin = max(this->z,roi.getZ());
	int Xmax = min(this->x+this->dimX,roi.getX()+roi.getdimX());
	int Ymax = min(this->y+this->dimY,roi.getY()+roi.getdimY());
	int Zmax = min(this->z+this->dimZ,roi.getZ()+roi.getdimZ());
	return ROI(Xmin,Ymin,Zmin,Xmax-Xmin,Ymax-Ymin,Zmax-Zmin);
}

bool ROI::contains(ROI roi)
{
	return (this->x<=roi.getX()) && (this->y<=roi.getX()) && (this->z<=roi.getX()) && (this->x+this->dimX>=roi.getX()+roi.getdimX())
								&& (this->y+this->dimY >= roi.getY()+roi.getdimY()) && (this->z+this->dimZ>=roi.getZ()+roi.getdimZ());
}

ROI ROI::subROI(ROI roi)
{
	int x = this->x +roi.getX();
	int y = this->y + roi.getY();
	int z = this->z + roi.getZ();
	int dimX = roi.getdimX();
	int dimY = roi.getdimY();
	int dimZ = roi.getdimZ();
	return ROI(x,y,z,dimX,dimY,dimZ);
}
