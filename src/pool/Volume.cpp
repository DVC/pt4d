#include "Volume.h"
using namespace std;

Volume::Volume()
{

}

Volume::Volume(string name)
{
	this->name = name;
	this->setDimensions();
	this->fileROI = ROI(0,0,0,fileDim.getX(), fileDim.getY(), fileDim.getZ());
	//this->data = new double[fileROI.getdimZ()][fileROI.getdimY()][fileROI.getdimX()];
	//this->roi = new ROI(0,0,0,fileROI.getdimX(),fileROI.getdimY(),fileROI.getdimZ());
}



Volume::Volume(string name, Coord fileDim)
{
	this->name = name;
	this->fileDim = fileDim;
	this->fileROI = ROI(0,0,0,fileDim.getX(), fileDim.getY(), fileDim.getZ());
	//this->data = new double[fileROI.getdimZ()][fileROI.getdimY()][fileROI.getdimX()];
	//this->roi = new ROI(0,0,0,fileROI.getdimX(),fileROI.getdimY(),fileROI.getdimZ());
}

Volume::Volume(string name, Coord fileDim, ROI fileROI)
{
	this->name = name;
	this->fileDim = fileDim;
	this->fileROI = fileROI;
	//this->data = new double[fileROI.getdimZ()][fileROI.getdimY()][fileROI.getdimX()];
	//this->roi = new ROI(0,0,0,fileROI.getdimX(),fileROI.getdimY(),fileROI.getdimZ());
}

void Volume::setRoi(ROI roi) {
	this->roi = roi;
}

Volume Volume::duplicateRoi()
{
	Volume result = Volume(this->name, this->fileDim, this->fileROI.subROI(this->roi));
	int p=0;
	int q=0;
	int xmin = this->roi.getX();
	int dimx = this->getdimX();
	int xmax = xmin + dimx;
	int ymin = this->roi.getY();
	int dimy = this->getdimY();
	int ymax = ymin + dimy;
	int zmin = this->roi.getZ();
	int dimz = this->getdimZ();
	int zmax = zmin + dimz;
	double* newdata = new double[dimz*dimy*dimx];
	for(int k =zmin; k<zmax; k++)
	{
		q = k*dimx*dimy;
		for(int j=ymin; j<ymax; j++)
		{
			q+= j*dimx+xmin;
			for(int i=xmin; i<xmax; i++)
			{
				newdata[p]=this->data[q];
				p++;
				q++;
			}
		}
	}
	result.setData(newdata);
	result.setRoi(ROI(0,0,0,dimx,dimy,dimz));
	return result;
}

void Volume::recolor(int new_color[])
{
	for(int k=0; k<this->getdimX()*this->getdimY()*this->getdimZ(); k++)
	{
		this->data[k] = new_color[(int)this->data[k]];
	}
}

int Volume::vol()
{
	int vol=0;
	for(int k=0; k<this->getdimX()*this->getdimY()*this->getdimZ(); k++)
	{
			vol+=this->data[k];
	}
	return vol;
}

Coord Volume::getFileDim() const {
	return fileDim;
}

void Volume::setFileDim(Coord fileDim) {
	this->fileDim = fileDim;
}

ROI Volume::getFileRoi() const {
	return fileROI;
}

void Volume::setFileRoi(ROI fileRoi) {
	fileROI = fileRoi;
}

string Volume::getName() const {
	return name;
}

void Volume::setName(string name) {
	this->name = name;
}

ROI Volume::getRoi() const {
	return roi;
}



const double* Volume::getData() const {
	return data;
}

void Volume::setData(double* data) {
	this->data = data;
}

void Volume::setData() {

	int xmin = this->fileROI.getX();
	int dimx = this->getdimX();
	int xmax = xmin + dimx;
	int ymin = this->fileROI.getY();
	int dimy = this->getdimY();
	int ymax = ymin + dimy;
	int zmin = this->fileROI.getZ();
	int dimz = this->getdimZ();
	int zmax = zmin + dimz;
	this->data = new double[dimz*dimy*dimx];

	FILE * fichier;
	fichier = fopen(name.c_str(),"rb");

	if(fichier)  // si l'ouverture a réussi
	{
		int p=0;

		for(int k =zmin; k<zmax; k++)
		{
			for(int j=ymin; j<ymax; j++)
			{
					int q= k*dimx*dimy + j*dimx+xmin;
					fseek ( fichier , q*sizeof(data[0]) , SEEK_SET );
					fread( &data[p] , sizeof(data[0]) , (unsigned long)dimx , fichier );
					p+=dimx;
			}
		}
		this->roi = ROI(0,0,0,dimx,dimy,dimz);
	}
	else  // sinon
		cerr << "Cannot open " << name << endl;
}


int Volume::getdimX(){
	return fileROI.getdimX();
}

int Volume::getdimY(){
	return fileROI.getdimY();
}

int Volume::getdimZ(){
	return fileROI.getdimZ();
}

void Volume::setDimensions()
{
		const char* img1 = this->name.c_str();
	 	string mode = "r";
	 	TIFF* tif = TIFFOpen(img1,mode.c_str());
	 	if (tif) {
	 		uint32 imageWidth, imageLength;
	 		TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &imageWidth);
	 		TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &imageLength);
	 		//TIFFGetField(tif, TIFFTAG_IMAGEDEPTH, &imageDepth);
	 		int dircount = 0;
	         do {
	             dircount++;
	         } while (TIFFReadDirectory(tif));
	         TIFFClose(tif);
	         this->fileDim = Coord((int)imageWidth,(int)imageLength,(int)dircount);
	 	}
	 	else
	 	{
	         this->fileDim = Coord((int)0,(int)0,(int)0);
	 	}
}
