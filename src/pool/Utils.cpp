#include "Utils.h"

int diffclock(clock_t clock1,clock_t clock2)
{
	      double diffticks=clock1-clock2;
	      //int diffms=(int)(diffticks/(CLOCKS_PER_SEC/1000));
	      int diffms=(int)(diffticks/(1000000/1000));
	      return diffms;
}

log4cpp::Category& debuglogger = log4cpp::Category::getInstance(std::string("debuglogger"));
log4cpp::Category& logger = log4cpp::Category::getInstance(std::string("debuglogger.logger"));

void DummyHandler(const char* module, const char* fmt, va_list ap){};

double median(const double* u, const int N)
{
    double* tmp = new double[N];
    for(int i=0; i<N; i++)
        tmp[i]=u[i];
    sort(tmp,N);
    return tmp[(int)(N/2-1)];   
}  



void qsort(double* u, int lo0, int hi0)
{
    int lo = lo0;
    int hi = hi0;
    int mid;
    if( hi0 > lo0)
    {
        mid = u[ (int)((lo0+hi0)/2) ];
        while (lo<=hi)
        {
            while( ( lo < hi0 ) && ( u[lo]<mid ) )
                ++lo;
            while( ( hi > lo0 ) && ( u[hi]>mid ) )
                --hi;
                     
            if( lo <= hi )
            {
                swap(u,lo,hi);
                ++lo;
                --hi;
            }
        }
        if ( lo0  < hi )
            qsort(u,lo0,hi);
        if( lo < hi0 )
            qsort(u,lo,hi0);
     }
}

void swap(double* u, int i, int j)
{
    double tmp = u[i];
    u[i] = u[j];
    u[j] = tmp;   
}

void sort(double* u, int N)
{
    qsort(u,0,N-1);
}

double* invert(double* A)
{
    double* s = new double[9];
    double detA = A[0]*(A[4]*A[8]-A[7]*A[5])+A[3]*(A[7]*A[2]-A[1]*A[8])+A[6]*(A[1]*A[5]-A[4]*A[2]);
    s[0]=(A[4]*A[8]-A[5]*A[7])/detA;
    s[1]=(A[7]*A[2]-A[1]*A[8])/detA;
    s[2]=(A[1]*A[5]-A[4]*A[2])/detA;
    s[3]=(A[6]*A[5]-A[1]*A[8])/detA;
    s[4]=(A[0]*A[8]-A[2]*A[6])/detA;
    s[5]=(A[2]*A[3]-A[0]*A[5])/detA;
    s[6]=(A[3]*A[7]-A[6]*A[4])/detA;
    s[7]=(A[6]*A[1]-A[0]*A[7])/detA;
    s[8]=(A[0]*A[4]-A[3]*A[1])/detA;
    return s;
}

#if LAPACKE || INTEL
 int solve_syst(double* A, double* b, int n)
{
    int* P = new int[n];
    int info = LAPACKE_dgesv(LAPACK_COL_MAJOR, n, 1, A, n, P, b, n);
    return(info);
}


int solve_MXA_MU(double* M, double* X, double* U, double *A, int N0, int n)
{
    //LDEBUG << "USe CBlas";
    double* C = new double[n*n]; 
    cblas_dgemm(CblasColMajor,CblasNoTrans,CblasNoTrans, n, n, N0, 1., M, n, X, N0, 0., C, n);
    cblas_dgemm(CblasColMajor,CblasNoTrans,CblasNoTrans, n, 1, N0, 1., M, n, U, N0, 0., A, n);
    return solve_syst(C, A, n);
}

#else

int solve_MXA_MU(double* M, double* X, double* U, double *A, int N0, int n)
{
    LDEBUG << "USe CuSolver";
    double* C = new double[n*n];
 
    for(int i = 0; i <  n ; i++)
    	for(int j = 0; j <  n ; j++)
        {
            C[i*n+j]=0;
    	    for(int k = 0; k <  N0 ; k++)
	    {
               C[i*n+j]+=M[i+k*N0]*X[k+j*N0];
               //C[i+j*n]+=M[i*n+k]*X[k*n+j];
            }
        }
    for(int i = 0; i <  n ; i++)
    {
        A[i]=0;
    	for(int k = 0; k <  N0 ; k++)
	{
           A[i]+=M[i+k*N0]*U[k];
           //A[i]+=M[i*n+k]*U[k];
        }
    }
   return solve_syst(C, A, n);
}

/*
int solve_MXA_MU(double* M, double* X, double* U, double *A, int N0, int n)
{
    double *d_M = 0;
    double *d_X = 0;
    double *d_U = 0;
    double *d_A = 0;
    double *d_Ccopy = 0;
    double *d_C = 0;
    double alpha = 1.0;
    double beta = 0.0;
    int n2 = n * n;
    int bufferSize = 0;
    int *info = NULL;
    double *buffer = NULL;
    int h_info = 0;
    int *ipiv = NULL; // pivoting sequence
    
    cusolverDnHandle_t handle = NULL;
    cublasHandle_t cublasHandle = NULL; // used in residual evaluation
    cudaStream_t stream = NULL;
        
    checkCudaErrors(cusolverDnCreate(&handle));
    checkCudaErrors(cublasCreate(&cublasHandle));
    checkCudaErrors(cudaStreamCreate(&stream));
    checkCudaErrors(cudaMalloc(&info, sizeof(int)));
    checkCudaErrors(cudaMalloc(&d_C, sizeof(double)*n2));
    checkCudaErrors(cudaMalloc(&ipiv, sizeof(int)*n));

    checkCudaErrors(cusolverDnSetStream(handle, stream));
    checkCudaErrors(cublasSetStream(cublasHandle, stream));
    
    cublasFillMode_t uplo = CUBLAS_FILL_MODE_LOWER;


    checkCudaErrors(cudaMalloc((void **)&d_M, n*N0 * sizeof(M[0])));
    checkCudaErrors(cudaMalloc((void **)&d_X, n*N0 * sizeof(X[0])));
    checkCudaErrors(cudaMalloc((void **)&d_Ccopy, n2 * sizeof(M[0])));
    checkCudaErrors(cudaMalloc((void **)&d_U, N0 * sizeof(U[0])));
    checkCudaErrors(cudaMalloc((void **)&d_A, n * sizeof(A[0])));
    
    checkCudaErrors(cudaMemcpy(d_M, M, sizeof(double)*n*N0, cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(d_X, X, sizeof(double)*n*N0, cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(d_U, U, sizeof(double)*N0, cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(d_A, A, sizeof(double)*n, cudaMemcpyHostToDevice));
    
    checkCudaErrors(cublasDgemm(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_N, n, n, N0, &alpha, d_M, n, d_X, N0, &beta, d_Ccopy, n));
    checkCudaErrors(cublasDgemm(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_N, n, 1, N0, &alpha, d_M, n, d_U, N0, &beta, d_A, n));
    checkCudaErrors(cudaDeviceSynchronize());

    LDEBUG << "cublasDgemm done";

    checkCudaErrors(cudaMemcpy(d_C, d_Ccopy, sizeof(double)*n2, cudaMemcpyDeviceToDevice));
    checkCudaErrors(cudaMemset(info, 0, sizeof(int)));

    checkCudaErrors(cusolverDnDgetrf_bufferSize(handle, n, n, (double*)d_Ccopy, n, &bufferSize));
    checkCudaErrors(cudaMalloc(&buffer, sizeof(double)*bufferSize));
    LDEBUG << format("cuSolver requires %.6f Mbytes on GPU memory\n\n",(float)(bufferSize*sizeof(double))/1048576.0f);


    checkCudaErrors(cusolverDnDgetrf(handle, n, n, d_C, n, buffer, ipiv, info));
    checkCudaErrors(cudaMemcpy(&h_info, info, sizeof(int), cudaMemcpyDeviceToHost));

    LDEBUG << "cusolverDnDgetrf done";

    if ( 0 != h_info ){
        LINFO << "Error: LU factorization failed\n";
    }

    //checkCudaErrors(cudaMemcpy(d_A, d_, sizeof(double)*n, cudaMemcpyDeviceToDevice));
    checkCudaErrors(cusolverDnDgetrs(handle, CUBLAS_OP_N, n, 1, d_C, n, ipiv, d_A, n, info));
    checkCudaErrors(cudaDeviceSynchronize());
  
    LDEBUG << "cusolverDnDgetrs done";
    //checkCudaErrors(cudaMemcpy(d_A, d_b, sizeof(double)*n, cudaMemcpyDeviceToDevice));

    //checkCudaErrors(cusolverDnDpotrs(handle, uplo, n, 1, d_C, n, d_A, n, info));

    checkCudaErrors(cudaDeviceSynchronize());

    checkCudaErrors(cudaMemcpy(&A, d_A, sizeof(double)*n, cudaMemcpyDeviceToHost));

    if (info  ) { checkCudaErrors(cudaFree(info)); }
    if (buffer) { checkCudaErrors(cudaFree(buffer)); }
    if (handle) { checkCudaErrors(cusolverDnDestroy(handle)); }
    if (cublasHandle) { checkCudaErrors(cublasDestroy(cublasHandle)); }
    if (stream) { checkCudaErrors(cudaStreamDestroy(stream)); }
    if (ipiv  ) { checkCudaErrors(cudaFree(ipiv));}

    if (d_M) { checkCudaErrors(cudaFree(d_M)); }
    if (d_A) { checkCudaErrors(cudaFree(d_A)); }
    if (d_C) { checkCudaErrors(cudaFree(d_C)); }
    if (d_Ccopy) { checkCudaErrors(cudaFree(d_Ccopy)); }
    if (d_X) { checkCudaErrors(cudaFree(d_X)); }
    if (d_U) { checkCudaErrors(cudaFree(d_U)); }

    return 0;
}
*/

int linearSolverLU(
    cusolverDnHandle_t handle,
    int n,
    const double *Acopy,
    int lda,
    const double *b,
    double *x)
{
    int bufferSize = 0;
    int *info = NULL;
    double *buffer = NULL;
    double *A = NULL;
    int *ipiv = NULL; // pivoting sequence
    int h_info = 0;

    checkCudaErrors(cusolverDnDgetrf_bufferSize(handle, n, n, (double*)Acopy, lda, &bufferSize));

    checkCudaErrors(cudaMalloc(&info, sizeof(int)));
    checkCudaErrors(cudaMalloc(&buffer, sizeof(double)*bufferSize));
    checkCudaErrors(cudaMalloc(&A, sizeof(double)*lda*n));
    checkCudaErrors(cudaMalloc(&ipiv, sizeof(int)*n));


    // prepare a copy of A because getrf will overwrite A with L
    checkCudaErrors(cudaMemcpy(A, Acopy, sizeof(double)*lda*n, cudaMemcpyDeviceToDevice));
    checkCudaErrors(cudaMemset(info, 0, sizeof(int)));


    checkCudaErrors(cusolverDnDgetrf(handle, n, n, A, lda, buffer, ipiv, info));
    checkCudaErrors(cudaMemcpy(&h_info, info, sizeof(int), cudaMemcpyDeviceToHost));

    if ( 0 != h_info ){
        LINFO << "Error: LU factorization failed\n";
    }

    checkCudaErrors(cudaMemcpy(x, b, sizeof(double)*n, cudaMemcpyDeviceToDevice));
    checkCudaErrors(cusolverDnDgetrs(handle, CUBLAS_OP_T, n, 1, A, lda, ipiv, x, n, info));
    checkCudaErrors(cudaDeviceSynchronize());
    checkCudaErrors(cudaMemcpy(&h_info, info, sizeof(int), cudaMemcpyDeviceToHost));

    if ( 0 != h_info ){
        LINFO << "Error:  on LU resolution\n";
    }


    if (info  ) { checkCudaErrors(cudaFree(info  )); }
    if (buffer) { checkCudaErrors(cudaFree(buffer)); }
    if (A     ) { checkCudaErrors(cudaFree(A)); }
    if (ipiv  ) { checkCudaErrors(cudaFree(ipiv));}

    return 0;
}
 
int solve_syst(double* A, double* b, int n)
{
    cusolverDnHandle_t handle = NULL;
    cublasHandle_t cublasHandle = NULL; // used in residual evaluation
    cudaStream_t stream = NULL;

    int rowsA = n; // number of rows of A
    int colsA = n; // number of columns of A
    int lda   = n; // leading dimension in dense matrix

    double *d_A = NULL; // a copy of h_A
    double *d_x = NULL; // x = A \ b
    double *d_b = NULL; // a copy of h_b

    checkCudaErrors(cusolverDnCreate(&handle));
    checkCudaErrors(cublasCreate(&cublasHandle));
    checkCudaErrors(cudaStreamCreate(&stream));

    checkCudaErrors(cusolverDnSetStream(handle, stream));
    checkCudaErrors(cublasSetStream(cublasHandle, stream));


    checkCudaErrors(cudaMalloc((void **)&d_A, sizeof(double)*lda*colsA));
    checkCudaErrors(cudaMalloc((void **)&d_x, sizeof(double)*colsA));
    checkCudaErrors(cudaMalloc((void **)&d_b, sizeof(double)*rowsA));

//    printf("step 4: prepare data on device\n");
    checkCudaErrors(cudaMemcpy(d_A, A, sizeof(double)*lda*colsA, cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(d_b, b, sizeof(double)*rowsA, cudaMemcpyHostToDevice));

//    printf("step 5: solve A*x = b \n");
    linearSolverLU(handle, rowsA, d_A, lda, d_b, d_x);
    checkCudaErrors(cudaMemcpy(b, d_x, sizeof(double)*colsA, cudaMemcpyDeviceToHost));

    if (handle) { checkCudaErrors(cusolverDnDestroy(handle)); }
    if (cublasHandle) { checkCudaErrors(cublasDestroy(cublasHandle)); }
    if (stream) { checkCudaErrors(cudaStreamDestroy(stream)); }

    if (d_A) { checkCudaErrors(cudaFree(d_A)); }
    if (d_x) { checkCudaErrors(cudaFree(d_x)); }
    if (d_b) { checkCudaErrors(cudaFree(d_b)); }

    return 0;
}
/*
int solve_MXA_MU(double* M, double* X, double* U, double *A, int N0, int n)
{
    double *d_M = 0;
    double *d_X = 0;
    double *d_U = 0;
    double *d_A = 0;
    double *d_Ccopy = 0;
    double *d_C = 0;
    double alpha = 1.0;
    double beta = 0.0;
    int n2 = n * n;
    int bufferSize = 0;
    int *info = NULL;
    double *buffer = NULL;
    int h_info = 0;
    
    cusolverDnHandle_t handle = NULL;
    cublasHandle_t cublasHandle = NULL; // used in residual evaluation
    cudaStream_t stream = NULL;
        
    checkCudaErrors(cusolverDnCreate(&handle));
    checkCudaErrors(cublasCreate(&cublasHandle));
    checkCudaErrors(cudaStreamCreate(&stream));
    checkCudaErrors(cudaMalloc(&info, sizeof(int)));
    checkCudaErrors(cudaMalloc(&d_C, sizeof(double)*n2));

    checkCudaErrors(cusolverDnSetStream(handle, stream));
    checkCudaErrors(cublasSetStream(cublasHandle, stream));
    
    cublasFillMode_t uplo = CUBLAS_FILL_MODE_LOWER;


    checkCudaErrors(cudaMalloc((void **)&d_M, n*N0 * sizeof(M[0])));
    checkCudaErrors(cudaMalloc((void **)&d_X, n*N0 * sizeof(X[0])));
    checkCudaErrors(cudaMalloc((void **)&d_Ccopy, n2 * sizeof(M[0])));
    checkCudaErrors(cudaMalloc((void **)&d_U, N0 * sizeof(U[0])));
    checkCudaErrors(cudaMalloc((void **)&d_A, n * sizeof(A[0])));
    
    checkCudaErrors(cudaMemcpy(d_M, M, sizeof(double)*n*N0, cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(d_X, X, sizeof(double)*n*N0, cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(d_U, U, sizeof(double)*N0, cudaMemcpyHostToDevice));
    checkCudaErrors(cudaMemcpy(d_A, A, sizeof(double)*n, cudaMemcpyHostToDevice));
    
    checkCudaErrors(cublasDgemm(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_N, n, n, N0, &alpha, d_M, n, d_X, N0, &beta, d_Ccopy, n));
    checkCudaErrors(cublasDgemm(cublasHandle, CUBLAS_OP_N, CUBLAS_OP_N, n, 1, N0, &alpha, d_M, n, d_U, N0, &beta, d_A, n));

    checkCudaErrors(cusolverDnDpotrf_bufferSize(handle, uplo, n, (double*)d_Ccopy, n, &bufferSize));

    checkCudaErrors(cudaMalloc(&buffer, sizeof(double)*bufferSize));
    LINFO << format("cuSolver requires %.6f Mbytes on GPU memory\n\n",(float)(bufferSize*sizeof(double))/1048576.0f);


    checkCudaErrors(cudaMemcpy(d_C, d_Ccopy, sizeof(double)*n2, cudaMemcpyDeviceToDevice));
    checkCudaErrors(cudaMemset(info, 0, sizeof(int)));


    checkCudaErrors(cusolverDnDpotrf(handle, uplo, n, d_C, n, buffer, bufferSize, info));

    checkCudaErrors(cudaMemcpy(&h_info, info, sizeof(int), cudaMemcpyDeviceToHost));

    if ( 0 != h_info ){
        LINFO << "Error: Cholesky factorization failed\n";
    }

    //checkCudaErrors(cudaMemcpy(d_A, d_b, sizeof(double)*n, cudaMemcpyDeviceToDevice));

    checkCudaErrors(cusolverDnDpotrs(handle, uplo, n, 1, d_C, n, d_A, n, info));

    checkCudaErrors(cudaDeviceSynchronize());

    checkCudaErrors(cudaMemcpy(&A, d_A, sizeof(double)*n, cudaMemcpyDeviceToHost));

    if (info  ) { checkCudaErrors(cudaFree(info)); }
    if (buffer) { checkCudaErrors(cudaFree(buffer)); }
    if (handle) { checkCudaErrors(cusolverDnDestroy(handle)); }
    if (cublasHandle) { checkCudaErrors(cublasDestroy(cublasHandle)); }
    if (stream) { checkCudaErrors(cudaStreamDestroy(stream)); }

    if (d_M) { checkCudaErrors(cudaFree(d_M)); }
    if (d_A) { checkCudaErrors(cudaFree(d_A)); }
    if (d_C) { checkCudaErrors(cudaFree(d_C)); }
    if (d_Ccopy) { checkCudaErrors(cudaFree(d_Ccopy)); }
    if (d_X) { checkCudaErrors(cudaFree(d_X)); }
    if (d_U) { checkCudaErrors(cudaFree(d_U)); }

    return 0;
}
*/
#endif

