#include "Coord.h"

using namespace std;
	Coord::Coord(){
		this->x=0;
		this->y=0;
		this->z=0;
	}

	Coord::Coord(double x, double y , double z)
	{
		this->x=x;
		this->y=y;
		this->z=z;
	}
	
	Coord::Coord(double x[])
	{
		this->x=x[0];
		this->y=x[1];
		this->z=x[2];
	}
	
	/*
	double[3] Coord::getCoord()
	{
		double x[] = new double[3];
		x[0]=this->x;
		x[1]=this->y;
		x[2]=this->z;
		return x;
	}
	*/
	
	 double Coord::distXY(const Coord& other) const
	{
		double x=this->x-other.getX();
		double y=this->y-other.getY();
		return sqrt(pow(x,2)+pow(y,2));
	}
	
	 double Coord::distX(const Coord& other) const
	{
		double x=this->x-other.getX();
		return fabs(x);
	}
		 double Coord::distY(const Coord& other) const
	{
		double y=this->y-other.getY();
		return fabs(y);
	}
		 double Coord::distZ(const Coord& other) const
	{
		double z=this->z-other.getZ();
		return fabs(z);
	}
	
	 double Coord::dist(const Coord& other) const
	{
		double x=this->x-other.getX();
		double y=this->y-other.getY();
		double z=this->z-other.getZ();
		return sqrt(pow(x,2)+pow(y,2)+pow(z,2));
	}
	
	 string Coord::toString() const
	{
		ostringstream o;
		o << this->x  << " " << this->y << " " << this->z;
		return o.str();
	}

    string Coord::display() const
	{
		ostringstream o;
		o << "(" << this->x  << "," << this->y << "," << this->z << ")";
		return o.str();
	}
		
/**	 int findColorInStack(int dx, int dy, int dz, ImageStack st)
	{
		int zmin=(int)this->z-dz;
		if(zmin<1)
			zmin=1;
		int zmax=(int)this->z+dz;
		if(zmax>st.getSize())
			zmax=st.getSize();
		int xmin=(int)this->x-dx;
		if(xmin<0)
			xmin=0;
		int xmax=(int)this->x+dx;
		if(xmax>st.getWidth())
			xmax=st.getWidth()-1;
		int ymin=(int)this->y-dy;
		if(ymin<0)
			ymin=0;
		int ymax=(int)this->y+dy;
		if(ymax>st.getHeight())
			ymax=st.getHeight();
		int max=0;
		for(int z=zmin; z<=zmax;z++)
		{
			ImageProcessor ip = st.getProcessor(z);
			for(int y=ymin; y<=ymax;y++)
			{
				for(int x=xmin; x<=xmax;x++)
				{
					int val=(int)ip.getPixelValue(y, x);
					if(val>0)
						max=val;
				}
			}
		}
		return max;
	}
*/
	
	 double Coord::getX() const {
		return x;
	}
	 void Coord::setX(double x) {
		this->x = x;
	}
	 double Coord::getY() const {
		return y;
	}
	 void Coord::setY(double y) {
		this->y = y;
	}
	 double Coord::getZ() const {
		return z;
	}
	 void Coord::setZ(double z) {
		this->z = z;
	}
	
    double* Coord::getVect() const	{
        double* x = new double[3];
        x[0] = this->x;
		x[1] = this->y;
		x[2] = this->z;
        return x;
	}
	
     Coord Coord::getInt() const
	{
		return Coord((int)(this->x),(int)(this->y),(int)(this->z));
	}
     bool Coord::isZero() const
    {   
        return ((this->x*this->x+this->y*this->y+this->z*this->z)==0);
    }


	 Coord Coord::Interpolate(const double zref[], const double zfinal[]) const
	{
		double zout = (this->z-zref[0])*((zfinal[1]-zfinal[0])/(zref[1]-zref[0]))+zfinal[0];
		return Coord(this->x,this->y,zout);
	}
	
	 /**
	 Coord Coord::shift(double depl[][])
	{
		return new Coord(this->x,this->y,depl[(int)this->z][3]+this->z);
	}
	*/
	 Coord Coord::shift(const vector<vector<double> > depl) const
	{
        if(this->z<0)
		    return Coord(this->x,this->y,this->z + depl[0][1]);
        else if(this->z>=depl.size())
		    return Coord(this->x,this->y,this->z + depl[depl.size()-1][1]);
		else
            return Coord(this->x,this->y,this->z + depl[(int)this->z][1]);
	}

	 Coord Coord::shift(const double depl[]) const
	{
		return Coord(this->x + depl[0],this->y + depl[1], depl[2]+this->z);
	}

    Coord Coord::shift(const Coord& value) const
	{
		return Coord(this->x + value.getX(),this->y + value.getY(), this->z+value.getZ());
	}


	Coord Coord::mult(double a)
	{
		return Coord(this->x*a,this->y*a,this->z*a);
	}
    
    Coord Coord::diff(const Coord& value) const
	{
		return Coord(this->x - value.getX(),this->y - value.getY(), this->z-value.getZ());
	}

   	Coord Coord::findDepl(const vector<Coord> pos,const vector<Coord > depl,double thrDist) const
	{
        Coord myDepl = Coord();
        double coeff = 0;
        for(int i=0; i<pos.size(); i++)
        {
            double d = this->dist(pos[i]);
            if(d<thrDist){
                double lcoeff = exp(-d*d*9/(thrDist*thrDist));
                coeff += lcoeff;
                myDepl+=lcoeff*depl[i];
            }
        }
        if(coeff>0)
        {
            myDepl/=coeff;
        }
        else
        {
            myDepl=this->findDepl(pos,depl,2*thrDist);
        }     
		return myDepl;
	}

   // void Coord::operator+=(Coord &a, const Coord &b) { a.setX( a.getX() + b.getX()); a.setY(a.getY() + b.getY()); a.setZ(a.getZ()+  b.getZ());}	
    
    //void Coord::operator/=(Coord &a, const double b) { a.setX( a.getX() + b); a.setY( a.getY() +b); a.setZ(a.getZ() + b);}
   void Coord::operator+=(const Coord &b) { this->x+= b.getX(); this->y += b.getY(); this->z += b.getZ();}	
   void Coord::operator/=(const double b) { this->x/= b; this->y /= b; this->z /= b;}	
    
    Coord operator*(const double b, const Coord &c) { return Coord(c.getX()*b,c.getY()*b,c.getZ()*b);}	

    Coord operator-(const Coord &a, const Coord &b) { return Coord(a.getX()-b.getX(),a.getY()-b.getY(),a.getZ()-b.getZ());}	
    Coord operator+(const Coord &a, const Coord &b) { return Coord(a.getX()+b.getX(),a.getY()+b.getY(),a.getZ()+b.getZ());}	

     
    vector<Coord> operator-(const vector<Coord> va, const vector<Coord> vb)
    {
            vector<Coord> vc;
            for(int i=0; (i<va.size())&&(i<vb.size()); i++)
            {
                vc.push_back(va[i]-vb[i]);
            }
        return vc;
    }

    Coord Coord::min(const Coord& value) const
	{
		return Coord(this->x<value.getX()?this->x:value.getX(),this->y<value.getY()?this->y:value.getY(), this->z<value.getZ()?this->z:value.getZ());
	} 


    Coord Coord::max(const Coord& value) const
	{
		return Coord(this->x>value.getX()?this->x:value.getX(),this->y>value.getY()?this->y:value.getY(), this->z>value.getZ()?this->z:value.getZ());
	} 

    bool operator<(const Coord &a, const Coord &b) { return (a.getX()<b.getX())&&(a.getY()<b.getY())&&(a.getZ()<b.getZ());}	
    bool operator<=(const Coord &a, const Coord &b) { return (a.getX()<=b.getX())&&(a.getY()<=b.getY())&&(a.getZ()<=b.getZ());}	
    bool operator>(const Coord &a, const Coord &b) { return (a.getX()>b.getX())&&(a.getY()>b.getY())&&(a.getZ()>b.getZ());}	
    bool operator>=(const Coord &a, const Coord &b) { return (a.getX()>=b.getX())&&(a.getY()>=b.getY())&&(a.getZ()>=b.getZ());}	





