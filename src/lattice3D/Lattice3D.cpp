#include "Lattice3D.h"

//#define DEBUGMODE

using namespace std;


int main(int nbarg, char* argv[]) {

    init("./");

    LINFO << "\n***************************************************************\n\n               Tracking of lattice structures (with DVC)          \n\nAuthor : Pierre Lhuissier\n\nContact : pierre.lhuissier@simap.grenoble-inp.fr\n\n***************************************************************\n\n" ;

    Parameters *param;
	param = (Parameters *)malloc(sizeof(Parameters));
    if(nbarg==2)
    {
        LINFO << "Read parameters from " << argv[1];
	    //Configuration file
	    readConfig(argv[1],param);
        LINFO << "Parameters readden";
    }
    else
	{
		LINFO << "Usage :  Lattice input.file";
        return 0;
	}
  
    LINFO << "Create Theoretical Lattice";
    Squeleton sqth = Squeleton();
    sqth.read(string(param->inputDir) + param->theoreticalSqueleton);
    sqth.update();
    
    vector<Squeleton*> sqreal = vector<Squeleton*>();

    vector<int> scanNumber = vector<int>();
    vector<string> scanNumberStr = vector<string>();
    vector<string> imagesFilesNames = vector<string>();
    vector<string> names = vector<string>();


    //for(int i = (int)(param->firstvol); i < (int)(param->lastvol) ; i = i + 1)
    for(int i=param->firstvol; i<=param->lastvol; i = i + param->intervol)
    {
		LINFO << "i=" << i;
       
        char numscan[4];
		sprintf(numscan,"%03d",i);
        bool skip=false;
        if(!((param->skippedvol).empty()))
        {
            for (vector<int>::iterator it = param->skippedvol.begin(); it!=param->skippedvol.end(); ++it) 
            {
                if(i==(*it))
                    skip=true;
            }
        }
        if(skip)
        {
            LINFO << "Skipping scan " << numscan;
        }
        else
        {
            LINFO << "Using scan " << numscan;
            scanNumberStr.push_back(string(numscan));
            scanNumber.push_back(i);
        }
    }
	
    int startExpe = ceil((1.*param->restart-param->firstvol)/param->intervol);
    int numberExpe = scanNumber.size();

    for(int i=0; i<scanNumber.size(); ++i)
    {
        string name = string(param->prefixmesh) + scanNumberStr[i] + param->suffixmesh;
        names.push_back(name);
        LINFO << "Scan " << name;
        string imageName = string(param->Directory) + string(param->inputDir) + param->prefiximage + scanNumberStr[i] + param->suffiximage;
        imagesFilesNames.push_back(imageName);
        Squeleton * sq = new Squeleton();
        if( i==0 || i>startExpe)
        {
            LINFO << "Initiate lattice from Theoretical";
            sq->read(string(param->inputDir) + param->theoreticalSqueleton);
        }
        else
        {
            LINFO << "Initiate lattice from already computed mesh";
            sq->read(string(param->outputDir) + names[i]);
        }
        sq->update();
        sq->splitStruts(param->nodesSpacing);
        sqreal.push_back(sq);
    }
	
    if(startExpe == 0)
        (sqreal[0])->writeVTKdef(string(param->outputDir) + names[0], (sqreal[0]), (sqreal[0]));

    for(int i=startExpe; i<scanNumber.size(); ++i)
    {
		LINFO << "Correl " << imagesFilesNames[i] << " and " << imagesFilesNames[i+1];
        
        Volume imRef = Volume(imagesFilesNames[i]);
		Volume imFinal = Volume(imagesFilesNames[i+1]);
		Squeleton* sqRef = &*(sqreal[i]);
		Squeleton* sqFinal = &*(sqreal[i+1]);

		sqRef->correl_DICGPU_rigid_guiding(*sqFinal, imRef, imFinal, param->correlDomainX, param->correlDomainY, param->correlDomainZ, param->thrX, param->thrY, param->thrZ, param->correlThr, param->initThrXfactor, param->initThrYfactor, param->initThrZfactor);
    
		sqFinal->writeVTKdef(string(param->outputDir) + names[i+1], (sqreal[0]), sqRef);
    }

	
    LINFO << "Finished!!!";
    
    free(param);
    
    finalize();
   
}



