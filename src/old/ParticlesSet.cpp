#include "ParticlesSet.h"
using namespace std;

	ParticlesSet::ParticlesSet() {
		this->name = "";
		this->StackdimX=0;
		this->StackdimY=0;
		this->StackdimZ=0;
		this->particles = new list<Particle*> ();
	}
	
	ParticlesSet::ParticlesSet(string name) {
		this->name = name;
		this->StackdimX=0;
		this->StackdimY=0;
		this->StackdimZ=0;
		this->setDimensions();
		this->particles = new list<Particle*> ();
	}

	ParticlesSet::ParticlesSet(string name, list<Particle*> *particles) {
		this->name = name;
		this->StackdimX=0;
		this->StackdimY=0;
		this->StackdimZ=0;
		this->setDimensions();
		this->particles = particles;
	}
	
	ParticlesSet::ParticlesSet(const string fileDirectory, const string particlesSetName, const vector<ParticlesSet> & raw) {
		 this->particles = new list<Particle*>();
		 ostringstream fileName;
		 fileName << fileDirectory << particlesSetName;
		 ifstream fichier(fileName.str().c_str(), ios::in);  //déclaration du flux et ouverture du fichier
		 if(fichier)  // si l'ouverture a réussi
	     {

		    	 string psName = "";
		    	 char schar[10000];
		    	 fichier.getline(schar,10000);
		    	 psName+=schar;
		    	 stringstream sstr;
		    	 sstr.clear();
		    	 sstr.str(psName);
		    	 string s;
		    	 int a;
		    	 if(sstr.str().size()>1)
		    	 {
		    		 sstr >> s;
		    		 this->name=s;
		    	 }
		    	 psName = "";
		    	 fichier.getline(schar,10000);
		    	 psName+=schar;
		    	 sstr.clear();
		    	 sstr.str(psName);
		    	 if(sstr.str().size()>1)
		    	 {
		    		 sstr >> a;
			    	 this->StackdimX = (int)a;
		    	 }
		    	 psName = "";
		    	 fichier.getline(schar,10000);
		    	 psName+=schar;
		    	 sstr.clear();
		    	 sstr.str(psName);
		    	 if(sstr.str().size()>1)
		    	 {
		    		 sstr >> a;
			    	 this->StackdimY = (int)a;
		    	 }
		    	 psName = "";
		    	 fichier.getline(schar,10000);
		    	 psName+=schar;
		    	 sstr.clear();
		    	 sstr.str(psName);
		    	 if(sstr.str().size()>1)
		    	 {
		    		 sstr >> a;
			    	 this->StackdimZ = (int)a;
		    	 }

		    	 cout << "Updated ParticlesSet " << particlesSetName << endl;
		    	 cout << "Name = " << this->name << endl;
		    	 cout << "dim = (" << this->StackdimX << "," << this->StackdimY << "," << this->StackdimZ << ")" << endl;
		    	 string dataline="";
		    	 while (fichier.good())
		    	 {
		    	 dataline="";
		         fichier.getline(schar,10000);
		    	 dataline+=schar;
		    	 sstr.clear();
		    	 sstr.str(dataline);
		    	 if(sstr.str().size()>1)
		    	 {
		    		sstr >> a;
		    		if(!isnan(1.*a))
		    		{
		    			int scan = (int)(1.*a);
		    			sstr >> a;
		    			int color = (int)(1.*a);
		    			Particle* part = raw[scan].findParticle(color);
		    			part->ParticleUpdate(fileDirectory,scan,color);
		    			this->particles->push_back(part);
			      }
		    	 }
		     }
			 fichier.close();
		}
		 else  // sinon
	     {
	    	 cerr << "Erreur à l'ouverture !" << endl;
	     	 cerr << "File " << fileName.str() << " not found!" << endl;
	     }
	}

	ParticlesSet::ParticlesSet(const string fileDirectory, const string particlesSetName) {
			 this->particles = new list<Particle*>();
			 ostringstream fileName;
			 fileName << fileDirectory << particlesSetName;
			 ifstream fichier(fileName.str().c_str(), ios::in);  //déclaration du flux et ouverture du fichier
			 if(fichier)  // si l'ouverture a réussi
		     {

			    	 string psName = "";
			    	 char schar[10000];
			    	 fichier.getline(schar,10000);
			    	 psName+=schar;
			    	 stringstream sstr;
			    	 sstr.clear();
			    	 sstr.str(psName);
			    	 string s;
			    	 int a;
			    	 if(sstr.str().size()>1)
			    	 {
			    		 sstr >> s;
			    		 this->name=s;
			    	 }
			    	 psName = "";
			    	 fichier.getline(schar,10000);
			    	 psName+=schar;
			    	 sstr.clear();
			    	 sstr.str(psName);
			    	 if(sstr.str().size()>1)
			    	 {
			    		 sstr >> a;
				    	 this->StackdimX = (int)a;
			    	 }
			    	 psName = "";
			    	 fichier.getline(schar,10000);
			    	 psName+=schar;
			    	 sstr.clear();
			    	 sstr.str(psName);
			    	 if(sstr.str().size()>1)
			    	 {
			    		 sstr >> a;
				    	 this->StackdimY = (int)a;
			    	 }
			    	 psName = "";
			    	 fichier.getline(schar,10000);
			    	 psName+=schar;
			    	 sstr.clear();
			    	 sstr.str(psName);
			    	 if(sstr.str().size()>1)
			    	 {
			    		 sstr >> a;
				    	 this->StackdimZ = (int)a;
			    	 }
			    	 cout << "New ParticlesSet " << particlesSetName << endl;
			    	 cout << "Name = " << this->name << endl;
			    	 cout << "dim = (" << this->StackdimX << "," << this->StackdimY << "," << this->StackdimZ << ")" << endl;

			    	string dataline="";
			    	while (fichier.good())
			    	{
			    	 dataline="";
			    	 fichier.getline(schar,10000);
			    	 dataline+=schar;
			    	 sstr.clear();
			    	 sstr.str(dataline);
			    	 if(sstr.str().size()>1)
			    	 {
			    		sstr >> a;
			    		if(!isnan(1.*a))
			    		{
			    			int scan = (int)(1.*a);
			    			sstr >> a;
			    			int color = (int)(1.*a);
			    			Particle* part = new Particle(fileDirectory,scan,color);
			    			this->particles->push_back(part);
				      }
			    	 }
			     }
				 fichier.close();
			}
			 else  // sinon
		     {
		    	 cerr << "Erreur à l'ouverture !" << endl;
		     	 cerr << "File " << fileName.str() << " not found!" << endl;
		     }
		}

	ParticlesSet::ParticlesSet(const ParticlesSet& particlesSet)
	{
		this->name = particlesSet.getName();
		this->StackdimX = particlesSet.getStackdimX();
		this->StackdimY = particlesSet.getStackdimY();
		this->StackdimZ = particlesSet.getStackdimZ();
		this->particles = particlesSet.getParticles();
	}

	ParticlesSet::ParticlesSet(const string name, const vector<Particle*>& partraw)
	{
		this->name = name;
		this->StackdimX=0;
		this->StackdimY=0;
		this->StackdimZ=0;
		this->setDimensions();
		cout << "New ParticlesSet of " << partraw.size() << " objects with image " << name << " (" << StackdimX << "," << StackdimY << "," << StackdimZ << ")" << endl;
		this->particles = new list<Particle*>();
		//cout << partraw[0]->fullInfoTostring() << endl;
		for(int i=0; i<partraw.size(); i++)
		{
			//cout << "Particle* pt(0);" << endl;
			Particle* pt;
			//cout << "*pt = partraw[" << i <<"];" << endl;
			pt = partraw[i];
			//cout << "this->particles->push_back(pt);" << endl;
			this->particles->push_back(pt);
		}
		//cout << partraw[partraw.size()-1]->fullInfoTostring() << endl;
	}

	ParticlesSet::~ParticlesSet()
	{
		this->name="";
		this->StackdimX=0;
		this->StackdimY=0;
		this->StackdimZ=0;
		this->particles->clear();
        delete particles;
	}

	ParticlesSet &ParticlesSet::operator = (const ParticlesSet &particlesSet)
	{
		this->name = particlesSet.getName();
		this->StackdimX = particlesSet.getStackdimX();
		this->StackdimY = particlesSet.getStackdimY();
		this->StackdimZ = particlesSet.getStackdimZ();
		this->particles = particlesSet.getParticles();
		return *this;
	}


	string ParticlesSet::getName() const {
		return name;
	}

	void ParticlesSet::setName(string name) {
		this->name = name;
	}

    list<Particle*>*  ParticlesSet::getParticles() const {
		list<Particle*> *toto = new list<Particle*>();
		for (list<Particle*>::const_iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit)
		{
				toto->push_back(&**pit);
		}
		return toto;
	}

	 void ParticlesSet::setParticles(list<Particle*> *particles) {
		this->particles = particles;
	}


	list<Particle*>::iterator ParticlesSet::getIteratorBegin(){
		return this->particles->begin();
	}

	list<Particle*>::iterator ParticlesSet::getIteratorEnd(){
		return this->particles->end();
	}

	Particle* ParticlesSet::getParticle(int i){
		int j=0;
	 	for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit, ++j)
	 	{
			if(i==j)
				return *pit;
		}
		Particle* emptyPart(0);
		return emptyPart;
	}

	 void ParticlesSet::addParticle(Particle* particle) {
		this->particles->push_back(particle);
	}
	
	 int ParticlesSet::Size() const {
		return particles->size();
	}
	
	 void ParticlesSet::removeParticle(Particle* part)
	{
        this->particles->remove(part);
	}


	void ParticlesSet::removeParticle(int i)
	{
		list<Particle*>::iterator pit=this->particles->begin();
		int j=0;
		while(j<i)
		{
			++pit;
		}
		this->particles->erase(pit);
	}
	
	 ParticlesSet ParticlesSet::subSetVol(double vMin)
	{
		ParticlesSet* subset = new ParticlesSet(this->name);
		//cout << "Extracting subset " << this->name << "subset" << endl;
		int nbpart = 0;
		int visited = 0;
		for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit)
		{
			visited++;
			if(((*pit)->getVolume()>vMin))
			{
				subset->addParticle(*pit);
				nbpart++;
			}
		}
		cout << "The subset has been created and contains " << nbpart << " particles" << endl; 
		return *subset;
	}

	 ParticlesSet ParticlesSet::subSet(const Coord& coord, double distXY, double distZ)
	{
		ParticlesSet* subset = new ParticlesSet(this->name);
		//cout << "Extracting subset " << this->name << "subset" << endl;
		int nbpart = 0;
		int visited = 0;
		for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit)
		{
			visited++;
			if(((*pit)->getCoord().distXY(coord)<distXY)&&((*pit)->getCoord().distZ(coord)<distZ))
			{
				subset->addParticle(*pit);
				nbpart++;
			}
		}
		//cout << "The subset has been created and contains " << nbpart << " particles" << endl; 
		return *subset;
	}

	 void ParticlesSet::setDimensions()
	 {
		const char* img1 = this->name.c_str();
	 	string mode = "r";
	 	TIFF* tif = TIFFOpen(img1,mode.c_str());
	 	if (tif) {
	 		uint32 imageWidth, imageLength;
	 		TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &imageWidth);
	 		TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &imageLength);
	 		//TIFFGetField(tif, TIFFTAG_IMAGEDEPTH, &imageDepth);
	 		int dircount = 0;
	         do {
	             dircount++;
	         } while (TIFFReadDirectory(tif));
	         TIFFClose(tif);
	         this->StackdimX = (int)imageWidth;
	         this->StackdimY = (int)imageLength;
	         this->StackdimZ = (int)dircount;
	 	}
	 	else
	 	{
	 		this->StackdimX = 0;
	 		this->StackdimY = 0;
	 		this->StackdimZ = 0;
	 	}
	 }


	vector<ParticlesSet> ParticlesSet::findBestCorrel_DICGPU(ParticlesSet &pfinal, vector<Coord> refPoints, vector<Coord> refDepl, double distXY, double distZ)
	{
		vector<ParticlesSet>* potentialCorrel = new vector<ParticlesSet>();
		Volume imRef = Volume(this->name, Coord(this->StackdimX, this->StackdimY, this->StackdimZ));
		Volume imFinal = Volume(pfinal.getName(), Coord(pfinal.getStackdimX(), pfinal.getStackdimY(), pfinal.getStackdimZ()));
		cout << "Initialize GPU" << endl;
		Correl3DGPU corr = Correl3DGPU(imRef.getName().c_str(), imFinal.getName().c_str());
		corr.setfromFull(true);
		corr.setFilter(false);
		corr.setNormalize(true);
		corr.setIntersect(false);
		corr.setReducedWindow(true);
		corr.setWindow("none");
		corr.init();
		//corr.setFullRef(imRef.getName().c_str(), imRef.getdimZ(), imRef.getdimY(), imRef.getdimX());
		//corr.setFullTarget(imFinal.getName().c_str(), imFinal.getdimZ(), imFinal.getdimY(), imFinal.getdimX());
		cout << "Setting Ref" << endl;
		//cout << "Ref is " << imRef.getName().c_str() << endl;
		corr.setFullRef(imRef.getName().c_str());
		//cout << "Ref is set" << endl;
		cout << "Setting Target" << endl;
		corr.setFullTarget(imFinal.getName().c_str());
		//cout << "Target is set" << endl;
		clock_t startTime = clock();
		clock_t currentTime = clock();
		int globalTime = diffclock(currentTime,startTime);
		int deltaTime = diffclock(clock(),currentTime);
		int i=0;
		cout << "Ready to start correlations" << endl;
		//cout << this->getParticle(4)->fullInfoTostring() << endl;    
		for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit, ++i)
		{
			//i++;
			//cout << i << endl;
			//cout << this->getParticle(i)->tostring() << endl;    
			//cout << (*pit)->tostring() << endl;    
			if(i%100 == 0)
			{
				deltaTime = diffclock(clock(),currentTime);
				currentTime = clock();
				globalTime = diffclock(currentTime,startTime);
				cout << "Proceeded 100 pores in " << deltaTime << "ms ; " << i <<"/" << this->particles->size() <<" in " << globalTime/1000 << "s" <<endl;
			}
			
            corr.setImRef((int)(*pit)->getCoord().getZ(), (int)(*pit)->getCoord().getY(), (int)(*pit)->getCoord().getX());
            Coord guestDepl = (*pit)->getCoord().findDepl(refPoints,refDepl,30);
			ParticlesSet subset =  pfinal.subSet((*pit)->getCoord()+guestDepl, distXY, distZ);

            ParticlesSet potential = (*pit)->findBestCorrel_DICGPU(subset,corr,(*pit)->getCoord()+guestDepl);

        	potential.setName("potential for particle "+i);
			(*potentialCorrel).push_back(potential);
			
		}
		corr.freeFullImg();
		corr.free();
		return *potentialCorrel;
	}

	vector<ParticlesSet> ParticlesSet::findBestCorrel_DICGPU(ParticlesSet &pfinal, PointsSet refPoints, PointsSet finalPoints, double distXY, double distZ)
		{
			vector<ParticlesSet>* potentialCorrel = new vector<ParticlesSet>();
			Volume imRef = Volume(this->name, Coord(this->StackdimX, this->StackdimY, this->StackdimZ));
			Volume imFinal = Volume(pfinal.getName(), Coord(pfinal.getStackdimX(), pfinal.getStackdimY(), pfinal.getStackdimZ()));
			cout << "Initialize GPU" << endl;
			Correl3DGPU corr = Correl3DGPU(imRef.getName().c_str(), imFinal.getName().c_str());
			corr.setfromFull(true);
			corr.setFilter(false);
			corr.setNormalize(true);
			corr.setIntersect(false);
			corr.setReducedWindow(true);
			corr.setWindow("none");
			corr.init();
			//corr.setFullRef(imRef.getName().c_str(), imRef.getdimZ(), imRef.getdimY(), imRef.getdimX());
			//corr.setFullTarget(imFinal.getName().c_str(), imFinal.getdimZ(), imFinal.getdimY(), imFinal.getdimX());
			cout << "Setting Ref" << endl;
			//cout << "Ref is " << imRef.getName().c_str() << endl;
			corr.setFullRef(imRef.getName().c_str());
			//cout << "Ref is set" << endl;
			cout << "Setting Target" << endl;
			corr.setFullTarget(imFinal.getName().c_str());
			//cout << "Target is set" << endl;
			clock_t startTime = clock();
			clock_t currentTime = clock();
			int globalTime = diffclock(currentTime,startTime);
			int deltaTime = diffclock(clock(),currentTime);
			int i=0;
			cout << "Ready to start correlations" << endl;
			//cout << this->getParticle(4)->fullInfoTostring() << endl;
			for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit, ++i)
			{
				//i++;
				//cout << i << endl;
				//cout << this->getParticle(i)->tostring() << endl;
				//cout << (*pit)->tostring() << endl;
				if(i%100 == 0)
				{
					deltaTime = diffclock(clock(),currentTime);
					currentTime = clock();
					globalTime = diffclock(currentTime,startTime);
					cout << "Proceeded 100 pores in " << deltaTime << "ms ; " << i <<"/" << this->particles->size() <<" in " << globalTime/1000 << "s" <<endl;
				}

	            corr.setImRef((int)(*pit)->getCoord().getZ(), (int)(*pit)->getCoord().getY(), (int)(*pit)->getCoord().getX());
	            Coord guestPos = refPoints.estimatePositionGauss((*pit)->getCoord(),finalPoints, 0.1, sqrt(distXY*distXY+distZ*distZ)*3, sqrt(distXY*distXY+distZ*distZ), true);
	            ParticlesSet subset =  pfinal.subSet(guestPos, distXY, distZ);

	            ParticlesSet potential = (*pit)->findBestCorrel_DICGPU(subset,corr,guestPos);

	        	potential.setName("potential for particle "+i);
				(*potentialCorrel).push_back(potential);

			}
			corr.freeFullImg();
			corr.free();
			return *potentialCorrel;
		}

	 vector<ParticlesSet> ParticlesSet::findBestCorrel_noDIC(ParticlesSet &pfinal, PointsSet refPoints, PointsSet finalPoints, double distXY, double distZ)
	{
		vector<ParticlesSet>* potentialCorrel = new vector<ParticlesSet>();
		Volume imRef = Volume(this->name, Coord(this->StackdimX, this->StackdimY, this->StackdimZ));
		Volume imFinal = Volume(pfinal.getName(), Coord(pfinal.getStackdimX(), pfinal.getStackdimY(), pfinal.getStackdimZ()));
		clock_t startTime = clock();
		clock_t currentTime = clock();
		int globalTime = diffclock(currentTime,startTime);
		int deltaTime = diffclock(clock(),currentTime);
		int i=0;
		cout << "Ready to start correlations" << endl;
		//cout << this->getParticle(4)->fullInfoTostring() << endl;    
		for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit, ++i)
		{
			//i++;
			//cout << i << endl;
			//cout << this->getParticle(i)->tostring() << endl;    
			//cout << (*pit)->tostring() << endl;    
			if(i%100 == 0)
			{
				deltaTime = diffclock(clock(),currentTime);
				currentTime = clock();
				globalTime = diffclock(currentTime,startTime);
				cout << "Proceeded 100 pores in " << deltaTime << "ms ; " << i <<"/" << this->particles->size() <<" in " << globalTime/1000 << "s" <<endl;
			}
			
			Coord guestPos = refPoints.estimatePositionGauss((*pit)->getCoord(),finalPoints, 0.1, sqrt(distXY*distXY+distZ*distZ)*3, sqrt(distXY*distXY+distZ*distZ), true);
			ParticlesSet subset =  pfinal.subSet(guestPos, distXY, distZ);

            ParticlesSet potential = (*pit)->findBestCorrel_noDIC(subset,guestPos);

        	potential.setName("potential for particle "+i);
			(*potentialCorrel).push_back(potential);
			
		}
		return *potentialCorrel;
	}
		
	 vector<ParticlesSet> ParticlesSet::findBestCorrel_noDIC(ParticlesSet &pfinal, vector<Coord> refPoints, vector<Coord> refDepl, double distXY, double distZ)
		{
			vector<ParticlesSet>* potentialCorrel = new vector<ParticlesSet>();
			Volume imRef = Volume(this->name, Coord(this->StackdimX, this->StackdimY, this->StackdimZ));
			Volume imFinal = Volume(pfinal.getName(), Coord(pfinal.getStackdimX(), pfinal.getStackdimY(), pfinal.getStackdimZ()));
			clock_t startTime = clock();
			clock_t currentTime = clock();
			int globalTime = diffclock(currentTime,startTime);
			int deltaTime = diffclock(clock(),currentTime);
			int i=0;
			cout << "Ready to start correlations" << endl;
			//cout << this->getParticle(4)->fullInfoTostring() << endl;
			for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit, ++i)
			{
				//i++;
				//cout << i << endl;
				//cout << this->getParticle(i)->tostring() << endl;
				//cout << (*pit)->tostring() << endl;
				if(i%100 == 0)
				{
					deltaTime = diffclock(clock(),currentTime);
					currentTime = clock();
					globalTime = diffclock(currentTime,startTime);
					cout << "Proceeded 100 pores in " << deltaTime << "ms ; " << i <<"/" << this->particles->size() <<" in " << globalTime/1000 << "s" <<endl;
				}

	            Coord guestDepl = (*pit)->getCoord().findDepl(refPoints,refDepl,150);
				ParticlesSet subset =  pfinal.subSet((*pit)->getCoord()+guestDepl, distXY, distZ);

	            ParticlesSet potential = (*pit)->findBestCorrel_noDIC(subset,(*pit)->getCoord()+guestDepl);

	        	potential.setName("potential for particle "+i);
				(*potentialCorrel).push_back(potential);

			}
			return *potentialCorrel;
		}

	 vector<ParticlesSet> ParticlesSet::findBestCorrel_noDIC(ParticlesSet &pfinal, vector<vector<double> > depl, double distXY, double distZ)
	 	{
	 		vector<ParticlesSet>* potentialCorrel = new vector<ParticlesSet>();
	 		Volume imRef = Volume(this->name, Coord(this->StackdimX, this->StackdimY, this->StackdimZ));
	 		Volume imFinal = Volume(pfinal.getName(), Coord(pfinal.getStackdimX(), pfinal.getStackdimY(), pfinal.getStackdimZ()));
	 		clock_t startTime = clock();
	 		clock_t currentTime = clock();
	 		int globalTime = diffclock(currentTime,startTime);
	 		int deltaTime = diffclock(clock(),currentTime);
	 		int i=0;
	 		cout << "Ready to start correlations" << endl;
	 		for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit, ++i)
	 		{
	 			if(i%100 == 0)
	 			{
	 				deltaTime = diffclock(clock(),currentTime);
	 				currentTime = clock();
	 				globalTime = diffclock(currentTime,startTime);
	 				cout << "Proceeded 100 pores in " << deltaTime << "ms ; " << i <<"/" << this->particles->size() <<" in " << globalTime/1000 << "s" <<endl;
	 			}

	        	ParticlesSet subset =  pfinal.subSet((*pit)->getCoord().shift(depl), distXY, distZ);

	        	ParticlesSet potential = (*pit)->findBestCorrel_noDIC(subset,(*pit)->getCoord().shift(depl));
	 	     	potential.setName("potential for particle "+i);
	 			(*potentialCorrel).push_back(potential);

	 		}
	 		return *potentialCorrel;
	 	}


	 vector<ParticlesSet> ParticlesSet::checkCorrel(vector<ParticlesSet> potential, ParticlesSet pfinal, vector<ParticlesSet> potentialInv)
	{			
		vector<ParticlesSet>* checkedCorrel = new vector<ParticlesSet>();
		int ck=1;
		int round=0;
		while(ck>0)
		{
			round++;
			cout << "Check correl round " << round  << "\n" << endl;
			ck=0;
			int i=0;
			for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit, ++i)
			{
				//printf("Current particle : " + i +" ,color is " + this->particles[i].getColor() + "\n");
				int indpotInv = -1;
				indpotInv = pfinal.getPos(**potential[i].particles->begin());
				//printf("indpotInv : " + indpotInv+"\n");
				if((indpotInv>=0)&&((*pit)->equalColor(**potentialInv[indpotInv].particles->begin())))
				{
					ck++;
					//printf("found\n");
					ParticlesSet checked = ParticlesSet();
					checked.addParticle(*pit);
					checked.addParticle(*potential[i].particles->begin());
					//potential[i] = ParticlesSet();
					for(int j=0; j<potentialInv.size();j++)
					{
						if(potentialInv[j].contains(**pit))
						{
							potentialInv[j].removeParticle(potentialInv[j].getPos(**pit));
						}
					}
					//printf("removed\n");
				}
			}
		}
		return *checkedCorrel;
	}

	 void ParticlesSet::debugCorrel(vector<ParticlesSet> & potential, ParticlesSet & pfinal, vector<ParticlesSet> & potentialInv)
	{			
		int i=0;
		int ck=0;
    
        int nref=this->particles->size();
		//for each particle **pit of reference volume
        for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit, ++i)
		{
			cout << "Check correl particle " << (i+1) << "/" << nref << endl;
            if(*pit)
                    cout << "pit is ok and color is " << (*pit)->getColor() << endl;
			
            if(potential[i].getParticle(0))
            {
                cout << potential[i].Size() << " potential particles" << endl;
                int indpotInv = -1;
			    indpotInv = pfinal.getPos(*potential[i].getParticle(0));
			    cout << "Index of potInv " << indpotInv << endl;
			    Particle* potInv = potentialInv[indpotInv].getParticle(0);

                if(potInv){
                    cout << "potInv is ok" << endl;
                    cout << "Refcolor = " << (*pit)->getColor() << " and Found Color = " << potInv->getColor() << endl;
                }
            }
            else
            {
                cout << "No potential correl for this particle" << endl;
            }
		}
	}
	

	
	 vector<ParticlesSet> ParticlesSet::matchCorrel(vector<ParticlesSet> & potential, ParticlesSet & pfinal, vector<ParticlesSet> & potentialInv)
	{			
		vector<ParticlesSet>* checkedMain = new vector<ParticlesSet>();
		int i=0;
		int ck=0;
    
        int nref=this->particles->size();
		//for each particle **pit of reference volume
        for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit, ++i)
		{
			//cout << "Check correl particle " << (i+1) << "/" << nref << endl;
            if(potential[i].getParticle(0))
			{
                int indpotInv = -1;
			    indpotInv = pfinal.getPos(*potential[i].getParticle(0));
			    Particle* potInv = potentialInv[indpotInv].getParticle(0);
                //if(potInv){
                //    cout << "Refcolor = " << (*pit)->getColor() << " and Found Color = " << potInv->getColor() << endl;
                //}
                
                //if reference particule is the best correlation of its best correlation (both particles matches)
                if((indpotInv>=0) && (potInv) && ((*pit)->equalColor(*potInv)))
			    {
				    ck++;
				    //cout << "Checked++ (" << ck << "," << i << ")" << endl;
				    ParticlesSet* checked = new ParticlesSet(this->name);
				    //cout << "Particle " << (*pit)->getColor() << " and " << potential[i].getParticle(0)->getColor() << endl;
                    checked->addParticle(*pit);
				    checked->addParticle(potential[i].getParticle(0));
                    int j=0;
                    for (list<Particle*>::iterator pitF=pfinal.getIteratorBegin(); pitF!=pfinal.getIteratorEnd(); ++pitF, ++j)
		            {
					    if(potentialInv[j].contains(**pit))
					    {
                            //cout << " Add particle " << (*pitF)->getColor() << endl;
						    checked->addParticle(*pitF);
                            potentialInv[j].removeParticle(potentialInv[j].getPos(**pit));
					    }
				    }
				    checkedMain->push_back(*checked);
			    }
            }
            else
            {
                cout << "No potential correl for this particle" << endl;
            }
		}
        cout << "Found " << ck << "/" << nref << "particles matching" << endl;
		return *checkedMain;
	}
	


	 ParticlesSet ParticlesSet::closest(ParticlesSet pfinal, double distXY, double distZ)
	{
		ParticlesSet* potentialCorrel = new ParticlesSet();
		for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit)
		{
			ParticlesSet subset =  pfinal.subSet((*pit)->getCoord(), distXY, distZ);
			Particle* potential = (*pit)->findClosest(subset);
			potentialCorrel->addParticle(potential);
		}
		return *potentialCorrel;
	}
	
	 ParticlesSet ParticlesSet::findLost(ParticlesSet pool)
	{
		ParticlesSet lost = ParticlesSet();
		for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit)
		{
			if(!pool.contains(**pit))
				lost.addParticle(*pit);
		}
		return lost;
	}
	
	 bool ParticlesSet::contains(const Particle& part)
	{
		for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit)
		{
			if( *pit && (*pit)->equalColor(part))
				return true;
		}
		return false;
	}



	 int ParticlesSet::getPos(const Particle& part)
	{
		 int i=0;
		 for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit, ++i)
		{
			if((*pit)->equalColor(part))
				return i;
		}
		return -1;
	}

	 int ParticlesSet::setGlobalNumbers(int current)
	{
		 for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit)
		 {
			if(((*pit)->getGlobalColor()==0))
			{
				current++;
				(*pit)->setGlobalColor(current);
			}
		}
		return current;
	}
	
	 void ParticlesSet::matchGlobalNumberIntra(ParticlesSet partSet)
	{
		 list<Particle*>::iterator pitSet = partSet.particles->begin();
		 for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit)
		 {
			 (*pit)->setGlobalColor((*pitSet)->getGlobalColor());
		 }
	}
	 int ParticlesSet::globalNumberFromCheckedRef(vector<ParticlesSet> & VpS, int current, int scan)
	{
		for(int i=0; i<VpS.size();i++)
		{
			int pos = this->getPos(**VpS[i].getIteratorBegin());
			if((pos>=0)&&(this->getParticle(pos)->getGlobalColor()<=0))
			{	
				current++;
				Particle* pit = this->getParticle(pos);
				pit->setGlobalColor(current);
				pit->setFather(current);
				pit->setGrandfather(current);
				pit->setFatherconnect(scan);
				pit->setGrandfatherconnect(scan);
				pit->setBorn(scan);
				pit->setDead(scan);
			}
		}
		return current;
	}

	 int ParticlesSet::globalNumberFromCheckedFinal(vector<ParticlesSet> & VpS, ParticlesSet & ref, int current, int scan )
	{
		for(int i=0; i<VpS.size();i++)
		{
			int pos = this->getPos(*VpS[i].getParticle(1));
			int posref=ref.getPos(*VpS[i].getParticle(0));
			
			if((pos>=0)&&(posref>=0)&&(ref.getParticle(posref)->getGlobalColor()>0))
			{	
				Particle* pit = this->getParticle(pos);
				pit->setGlobalColor(ref.getParticle(posref)->getGlobalColor());
				pit->setFather(ref.getParticle(posref)->getFather());
				pit->setGrandfather(ref.getParticle(posref)->getGrandfather());
				pit->setFatherconnect(ref.getParticle(posref)->getFatherconnect());
				pit->setGrandfatherconnect(ref.getParticle(posref)->getGrandfatherconnect());
				pit->setBorn(scan);
				pit->setDead(ref.getParticle(posref)->getDead());
			}
			for(int j=2; j<VpS[i].Size(); j++)
			{
				pos = this->getPos(*VpS[i].getParticle(j));
				if((pos>=0)&&(posref>=0)&&(ref.getParticle(posref)->getGlobalColor()>0)&&(this->getParticle(pos)->getGlobalColor()<=0))
				{	
					current++;
					Particle* pit = this->getParticle(pos);
					pit->setGlobalColor(current);
					pit->setFather(ref.getParticle(posref)->getGlobalColor());
					pit->setGrandfather(ref.getParticle(posref)->getGrandfather());
					pit->setFatherconnect(scan);
					pit->setGrandfatherconnect(ref.getParticle(posref)->getGrandfatherconnect());
					pit->setBorn(scan);
					pit->setDead(scan);
				}
			}
		}
		return current;
	}



	 vector<ParticlesSet> ParticlesSet::findCorrel(ParticlesSet pfinal, double refSlices[], double refSlicesFinal[])
	{
		vector<ParticlesSet> potentialCorrel = vector<ParticlesSet>();
		for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit)
		{

			double dZ=1;
			double dXY=1;
			int subsetsize=0;
			ParticlesSet subset;
			while(subsetsize==0)
			{
				subset = pfinal.subSet((*pit)->getCoord().Interpolate(refSlicesFinal, refSlices), dXY, dZ);
				subsetsize = subset.Size();
			}
			ParticlesSet partS = ParticlesSet();
			partS.addParticle(*pit);
			partS.addParticle(*subset.particles->begin());
			potentialCorrel.push_back(partS);
		}
		return potentialCorrel;
	}
	
	 inline bool larger_volume(Particle* part1, Particle* part2){ return (part1->getVolume()<part2->getVolume()); }

	 ParticlesSet ParticlesSet::rankByVolume()
	{
		ParticlesSet ranked = ParticlesSet(*this);
		ranked.particles->sort(larger_volume);
		return ranked;
	}

	 Particle* ParticlesSet::findClosest(const Coord& XYZ)
	{
		if(this->Size()>0)
		{
			double bestDist=1000000000000;
			list<Particle*>::iterator best = this->particles->begin();
			for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit)
			{
				double dist = (*pit)->distance(XYZ);
				if(dist<bestDist)
				{
					best=pit;
					bestDist=dist;
				}
			}
			return *best;
		}
		else
		{

			Particle* emptyPart(0);
			return emptyPart;
		}
	}
	
	 Particle* ParticlesSet::findParticle(int color)
	{
		for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit)
		{
			if((*pit)->getColor()==color)
				return *pit;
		}
		Particle* emptyPart(0);
		return emptyPart;
	}
	
	 Particle* ParticlesSet::findParticle(int color) const
	{
		for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit)
		{
			if((*pit)->getColor()==color)
				return *pit;
		}
		Particle* emptyPart(0);
		return emptyPart;
	}

	 Particle* ParticlesSet::findGlobalColorParticle(int color)
	{
		for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit)
		{
				if((*pit)->getGlobalColor()==color)
				{
					cout << "Particle " << (*pit)->getColor() << " found" << endl;
					return *pit;
				}

		}
		Particle* emptyPart(0);
		cout << "No particle found" << endl;
		return emptyPart;
	}
	
	 string ParticlesSet::fullInfoTostring()
	{
		ostringstream colorLine;
		string header = "";
		for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit)
		{
				if(header=="")
				{
					header = (*pit)->writeHeader();
					colorLine << header << " ";
				}
				colorLine << (*pit)->fullInfoTostring() << " ";
		}
		return colorLine.str();
	}
	
	
	 string ParticlesSet::colorTostring()
	{
		 ostringstream colorLine;
		
		for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit)
		{
				if(*pit)
                    colorLine << (*pit)->getColor() << " ";
				else
					colorLine << -1 << " ";
		}
		return colorLine.str();
	}
	
	 string ParticlesSet::colorGlobalTostring()
	{
		  ostringstream colorLine;

			for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit)
			{
				if(*pit)
				      colorLine << (*pit)->getGlobalColor() << " ";
				else
					  colorLine << -1 << " ";
		    }
		return colorLine.str();
	}
	
	 void ParticlesSet::colorToFile(string fileName)
	{

		 ofstream fichier(fileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier

        if(fichier)  // si l'ouverture a réussi
        {
        	for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit)
        	{
        		fichier << (*pit)->getColor() << endl;
        	}

                fichier.close();  // on referme le fichier
        }
        else  // sinon
                cerr << "Erreur à l'ouverture !" << endl;
		
	}
	
	 void ParticlesSet::writeToFile(const string fileDirectory, const string particlesSetName) const
	{
		    ostringstream fileName;
			fileName << fileDirectory << "/" << particlesSetName;
			 ofstream fichier(fileName.str().c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier

	        if(fichier)  // si l'ouverture a réussi
	        {
	        	fichier << this->name << endl;
	        	fichier << this->StackdimX << endl;
	    		fichier << this->StackdimY << endl;
	    		fichier << this->StackdimZ << endl;
	        	for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit)
	        	{
	        		if(*pit){
	        			fichier << (*pit)->getScan() << " " << (*pit)->getColor() << endl;
	        			(*pit)->writeToFile(fileDirectory);
	        		}
	        	}
	            fichier.close();  // on referme le fichier
	        }
	        else  // sinon
	                cerr << "Erreur à l'ouverture !" << endl;

	}

	 void ParticlesSet::writeToFile(const string fileName)
		{

			 ofstream fichier(fileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier

	        if(fichier)  // si l'ouverture a réussi
	        {
	        	for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit)
	        	{
	        		fichier << (*pit)->tostring() << endl;

	        	}
	            fichier.close();  // on referme le fichier
	        }
	        else  // sinon
	                cerr << "Erreur à l'ouverture !" << endl;

		}

	 void ParticlesSet::write(const string fileName) const
	 {

			 ofstream fichier(fileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier

	        if(fichier)  // si l'ouverture a réussi
	        {
	        	for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit)
	        	{
	        		fichier << (*pit)->write() << endl;

	        	}
	            fichier.close();  // on referme le fichier
	        }
	        else  // sinon
	                cerr << "Erreur à l'ouverture !" << endl;

	}

	 string ParticlesSet::volumeTostring()
	{
		 ostringstream colorLine;

     	for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit)
     	{
     		if(*pit)
     						      colorLine << (*pit)->getVolume() << " ";
     						else
     							  colorLine << -1 << " ";
		}
		return colorLine.str();
	}
	
	 string ParticlesSet::sphericityTostring()
	{
		ostringstream colorLine;

     	for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit)
     	{
     		colorLine << (*pit)->getSphericity() << " ";

		}
		return colorLine.str();
	}
	
	 string ParticlesSet::shapeFactTostring()
	{
		 ostringstream colorLine;

	     	for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit)
	     	{
	     		colorLine << (*pit)->getShapeFact() << " ";

		}
		return colorLine.str();
	}
	
	 string ParticlesSet::shapeFact2Tostring()
	{
		 ostringstream colorLine;

	     	for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit)
	     	{
	     		colorLine << (*pit)->getShapeFact2() << " ";

		}
		return colorLine.str();
	}
	
	 string ParticlesSet::zAngleTostring()
	{
		 ostringstream colorLine;

	     	for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit)
	     	{
	     		colorLine << (*pit)->getZAngle() << " ";

		}
		return colorLine.str();
	}
	
	 string ParticlesSet::PosTostring()
	{
		 ostringstream colorLine;

	     	for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit)
	     	{
	     		if(*pit)
	     			colorLine << (*pit)->getCoord().getX() << " " << (*pit)->getCoord().getY() << " " << (*pit)->getCoord().getZ() <<" ";
	     		else
	     			colorLine << -1 << " " << -1 << " " << -1 << " ";


		}
		return colorLine.str();
	}
	
	
	


int ParticlesSet::getStackdimX() const {
	return this->StackdimX;
}

void ParticlesSet::setStackdimX(int stackdimX) {
	StackdimX = stackdimX;
}

int ParticlesSet::getStackdimY() const {
	return StackdimY;
}

void ParticlesSet::setStackdimY(int stackdimY) {
	StackdimY = stackdimY;
}

int ParticlesSet::getStackdimZ() const {
	return StackdimZ;
}

void ParticlesSet::setStackdimZ(int stackdimZ) {
	StackdimZ = stackdimZ;
}



/*
	 vector<ParticlesSet> ParticlesSet::findBestCorrel(ParticlesSet pfinal, double refSlices[], double refSlicesFinal[], double distXY, double distZ)
	{
		vector<ParticlesSet> potentialCorrel = vector<ParticlesSet>();
		for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit)
		{
			ParticlesSet subset =  pfinal.subSet((*pit)->getCoord().Interpolate(refSlices,refSlicesFinal), distXY, distZ);
			ParticlesSet potential = (*pit)->findBestCorrel(subset);
			potential.setName("potential for particle ");
			potentialCorrel.push_back(potential);
			pit++;
		}
		return potentialCorrel;
	}

	 vector<ParticlesSet> ParticlesSet::findBestCorrel_noDIC(ParticlesSet pfinal, double* depl, double distXY, double distZ)
	{
		vector<ParticlesSet> potentialCorrel = vector<ParticlesSet>();
		for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit)
		{
			ParticlesSet subset =  pfinal.subSet((*pit)->getCoord().shift(depl), distXY, distZ);
			ParticlesSet potential = (*pit)->findBestCorrel(subset);
			potentialCorrel.push_back(potential);
		}
		return potentialCorrel;
	}

	 vector<ParticlesSet> ParticlesSet::findBestCorrel_larger_noDIC(ParticlesSet pfinal, double* depl, double distXY, double distZ)
	{
		vector<ParticlesSet> potentialCorrel = vector<ParticlesSet>();
		for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit)
		{
			ParticlesSet subset =  pfinal.subSet((*pit)->getCoord().shift(depl), distXY, distZ);
			ParticlesSet potential = (*pit)->findBestCorrel_larger(subset);
			potentialCorrel.push_back(potential);
		}
		return potentialCorrel;
	}

	 vector<ParticlesSet> ParticlesSet::findBestCorrel_smaller_noDIC(ParticlesSet pfinal, double* depl, double distXY, double distZ)
	{
		vector<ParticlesSet> potentialCorrel = vector<ParticlesSet>();
		for (list<Particle*>::iterator pit=this->particles->begin(); pit!=this->particles->end(); ++pit)
		{
			ParticlesSet subset =  pfinal.subSet((*pit)->getCoord().shift(depl), distXY, distZ);
			ParticlesSet potential = (*pit)->findBestCorrel_smaller(subset);
			potentialCorrel.push_back(potential);
		}
		return potentialCorrel;
	}
*/
	 /*
	 vector<ParticlesSet> ParticlesSet::findBestCorrel_DIC(ParticlesSet pfinal, double depl[][], double distXY, double distZ)
	{
		vector<ParticlesSet> potentialCorrel = new vector<ParticlesSet>();
		Stack imRef = openStack(this->name, this->StackdimX, this->StackdimY, this->StackdimZ);
		Stack imFinal = openStack(pfinal.getName(), pfinal.getStackdimX(), pfinal.getStackdimY(), pfinal.getStackdimZ());

		for(int i=0; i<this->particles->length();i++)
		{
			printf("Particle "+this->particles[i].getColor()+" x="+this->particles[i].getCoord().getX()+" y="+this->particles[i].getCoord().getY()+" z="+this->particles[i].getCoord().getZ()+"\n");
			Stack imPart = this->particles[i].getParticleImagePlus(imRef);
			ParticlesSet subset =  pfinal.subSet(this->particles[i].getCoord().shift(depl), distXY, distZ);
			printf("Check correl with "+subset.Size()+" particles\n");
			ParticlesSet potential = this->particles[i].findBestCorrel_DIC(subset,imPart,imFinal);
			potential.setName("potential for particle "+i);
			potentialCorrel.push_back(potential);
			imPart = NULL;
		}
		return potentialCorrel;
	}
	*/
	/**

	 vector<ParticlesSet> findBestCorrel_DICGPU_old(ParticlesSet pfinal, double[][] depl, double distXY, double distZ)
	{
		vector<ParticlesSet> potentialCorrel = new vector<ParticlesSet>();
		ImagePlus imRef = IJ.openImage(this->name);
		ImagePlus imFinal = IJ.openImage(pfinal.name);
		Correl3DGPU corr = new Correl3DGPU();
		corr.setfromFull(true);
		corr.init();
		corr.setFullRef(imRef, imRef.getStackSize(), imRef.getHeight(), imRef.getWidth());
		corr.setFullTarget(imFinal, imFinal.getStackSize(), imFinal.getHeight(), imFinal.getWidth());
		for(int i=0; i<this->particles->length();i++)
		{
			//long startTime = System.currentTimeMillis();
			printf("Particle "+this->particles[i].getColor()+" x="+this->particles[i].getCoord().getX()+" y="+this->particles[i].getCoord().getY()+" z="+this->particles[i].getCoord().getZ()+"\n");
			//ImagePlus imPart = this->particles[i].getParticleImagePlus(imRef,"imPart");
			corr.setImRef((int)this->particles[i].getCoord().getZ(), (int)this->particles[i].getCoord().getY(), (int)this->particles[i].getCoord().getX());
			ParticlesSet subset =  pfinal.subSet(this->particles[i].getCoord().shift(depl), distXY, distZ);
			//printf("Check correl with "+subset.Size()+" particles\n");
			ParticlesSet potential = this->particles[i].findBestCorrel_DICGPU(subset,corr);
			potential.setName("potential for particle "+i);
			potentialCorrel.add(potential);
		}
		corr.freeFullImg();
		corr.free();
		return potentialCorrel;
	}

	*/


/*	ParticlesSet::ParticlesSet(string name, int StackdimX, int StackdimY, int StackdimZ) {
		this->name = name;
		this->StackdimX = StackdimX;
		this->StackdimY = StackdimY;
		this->StackdimZ = StackdimZ;
		this->particles = new list<Particle*> ();
	}

	ParticlesSet::ParticlesSet(string name, int StackdimX, int StackdimY, int StackdimZ, list<Particle*>  *particles) {
		this->name = name;
		this->StackdimX = StackdimX;
		this->StackdimY = StackdimY;
		this->StackdimZ = StackdimZ;
		this->particles = particles;
	}
	*/

/*
	ParticlesSet::ParticlesSet(const string name, int StackdimX, int StackdimY, int StackdimZ, const vector<Particle*>& partraw)
	{
		cout << "New ParticlesSet of " << partraw.size() << " objects with image " << name << " (" << StackdimX << "," << StackdimY << "," << StackdimZ << ")" << endl;
		this->name = name;
		this->StackdimX = StackdimX;
		this->StackdimY = StackdimY;
		this->StackdimZ = StackdimZ;
		this->particles = new list<Particle*>();
		//cout << partraw[0]->fullInfoTostring() << endl;
		for(int i=0; i<partraw.size(); i++)
		{
			//cout << "Particle* pt(0);" << endl;
			Particle* pt;
			//cout << "*pt = partraw[" << i <<"];" << endl;
			pt = partraw[i];
			//cout << "this->particles->push_back(pt);" << endl;
			this->particles->push_back(pt);
		}
		//cout << partraw[partraw.size()-1]->fullInfoTostring() << endl;
	}
	*/

/*
 void findNULLFromImage(ParticlesSet poolr, string ImfName, double thrXY,double thrZ)
{
	Opener op = new Opener();
	ImageStack st = op.openImage(ImfName).getStack();
	int dx=0;
	int dy=0;
	int dz=0;
	for(int i=0; i<this->particles->length(); i++)
	{
		if(this->getParticle(i)==NULL)
		{
			Coord center = poolr.getParticle(i).getCoord();
			int val=0;
			while((val==0)&&(Math.sqrt(dx*dx+dy*dy)<=thrXY)&&(dz<=thrZ))
			{
				val = center.findColorInStack(dx, dy, dz, st);
			}
			if(val!=0)
			{
				this->particles->set(i,poolr.findParticle(val));
			}
		}
	}
}
*/

/*
 void ParticlesSet::globalNumberFromCheckedFinal(vector<ParticlesSet> VpS, ParticlesSet ref)
{
	for(int i=0; i<VpS.size();i++)
	{
		int pos = this->getPos(*VpS[i].getParticle(1));
		int posref=ref.getPos(*VpS[i].getParticle(0));

		if((pos>=0)&&(posref>=0)&&(ref.getParticle(posref)->getGlobalColor()>0))
		{
			this->particles[pos].setGlobalColor(ref.getParticle(posref)->getGlobalColor());
		}
	}
}
*/
