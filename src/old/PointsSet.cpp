#include "PointsSet.h"
using namespace std;

	PointsSet::PointsSet() {
		this->name = "";
		this->StackdimX=0;
		this->StackdimY=0;
		this->StackdimZ=0;
		this->points = new list<Point*> ();
	}
	
	PointsSet::PointsSet(string name) {
		this->name = name;
		this->StackdimX=0;
		this->StackdimY=0;
		this->StackdimZ=0;
		this->setDimensions();
		this->points = new list<Point*> ();
	}

	PointsSet::PointsSet(string name, list<Point*> *points) {
		this->name = name;
		this->StackdimX=0;
		this->StackdimY=0;
		this->StackdimZ=0;
		this->setDimensions();
		this->points = points;
	}
	
	PointsSet::PointsSet(const string fileDirectory, const string pointsSetName, const vector<PointsSet> & raw) {
		 this->points = new list<Point*>();
		 ostringstream fileName;
		 fileName << fileDirectory << pointsSetName;
		 ifstream fichier(fileName.str().c_str(), ios::in);  //déclaration du flux et ouverture du fichier
		 if(fichier)  // si l'ouverture a réussi
	     {

		    	 string psName = "";
		    	 char schar[10000];
		    	 fichier.getline(schar,10000);
		    	 psName+=schar;
		    	 stringstream sstr;
		    	 sstr.clear();
		    	 sstr.str(psName);
		    	 string s;
		    	 int a;
		    	 if(sstr.str().size()>1)
		    	 {
		    		 sstr >> s;
		    		 this->name=s;
		    	 }
		    	 psName = "";
		    	 fichier.getline(schar,10000);
		    	 psName+=schar;
		    	 sstr.clear();
		    	 sstr.str(psName);
		    	 if(sstr.str().size()>1)
		    	 {
		    		 sstr >> a;
			    	 this->StackdimX = (int)a;
		    	 }
		    	 psName = "";
		    	 fichier.getline(schar,10000);
		    	 psName+=schar;
		    	 sstr.clear();
		    	 sstr.str(psName);
		    	 if(sstr.str().size()>1)
		    	 {
		    		 sstr >> a;
			    	 this->StackdimY = (int)a;
		    	 }
		    	 psName = "";
		    	 fichier.getline(schar,10000);
		    	 psName+=schar;
		    	 sstr.clear();
		    	 sstr.str(psName);
		    	 if(sstr.str().size()>1)
		    	 {
		    		 sstr >> a;
			    	 this->StackdimZ = (int)a;
		    	 }

		    	 LDEBUG << "Updated PointsSet " << pointsSetName;
		    	 LDEBUG << "Name = " << this->name;
		    	 LDEBUG << "dim = (" << this->StackdimX << "," << this->StackdimY << "," << this->StackdimZ << ")";
		    	 string dataline="";
		    	 while (fichier.good())
		    	 {
		    	 dataline="";
		         fichier.getline(schar,10000);
		    	 dataline+=schar;
		    	 sstr.clear();
		    	 sstr.str(dataline);
		    	 if(sstr.str().size()>1)
		    	 {
		    		sstr >> a;
		    		if(!isnan(1.*a))
		    		{
		    			int scan = (int)(1.*a);
		    			sstr >> a;
		    			int color = (int)(1.*a);
		    			Point* part = raw[scan].findPoint(color);
		    			part->PointUpdate(fileDirectory,scan,color);
		    			this->points->push_back(part);
			      }
		    	 }
		     }
			 fichier.close();
		}
		 else  // sinon
	     {
	    	 LERROR << "Erreur à l'ouverture !";
	     	 LERROR << "File " << fileName.str() << " not found!";
	     }
	}

	PointsSet::PointsSet(const string fileDirectory, const string pointsSetName) {
			 this->points = new list<Point*>();
			 ostringstream fileName;
			 fileName << fileDirectory << pointsSetName;
			 ifstream fichier(fileName.str().c_str(), ios::in);  //déclaration du flux et ouverture du fichier
			 if(fichier)  // si l'ouverture a réussi
		     {

			    	 string psName = "";
			    	 char schar[10000];
			    	 fichier.getline(schar,10000);
			    	 psName+=schar;
			    	 stringstream sstr;
			    	 sstr.clear();
			    	 sstr.str(psName);
			    	 string s;
			    	 int a;
			    	 if(sstr.str().size()>1)
			    	 {
			    		 sstr >> s;
			    		 this->name=s;
			    	 }
			    	 psName = "";
			    	 fichier.getline(schar,10000);
			    	 psName+=schar;
			    	 sstr.clear();
			    	 sstr.str(psName);
			    	 if(sstr.str().size()>1)
			    	 {
			    		 sstr >> a;
				    	 this->StackdimX = (int)a;
			    	 }
			    	 psName = "";
			    	 fichier.getline(schar,10000);
			    	 psName+=schar;
			    	 sstr.clear();
			    	 sstr.str(psName);
			    	 if(sstr.str().size()>1)
			    	 {
			    		 sstr >> a;
				    	 this->StackdimY = (int)a;
			    	 }
			    	 psName = "";
			    	 fichier.getline(schar,10000);
			    	 psName+=schar;
			    	 sstr.clear();
			    	 sstr.str(psName);
			    	 if(sstr.str().size()>1)
			    	 {
			    		 sstr >> a;
				    	 this->StackdimZ = (int)a;
			    	 }
			    	 LDEBUG << "New PointsSet " << pointsSetName;
			    	 LDEBUG << "Name = " << this->name;
			    	 LDEBUG << "dim = (" << this->StackdimX << "," << this->StackdimY << "," << this->StackdimZ << ")";

			    	string dataline="";
			    	while (fichier.good())
			    	{
			    	 dataline="";
			    	 fichier.getline(schar,10000);
			    	 dataline+=schar;
			    	 sstr.clear();
			    	 sstr.str(dataline);
			    	 if(sstr.str().size()>1)
			    	 {
			    		sstr >> a;
			    		if(!isnan(1.*a))
			    		{
			    			int scan = (int)(1.*a);
			    			sstr >> a;
			    			int color = (int)(1.*a);
			    			Point* part = new Point(fileDirectory,scan,color);
			    			this->points->push_back(part);
				      }
			    	 }
			     }
				 fichier.close();
			}
			 else  // sinon
		     {
		    	 LERROR << "Erreur à l'ouverture !";
		     	 LERROR << "File " << fileName.str() << " not found!";
		     }
		}

	PointsSet::PointsSet(const PointsSet& pointsSet)
	{
		this->name = pointsSet.getName();
		this->StackdimX = pointsSet.getStackdimX();
		this->StackdimY = pointsSet.getStackdimY();
		this->StackdimZ = pointsSet.getStackdimZ();
		this->points = pointsSet.getPoints();
	}

	PointsSet::PointsSet(const string name, const vector<Point*>& partraw)
	{
		this->name = name;
		this->StackdimX=0;
		this->StackdimY=0;
		this->StackdimZ=0;
		this->setDimensions();
		LINFO << "New PointsSet of " << partraw.size() << " points with image " << name << " (" << StackdimX << "," << StackdimY << "," << StackdimZ << ")";
		this->points = new list<Point*>();
		for(int i=0; i<partraw.size(); i++)
		{
			Point* pt;
			pt = partraw[i];
			this->points->push_back(pt);
		}
	}

	PointsSet::~PointsSet()
	{
		this->name="";
		this->StackdimX=0;
		this->StackdimY=0;
		this->StackdimZ=0;
		this->points->clear();
        delete points;
	}

	PointsSet &PointsSet::operator = (const PointsSet &pointsSet)
	{
		this->name = pointsSet.getName();
		this->StackdimX = pointsSet.getStackdimX();
		this->StackdimY = pointsSet.getStackdimY();
		this->StackdimZ = pointsSet.getStackdimZ();
		this->points = pointsSet.getPoints();
		return *this;
	}

	string PointsSet::getName() const {
		return name;
	}

	void PointsSet::setName(string name) {
		this->name = name;
	}

    list<Point*>*  PointsSet::getPoints() const {
		list<Point*> *toto = new list<Point*>();
		for (list<Point*>::const_iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
		{
				toto->push_back(&**pit);
		}
		return toto;
	}

	 void PointsSet::setPoints(list<Point*> *points) {
		this->points = points;
	}


	list<Point*>::iterator PointsSet::getIteratorBegin(){
		return this->points->begin();
	}

	list<Point*>::iterator PointsSet::getIteratorEnd(){
		return this->points->end();
	}

	list<Point*>::iterator PointsSet::getIteratorBegin() const {
		return this->points->begin();
	}

	list<Point*>::iterator PointsSet::getIteratorEnd() const {
		return this->points->end();
	}

	Point* PointsSet::getPoint(int i){
		int j=0;
	 	for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit, ++j)
	 	{
			if(i==j)
				return *pit;
		}
		Point* emptyPart(0);
		return emptyPart;
	}

	 void PointsSet::addPoint(Point* point) {
		this->points->push_back(point);
	}
	
	 int PointsSet::Size() const {
		return points->size();
	}
	
	 void PointsSet::removePoint(Point* part)
	{
        this->points->remove(part);
	}


	void PointsSet::removePoint(int i)
	{
		list<Point*>::iterator pit=this->points->begin();
		int j=0;
		while(j<i)
		{
			++pit;
		}
		this->points->erase(pit);
	}
	
	PointsSet PointsSet::subSet(const PointsSet& ps) const
	{
		PointsSet* subset = new PointsSet(this->name);
		int nbpart = 0;
		int visited = 0;
		for(list<Point*>::iterator pitF=ps.getIteratorBegin(); pitF!=ps.getIteratorEnd(); ++pitF)
		{
	            int i = (*pitF)->getColor();
				//cout << "Final points " << i << endl;
                if(this->findPoint(i))
                {
				    //cout << "Found" << endl;
                    subset->addPoint(this->findPoint(i));
                    //cout << "Added" << endl;
				}
                nbpart++;
		}
		//cout << "Final subset contains " << nbpart << " points" << endl;
		return *subset;
	}

	 PointsSet PointsSet::subSet(const Coord& coord, double distXY, double distZ) const
	{
		PointsSet* subset = new PointsSet(this->name);
		int nbpart = 0;
		int visited = 0;
		for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
		{
			visited++;
			if(((*pit)->getCoord().distXY(coord)<distXY)&&((*pit)->getCoord().distZ(coord)<distZ))
			{
				subset->addPoint(*pit);
				nbpart++;
			}
		}
		return *subset;
	}

	 PointsSet PointsSet::subSet(const Coord& coord, double dist) const
	{
		PointsSet* subset = new PointsSet(this->name);
		int nbpart = 0;
		int visited = 0;
		for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
		{
			visited++;
			if(((*pit)->getCoord().dist(coord)<dist))
			{
				subset->addPoint(*pit);
				nbpart++;
			}
		}
		return *subset;
	}


	PointsSet PointsSet::subSetCorrel(double thr) const
	{
		PointsSet* subset = new PointsSet(this->name);
		int nbpart = 0;
		int visited = 0;
		for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
		{
            //cout << "Point "  << (*pit)->getColor() << " Correl=" << (*pit)->getCorrel() << endl;
			visited++;
			if((*pit)->getCorrel()>thr)
			{
				subset->addPoint(*pit);
				nbpart++;
			}
		}
		return *subset;
	}

	PointsSet PointsSet::subSetTotalCorrel(double thr) const
	{
		PointsSet* subset = new PointsSet(this->name);
		int nbpart = 0;
		int visited = 0;
		for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
		{
            //cout << "Point "  << (*pit)->getColor() << " Correl=" << (*pit)->getCorrel() << endl;
			visited++;
			if((*pit)->getTotalCorrel()>thr)
			{
				subset->addPoint(*pit);
				nbpart++;
			}
		}
		return *subset;
	}



	 void PointsSet::setDimensions()
	 {
		const char* img1 = this->name.c_str();
	 	string mode = "r";
	 	TIFF* tif = TIFFOpen(img1,mode.c_str());
	 	if (tif) {
	 		uint32 imageWidth, imageLength;
	 		TIFFGetField(tif, TIFFTAG_IMAGEWIDTH, &imageWidth);
	 		TIFFGetField(tif, TIFFTAG_IMAGELENGTH, &imageLength);
	 		//TIFFGetField(tif, TIFFTAG_IMAGEDEPTH, &imageDepth);
	 		int dircount = 0;
	         do {
	             dircount++;
	         } while (TIFFReadDirectory(tif));
	         TIFFClose(tif);
	         this->StackdimX = (int)imageWidth;
	         this->StackdimY = (int)imageLength;
	         this->StackdimZ = (int)dircount;
	 	}
	 	else
	 	{
	 		this->StackdimX = 0;
	 		this->StackdimY = 0;
	 		this->StackdimZ = 0;
	 	}
	 }


	 PointsSet PointsSet::findBestCorrel_DICGPU(PointsSet &pfinal, int correlDomain, double distXY, double distZ, double correlThr)
	{
		Volume imRef = Volume(this->name, Coord(this->StackdimX, this->StackdimY, this->StackdimZ));
		Volume imFinal = Volume(pfinal.getName(), Coord(pfinal.getStackdimX(), pfinal.getStackdimY(), pfinal.getStackdimZ()));
		LINFO << "Initialize GPU";
		Correl3DGPU corr = Correl3DGPU(imRef.getName().c_str(), imFinal.getName().c_str());
		corr.setfromFull(true);
		corr.setFilter(false);
		corr.setNormalize(true);
		corr.setIntersect(false);
		corr.setReducedWindow(false);
		corr.setdisplThr(false, distZ, distXY, distXY);
		corr.setDomain(correlDomain);
        corr.setWindow("none");
        //corr.setWindow("hanning");

		corr.setFFT(false);
        corr.setHalfPattern(correlDomain);
        corr.setDisplacement(distXY);
        corr.setCorrelThr(correlThr);

        corr.init();

		LINFO << "Correlation parameters:";
        LINFO << corr.optionsToString();
        LINFO << "Setting Ref";
		corr.setFullRef(imRef.getName().c_str());
		LINFO << "Setting Target";
		corr.setFullTarget(imFinal.getName().c_str());
		clock_t startTime = clock();
		clock_t currentTime = clock();
		int globalTime = diffclock(currentTime,startTime);
		int deltaTime = diffclock(clock(),currentTime);
		int i=0;
		LINFO << "Ready to start correlations";
		list<Point*>::iterator pitF=(pfinal).getPoints()->begin();
        for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit, ++i)
		{
          	if(i%100 == 0)
			{
				deltaTime = diffclock(clock(),currentTime);
				currentTime = clock();
				globalTime = diffclock(currentTime,startTime);
				LINFO << "Proceeded 100 points in " << deltaTime << "ms ; " << i <<"/" << this->points->size() <<" in " << globalTime/1000 << "s";
			}
            corr.setImRef((int)(*pit)->getCoord().getZ(), (int)(*pit)->getCoord().getY(), (int)(*pit)->getCoord().getX());
            Point potential = (*pit)->findBestCorrel_DICGPU(corr);
			(*pitF)->PointUpdate(potential);
	        ++pitF;
	    }
		pfinal.countValid(correlThr);
        corr.freeFullImg();
		corr.free();
		return pfinal;
	}

	 PointsSet PointsSet::findBestCorrel_DICGPU_aniso(PointsSet &pfinal, int correlDomainX, int correlDomainY, int correlDomainZ, double distX, double distY, double distZ, double correlThr)
	{
		Volume imRef = Volume(this->name, Coord(this->StackdimX, this->StackdimY, this->StackdimZ));
		Volume imFinal = Volume(pfinal.getName(), Coord(pfinal.getStackdimX(), pfinal.getStackdimY(), pfinal.getStackdimZ()));
		LINFO << "Initialize GPU";
		Correl3DGPU corr = Correl3DGPU(imRef.getName().c_str(), imFinal.getName().c_str());
		corr.setfromFull(true);
		corr.setFilter(false);
		corr.setNormalize(true);
		corr.setIntersect(false);
		corr.setReducedWindow(false);
		corr.setdisplThr(false, distZ, distX, distY);
		corr.setDomain(correlDomainX);
        corr.setWindow("none");
        //corr.setWindow("hanning");

		corr.setFFT(false);
        corr.setaniso(true);
        corr.setHalfPatternX(correlDomainX);
        corr.setHalfPatternY(correlDomainY);
        corr.setHalfPatternZ(correlDomainZ);
        corr.setDisplacementX(distX);
        corr.setDisplacementY(distY);
        corr.setDisplacementZ(distZ);
        corr.setCorrelThr(correlThr);

        corr.init();

		LINFO << "Correlation parameters:";
        LINFO << corr.optionsToString();
        LINFO << "Setting Ref";
		corr.setFullRef(imRef.getName().c_str());
		LINFO << "Setting Target";
		corr.setFullTarget(imFinal.getName().c_str());
		clock_t startTime = clock();
		clock_t currentTime = clock();
		int globalTime = diffclock(currentTime,startTime);
		int deltaTime = diffclock(clock(),currentTime);
		int i=0;
		LINFO << "Ready to start correlations";
		list<Point*>::iterator pitF=(pfinal).getPoints()->begin();
        for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit, ++i)
		{
          	if(i%100 == 0)
			{
				deltaTime = diffclock(clock(),currentTime);
				currentTime = clock();
				globalTime = diffclock(currentTime,startTime);
				LINFO << "Proceeded 100 points in " << deltaTime << "ms ; " << i <<"/" << this->points->size() <<" in " << globalTime/1000 << "s";
			}
            corr.setImRef((int)(*pit)->getCoord().getZ(), (int)(*pit)->getCoord().getY(), (int)(*pit)->getCoord().getX());
            Point potential = (*pit)->findBestCorrel_DICGPU(corr);
			(*pitF)->PointUpdate(potential);
	        ++pitF;
	    }
		pfinal.countValid(correlThr);
        corr.freeFullImg();
		corr.free();
		return pfinal;
	}

	 PointsSet PointsSet::findBestCorrel_DICGPU_texture(PointsSet &pfinal, int correlDomainX, int correlDomainY, int correlDomainZ, double distX, double distY, double distZ, double correlThr)
	{
		Volume imRef = Volume(this->name, Coord(this->StackdimX, this->StackdimY, this->StackdimZ));
		Volume imFinal = Volume(pfinal.getName(), Coord(pfinal.getStackdimX(), pfinal.getStackdimY(), pfinal.getStackdimZ()));
		LINFO << "Initialize GPU";
		Correl3DGPU corr = Correl3DGPU(imRef.getName().c_str(), imFinal.getName().c_str());
		corr.setfromFull(true);
		corr.setusetex(true);
		corr.setFilter(false);
		corr.setNormalize(true);
		corr.setIntersect(false);
		corr.setReducedWindow(false);
		corr.setdisplThr(false, distZ, distX, distY);
		corr.setDomain(correlDomainX);
        corr.setWindow("none");
        //corr.setWindow("hanning");

		corr.setFFT(false);
        corr.setaniso(true);
        corr.setHalfPatternX(correlDomainX);
        corr.setHalfPatternY(correlDomainY);
        corr.setHalfPatternZ(correlDomainZ);
        corr.setDisplacementX(distX);
        corr.setDisplacementY(distY);
        corr.setDisplacementZ(distZ);
        corr.setCorrelThr(correlThr);

        corr.init();

		LINFO << "Correlation parameters:";
        LINFO << corr.optionsToString();
        LINFO << "Setting Ref";
		corr.setFullRef(imRef.getName().c_str());
		LINFO << "Setting Target";
		corr.setFullTarget(imFinal.getName().c_str());
		clock_t startTime = clock();
		clock_t currentTime = clock();
		int globalTime = diffclock(currentTime,startTime);
		int deltaTime = diffclock(clock(),currentTime);
		int i=0;
		LINFO << "Ready to start correlations";
		list<Point*>::iterator pitF=(pfinal).getPoints()->begin();
        for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit, ++i)
		{
          	if(i%100 == 0)
			{
				deltaTime = diffclock(clock(),currentTime);
				currentTime = clock();
				globalTime = diffclock(currentTime,startTime);
				LINFO << "Proceeded 100 points in " << deltaTime << "ms ; " << i <<"/" << this->points->size() <<" in " << globalTime/1000 << "s";
			}
            corr.setRef((*pit)->getCoord().getZ(), (*pit)->getCoord().getY(), (*pit)->getCoord().getX());
            Point potential = (*pit)->findBestCorrel_DICGPU(corr);
			(*pitF)->PointUpdate(potential);
	        ++pitF;
	    }
		pfinal.countValid(correlThr);
        corr.freeFullImg();
		corr.free();
		return pfinal;
	}


	 PointsSet PointsSet::findBestCorrel_DICGPU(PointsSet &pfinal, int correlDomain, double distXY, double distZ, double correlThr, vector<vector<double> > depl)
	{
		Volume imRef = Volume(this->name, Coord(this->StackdimX, this->StackdimY, this->StackdimZ));
		Volume imFinal = Volume(pfinal.getName(), Coord(pfinal.getStackdimX(), pfinal.getStackdimY(), pfinal.getStackdimZ()));
		LINFO << "Initialize GPU";
		Correl3DGPU corr = Correl3DGPU(imRef.getName().c_str(), imFinal.getName().c_str());
		corr.setfromFull(true);
		corr.setFilter(false);
		corr.setNormalize(true);
		corr.setIntersect(false);
		corr.setReducedWindow(false);
		corr.setdisplThr(false, distZ, distXY, distXY);
		corr.setDomain(correlDomain);
        corr.setWindow("none");
        //corr.setWindow("hanning");

		corr.setFFT(false);
        corr.setHalfPattern(correlDomain);
        corr.setDisplacement(distXY);
        corr.setCorrelThr(correlThr);

        corr.init();

		LINFO << "Correlation parameters:";
        LINFO << corr.optionsToString();
        LINFO << "Setting Ref";
		corr.setFullRef(imRef.getName().c_str());
		LINFO << "Setting Target";
		corr.setFullTarget(imFinal.getName().c_str());
		clock_t startTime = clock();
		clock_t currentTime = clock();
		int globalTime = diffclock(currentTime,startTime);
		int deltaTime = diffclock(clock(),currentTime);
		int i=0;
		LINFO << "Ready to start correlations";
		list<Point*>::iterator pitF=(pfinal).getPoints()->begin();
        for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit, ++i)
		{
          	if(i%100 == 0)
			{
				deltaTime = diffclock(clock(),currentTime);
				currentTime = clock();
				globalTime = diffclock(currentTime,startTime);
				LINFO << "Proceeded 100 points in " << deltaTime << "ms ; " << i <<"/" << this->points->size() <<" in " << globalTime/1000 << "s";
			}
            corr.setImRef((int)(*pit)->getCoord().getZ(), (int)(*pit)->getCoord().getY(), (int)(*pit)->getCoord().getX());
            Point potential = (*pit)->findBestCorrel_DICGPU(corr, (*pit)->getCoord().shift(depl));
			(*pitF)->PointUpdate(potential);
	        ++pitF;
	    }
		pfinal.countValid(correlThr);
        corr.freeFullImg();
		corr.free();
		return pfinal;
	}



	 PointsSet PointsSet::optimizeCorrel_DICGPU(PointsSet &pfinal, int correlDomain, int distThr, double correlThr, int distLocal)
	{
		Volume imRef = Volume(this->name, Coord(this->StackdimX, this->StackdimY, this->StackdimZ));
		Volume imFinal = Volume(pfinal.getName(), Coord(pfinal.getStackdimX(), pfinal.getStackdimY(), pfinal.getStackdimZ()));
		LINFO << "Initialize GPU";
		Correl3DGPU corr = Correl3DGPU(imRef.getName().c_str(), imFinal.getName().c_str());
		corr.setfromFull(true);
		corr.setFilter(false);
		corr.setNormalize(true);
		corr.setIntersect(false);
		corr.setReducedWindow(false);
		corr.setdisplThr(false, distThr, distThr, distThr);
		corr.setDomain(correlDomain);
        corr.setWindow("none");
        //corr.setWindow("hanning");

		corr.setFFT(false);
        corr.setHalfPattern(correlDomain);
        corr.setDisplacement(distThr);
        corr.setCorrelThr(correlThr);

        corr.init();

		LINFO << "Correlation parameters:";
        LINFO << corr.optionsToString();
        LINFO << "Setting Ref";
		corr.setFullRef(imRef.getName().c_str());
		LINFO << "Setting Target";
		corr.setFullTarget(imFinal.getName().c_str());
		clock_t startTime = clock();
		clock_t currentTime = clock();
		int globalTime = diffclock(currentTime,startTime);
		int deltaTime = diffclock(clock(),currentTime);
		int i=0;
		LINFO << "Ready to start correlations";
		list<Point*>::iterator pitF=(pfinal).getIteratorBegin();
        for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit, ++i)
		{
			//if((*pitF)->getCorrel()<correlThr)
            //{
	            //LINFO << "Point " << (i+1);
                corr.setImRef((int)(*pit)->getCoord().getZ(), (int)(*pit)->getCoord().getY(), (int)(*pit)->getCoord().getX());
                Coord target = this->estimatePositionEq((*pit)->getCoord(), pfinal, correlThr, 1.*distLocal);
                Coord targetInt = target.getInt();
                //cout << "x=" << (*pit)->getCoord().getX() << " y=" << (*pit)->getCoord().getY() <<" z=" << (*pit)->getCoord().getZ() << endl;
                //cout << "x=" << target.getX() << " y= " << target.getY() <<" z=" << target.getZ() << endl;
                Point potential = (*pit)->findBestCorrel_DICGPU(corr, targetInt);
			    (*pitF)->PointUpdate(potential);
	        //}
            ++pitF;
	    }
		corr.freeFullImg();
		corr.free();
		return pfinal;
	}


	 PointsSet PointsSet::optimizeCorrel_DICGPU_aniso(PointsSet &pfinal, int correlDomainX, int correlDomainY, int correlDomainZ, int distThrX, int distThrY, int distThrZ, double correlThr, int distLocal)
	{
		Volume imRef = Volume(this->name, Coord(this->StackdimX, this->StackdimY, this->StackdimZ));
		Volume imFinal = Volume(pfinal.getName(), Coord(pfinal.getStackdimX(), pfinal.getStackdimY(), pfinal.getStackdimZ()));
		LINFO << "Initialize GPU";
		Correl3DGPU corr = Correl3DGPU(imRef.getName().c_str(), imFinal.getName().c_str());
		corr.setfromFull(true);
		corr.setFilter(false);
		corr.setNormalize(true);
		corr.setIntersect(false);
		corr.setReducedWindow(false);
		corr.setdisplThr(false, distThrZ, distThrX, distThrY);
		corr.setDomain(correlDomainX);
        corr.setWindow("none");
        //corr.setWindow("hanning");

		corr.setFFT(false);
        corr.setaniso(true);
        corr.setHalfPatternX(correlDomainX);
        corr.setHalfPatternY(correlDomainY);
        corr.setHalfPatternZ(correlDomainZ);
        corr.setDisplacementX(distThrX);
        corr.setDisplacementY(distThrY);
        corr.setDisplacementZ(distThrZ);
        corr.setCorrelThr(correlThr);

        corr.init();

		LINFO << "Correlation parameters:";
        LINFO << corr.optionsToString();
        LINFO << "Setting Ref";
		corr.setFullRef(imRef.getName().c_str());
		LINFO << "Setting Target";
		corr.setFullTarget(imFinal.getName().c_str());
		clock_t startTime = clock();
		clock_t currentTime = clock();
		int globalTime = diffclock(currentTime,startTime);
		int deltaTime = diffclock(clock(),currentTime);
		int i=0;
		LINFO << "Ready to start correlations";
		list<Point*>::iterator pitF=(pfinal).getIteratorBegin();
        for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit, ++i)
		{
			//if((*pitF)->getCorrel()<correlThr)
            //{
	            //LINFO << "Point " << (i+1);
                corr.setImRef((int)(*pit)->getCoord().getZ(), (int)(*pit)->getCoord().getY(), (int)(*pit)->getCoord().getX());
                Coord target = this->estimatePositionEq((*pit)->getCoord(), pfinal, correlThr, 1.*distLocal);
                Coord targetInt = target.getInt();
                //cout << "x=" << (*pit)->getCoord().getX() << " y=" << (*pit)->getCoord().getY() <<" z=" << (*pit)->getCoord().getZ() << endl;
                //cout << "x=" << target.getX() << " y= " << target.getY() <<" z=" << target.getZ() << endl;
                Point potential = (*pit)->findBestCorrel_DICGPU(corr, targetInt);
			    (*pitF)->PointUpdate(potential);
	        //}
            ++pitF;
	    }
		corr.freeFullImg();
		corr.free();
		return pfinal;
	}

	 PointsSet PointsSet::optimizeCorrel_DICGPU_texture(PointsSet &pfinal, int correlDomainX, int correlDomainY, int correlDomainZ, int distThrX, int distThrY, int distThrZ, double correlThr, int distLocal)
	{
		Volume imRef = Volume(this->name, Coord(this->StackdimX, this->StackdimY, this->StackdimZ));
		Volume imFinal = Volume(pfinal.getName(), Coord(pfinal.getStackdimX(), pfinal.getStackdimY(), pfinal.getStackdimZ()));
		LINFO << "Initialize GPU";
		Correl3DGPU corr = Correl3DGPU(imRef.getName().c_str(), imFinal.getName().c_str());
		corr.setusetex(true);
		corr.setfromFull(true);
		corr.setFilter(false);
		corr.setNormalize(true);
		corr.setIntersect(false);
		corr.setReducedWindow(false);
		corr.setdisplThr(false, distThrZ, distThrX, distThrY);
		corr.setDomain(correlDomainX);
        corr.setWindow("none");
        //corr.setWindow("hanning");

		corr.setFFT(false);
        corr.setaniso(true);
        corr.setHalfPatternX(correlDomainX);
        corr.setHalfPatternY(correlDomainY);
        corr.setHalfPatternZ(correlDomainZ);
        corr.setDisplacementX(distThrX);
        corr.setDisplacementY(distThrY);
        corr.setDisplacementZ(distThrZ);
        corr.setCorrelThr(correlThr);

        corr.init();

		LINFO << "Correlation parameters:";
        LINFO << corr.optionsToString();
        LINFO << "Setting Ref";
		corr.setFullRef(imRef.getName().c_str());
		LINFO << "Setting Target";
		corr.setFullTarget(imFinal.getName().c_str());
		clock_t startTime = clock();
		clock_t currentTime = clock();
		int globalTime = diffclock(currentTime,startTime);
		int deltaTime = diffclock(clock(),currentTime);
		int i=0;
		LINFO << "Ready to start correlations";
		list<Point*>::iterator pitF=(pfinal).getIteratorBegin();
        for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit, ++i)
		{
			//if((*pitF)->getCorrel()<correlThr)
            //{
	            //LINFO << "Point " << (i+1);
                corr.setRef((*pit)->getCoord().getZ(), (*pit)->getCoord().getY(), (*pit)->getCoord().getX());
                Coord target = this->estimatePositionEq((*pit)->getCoord(), pfinal, correlThr, 1.*distLocal);
                Point potential = (*pit)->optimizeCorrel_DICGPU(corr, target);
			    (*pitF)->PointUpdate(potential);
	        //}
            ++pitF;
	    }
		corr.freeFullImg();
		corr.free();
		return pfinal;
	}


	 int PointsSet::getPos(const Point& part)
	{
		 int i=0;
		 for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit, ++i)
		{
			if((*pit)->equalColor(part))
				return i;
		}
		return -1;
	}

    Coord PointsSet::estimateDepl(const Coord& ca, const PointsSet& pfinal, double correlThr, double dist)
    {
        PointsSet initsubsetdist = this->subSet(ca, dist);
        PointsSet finalsubsetdist = pfinal.subSet(initsubsetdist);
        PointsSet finalsubset = finalsubsetdist.subSetCorrel(correlThr);
        PointsSet subset = this->subSet(finalsubset);
        Coord target = Coord(0,0,0);
        double norm = 0;
        int points=0;
        list<Point*>::iterator pitF=finalsubset.getIteratorBegin();
        for (list<Point*>::iterator pit=subset.getIteratorBegin(); pit!=subset.getIteratorEnd(); ++pit)
		{
            points++;
			double dista = ca.dist((*pit)->getCoord());
            Coord ref = (*pit)->getCoord().mult(-1.);
            Coord final = (*pitF)->getCoord();
            Coord depl = final.shift(ref);
            double coeff =exp(-4*dista*dista/(dist*dist)); 
            Coord add = depl.mult(coeff);
            norm += coeff;
            target = target.shift(add);
	        ++pitF;
        } 
        if(points>0)
        {
            target = target.mult(1./norm);
        }
        else
        {
            target = this->estimateDepl(ca, pfinal, correlThr, 2*dist);
        }
        //cout << "result " << "dx=" << target.getX() << " dy=" << target.getY() << " dz=" << target.getZ() << endl;
        return target;
   }

    Coord PointsSet::estimateDepl(const Coord& ca, const PointsSet& pfinal, double correlThr, double thrdist, double interactdist)
    {
        PointsSet initsubsetdist = this->subSet(ca, thrdist);
        PointsSet finalsubsetdist = pfinal.subSet(initsubsetdist);
        PointsSet finalsubset = finalsubsetdist.subSetCorrel(correlThr);
        PointsSet subset = this->subSet(finalsubset);
        Coord target = Coord(0,0,0);
        double norm = 0;
        int points=0;
        list<Point*>::iterator pitF=finalsubset.getIteratorBegin();
        for (list<Point*>::iterator pit=subset.getIteratorBegin(); pit!=subset.getIteratorEnd(); ++pit)
		{
            points++;
			double dista = ca.dist((*pit)->getCoord());
            Coord ref = (*pit)->getCoord().mult(-1.);
            Coord final = (*pitF)->getCoord();
            Coord depl = final.shift(ref);
            double coeff =exp(-dista*dista/(interactdist*interactdist)); 
            Coord add = depl.mult(coeff);
            norm += coeff;
            target = target.shift(add);
	        ++pitF;
        } 
        if(points>0)
        {
            target = target.mult(1./norm);
        }
        else
        {
            target = this->estimateDepl(ca, pfinal, correlThr, 2*thrdist, interactdist);
        }
        //cout << "result " << "dx=" << target.getX() << " dy=" << target.getY() << " dz=" << target.getZ() << endl;
        return target;
   }

    Coord PointsSet::estimateLargeDepl(const Coord& ca, const PointsSet& pfinal, double correlThr, double thrdist, double interactdist)
    {
        PointsSet finalsubsetdist = pfinal.subSet(ca, thrdist);
        PointsSet finalsubset = finalsubsetdist.subSetCorrel(correlThr);
        PointsSet subset = this->subSet(finalsubset);
        Coord target = Coord(0,0,0);
        double norm = 0;
        int points=0;
        list<Point*>::iterator pit=subset.getIteratorBegin();
        for (list<Point*>::iterator pitF=finalsubset.getIteratorBegin(); pitF!=finalsubset.getIteratorEnd(); ++pitF)
		{
            points++;
			double dista = ca.dist((*pitF)->getCoord());
            Coord final = (*pitF)->getCoord();
            Coord ref = (*pit)->getCoord().mult(-1);
            Coord depl = final.shift(ref);
            double coeff =exp(-dista*dista/(interactdist*interactdist)); 
            Coord add = depl.mult(coeff);
            norm += coeff;
            target = target.shift(add);
	        ++pit;
        } 
        if(points>0)
        {
            target = target.mult(1./norm);
        }
        else
        {
            target = this->estimateLargeDepl(ca, pfinal, correlThr, 2*thrdist, interactdist);
        }
        //cout << "result " << "dx=" << target.getX() << " dy=" << target.getY() << " dz=" << target.getZ() << endl;
        return target;
   }

   
   
     Coord PointsSet::estimatePosition(const Coord& ca, const PointsSet& pfinal, double correlThr, double dist)
    {
	    //cout << "Estimate position" << endl;
	    //cout << "Search distance subset" << endl;
        PointsSet initsubsetdist = this->subSet(ca, dist);
	    //cout << "size=" << initsubsetdist.Size() << endl;
        //cout << "Search final subset dist" << endl;
        PointsSet finalsubsetdist = pfinal.subSet(initsubsetdist);
        //cout << "Done" << endl;
	    //cout << "size=" << finalsubsetdist.Size() << endl;
	    //cout << "Search correl subset" << endl;
        PointsSet finalsubset = finalsubsetdist.subSetCorrel(correlThr);
	    //cout << "size=" << finalsubset.Size() << endl;
        //cout << "Search init subset correl" << endl;
        PointsSet subset = this->subSet(finalsubset);
	    //cout << "size=" << subset.Size() << endl;
        Coord target = Coord(0,0,0);
        double norm = 0;
        int points=0;
        list<Point*>::iterator pitF=finalsubset.getIteratorBegin();
        for (list<Point*>::iterator pit=subset.getIteratorBegin(); pit!=subset.getIteratorEnd(); ++pit)
		{
            points++;
	        //cout << "Dist" << endl;
			double dista = ca.dist((*pit)->getCoord());
	        //cout << "Dist = " << dista << endl;
            Coord ref = (*pit)->getCoord().mult(-1.);
            Coord final = (*pitF)->getCoord();
            Coord depl = final.shift(ref);
            Coord add = depl.mult(1./dista);
            norm += 1./dista;
            target = target.shift(add);
            //cout << "pt " << (*pit)->getColor() << " dx=" << depl.getX() << " dy=" << depl.getY() << " dz=" << depl.getZ() << endl;
	        ++pitF;
        } 
        if(points>0)
        {
            target = target.mult(1./norm);
            target = ca.shift(target);
        }
        else
        {
            target = this->estimatePosition(ca, pfinal, correlThr, 2*dist);
        }
        //cout << "result " << "dx=" << target.getX() << " dy=" << target.getY() << " dz=" << target.getZ() << endl;
        return target;
   }

     Coord PointsSet::estimatePositionGauss(const Coord& ca, const PointsSet& pfinal, double correlThr, double distThr, double interactdist, bool useTotalCorrel)
    {
	    //cout << "Estimate position" << endl;
	    //cout << "Search distance subset" << endl;
        PointsSet initsubsetdist = this->subSet(ca, distThr);
	    //cout << "size=" << initsubsetdist.Size() << endl;
        //cout << "Search final subset dist" << endl;
        PointsSet finalsubsetdist = pfinal.subSet(initsubsetdist);
        //cout << "Done" << endl;
	    //cout << "size=" << finalsubsetdist.Size() << endl;
	    //cout << "Search correl subset" << endl;
        PointsSet finalsubset;

        if(useTotalCorrel)
            finalsubset = finalsubsetdist.subSetTotalCorrel(correlThr);
	    else
            finalsubset = finalsubsetdist.subSetCorrel(correlThr);
            
        //cout << "size=" << finalsubset.Size() << endl;
        //cout << "Search init subset correl" << endl;
        PointsSet subset = this->subSet(finalsubset);
	    //cout << "size=" << subset.Size() << endl;
        Coord target = Coord(0,0,0);
        double norm = 0;
        int points=0;
        list<Point*>::iterator pitF=finalsubset.getIteratorBegin();
        for (list<Point*>::iterator pit=subset.getIteratorBegin(); pit!=subset.getIteratorEnd(); ++pit)
		{
            points++;
	        //cout << "Dist" << endl;
			double dista = ca.dist((*pit)->getCoord());
	        //cout << "Dist = " << dista << endl;
            Coord ref = (*pit)->getCoord().mult(-1.);
            Coord final = (*pitF)->getCoord();
            Coord depl = final.shift(ref);
            double coeff =exp(-dista*dista/(interactdist*interactdist)); 
            Coord add = depl.mult(coeff);
            norm += coeff;
            target = target.shift(add);
            //cout << "pt " << (*pit)->getColor() << " dx=" << depl.getX() << " dy=" << depl.getY() << " dz=" << depl.getZ() << endl;
	        ++pitF;
        } 
        if(points>0)
        {
            target = target.mult(1./norm);
            target = ca.shift(target);
        }
        else
        {
            target = this->estimatePositionGauss(ca, pfinal, correlThr, 2*distThr, interactdist, useTotalCorrel);
        }
        //cout << "result " << "dx=" << target.getX() << " dy=" << target.getY() << " dz=" << target.getZ() << endl;
        return target;
   }
     
    Coord PointsSet::estimatePositionEq(const Coord& ca, const PointsSet& pfinal, double correlThr, double dist)
    {
	    //cout << "Estimate position" << endl;
	    //cout << "Search distance subset" << endl;
        PointsSet initsubsetdist = this->subSet(ca, dist);
	    //cout << "size=" << initsubsetdist.Size() << endl;
        //cout << "Search final subset dist" << endl;
        PointsSet finalsubsetdist = pfinal.subSet(initsubsetdist);
        //cout << "Done" << endl;
	    //cout << "size=" << finalsubsetdist.Size() << endl;
	    //cout << "Search correl subset" << endl;
        PointsSet finalsubset = finalsubsetdist.subSetCorrel(correlThr);
	    //cout << "size=" << finalsubset.Size() << endl;
        //cout << "Search init subset correl" << endl;
        PointsSet subset = this->subSet(finalsubset);
	    //cout << "size=" << subset.Size() << endl;
        Coord target = Coord(0,0,0);
        double norm = 0;
        int points=0;
        list<Point*>::iterator pitF=finalsubset.getIteratorBegin();
        for (list<Point*>::iterator pit=subset.getIteratorBegin(); pit!=subset.getIteratorEnd(); ++pit)
		{
            points++;
	        //cout << "Dist" << endl;
			double dista = ca.dist((*pit)->getCoord());
	        //cout << "Dist = " << dista << endl;
            Coord ref = (*pit)->getCoord().mult(-1.);
            Coord final = (*pitF)->getCoord();
            Coord depl = final.shift(ref);
            Coord add = depl.mult(1.);
            norm += 1.;
            target = target.shift(add);
            //cout << "pt " << (*pit)->getColor() << " dx=" << depl.getX() << " dy=" << depl.getY() << " dz=" << depl.getZ() << endl;
	        ++pitF;
        } 
        if(points>0)
        {
            target = target.mult(1./norm);
            target = ca.shift(target);
        }
        else
        {
            target = this->estimatePositionEq(ca, pfinal, correlThr, 2*dist);
        }
        //cout << "result " << "dx=" << target.getX() << " dy=" << target.getY() << " dz=" << target.getZ() << endl;
        return target;
   }


	 vector<PointsSet> PointsSet::findCorrel(PointsSet pfinal, double refSlices[], double refSlicesFinal[])
	{
		vector<PointsSet> potentialCorrel = vector<PointsSet>();
		for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
		{

			double dZ=1;
			double dXY=1;
			int subsetsize=0;
			PointsSet subset;
			while(subsetsize==0)
			{
				subset = pfinal.subSet((*pit)->getCoord().Interpolate(refSlicesFinal, refSlices), dXY, dZ);
				subsetsize = subset.Size();
			}
			PointsSet partS = PointsSet();
			partS.addPoint(*pit);
			partS.addPoint(*subset.points->begin());
			potentialCorrel.push_back(partS);
		}
		return potentialCorrel;
	}
	
	 Point* PointsSet::findClosest(const Coord& XYZ)
	{
		if(this->Size()>0)
		{
			double bestDist=1000000000000;
			list<Point*>::iterator best = this->points->begin();
			for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
			{
				double dist = (*pit)->distance(XYZ);
				if(dist<bestDist)
				{
					best=pit;
					bestDist=dist;
				}
			}
			return *best;
		}
		else
		{

			Point* emptyPart(0);
			return emptyPart;
		}
	}
	
	 Point* PointsSet::findPoint(int color)
	{
		for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
		{
			if((*pit)->getColor()==color)
				return *pit;
		}
		Point* emptyPart(0);
		return emptyPart;
	}
	
	 Point* PointsSet::findPoint(int color) const
	{
		for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
		{
			if((*pit)->getColor()==color)
				return *pit;
		}
		Point* emptyPart(0);
		return emptyPart;
	}

	 string PointsSet::fullInfoTostring()
	{
		ostringstream colorLine;
		string header = "";
		for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
		{
				//if(header=="")
				//{
				//	header = (*pit)->writeHeader();
				//	colorLine << header << " ";
				//}
				colorLine << (*pit)->tostring() << " ";
		}
		return colorLine.str();
	}
	
	
	 string PointsSet::colorTostring()
	{
		 ostringstream colorLine;
		
		for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
		{
				if(*pit)
                    colorLine << (*pit)->getColor() << " ";
				else
					colorLine << -1 << " ";
		}
		return colorLine.str();
	}
	
	 void PointsSet::colorToFile(string fileName)
	{

		 ofstream fichier(fileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier

        if(fichier)  // si l'ouverture a réussi
        {
        	for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
        	{
        		fichier << (*pit)->getColor() << endl;
        	}

                fichier.close();  // on referme le fichier
        }
        else  // sinon
                LERROR << "Erreur à l'ouverture !";
		
	}
	
	 void PointsSet::writeToFile(const string fileDirectory, const string pointsSetName) const
	{
		    ostringstream fileName;
			fileName << fileDirectory << "/" << pointsSetName;
			 ofstream fichier(fileName.str().c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier

	        if(fichier)  // si l'ouverture a réussi
	        {
	        	fichier << this->name << endl;
	        	fichier << this->StackdimX << endl;
	    		fichier << this->StackdimY << endl;
	    		fichier << this->StackdimZ << endl;
	        	for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
	        	{
	        		if(*pit){
	        			fichier << (*pit)->getScan() << " " << (*pit)->getColor() << endl;
	        			(*pit)->writeToFile(fileDirectory);
	        		}
	        	}
	            fichier.close();  // on referme le fichier
	        }
	        else  // sinon
	                LERROR << "Erreur à l'ouverture !";

	}

	 void PointsSet::writeToFile(const string fileName)
		{

			 ofstream fichier(fileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier

	        if(fichier)  // si l'ouverture a réussi
	        {
	        	for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
	        	{
	        		fichier << (*pit)->tostring() << endl;

	        	}
	            fichier.close();  // on referme le fichier
	        }
	        else  // sinon
	                LERROR << "Erreur à l'ouverture !";

		}

	 void PointsSet::writeToCorrelManuPtsFile(const string fileName)
		{

			 ofstream fichier(fileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier

	        if(fichier)  // si l'ouverture a réussi
	        {
                fichier << this->Size() << " " << this->Size() << " " << 1 << " " << 1 << endl;
	        	for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
	        	{
	        		fichier << (*pit)->getColor() << " ";
                    fichier << 1 << " ";
                    fichier << (*pit)->getCoord().getX() << " ";
                    fichier << (*pit)->getCoord().getY()*(-1) << " ";
                    fichier << (*pit)->getCoord().getZ()*(-1) << endl;
	        	}
	            fichier.close();  // on referme le fichier
	        }
	        else  // sinon
	                LERROR << "Erreur à l'ouverture !";

		}

	 void PointsSet::writeToPtsFile(const string fileName)
		{

			 ofstream fichier(fileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier

	        if(fichier)  // si l'ouverture a réussi
	        {
                fichier << this->Size() << " " << this->Size() << " " << 1 << " " << 1 << endl;
	        	for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
	        	{
	        		fichier << (*pit)->getColor() << " ";
                    fichier << (*pit)->getScan() << " ";
                    fichier << (*pit)->getCoord().getX() << " ";
                    fichier << (*pit)->getCoord().getY()*(-1) << " ";
                    fichier << (*pit)->getCoord().getZ()*(-1) << " ";
                    fichier << (*pit)->getCorrel() << endl;
	        	}
	            fichier.close();  // on referme le fichier
	        }
	        else  // sinon
	                LERROR << "Erreur à l'ouverture !";

		}


    void PointsSet::writeVTKDisplacement(const string fileName, const PointsSet reference, const PointsSet lastStep)
    {
			 ofstream fichier(fileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier

	        if(fichier)  // si l'ouverture a réussi
	        {
                fichier << "# vtk DataFile Version 3.1" << endl;
                fichier << "Correl points" << endl;
                fichier << "ASCII" << endl;
                fichier << "DATASET UNSTRUCTURED_GRID" << endl;
                fichier << "POINTS " << this->Size() << " FLOAT" << endl;
	        	for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
	        	{
                    fichier << (*pit)->getCoord().getX() << " ";
                    fichier << (*pit)->getCoord().getY() << " ";
                    fichier << (*pit)->getCoord().getZ() << endl;
	        	}
                fichier << endl;
                fichier << "POINT_DATA " << this->Size() << endl;
                fichier << "SCALARS CorrelFactor FLOAT 1" << endl;
                fichier << "LOOKUP_TABLE default" << endl;
	        	for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
                {
                    fichier << (*pit)->getCorrel() << endl;
                }       
                fichier << "SCALARS TotalCorrelFactor FLOAT 1" << endl;
                fichier << "LOOKUP_TABLE default" << endl;
	        	for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
                {
                    fichier << (*pit)->getTotalCorrel() << endl;
                }//fichier << endl;
                fichier << "VECTORS GlobalDisplacement FLOAT" << endl;
                list<Point*>::iterator pitRef=reference.getIteratorBegin();
            	for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
	        	{
                    fichier << ((*pit)->getCoord().getX() - (*pitRef)->getCoord().getX()) << " ";
                    fichier << ((*pit)->getCoord().getY() - (*pitRef)->getCoord().getY()) << " ";
                    fichier << ((*pit)->getCoord().getZ() - (*pitRef)->getCoord().getZ()) << endl;
                    ++pitRef;
	        	}
                //fichier << endl;
                fichier << "VECTORS IncrementalDisplacement FLOAT" << endl;
                list<Point*>::iterator pitL=lastStep.getIteratorBegin();
            	for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
	        	{
                    fichier << ((*pit)->getCoord().getX() - (*pitL)->getCoord().getX()) << " ";
                    fichier << ((*pit)->getCoord().getY() - (*pitL)->getCoord().getY()) << " ";
                    fichier << ((*pit)->getCoord().getZ() - (*pitL)->getCoord().getZ()) << endl;
                    ++pitL;
	        	}
                fichier << endl;

                fichier.close();  // on referme le fichier
	        }
	        else  // sinon
	                LERROR << "Erreur à l'ouverture !";

	}


	 void PointsSet::write(const string fileName) const
	 {

			 ofstream fichier(fileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier

	        if(fichier)  // si l'ouverture a réussi
	        {
	        	for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
	        	{
	        		fichier << (*pit)->write() << endl;

	        	}
	            fichier.close();  // on referme le fichier
	        }
	        else  // sinon
	                LERROR << "Erreur à l'ouverture !";

	}

	 string PointsSet::correlTostring()
	{
		 ostringstream colorLine;

     	for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
     	{
     		if(*pit)
     						      colorLine << (*pit)->getCorrel() << " ";
     						else
     							  colorLine << 0 << " ";
		}
		return colorLine.str();
	}

int PointsSet::countValid(double thr) const
{
    int valid = 0;
    for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
    {
        if((*pit)&&((*pit)->getCorrel()>thr))
            valid++;
    }
    return valid;
}
		

	 string PointsSet::PosTostring()
	{
		 ostringstream colorLine;

	     	for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
	     	{
	     		if(*pit)
	     			colorLine << (*pit)->getCoord().getX() << " " << (*pit)->getCoord().getY() << " " << (*pit)->getCoord().getZ() <<" ";
	     		else
	     			colorLine << -1 << " " << -1 << " " << -1 << " ";


		}
		return colorLine.str();
	}
	
	
	
void PointsSet::renumber()
{
    int i=1;
    for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
    {
        (*pit)->setColor(i);
        i++;
    }
}



int PointsSet::getStackdimX() const {
	return this->StackdimX;
}

void PointsSet::setStackdimX(int stackdimX) {
	StackdimX = stackdimX;
}

int PointsSet::getStackdimY() const {
	return StackdimY;
}

void PointsSet::setStackdimY(int stackdimY) {
	StackdimY = stackdimY;
}

int PointsSet::getStackdimZ() const {
	return StackdimZ;
}

void PointsSet::setStackdimZ(int stackdimZ) {
	StackdimZ = stackdimZ;
}

void PointsSet::setScan(int scan)
{
    for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
    {
        (*pit)->setScan(scan);
    }
}


PointsSet PointsSet::computeMesh(PointsSet& newMesh, PointsSet& pInit, PointsSet& pFinal, double correlThr, double distThr, double distLocal)
{
    LINFO << "Computing Mesh";
    int i=0;
    list<Point*>::iterator pitNM=(newMesh).getIteratorBegin();

    for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
    {	
        //LINFO << "Point " << i << " (" << (*pit)->getCoord().getX() << "," << (*pit)->getCoord().getY() << "," << (*pit)->getCoord().getZ() << ")"; 
        Coord target = pInit.estimatePositionGauss((*pit)->getCoord(),pFinal, correlThr, distThr, distLocal, false);
        LINFO << "Point " << i << " (" << (*pit)->getCoord().getX() << "," << (*pit)->getCoord().getY() << "," << (*pit)->getCoord().getZ() << ") => (" << target.getX() << "," << target.getY() << "," << target.getZ() << ")";
        i++; 
        (*pitNM)->setCoord(target);
	    ++pitNM;
    }
    return newMesh;
}

void PointsSet::computeSafeMesh(PointsSet& newMesh, PointsSet& pInit, PointsSet& pFinal, double correlThr, double distThr, double distLocal)
{
    LINFO << "Computing Mesh";
    int i=0;
    list<Point*>::iterator pitNM=(newMesh).getIteratorBegin();

    for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
    {	
        //LINFO << "Point " << i << " (" << (*pit)->getCoord().getX() << "," << (*pit)->getCoord().getY() << "," << (*pit)->getCoord().getZ() << ")"; 
        Coord target = pInit.estimatePositionGauss((*pit)->getCoord(),pFinal, correlThr, distThr, distLocal, true);
        LINFO << "Point " << i << " (" << (*pit)->getCoord().getX() << "," << (*pit)->getCoord().getY() << "," << (*pit)->getCoord().getZ() << ") => (" << target.getX() << "," << target.getY() << "," << target.getZ() << ")";
        i++; 
        (*pitNM)->setCoord(target);
	    ++pitNM;
    }
}



    void PointsSet::writeVTKMesh(const string fileName, const PointsSet reference, const PointsSet lastStep)
    {
			 ofstream fichier(fileName.c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier

	        if(fichier)  // si l'ouverture a réussi
	        {
                fichier << "# vtk DataFile Version 3.1" << endl;
                fichier << "Mesh points" << endl;
                fichier << "ASCII" << endl;
                fichier << "DATASET STRUCTURED_GRID" << endl;
                fichier << "DIMENSIONS " << this->StackdimX << " " << this->StackdimY << " " << this->StackdimZ << endl;
                fichier << "POINTS " << this->Size() << " FLOAT" << endl;
	        	for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
	        	{
                    fichier << (*pit)->getCoord().getX() << " ";
                    fichier << (*pit)->getCoord().getY() << " ";
                    fichier << (*pit)->getCoord().getZ() << endl;
	        	}
                fichier << endl;
                fichier << "POINT_DATA " << this->Size() << endl;
                //fichier << "SCALARS CorrelFactor FLOAT 1" << endl;
                //fichier << "LOOKUP_TABLE default" << endl;
	        	//for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
                //{
                //    fichier << (*pit)->getCorrel() << endl;
                //}       
                //fichier << endl;
                fichier << "VECTORS GlobalDisplacement FLOAT" << endl;
                list<Point*>::iterator pitRef=reference.getIteratorBegin();
            	for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
	        	{
                    fichier << ((*pit)->getCoord().getX() - (*pitRef)->getCoord().getX()) << " ";
                    fichier << ((*pit)->getCoord().getY() - (*pitRef)->getCoord().getY()) << " ";
                    fichier << ((*pit)->getCoord().getZ() - (*pitRef)->getCoord().getZ()) << endl;
                    ++pitRef;
	        	}
                //fichier << endl;
                fichier << "VECTORS IncrementalDisplacement FLOAT" << endl;
                list<Point*>::iterator pitL=lastStep.getIteratorBegin();
            	for (list<Point*>::iterator pit=this->points->begin(); pit!=this->points->end(); ++pit)
	        	{
                    fichier << ((*pit)->getCoord().getX() - (*pitL)->getCoord().getX()) << " ";
                    fichier << ((*pit)->getCoord().getY() - (*pitL)->getCoord().getY()) << " ";
                    fichier << ((*pit)->getCoord().getZ() - (*pitL)->getCoord().getZ()) << endl;
                    ++pitL;
	        	}
                fichier << endl;

                fichier.close();  // on referme le fichier
	        }
	        else  // sinon
	                LERROR << "Erreur à l'ouverture !";

	}

	void PointsSet::totalCorrel(std::vector<PointsSet> VpS)
    {
        for (list<Point*>::iterator pit=VpS[0].getIteratorBegin(); pit!=VpS[0].getIteratorEnd(); ++pit)
        {
            (*pit)->setTotalCorrel((*pit)->getCorrel());
        }        
        for(int i=1; i<VpS.size(); i++)
        {
		    list<Point*>::iterator pitP=VpS[i-1].getIteratorBegin();
            for (list<Point*>::iterator pit=VpS[i].getIteratorBegin(); pit!=VpS[i].getIteratorEnd(); ++pit)
            {
                double oldC = (*pitP)->getTotalCorrel();
                double newC = (*pit)->getCorrel();
                if(oldC<newC)
                    newC=oldC;
                (*pit)->setTotalCorrel(newC);
                ++pitP;
            }
        }
    }
