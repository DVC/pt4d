
#include "Point.h"
#include "PSet.h"

using namespace std;
	Point::Point() : P()
	{

	}

	Point::~Point()
	{

	}

	Point::Point(int scan, int color, Coord coord, double correl) : P(scan, color, coord, correl)
	{

	}
	
	Point::Point(int scan, int color, Coord coord) : P(scan, color, coord)
	{
	}

	Point::Point(string linestr){
		 double a,b,c;
		 string paramline;
		 stringstream sstr;
		 sstr.str(linestr);
		 if(sstr.str().size()>1)
		 {
		     sstr >> a;
		     if(!isnan(1.*a))
		     {
		    	 	this->scan = (int)(1.*a);
		    	 	sstr >> a;
		    	    this->color = (int)(1.*a);
		     		sstr >> a;
		     		sstr >> b;
		     		sstr >> c;
		     		this->coord = Coord((double)(1.*a),(double)(1.*b),(double)(1.*c));
		     		sstr >> a;
		     		this->correl = 0;
                    this->totalcorrel = 0;
		     	 	this->roisize = 64;
		     }
	  	}
	 }


	 Point::Point(const string fileDirectory, int scan, int color) {
		 this->scan = scan;
		 this->color = color;
		this->coord = Coord();
		this->correl = 0;
        this->totalcorrel = 0;
 		this->roisize = 64;
		 ostringstream fileName;
		 char buff [200];
		 sprintf(buff,"/Point_%04d_%05d",scan,color);
		 fileName << fileDirectory << buff;

		 ifstream File (fileName.str().c_str(),ios::in);
		    if (!File)
		    {
		    	LERROR << "Error : impossible to open file " << fileName << " in read only mode...";
		    	LERROR << "Point color is " << color << " from scan " << scan;
		    }
		    else
		    {
			LINFO << "File of Point opened : color is " << color << " from scan " << scan;
		    }
		    double a,b,c;
		    		 string dataline="";
		    		 stringstream sstr;
		 		    char schar[10000];
		 		    File.getline(schar,10000);
		 		    dataline+=schar;
		    		 sstr.str(dataline);
		    		 if(sstr.str().size()>1)
		    		 {
		    		     sstr >> a;
		    		     if(!isnan(1.*a))
		    		     {
		    		    	 	this->scan = (int)(1.*a);
		    		    	 	sstr >> a;
		    		    	    this->color = (int)(1.*a);
		    		     		sstr >> a;
		    		     		sstr >> b;
		    		     		sstr >> c;
		    		     		this->coord = Coord((double)(1.*a),(double)(1.*b),(double)(1.*c));
		    		     		sstr >> a;
		    		     		this->correl = (int)(1.*a);
                                this->totalcorrel = (int)(1.*a);
		    		     		sstr >> a;
    	    		     		this->roisize = 64;
	    		     }
		    	  	}
	 	 }


	 Point*  Point::findClosest(PSet<Point>& potential)
	{
		if(potential.Size()>0)
		{
			double bestDist=1000000000000;
			list<Point*>::iterator best = potential.getIteratorBegin();
			for (list<Point*>::iterator pit=potential.getIteratorBegin(); pit!=potential.getIteratorEnd(); ++pit)
			{
				double dist = this->distance(**pit);
				if(dist<bestDist)
				{
					best=pit;
					bestDist=dist;
				}
			}
			return *best;
		}
		else
		{
			Point* emptyPart(0);
            // = new Particle();
			return emptyPart;
		}
	}

	/**

    void Point::PointUpdate(Point point) {
		this->scan = point.getScan();
		this->color = point.getColor();
		this->coord = point.getCoord();
		this->correl = point.getCorrel();
		this->totalcorrel = point.getTotalCorrel();
		this->roisize = 64;
 	}

	 void Point::PointUpdate(const string fileDirectory, int scan, int color) {
		 ostringstream fileName;
		 char buff [200];
		 sprintf(buff,"/Point_%04d_%05d",scan,color);

		 fileName << fileDirectory << buff;

		 ifstream File (fileName.str().c_str(),ios::in);
		    if (!File)
		    {
		    	LERROR << "Error : impossible to open file " << fileName << " in read only mode...";
		    }
		    else
		    {
		    	LINFO << "File of Point opened for update : color is " << color << " from scan " << scan;

		    }

		    string dataline="";
		    stringstream sstr;
		    char schar[10000];
		    File.getline(schar,10000);
		    dataline+=schar;
		    this->PointUpdate(dataline);
	 	 }

	 void Point::PointUpdate(string linestr) {
	 		 double a,b,c;
	 		 string paramline;
	 		 stringstream sstr;
	 		 sstr.str(linestr);
	 		 if(sstr.str().size()>1)
	 		 {
	 		     sstr >> a;
	 		     if(!isnan(1.*a))
	 		     {
	 		    	 	this->scan = (int)(1.*a);
	 		    	 	sstr >> a;
	 		    	    this->color = (int)(1.*a);
	 		     		sstr >> a;
	 		     		sstr >> b;
	 		     		sstr >> c;
	 		     		this->coord = Coord((double)(1.*a),(double)(1.*b),(double)(1.*c));
	 		     		sstr >> a;
	 		     		this->correl = (int)(1.*a);
	 		     		this->totalcorrel = (int)(1.*a);
	 		     	    this->roisize = 64;
	 		     }
	 	  	}
	 	 }

	 string Point::write() const{
		 ostringstream sstr;
		 sstr << this->scan << " ";
		 sstr << this->color << " ";
		 sstr << this->coord.toString() << " ";
		 sstr << this->correl;
		 sstr << this->totalcorrel;
    	 return sstr.str();
	 }

	 string Point::getFileName() const
	 {
		 char buff [200];
		 sprintf(buff,"/Point_%04d_%05d",this->scan,this->color);
	 	 	return (string)buff;
	 }

	 void Point::writeToFile(const string fileDirectory) const
	 {
		 	 	 ostringstream fileName;
		 	 	 char buff[200];
		 	 	 sprintf(buff,"/Point_%04d_%05d",this->scan,this->color);

		 	 	 fileName << fileDirectory << buff;

	 			 ofstream fichier(fileName.str().c_str(), ios::out | ios::trunc);  //déclaration du flux et ouverture du fichier

	 	        if(fichier)  // si l'ouverture a réussi
	 	        {
	 	        	fichier << this->write() << endl;
	 	            fichier.close();  // on referme le fichier
	 	        }
	 	        else  // sinon
	 	                LERROR << "Erreur à l'ouverture !";

	 	}

	 int Point::getScan() const {
		return scan;
	}

	 void Point::setScan(int scan) {
		this->scan = scan;
	}

	 int Point::getColor() const {
		return color;
	}

	 void Point::setColor(int color) {
		this->color = color;
	}

	 Coord Point::getCoord() const {
		return coord;
	}

	 void Point::setCoord(Coord coord) {
		this->coord = coord;
	}

	 double Point::getCorrel() const {
		return this->correl;
	}

	 void Point::setCorrel(double correl) {
		this->correl = correl;
	}

	 double Point::getTotalCorrel() const {
		return this->totalcorrel;
	}

	 void Point::setTotalCorrel(double correl) {
		this->totalcorrel = correl;
    }
		 string Point::tostring() const {
		ostringstream sstr;
		sstr << this->color << " ";
		sstr << this->coord.toString() << " ";
		sstr << this->correl << " ";
		sstr << this->totalcorrel;
		return sstr.str();
	}
	
	 string Point::tostringFull() const {
		ostringstream sstr;
		sstr << this->color << " ";
		sstr << this->scan << " ";
		sstr << this->coord.toString() << " ";
		sstr << this->correl << " ";
		sstr << this->totalcorrel;
		return sstr.str();
	}
	
	 bool Point::equalColor(const Point& part) const {
		return (this->color == part.getColor());
	}

    double  Point::distance(const Point& part) const
	{
		return this->coord.dist(part.getCoord());
	}

	 double  Point::distance(const Coord& XYZ) const
	{
		return this->coord.dist(XYZ);
	}

	

	 Volume Point::getPointSubVolume(Volume& fullIm) const
	{
		fullIm.setRoi(ROI((int)this->getCoord().getX()-roisize/2,(int)this->getCoord().getY()-roisize/2,(int)this->getCoord().getZ()-roisize/2,roisize,roisize,roisize));
		int* new_color = new int[65256];
		for(int j = 0; j<65256; j++)
		{
			new_color[j] = 0;
		}
		new_color[(int)this->getColor()] = 1;

		Volume partRef = fullIm.duplicateRoi();
		partRef.recolor(new_color);
		return partRef;
	}


	 Point Point::findBestCorrel_DICGPU(Correl3DGPU& correlator){ //The very good one for a single solution
		int pt = this->getColor();
        Coord target = this->coord;
		double* value = new double[4];
        
        bool usetex = correlator.getusetex();
        if(usetex)
        {
            correlator.setTarget(target.getZ(), target.getY(), target.getX());
            value = correlator.correltex();
            //value = correlator.correltexscreen();
        }
        else
        {
            correlator.setImTarget((int)target.getZ(), (int)target.getY(), (int)target.getX());
		    value = correlator.correl();
		}
        //cout << "Correl done (" << value[0] << ',' << value[1] << "," << value[2] << ") corr=" << value[3] << endl;
		//Coord c = target.shift(value);
		//cout << "Coord " << target.toString() << endl;
		//cout << "Coord shifted " << c.toString() << endl;
		Point *cible = new Point(this->scan,this->color,target.shift(value), value[3]);
		//cout << "Closest found particle " << cible->getColor() << " coord " << cible->getCoord().toString() <<  endl;

		//printf("Correl : dx = "+ (int)value[0] + ", dy = "+ (int)value[1] + ", dz = " + (int)value[2] + ", corr = " + value[3] + ", time = "+currentTime + "ms");
	//	cout << "Point " << pt << " : x = " << (int)target.getX() << ", y = " << (int)target.getY() << ", z = " << (int)target.getZ() << endl;
		if(value[3]< correlator.getCorrelThr())
            LINFO << "Point " << pt << " : dx = " << (int)value[0] << ", dy = " << (int)value[1] << ", dz = " << (int)value[2] << ", corr = " << value[3] << " not kept";
        else
            LINFO << "Point " << pt << " : dx = " << (int)value[0] << ", dy = " << (int)value[1] << ", dz = " << (int)value[2] << ", corr = " << value[3] << " OK";
        return *cible;
	}


Point Point::findBestCorrel_DICGPU(Correl3DGPU& correlator, Coord target){ //The very good one for a single solution
	if(correlator.validTarget(target)&&(this->getCorrel()!=-1)){
        int pt = this->getColor();
        
        bool usetex = correlator.getusetex();
		double* value = new double[4];

        if(usetex)
        {
            correlator.setTarget(target.getZ(), target.getY(), target.getX());
            value = correlator.correltex();
        }
        else
        {
            correlator.setImTarget((int)target.getZ(), (int)target.getY(), (int)target.getX());
            value = correlator.correl();
		}
        
		//cout << "Correl done (" << value[0] << ',' << value[1] << "," << value[2] << ") corr=" << value[3] << endl;
		//Coord c = target.shift(value);
		//cout << "Coord " << target.toString() << endl;
		//cout << "Coord shifted " << c.toString() << endl;
		Point *cible = new Point(this->scan,this->color,target.shift(value), value[3]);
		//cout << "Closest found particle " << cible->getColor() << " coord " << cible->getCoord().toString() <<  endl;

		//printf("Correl : dx = "+ (int)value[0] + ", dy = "+ (int)value[1] + ", dz = " + (int)value[2] + ", corr = " + value[3] + ", time = "+currentTime + "ms");
	    //	cout << "Point " << pt << " : x = " << (int)target.getX() << ", y = " << (int)target.getY() << ", z = " << (int)target.getZ() << endl;
		Coord depl = cible->getCoord().diff(this->getCoord());

        if(value[3]< correlator.getCorrelThr())
        {
            LINFO << "Point " << pt << " : dx_corr = " << (int)value[0] << ", dy_corr = " << (int)value[1] << ", dz_corr = " << (int)value[2] << ", corr = " << value[3] << " not kept";
            LINFO << "Point " << pt << " : dx_step = " << depl.getX() << ", dy_step = " << depl.getY() << ", dz_step = " << depl.getZ() << ", corr = " << value[3] << " not kept";
        }
        else
        {
            LINFO << "Point " << pt << " : dx_corr = " << (int)value[0] << ", dy_corr = " << (int)value[1] << ", dz_corr = " << (int)value[2] << ", corr = " << value[3] << " OK";
            LINFO << "Point " << pt << " : dx_step = " << depl.getX() << ", dy_step = " << depl.getY() << ", dz_step = " << depl.getZ() << ", corr = " << value[3] << " OK";
		}
        return *cible;
	}
    else
    {
		Point *cible = new Point(this->scan,this->color,target, -1);
        return *cible;
    }
 : P(string linestr)
}

Point Point::optimizeCorrel_DICGPU(Correl3DGPU& correlator, Coord target){ //The very good one for a single solution
	if(correlator.validTarget(target)&&(this->getCorrel()!=-1)){
        int pt = this->getColor();
        
        bool usetex = correlator.getusetex();
		double* value = new double[4];

        if(usetex)
        {
            correlator.setTarget(target.getZ(), target.getY(), target.getX());
            value = correlator.correltexscreen();
        }
        else
        {
            correlator.setImTarget((int)target.getZ(), (int)target.getY(), (int)target.getX());
            value = correlator.correl();
		}
        
		//cout << "Correl done (" << value[0] << ',' << value[1] << "," << value[2] << ") corr=" << value[3] << endl;
		//Coord c = target.shift(value);
		//cout << "Coord " << target.toString() << endl;
		//cout << "Coord shifted " << c.toString() << endl;
		Point *cible = new Point(this->scan,this->color,target.shift(value), value[3]);
		//cout << "Closest found particle " << cible->getColor() << " coord " << cible->getCoord().toString() <<  endl;

		//printf("Correl : dx = "+ (int)value[0] + ", dy = "+ (int)value[1] + ", dz = " + (int)value[2] + ", corr = " + value[3] + ", time = "+currentTime + "ms");
	    //	cout << "Point " << pt << " : x = " << (int)target.getX() << ", y = " << (int)target.getY() << ", z = " << (int)target.getZ() << endl;
		Coord depl = cible->getCoord().diff(this->getCoord());

        if(value[3]< correlator.getCorrelThr())
        {
            LINFO << "Point " << pt << " : dx_corr = " << (int)value[0] << ", dy_corr = " << (int)value[1] << ", dz_corr = " << (int)value[2] << ", corr = " << value[3] << " not kept";
            LINFO << "Point " << pt << " : dx_step = " << depl.getX() << ", dy_step = " << depl.getY() << ", dz_step = " << depl.getZ() << ", corr = " << value[3] << " not kept";
        }
        else
        {
            LINFO << "Point " << pt << " : dx_corr = " << (int)value[0] << ", dy_corr = " << (int)value[1] << ", dz_corr = " << (int)value[2] << ", corr = " << value[3] << " OK";
            LINFO << "Point " << pt << " : dx_step = " << depl.getX() << ", dy_step = " << depl.getY() << ", dz_step = " << depl.getZ() << ", corr = " << value[3] << " OK";
		}
        return *cible;
	}
    else
    {
		Point *cible = new Point(this->scan,this->color,target, -1);
        return *cible;
    }

}
	    Volume Point::getPointSubVolumeSafe(Volume& src, const string volumeName) const
		{
			int* new_color = new int[65256];
			for(int j = 0; j<65256; j++)
			{
				new_color[j] = 0;
			}
			new_color[(int)this->getColor()] = 1;
			//stack.setRoi(new Rectangle((int)this->getCoord().getX()-roisize/2,(int)this->getCoord().getY()-roisize/2,roisize,roisize));
			ROI roi = ROI((int)this->getCoord().getX()-roisize/2,(int)this->getCoord().getY()-roisize/2,(int)this->getCoord().getZ()-roisize/2,roisize,roisize,roisize);
		    ROI image = ROI(0,0,0,src.getdimX(),src.getdimY(),src.getdimZ());
		    ROI intersect = roi.intersection(image);
			Volume dest;
		    if(intersect.contains(roi))
		    {
		    	src.setRoi(roi);
		    	dest = src.duplicateRoi();
		    }
		    else
		    {
		    	printf("border ROI");
		    	src.setRoi(intersect);
		    	dest = src.duplicateRoi();
		    }
		    dest.recolor(new_color);
		    return dest;
		}

*/
